<!-- <a href="http://mera-pt.ru/about/delivery/" style="text-decoration: none;" target="_blank">
<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:xlink="http://www.w3.org/1999/xlink"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   width="265"
   height="126"
   id="svg2"
   version="1.1"
   inkscape:version="0.48.5 r10040"
   sodipodi:docname="Bosch-brand2.svg">
  <defs
     id="defs4">
    <linearGradient
       id="linearGradient7866">
      <stop
         style="stop-color:#628cb2;stop-opacity:1;"
         offset="0"
         id="stop7868" />
      <stop
         style="stop-color:#043e6d;stop-opacity:1;"
         offset="1"
         id="stop7870" />
    </linearGradient>
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient7866"
       id="linearGradient7872"
       x1="132.5"
       y1="926.36218"
       x2="132.5"
       y2="1013.3622"
       gradientUnits="userSpaceOnUse"
       gradientTransform="translate(0,-452.36218)" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient7866"
       id="linearGradient7896"
       gradientUnits="userSpaceOnUse"
       gradientTransform="translate(0,-452.36218)"
       x1="132.5"
       y1="926.36218"
       x2="132.5"
       y2="1013.3622" />
    <linearGradient
       id="linearGradient4201">
      <stop
         style="stop-color:#a00016;stop-opacity:1;"
         offset="0"
         id="stop4203" />
      <stop
         style="stop-color:#ba0016;stop-opacity:1;"
         offset="1"
         id="stop4205" />
    </linearGradient>
    <linearGradient
       id="linearGradient4258">
      <stop
         style="stop-color:#a00016;stop-opacity:1;"
         offset="0"
         id="stop4260" />
      <stop
         style="stop-color:#ba0016;stop-opacity:1;"
         offset="1"
         id="stop4262" />
    </linearGradient>
    <linearGradient
       id="linearGradient4265">
      <stop
         style="stop-color:#a00016;stop-opacity:1;"
         offset="0"
         id="stop4267" />
      <stop
         style="stop-color:#ba0016;stop-opacity:1;"
         offset="1"
         id="stop4269" />
    </linearGradient>
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient4201"
       id="linearGradient4241"
       x1="243.75055"
       y1="874.93317"
       x2="243.75055"
       y2="871.93317"
       gradientUnits="userSpaceOnUse" />
    <linearGradient
       id="linearGradient4272">
      <stop
         style="stop-color:#a00016;stop-opacity:1;"
         offset="0"
         id="stop4274" />
      <stop
         style="stop-color:#ba0016;stop-opacity:1;"
         offset="1"
         id="stop4276" />
    </linearGradient>
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient4201"
       id="linearGradient4339"
       gradientUnits="userSpaceOnUse"
       gradientTransform="translate(6.0287449,141.06439)"
       x1="243.75055"
       y1="874.93317"
       x2="243.75055"
       y2="871.93317" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient4201"
       id="linearGradient4341"
       gradientUnits="userSpaceOnUse"
       gradientTransform="translate(6.0287449,141.06439)"
       x1="337.41629"
       y1="874.93317"
       x2="337.41629"
       y2="871.93317" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient4201"
       id="linearGradient4343"
       gradientUnits="userSpaceOnUse"
       gradientTransform="translate(6.0287449,141.06439)"
       x1="420.41632"
       y1="874.93317"
       x2="420.41632"
       y2="871.93317" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient4201"
       id="linearGradient4345"
       gradientUnits="userSpaceOnUse"
       gradientTransform="translate(6.0287449,141.06439)"
       x1="306.41599"
       y1="874.93317"
       x2="306.41632"
       y2="871.93317" />
  </defs>
  <sodipodi:namedview
     id="base"
     pagecolor="#ffffff"
     bordercolor="#666666"
     borderopacity="1.0"
     inkscape:pageopacity="0"
     inkscape:pageshadow="2"
     inkscape:zoom="1"
     inkscape:cx="243.8103"
     inkscape:cy="74.253951"
     inkscape:document-units="px"
     inkscape:current-layer="layer1"
     showgrid="false"
     inkscape:window-width="1366"
     inkscape:window-height="716"
     inkscape:window-x="-8"
     inkscape:window-y="-8"
     inkscape:window-maximized="1"
     inkscape:snap-page="true"
     inkscape:snap-bbox="true"
     inkscape:bbox-paths="true"
     inkscape:bbox-nodes="true"
     inkscape:snap-bbox-edge-midpoints="true"
     inkscape:snap-bbox-midpoints="true"
     inkscape:object-paths="true"
     inkscape:snap-intersection-paths="true"
     inkscape:object-nodes="true"
     inkscape:snap-smooth-nodes="true" />
  <metadata
     id="metadata7">
    <rdf:RDF>
      <cc:Work
         rdf:about="">
        <dc:format>image/svg+xml</dc:format>
        <dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
        <cc:license
           rdf:resource="http://creativecommons.org/licenses/by-sa/3.0/" />
      </cc:Work>
      <cc:License
         rdf:about="http://creativecommons.org/licenses/by-sa/3.0/">
        <cc:permits
           rdf:resource="http://creativecommons.org/ns#Reproduction" />
        <cc:permits
           rdf:resource="http://creativecommons.org/ns#Distribution" />
        <cc:requires
           rdf:resource="http://creativecommons.org/ns#Notice" />
        <cc:requires
           rdf:resource="http://creativecommons.org/ns#Attribution" />
        <cc:permits
           rdf:resource="http://creativecommons.org/ns#DerivativeWorks" />
        <cc:requires
           rdf:resource="http://creativecommons.org/ns#ShareAlike" />
      </cc:License>
    </rdf:RDF>
  </metadata>
  <g
     inkscape:label="Layer 1"
     inkscape:groupmode="layer"
     id="layer1"
     transform="translate(0,-926.36218)">
    <rect
       style="fill:#e8e8e8;fill-opacity:1;stroke:none"
       id="rect3033"
       width="265"
       height="126"
       x="0"
       y="926.36218"
       rx="14"
       ry="14" />
    <path
       style="fill:url(#linearGradient7896);fill-opacity:1;stroke:none"
       d="m 14,474 c -7.756,0 -14,6.244 -14,14 l 0,73 265,0 0,-73 c 0,-7.756 -6.244,-14 -14,-14 z"
       id="rect7346"
       inkscape:connector-curvature="0"
       sodipodi:nodetypes="sccccss"
       transform="translate(0,452.36218)" />
    <text
       xml:space="preserve"
       style="font-size:78.0451355px;font-style:normal;font-variant:normal;font-weight:bold;font-stretch:normal;line-height:125%;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;font-family:Verdana;-inkscape-font-specification:Verdana Bold"
       x="11.095959"
       y="998.23358"
       id="text7880"
       sodipodi:linespacing="125%"><tspan
         sodipodi:role="line"
         id="tspan7882"
         x="11.095959"
         y="998.23358"
         style="font-style:normal;font-variant:normal;font-weight:bold;font-stretch:normal;fill:#ffffff;font-family:Verdana;-inkscape-font-specification:Verdana Bold">2</tspan></text>
    <text
       xml:space="preserve"
       style="font-size:67.61245728px;font-style:normal;font-weight:normal;line-height:125%;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;font-family:Sans"
       x="71.494141"
       y="965.5614"
       id="text7884"
       sodipodi:linespacing="125%"><tspan
         sodipodi:role="line"
         id="tspan7886"
         x="71.494141"
         y="965.5614"
         style="font-size:28px;font-style:normal;font-variant:normal;font-weight:bold;font-stretch:normal;fill:#ffffff;font-family:Verdana;-inkscape-font-specification:Verdana Bold">дня</tspan></text>
    <text
       xml:space="preserve"
       style="font-size:67.61245728px;font-style:normal;font-weight:normal;line-height:125%;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;font-family:Sans"
       x="72.041016"
       y="984.995"
       id="text4522"
       sodipodi:linespacing="125%"><tspan
         id="tspan4524"
         sodipodi:role="line"
         x="72.041016"
         y="984.995"
         style="font-size:12px;font-style:normal;font-variant:normal;font-weight:bold;font-stretch:normal;letter-spacing:0px;fill:#ffffff;font-family:Arial;-inkscape-font-specification:Arial">доставка при наличии товара</tspan><tspan
         id="tspan4526"
         sodipodi:role="line"
         x="72.041016"
         y="999.995"
         style="font-size:12px;font-style:normal;font-variant:normal;font-weight:bold;font-stretch:normal;letter-spacing:0px;fill:#ffffff;font-family:Arial;-inkscape-font-specification:Arial">на складе в г. Новосибирск</tspan></text>

    <g
       id="g4331"
       transform="matrix(0.59984306,0,0,0.59984306,-70.32838,426.02717)">
      <path
         sodipodi:nodetypes="cccsssccscccccscc"
         inkscape:connector-curvature="0"
         d="m 261.73404,1005.9232 -4.79836,22.5745 -7.15638,0 7.48788,-35.22775 c 0.40189,-1.8907 2.26045,-3.0977 4.15121,-2.6958 1.1381,0.2419 2.08158,1.0336 2.51744,2.1124 l 10.08735,24.96705 10.08734,-24.96705 c 0.43587,-1.0788 1.37935,-1.8705 2.51745,-2.1124 1.89076,-0.4019 3.74932,0.8051 4.15121,2.6958 l 7.48787,35.22775 -7.15638,0 -4.79836,-22.5745 -9.04399,22.3847 c -0.72411,1.7922 -2.76402,2.6581 -4.55626,1.934 -0.88031,-0.3557 -1.57835,-1.0537 -1.93402,-1.934 z"
         style="fill:url(#linearGradient4339);fill-opacity:1;stroke:none"
         id="path3419" />
      <path
         sodipodi:nodetypes="ccssccsssscccc"
         inkscape:connector-curvature="0"
         d="m 343.44504,1010.4977 c 0,-11.04575 8.95431,-20.00005 20,-20.00005 11.04571,0 20.00001,8.9543 20.00001,20.00005 0,11.0457 -8.9543,20 -20.00001,20 -2.8459,0 -5.54912,-0.6032 -8,-1.6747 l 0,-8.0625 c 2.20459,1.7206 4.98678,2.7372 8,2.7372 7.17971,0 13,-5.8203 13,-13 0,-7.1797 -5.82029,-13.00005 -13,-13.00005 -7.17969,0 -12.99999,5.82035 -12.99999,13.00005 l 0,32 -7.00001,0 0,-32 z"
         style="color:#000000;fill:url(#linearGradient4341);fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:1px;marker:none;visibility:visible;display:inline;overflow:visible;enable-background:accumulate"
         id="path3421" />
      <path
         sodipodi:nodetypes="csccssssccscc"
         inkscape:connector-curvature="0"
         d="m 406.44505,990.49765 c 11.04571,0 20,8.9543 20,20.00005 l 0,18 -7,0 0,-18 c 0,-7.1797 -5.82029,-13.00005 -13,-13.00005 -7.17969,0 -13,5.82035 -13,13.00005 0,7.1797 5.82031,13 13,13 3.01323,0 5.79541,-1.0166 8,-2.7372 l 0,8.0625 c -2.45088,1.0715 -5.15409,1.6747 -8,1.6747 -11.04569,0 -20,-8.9543 -20,-20 0,-11.04575 8.95431,-20.00005 20,-20.00005 z"
         style="color:#000000;fill:url(#linearGradient4343);fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:1px;marker:none;visibility:visible;display:inline;overflow:visible;enable-background:accumulate"
         id="path3423" />
      <path
         sodipodi:nodetypes="sccsssccccsccs"
         inkscape:connector-curvature="0"
         d="m 319.44506,1030.4977 c 5.52284,0 10.53695,-2.2429 14.15624,-5.8622 l -4.96876,-4.9375 c -2.35253,2.3526 -5.59764,3.7997 -9.18748,3.7997 -7.1797,0 -13,-5.8203 -13,-13 0,-7.1797 5.8203,-13.00005 13,-13.00005 5.96725,0 10.97192,4.02305 12.49999,9.50005 l -19.49999,0 0,7 26.68749,0 c 0.2058,-1.1526 0.3125,-2.3195 0.3125,-3.5064 0,-11.04575 -8.9543,-19.99365 -19.99979,-19.99365 -11.04549,0 -19.99979,8.9543 -20,20.00005 -2e-4,11.0457 8.9541,20 19.9998,20 z"
         style="color:#000000;fill:url(#linearGradient4345);fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:1px;marker:none;visibility:visible;display:inline;overflow:visible;enable-background:accumulate"
         id="path3425" />
    </g>
  </g>
</svg>
</a> -->
<a href="/about/delivery/">
	<span><span>2</span><span>дня</span><span>доставка при наличии товара на складе в г. Новосибирске</span></span>
	<span class="sertif_logo"></span>
</a>
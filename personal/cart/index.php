<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");
$APPLICATION->SetPageProperty('title', "Корзина");
?>
<div id="full_cart">
<?$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket",
	"h2o_main",
	array(
		"COLUMNS_LIST" => array(
			0 => "NAME",
			1 => "PROPS",
			2 => "DELETE",
			3 => "PRICE",
			4 => "QUANTITY",
		),
		"PATH_TO_ORDER" => "/personal/cart/",
		"HIDE_COUPON" => "N",
		"PRICE_VAT_SHOW_VALUE" => "Y",
		"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
		"USE_PREPAYMENT" => "N",
		"QUANTITY_FLOAT" => "N",
		"SET_TITLE" => "Y",
		"ACTION_VARIABLE" => "action",
		"OFFERS_PROPS" => array(
		)
	),
	false
); ?>
</div>
       <?//if (($USER->IsAuthorized())):?>
	      <?if (($cartLine>0)||($_REQUEST["ORDER_ID"])){?>
		     <br>
		     <br>
		     <?$APPLICATION->IncludeComponent(
	"h2o:sale.order.ajax",
	"h2o_main",
	array(
		"PAY_FROM_ACCOUNT" => "N",
		"ONLY_FULL_PAY_FROM_ACCOUNT" => "N",
		"COUNT_DELIVERY_TAX" => "N",
		"ALLOW_AUTO_REGISTER" => "Y",
		"SEND_NEW_USER_NOTIFY" => "Y",
		"DELIVERY_NO_AJAX" => "N",
		"DELIVERY_NO_SESSION" => "N",
		"TEMPLATE_LOCATION" => "popup",
		"DELIVERY_TO_PAYSYSTEM" => "p2d",
		"USE_PREPAYMENT" => "N",
		"PROP_1" => array(
		),
		"PROP_2" => array(
		),
		"ALLOW_NEW_PROFILE" => "Y",
		"SHOW_PAYMENT_SERVICES_NAMES" => "Y",
		"SHOW_STORES_IMAGES" => "N",
		"PATH_TO_BASKET" => "/personal/cart/",
		"PATH_TO_PERSONAL" => "/personal/",
		"PATH_TO_PAYMENT" => "/personal/order/payment/",
		"PATH_TO_AUTH" => "/auth/",
		"SET_TITLE" => "N",
		"PRODUCT_COLUMNS" => array(
		),
		"DISABLE_BASKET_REDIRECT" => "N"
	),
	false
);?>
	      <?}else{?>
		     <h2 class="empty_cart_title">В корзине еще нет товаров</h2>
	      <?}?>
       <?//else:?>
	      <!--<h2 style="letter-spacing: 2px; border-bottom: 2px solid #eee;padding-bottom: 12px;margin-right: 19px;">Для оформления заказа необходимо авторизоваться на сайте</h2>-->
	      <!--<p>Если вы уже зарегистрированы на сайте, то вы можете <a data-remodal-target="modal-auth">авторизоваться</a>.</p>-->
	      <!--<p>Если вы еще не являетесь пользователем сайта, вы можете <a data-remodal-target="modal-reg">зарегистрироваться</a>.</p>-->
       <?//endif;?>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Персональный раздел");
?><div class="bx_page">
	<? if($GLOBALS['isMobile']) { ?>
		<h2><i class="fa fa-user" aria-hidden="true"></i> Личный кабинет</h2>
	<? } else { ?>
		<h2>Личная информация</h2>	
	<? }?>
	<div>
		<div class="media_no_mobile"><a href="profile/">Изменить регистрационные данные</a></div>
		<div class="media_mobile">Регистрационные данные <a href="profile/">Изменить</a></div>
	</div>
	<h2>Заказы</h2>
	<div class="personal_-link_to_orders">
		<a href="order/"><i class="fa fa-list media_mobile" aria-hidden="true"></i>Ознакомиться с состоянием заказов</a><br>
		<a href="cart/"><i class="fa fa-shopping-cart media_mobile" aria-hidden="true"></i>Посмотреть содержимое корзины</a><br>
		<a href="order/"><i class="fa fa-history media_mobile" aria-hidden="true"></i>Посмотреть историю заказов</a><br>
	</div>
	<h2>Подписка</h2>
	<div>
		<a href="subscribe/">Изменить подписку</a>
	</div>
	
	<?if($USER->isAuthorized()):?>
	<a href='?logout=yes' class="logout_link">Выйти</a> 
	<?endif?>
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?

die;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

/*include($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/include/PHPExcel.php");
include($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/include/PHPExcel/IOFactory.php");*/

global $USER;
if (!$USER->IsAdmin()) {
    die ('Только администраторам');
}

/*ini_set('display_errors', 1);
error_reporting(E_ERROR);*/

CModule::IncludeModule("sale");
class chunkReadFilter implements PHPExcel_Reader_IReadFilter{
    private $_startRow = 0;
    private $_endRow = 0;
    
    public function setRows($startRow, $chunkSize) {
        $this->_startRow    = $startRow;
        $this->_endRow      = $startRow + $chunkSize;
    }
    
    public function readCell($column, $row, $worksheetName = '') {
        if (($row == 1) || ($row >= $this->_startRow && $row < $this->_endRow)) {
            return true;
        }
        return false;
    }
}
echo "<pre>";
$arCnt = Array();
$arCnt["start_time"] = time();
$ar = CSaleDiscount::GetByID(1);
$arar = unserialize($ar['ACTIONS']);
$ct = 1 - $arar['CHILDREN'][0]['DATA']['Value']/100;
ini_set('memory_liit', '128M');
$arFiles = Array (
    Array (
        "NAME" => "Pricelist_Accessories.xlsx",
        "COLUMN" => 4,
    ),
    Array (
        "NAME" => "Pricelist_Blue.xlsx",
        "COLUMN" => 6,
    ),
    Array (
        "NAME" => "Pricelist_Green.xlsx",
        "COLUMN" => 6,
    ),
    Array (
        "NAME" => "Pricelist_MT.xlsx",
        "COLUMN" => 9,
    ),
);
$ftp_server = "109.171.114.45";
$ftp_user_name = "mera-pt";
$ftp_user_pass = "555mera-pt";
$conn_id = ftp_connect($ftp_server);
foreach($arFiles as $arFile)
{
    $file = $arFile["NAME"];
    $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
    ftp_pasv($conn_id, true);
    if (ftp_get($conn_id, $file, $file, FTP_BINARY)) {
        echo "Произведена запись в $file\n";
    } else {
        echo "Не удалось завершить операцию $file\n";
    }
}
// закрытие соединения
ftp_close($conn_id);

foreach ($arFiles as $arFile)
{
    set_time_limit(1800);
    //set_time_limit(120);
    $pr = $arFile["COLUMN"];
    $file = $arFile["NAME"];
    if (!file_exists($file)) 
    {
        continue;
    }
    else
    {
        echo "open $file\n";
    }
    $startRow = 1;
    $chunkSize = 50;
    $active_sheet = 0;

    $objReader = PHPExcel_IOFactory::createReaderForFile($file);
    $objReader->setReadDataOnly(true);
    $objRow = $objReader->load($file);
    $objRow->setActiveSheetIndex($active_sheet);
    $objWorksheetRow = $objRow->getActiveSheet();
    $hr  = $objWorksheetRow->getHighestRow();
    $sheet = $objRow->getSheetCount();
    $objRow->disconnectWorksheets();                //чистим 
    unset($objRow);

    //внешний цикл, пока файл не кончится
    $cnt = 0;
    $array = array();
    while (1)
    {
        //echo "start from sheet $active_sheet row $startRow\n";
        $chunkFilter = new chunkReadFilter(); 
        $objReader->setReadFilter($chunkFilter);
        $chunkFilter->setRows($startRow,$chunkSize);    //устанавливаем знаечние фильтра
        $objPHPExcel = $objReader->load($file);     //открываем файл
        $objPHPExcel->setActiveSheetIndex($active_sheet);       //устанавливаем индекс активной страницы
        $objWorksheet = $objPHPExcel->getActiveSheet(); //делаем активной нужную страницу
        $highestColumn      = $objWorksheet->getHighestColumn();
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
        if($startRow < $hr)
        {
            if($hr <= ($startRow + $chunkSize))
            {
                $row_c = $hr;
            }
            else
            {
                $row_c = $startRow + $chunkSize;
            }
            for ($i = $startRow; $i < $row_c; $i++)     //внутренний цикл по строкам
            {
                $item = array();
                for ($col = 0; $col < $highestColumnIndex; ++ $col)
                {
                    $value = ($objWorksheet->getCellByColumnAndRow($col, $i));   
                    
                    $val   = $value->getOldCalculatedValue();
                    if($file == 'Pricelist_Blue.xlsx')
                    {
                        if($val == 0 || $val == '')
                        {
                            $val = $value->getValue();
                        }
                    }
                    else
                    {
                        if($val == 0 || $val == '')
                        {
                            $val   = $value->getCalculatedValue();
                        }
                        if($val == 0 || $val == '')
                        {
                            $val   = $value->getValue();
                        }
                    }
                    array_push($item, $val);
                }
                array_push($array, $item);  
            }
            $startRow = $row_c;                 
        }
        else
        { 
            if ($active_sheet < ($sheet - 1))
            {
                $startRow = 0;
                unset($_REQUEST['startRow']);
                $active_sheet += 1;
                $objRow = $objReader->load($file);
                $objRow->setActiveSheetIndex($active_sheet);
                $objWorksheetRow = $objRow->getActiveSheet();
                $hr  = $objWorksheetRow->getHighestRow();
                $sheet  = $objRow->getSheetCount();
                $objRow->disconnectWorksheets();                //чистим 
                unset($objRow);
            } 
            else 
            {
                unset($hr);
                unset($active_sheet);
                break;
            }
        }
        $cnt++;
        if ($cnt > 3)
        {
            break;
        }
    }
    $objPHPExcel->disconnectWorksheets();               //чистим 
    unset($objPHPExcel);

    // PRICE_1C

    $log = new Logger("cron.log");
    foreach($array as $element){  
        if(($element[2]) != ""){
            $elem_xml = (str_replace(".", "", $element[2]));
            $elem_xml2 = (str_replace(".", "", $element[3]));
            $arSelect = Array("ID", "IBLOCK_ID", "NAME", "XML_ID", 'CATALOG_GROUP_1');
            $arFilter = Array(
                "!PROPERTY_PRICE_1C" => "да",
                Array (
                    "LOGIC" => "OR", 
                    array("EXTERNAL_ID"=>($elem_xml)), 
                    array("EXTERNAL_ID"=>($elem_xml2)),
                ),
            );
            $val_pr = 0;
            if(intval ($element[$pr]) > 0){
                $val_pr = $element[$pr];
            }else{
                if(intval ($element[($pr - 1)]) > 0){
                    $val_pr = $element[($pr - 1)];
                }elseif(intval ($element[($pr - 2)]) > 0){
                    $val_pr = $element[($pr - 2)];
                }
            }
            if ($val_pr == 0) { continue; }
            $val_pr_nal = $val_pr * $ct;
            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
            if($ob = $res->Fetch()){        
                $PRODUCT_ID = $ob['ID'];
                $PRICE_TYPE_ID = 2;   
                $arFields = Array(
                    "PRODUCT_ID" => $PRODUCT_ID,
                    "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
                    "PRICE" => $val_pr,
                    "CURRENCY" => "RUB",
                );
                $res = CPrice::GetList (array(),array("PRODUCT_ID" => $PRODUCT_ID,"CATALOG_GROUP_ID" => $PRICE_TYPE_ID));
                if ($arr = $res->Fetch()){
                    $arCnt["UPDATE_2"]++;
                    CPrice::Update($arr["ID"], $arFields);
                    //$log->log("update ".$element[$pr]);
                    //echo "Update price $PRICE_TYPE_ID for {$ob["NAME"]} {$ob["XML_ID"]} to $val_pr\n";
                }else{
                    $arCnt["ADD_2"]++;
                    //echo "Add price $PRICE_TYPE_ID for {$ob["NAME"]} {$ob["XML_ID"]} to $val_pr\n";
                    CPrice::Add($arFields);
                    //$log->log("add ".$ob["NAME"]);
                }
                
                $PRICE_TYPE_ID_NAL = 3;   
                $arFieldsNal = Array(
                    "PRODUCT_ID" => $PRODUCT_ID,
                    "CATALOG_GROUP_ID" => $PRICE_TYPE_ID_NAL,
                    "PRICE" => $val_pr_nal,
                    "CURRENCY" => "RUB",
                );
                $res_nal = CPrice::GetList(array(),array("PRODUCT_ID" => $PRODUCT_ID,"CATALOG_GROUP_ID" => $PRICE_TYPE_ID_NAL));
                if ($arr_nal = $res_nal->Fetch()){
                    $arCnt["UPDATE_3"]++;
                    //echo "Update price $PRICE_TYPE_ID_NAL for {$ob["NAME"]} {$ob["XML_ID"]} to $val_pr_nal\n";
                    CPrice::Update($arr_nal["ID"], $arFieldsNal);
                    //$log->log("update ".$element[$pr]);
                }else{
                    $arCnt["ADD_3"]++;
                    //echo "Add price $PRICE_TYPE_ID_NAL for {$ob["NAME"]} {$ob["XML_ID"]} to $val_pr_nal\n";
                    CPrice::Add($arFieldsNal);
                    //$log->log("add ".$ob["NAME"]);
                }
            }
        }
    }

}

$arCnt["end_time"] = time();
echo "time: ".($arCnt["end_time"] - $arCnt["start_time"]);
echo '<pre>'; print_r($arCnt); echo "#".__LINE__."@".__FILE__; echo '</pre>';

echo "</pre>";

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
<div id="city_popup" style="display: none; width: 600px;">
	<div class="popup__title">Выбор города</div>
	<div class="popup__yourcity">
		<div class="popup__yourcity__cont">
			Ваш город - <a href="#" onclick="$.fancybox.close(); return false;"><?=$_SESSION["CURRENT_CITY"]["NAME"];?></a>?<br/><br/>
			<button class="popup__button" onclick="$.fancybox.close();">Да, все верно</button>
		</div>
	</div>
	<div class="popup__line"></div>
	<div class="popup__city">
		<div class="popup__cityList" id="select_city">
			Нет, мой город другой:<br/><br/>
			<?foreach ($_SESSION["ALL_CITY"] as $arCity)
			{
				if ($arCity["NAME"] != $_SESSION["CURRENT_CITY"]["NAME"])
				{
					?>
					<a href="javascript:void(0);" class="<?=$arCity["ID"];?>"><?=$arCity["NAME"];?></a><br/>
				<?}
			}?>
			<br /><br /><a style="cursor: pointer; color: #e2001a">Другой город</a>
		</div>
	</div>
	<a class="close" title="Закрыть" onclick="document.getElementById('parent_popup').style.display='none';">X</a>
</div>
<?

?>
<script type="text/javascript">
$(document).ready(function(){
	$(".fancybox").fancybox();

	$('#select_city a').click(function(){
		var city = $(this).attr('class');
		$.get('/include/set_city.php', {SET_CITY: "Y", CURRENT_CITY: city}, function(res){
			location.reload();
			/*if (res.length > 0)
			{
				//location.reload();
				location.host = res;
			}*/
			$.fancybox.close();
		});
	});
	<?
	if ($_SESSION["SHOW_CITY_POPUP"] == "Y")
	{
		unset ($_SESSION["SHOW_CITY_POPUP"]);
		/*?>$('#show_city_popup').click();<?*/
	}
	?>
});
</script>
<?

define("STOP_STATISTICS", true);
define('NO_AGENT_CHECK', true);

require ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
/*
CUtil::JSPostUnescape();

$arRes = array();
$newProductId = false;
$newBasketId = false;
$action_var = (isset($_POST["action_var"]) && strlen(trim($_POST["action_var"])) > 0) ? trim($_POST["action_var"]) : "action";
$arErrors = array();

include($_SERVER["DOCUMENT_ROOT"]."/bitrix/components/bitrix/sale.basket.basket/functions.php");

if (isset($_POST[$action_var]) && strlen($_POST[$action_var]) > 0)
{
	echo "1";
	if ($_POST[$action_var] == "recalculate")
	{

		echo "2";

		// todo: extract duplicated code to function

		CBitrixComponent::includeComponentClass("bitrix:sale.basket.basket");

		echo "3";

		$basket = new CBitrixBasketComponent();
		$basket->onIncludeComponentLang();

		echo "4";

		$basket->weightKoef = htmlspecialcharsbx(COption::GetOptionString('sale', 'weight_koef', 1, SITE_ID));
		$basket->weightUnit = htmlspecialcharsbx(COption::GetOptionString('sale', 'weight_unit', "", SITE_ID));
		$basket->columns = $arColumns;
		$basket->offersProps = $strOffersProps;

		$basket->quantityFloat = (isset($_POST["quantity_float"]) && $_POST["quantity_float"] == "Y") ? "Y" : "N";
		$basket->countDiscount4AllQuantity = (isset($_POST["count_discount_4_all_quantity"]) && $_POST["count_discount_4_all_quantity"] == "Y") ? "Y" : "N";
		$basket->priceVatShowValue = (isset($_POST["price_vat_show_value"]) && $_POST["price_vat_show_value"] == "Y") ? "Y" : "N";
		$basket->hideCoupon = (isset($_POST["hide_coupon"]) && $_POST["hide_coupon"] == "Y") ? "Y" : "N";
		$basket->usePrepayment = (isset($_POST["use_prepayment"]) && $_POST["use_prepayment"] == "Y") ? "Y" : "N";

		$res = $basket->recalculateBasket($_POST);
		echo "5";
	}
}*/

if (isset($_POST[$action_var]) && strlen($_POST[$action_var]) > 0)
{
	CModule::IncludeModule ("sale");
	CModule::IncludeModule ("catalog");

	$strColumns = isset($_POST["select_props"]) ? $_POST["select_props"] : "";
	$arColumns = explode(",", $strColumns);

	global $USER;
	$arPost = $_POST;

	$arTmpItems = array();
	$dbItems = CSaleBasket::GetList(
		array("PRICE" => "DESC"),
		array(
			"FUSER_ID" => CSaleBasket::GetBasketUserID(),
			"LID" => SITE_ID,
			"ORDER_ID" => "NULL"
		),
		false,
		false,
		array(
			"ID", "NAME", "PRODUCT_PROVIDER_CLASS", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID",
			"QUANTITY", "DELAY", "CAN_BUY", "CURRENCY", "SUBSCRIBE", "TYPE", "SET_PARENT_ID", "NOTES"
		)
	);
	while ($arItem = $dbItems->Fetch())
	{
		if (CSaleBasketHelper::isSetItem($arItem))
			continue;

		$arTmpItems[] = $arItem;
	}

	if (!empty($arTmpItems))
	{
		foreach ($arTmpItems as $arItem)
		{
			if (!isset($arPost["QUANTITY_".$arItem["ID"]]) || floatval($arPost["QUANTITY_".$arItem["ID"]]) <= 0)
			{
				$quantityTmp = ($isFloatQuantity === true) ? floatval($arItem["QUANTITY"]) : intval($arItem["QUANTITY"]);
			}
			else
			{
				$quantityTmp = ($isFloatQuantity === true) ? floatval($arPost["QUANTITY_".$arItem["ID"]]) : intval($arPost["QUANTITY_".$arItem["ID"]]);
			}

			$deleteTmp = ($arPost["DELETE_".$arItem["ID"]] == "Y") ? "Y" : "N";
			$delayTmp = ($arPost["DELAY_".$arItem["ID"]] == "Y") ? "Y" : "N";

			if ($deleteTmp == "Y" && in_array("DELETE", $arColumns))
			{
				if ($arItem["SUBSCRIBE"] == "Y" && is_array($_SESSION["NOTIFY_PRODUCT"][$USER->GetID()]))
					unset($_SESSION["NOTIFY_PRODUCT"][$USER->GetID()][$arItem["PRODUCT_ID"]]);

				CSaleBasket::Delete($arItem["ID"]);
			}
			elseif ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y")
			{
				$arFields = array();

				if (in_array("QUANTITY", $arColumns))
					$arFields["QUANTITY"] = $quantityTmp;
				if (in_array("DELAY", $arColumns))
					$arFields["DELAY"] = $delayTmp;

				if (!empty($arFields)
					&&
						($arItem["QUANTITY"] != $arFields["QUANTITY"] && in_array("QUANTITY", $arColumns)
							|| $arItem["DELAY"] != $arFields["DELAY"] && in_array("DELAY", $arColumns))
					)
					CSaleBasket::Update($arItem["ID"], $arFields);
			}
			elseif ($arItem["DELAY"] == "Y" && $arItem["CAN_BUY"] == "Y")
			{
				$arFields = array();

				if (in_array("DELAY", $arColumns))
					$arFields["DELAY"] = $delayTmp;

				if (!empty($arFields)
					&&
						($arItem["DELAY"] != $arFields["DELAY"] && in_array("DELAY", $arColumns))
					)
					CSaleBasket::Update($arItem["ID"], $arFields);
			}
		}
	}

}

$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket", 
	"h2o_main", 
	array(
		"COLUMNS_LIST" => array(
			0 => "NAME",
			1 => "PROPS",
			2 => "DELETE",
			3 => "PRICE",
			4 => "QUANTITY",
		),
		"PATH_TO_ORDER" => "/personal/cart/",
		"HIDE_COUPON" => "N",
		"PRICE_VAT_SHOW_VALUE" => "Y",
		"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
		"USE_PREPAYMENT" => "N",
		"QUANTITY_FLOAT" => "N",
		"SET_TITLE" => "Y",
		"ACTION_VARIABLE" => "action",
		"OFFERS_PROPS" => array(
		)
	),
	false
);
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Контактная информация | Компания Мера");
$APPLICATION->SetTitle("Контакты");
?><?$arPos = explode(",", $_SESSION['CURRENT_CITY']['CITY_MAP']);?>
<!-- <pre>	<?//print_r($arPos)?> </pre> -->
<div class="phonewrapp" style="position: relative;">
	<p>
		<i class="fa fa-phone" aria-hidden="true"></i> <span class="phone"><?=$_SESSION['CURRENT_CITY']['CITY_PHONE_CODE']?> <span class="blue"><?=$_SESSION['CURRENT_CITY']['CITY_PHONE'][0]?><?=$_SESSION['CURRENT_CITY']['CITY_PHONE'][1]?></span></span>
	</p>
	<a href="print.php" target="_blank" style="color:#999999; font-size: 13px;position: absolute;right: 20px;top: 0;">Версия для печати</a>
</div>
<div class="phonewrapp" style="position: relative;">
	<p>
		<i class="fa fa-envelope" aria-hidden="true"></i> <span class="phone">E-mail: <a href="mailto:info@mera-pt.ru">info@mera-pt.ru</a></span>
	</p>
</div>
<div class="adresswrapp">
	<p>
		<i class="fa fa-map-marker" aria-hidden="true"></i> <span class="adress"><?=$_SESSION['CURRENT_CITY']['CITY_ADDRESS']?></span><br>
	</p>
	<h2><?=GetMessage("HOW_US_TO_REACH")?></h2>
	<div id="map">
		<br>
		<br><?//$city_adres = $_SESSION["CURRENT_CITY"]["CITY_ADDRESS"];?>
		<!--"MAP_DATA" => "a:4:{s:3:\"LON\";s:15:\"$arPos[1]\";s:3:\"LAT\";s:15:\"$arPos[0]\";s:5:\"SCALE\";s:2:\"10\";s:10:\"PLACEMARKS\";a:1:{s:1:\"0\";a:3:{s:4:\"TEXT\";s:22:\$arProperty['shop_city']['VALUE']\;s:3:\"LAT\";d:$arPos[0];s:3:\"LON\";d:$arPos[1];}}}",-->
		<?
		switch ($_SESSION["CURRENT_CITY"]["NAME"]) {
		case "Новосибирск":
		?>
		<a class="dg-widget-link" href="http://2gis.ru/novosibirsk/firm/141265770460086/center/82.92426824569704,55.020801939693406/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Новосибирска</a>
		<div class="dg-widget-link">
			<a href="http://2gis.ru/novosibirsk/firm/141265770460086/photos/141265770460086/center/82.92426824569704,55.020801939693406/zoom/17?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=photos">Фотографии компании</a>
		</div>
		<div class="dg-widget-link">
			<a href="http://2gis.ru/novosibirsk/center/82.924276,55.019746/zoom/16/routeTab/rsType/bus/to/82.924276,55.019746╎Мера, ООО?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти проезд до Мера, ООО</a>
		</div>
		<script data-skip-moving="true" charset="utf-8" src="http://widgets.2gis.com/js/DGWidgetLoader.js"></script>
		<script data-skip-moving="true" charset="utf-8">new DGWidgetLoader({"width":700,"height":500,"borderColor":"#a3a3a3","pos":{"lat":55.020801939693406,"lon":82.92426824569704,"zoom":16},"opt":{"city":"novosibirsk"},"org":[{"id":"141265770460086"}]});</script>
		<?
		break;
		case "Омск":
		?>
		<a class="dg-widget-link" href="http://2gis.ru/omsk/firm/282003258182659/center/73.3524513244629,54.99973669706239/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Омска</a>
		<div class="dg-widget-link">
		<a href="http://2gis.ru/omsk/firm/282003258182659/photos/282003258182659/center/73.3524513244629,54.99973669706239/zoom/17?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=photos">Фотографии компании</a>
		</div>
		<div class="dg-widget-link">
		<a href="http://2gis.ru/omsk/center/73.352459,54.998876/zoom/16/routeTab/rsType/bus/to/73.352459,54.998876╎Мера, ООО?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти проезд до Мера, ООО</a>
		</div>
		<script data-skip-moving="true" charset="utf-8" src="http://widgets.2gis.com/js/DGWidgetLoader.js"></script>
		<script data-skip-moving="true" charset="utf-8">new DGWidgetLoader({"width":700,"height":500,"borderColor":"#a3a3a3","pos":{"lat":54.99973669706239,"lon":73.3524513244629,"zoom":16},"opt":{"city":"omsk"},"org":[{"id":"282003258182659"}]});</script>
		<?
		break;
		}
		?>
		<noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript>
		<?
		/*$APPLICATION->IncludeComponent("simai:maps.2gis.simple", "h2o", Array(
			"MAP_WIDTH" => "700",	// Ширина карты
				"MAP_HEIGHT" => "500",	// Высота карты
				"MAP_ZOOM" => "17",	// Маштаб карты (от 0 до 17)
				"MAP_CONTROL_ZOOM" => "Y",	// Ползунок масштаба
				"MAP_CONTROL_DBLCLICK_ZOOM" => "Y",	// Приближение по двойному щелчку
				"MAP_CONTROL_FULLSCREEN_BUTTON" => "Y",	// Кнопка перехода в полноэкранный режим
				"MAP_CONTROL_GEOCLICKER" => "Y",	// Отображать сведения о гео объектах по щелчку
				"MAP_CONTROL_RIGHTBUTTON_MAGNIFIER" => "N",	// Приближение области правой кнопкой мыши
				"MAP_DATA" => "a:4:{s:3:\"LON\";s:15:\"\\\$arPos[1]\";s:3:\"LAT\";s:15:\"\\\$arPos[0]\";s:5:\"SCALE\";s:2:\"10\";s:10:\"PLACEMARKS\";a:1:{s:1:\"0\";a:3:{s:4:\"TEXT\";s:4:\"null\";s:3:\"LAT\";d:\\\$arPos[0];s:3:\"LON\";d:\\\$arPos[1];}}}",	// Данные карты
				"LAT" => $arPos[0],
				"LON" => $arPos[1],
				"TEXT" => $city_adres
			),
			false
		);*/?><br>
	</div>
	
	<h2>Задать вопрос</h2>
	<?$APPLICATION->IncludeComponent(
		"bitrix:main.feedback", 
		"eshop_adapt", 
		array(
			"USE_CAPTCHA" => "Y",
			"OK_TEXT" => "Спасибо, ваше сообщение принято.",
			"EMAIL_TO" => "info@mera-pt.ru",
			"REQUIRED_FIELDS" => array(
			),
			"EVENT_MESSAGE_ID" => array(
			)
		),
		false
	);?>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>
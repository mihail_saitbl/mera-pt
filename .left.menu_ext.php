<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;
$aMenuLinksExt = array();
if(CModule::IncludeModule('iblock'))
{
	$arFilter = array(
		"TYPE" => "catalog",
		"SITE_ID" => SITE_ID,
		"ID" => 5,
	);

	$dbIBlock = CIBlock::GetList(array('SORT' => 'ASC', 'ID' => 'ASC'), $arFilter);
	$dbIBlock = new CIBlockResult($dbIBlock);

	if ($arIBlock = $dbIBlock->GetNext())
	{
		if(defined("BX_COMP_MANAGED_CACHE"))
			$GLOBALS["CACHE_MANAGER"]->RegisterTag("iblock_id_".$arIBlock["ID"]);

		if($arIBlock["ACTIVE"] == "Y")
		{
			$aMenuLinksExt = $APPLICATION->IncludeComponent("bitrix:menu.sections", "", array(
				"IS_SEF" => "Y",
				"SEF_BASE_URL" => "",
				"SECTION_PAGE_URL" => $arIBlock['SECTION_PAGE_URL'],
				"DETAIL_PAGE_URL" => $arIBlock['DETAIL_PAGE_URL'],
				"IBLOCK_TYPE" => $arIBlock['IBLOCK_TYPE_ID'],
				"IBLOCK_ID" => $arIBlock['ID'],
				"DEPTH_LEVEL" => "3",
				"CACHE_TYPE" => "A",
				"CACHE_TYME" => "8600"
			), false, Array('HIDE_ICONS' => 'Y'));
		}
	}

	if(defined("BX_COMP_MANAGED_CACHE"))
		$GLOBALS["CACHE_MANAGER"]->RegisterTag("iblock_id_new");
}
if ($USER->IsAdmin()){
// print_r($aMenuLinksExt);
// echo 1111111;
}

foreach ($aMenuLinksExt as $key=>$men){
	$aMenuLinksExt[$key][3]['DEPTH_LEVEL']=$men[3]['DEPTH_LEVEL']+1;
}

$arMenuServices = $APPLICATION->IncludeComponent("bitrix:menu", "services", Array(
	"ROOT_MENU_TYPE" => "services",	// Тип меню для первого уровня
	"MENU_CACHE_TYPE" => "A",	// Тип кеширования
	"MENU_CACHE_TIME" => "36000000",	// Время кеширования (сек.)
	"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
	"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
	"MAX_LEVEL" => "3",	// Уровень вложенности меню
	"CHILD_MENU_TYPE" => "services",	// Тип меню для остальных уровней
	"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
	"DELAY" => "N",	// Откладывать выполнение шаблона меню
	"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
	),
	false
);
global $menu_serv;
foreach ($menu_serv as $key=>$men){
	$menu_serv[$key][3]['DEPTH_LEVEL']=$men[3]['DEPTH_LEVEL']+1;
}


$arCat = array(array("<img src='".SITE_TEMPLATE_PATH."/images/logo/darr.png' alt=''>Весь каталог", "/catalog/", Array('/catalog/'), Array('FROM_IBLOCK'=>1,'IS_PARENT'=>1,'DEPTH_LEVEL' => 1)));
$arServ = array(array("<img src='".SITE_TEMPLATE_PATH."/images/logo/darr.png' alt=''>Услуги", "/uslugi/", Array('/uslugi/'), Array('FROM_IBLOCK'=>1,'IS_PARENT'=>1,'DEPTH_LEVEL' => 1)));
$arServ = array_merge($arServ, $menu_serv);
$aMenuLinksExt = array_merge($arCat, $aMenuLinksExt);
$aMenuLinks = array_merge($aMenuLinksExt, $aMenuLinks);
$aMenuLinks = array_merge($aMenuLinks, $arServ);
?> 
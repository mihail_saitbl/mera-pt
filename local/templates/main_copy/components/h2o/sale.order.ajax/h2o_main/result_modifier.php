<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arParams['AJAX_ID'] = CH2oAjax::getComponentID($this->__component);
$arResult['AJAX_ID'] = CH2oAjax::getComponentID($this->__component);
$arNewProps = array_merge($arResult["ORDER_PROP"]["USER_PROPS_N"],$arResult["ORDER_PROP"]["USER_PROPS_Y"]);
function cmp_props($a, $b) {
    if ($a['SORT'] == $b['SORT']) {
        return 0;
    }
    return ($a['SORT'] < $b['SORT']) ? -1 : 1;
}
uasort($arNewProps, 'cmp_props');
$arResult['ORDER_PROP']['ALL_USER_PROPS'] = $arNewProps;
?>
<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$arParams['AJAX_ID'] = CH2oAjax::getComponentID($this->__component);
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CDatabase $DB */
if (!isset($arParams['LINE_ELEMENT_COUNT']))
	$arParams['LINE_ELEMENT_COUNT'] = 3;
$arParams['LINE_ELEMENT_COUNT'] = intval($arParams['LINE_ELEMENT_COUNT']);
if (2 > $arParams['LINE_ELEMENT_COUNT'] || 5 < $arParams['LINE_ELEMENT_COUNT'])
	$arParams['LINE_ELEMENT_COUNT'] = 3;

// Определяем, находятся ли товары в избранном
if ($USER->isAuthorized())
{
	$rsUser = CUser::GetByID($USER->GetID());
	$arUser = $rsUser->Fetch();
	foreach ($arResult["ITEMS"] as &$arElement)
		$arElement['IN_FAVORITES'] = in_array($arElement['ID'],$arUser['UF_FAVORITES']);
}
/*
			$arFilter = array("ID"=>5);
			$arSelect = array("ID", "XML_ID", "NAME");
			$resof = CIBlockElement::GetList(array(),array('IBLOCK_ID' => 5)); 
while ($arSection = $resof->Fetch())
{ //print_r($arSection); 
}

print_r($arResult['ITEMS']['XML_ID']);
*/
foreach($arResult['ITEMS'] as $k=>&$arElement)
{

    $arPrice = array();
    $date = "";
    $arPrice = CCatalogProduct::GetOptimalPrice($arElement['ID'], 1, $USER->GetUserGroupArray(), "N");
    if(!empty($arPrice['DISCOUNT'])){
    	$date = date_create($arPrice['DISCOUNT']["ACTIVE_TO"]);
    	$arProductDiscounts["ACTIVE_TO"] = date_format($date, 'd.m.Y');
    	$arElement["DISCOUNT"] = $arProductDiscounts;
    }
	
	$IBLOCK_ID = $arElement["IBLOCK_ID"]; 
	$ID = $arElement["ID"];
	$arInfo = CCatalogSKU::GetInfoByProductIBlock($IBLOCK_ID); 
	if (is_array($arInfo)){
		$minPrice = "";
		$minCurr = "";
		$rsOffers = CIBlockElement::GetList(array(),array('IBLOCK_ID' =>  $arInfo['IBLOCK_ID'], 'PROPERTY_'.$arInfo['SKU_PROPERTY_ID'] => $ID));
		while ($arOffer = $rsOffers->GetNext()){
 
			$arFilter = array("ID"=>$arOffer["ID"]);
			$arSelect = array("ID", "XML_ID", "NAME");
			$resof = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			if($ar_resof = $resof->GetNext()){
				$db_price = CPrice::GetList(array(),array("PRODUCT_ID" => $ar_resof['ID'],));
				if ($arPrice = $db_price->Fetch()){
					$ar_resof["PRICE"] = $arPrice;
					if ($minPrice==""){
						$minPrice = $arPrice["PRICE"];
						$minCurr = $arPrice["CURRENCY"];
					}elseif($minPrice>$arPrice["PRICE"]){
						$minPrice = $arPrice["PRICE"];
						$minCurr = $arPrice["CURRENCY"];
					}
				}
				$arResult['ITEMS'][$k]["OFFERS"][] = $ar_resof;
			}
		}
		$arResult['ITEMS'][$k]["MIN_PRICE"] = $minPrice;
		$arResult['ITEMS'][$k]["MIN_CURR"] = $minCurr;
	}
	$arResult['ITEMS_ID'][] = $ID;
 
}




if (CModule::IncludeModule("blog"))
{
// выберем все опубликованные сообщения 
$SORT = Array("NAME" => "ASC");
$arFilter = Array(
    "PUBLISH_STATUS" => BLOG_PUBLISH_STATUS_PUBLISH,
    ">NUM_COMMENTS" => 0 
    );	 
$SELECT = array();  

$dbPosts = CBlogPost::GetList(
        $SORT,
        $arFilter
    );

$i = 0;
while($arPost = $dbPosts->Fetch()) { 
	$arResult["COMMENTS"][$i] = $arPost;
$i++;
}

	/*
	foreach ($arResult["ITEMS"] as &$arElement) {
	while($arPost = $dbPosts->Fetch()) { 
		if($arElement["NAME"] == $arPost["TITLE"]) { 
 $arElement["COMMENTS"][$arElement["ID"]] = $arPost["NUM_COMMENTS"]; }else{
$arElement["COMMENTS"][$arElement["ID"]] = 0;
		} 
 
      }
	}
*/
	//print_r($arResult["COMMENTS"]);

}


CModule::includeModule("sale");
$arBasketItemsID = array();
$dbBasketItems = CSaleBasket::GetList(
	array(
		"NAME" => "ASC",
		"ID" => "ASC"
	    ),
	array(
		"FUSER_ID" => CSaleBasket::GetBasketUserID(),
		"LID" => SITE_ID,
		"ORDER_ID" => "NULL",
	    ),
	false,
	false,
	array("ID", "CALLBACK_FUNC", "MODULE", 
	      "PRODUCT_ID", "QUANTITY", "DELAY", 
	      "CAN_BUY", "PRICE", "WEIGHT")
    );
while ($arItems = $dbBasketItems->Fetch())
{
	if($arItems['CAN_BUY'] != "Y"){
		continue;
	}
    /*if (strlen($arItems["CALLBACK_FUNC"]) > 0)
    {
	CSaleBasket::UpdatePrice($arItems["ID"], 
				 $arItems["CALLBACK_FUNC"], 
				 $arItems["MODULE"], 
				 $arItems["PRODUCT_ID"], 
				 $arItems["QUANTITY"]);
	$arItems2 = CSaleBasket::GetByID($arItems["ID"]);
    }*/

    $arBasketItemsID[] = $arItems['PRODUCT_ID'];
    $arBasketItemsQuantity[$arItems['PRODUCT_ID']] = $arItems["QUANTITY"];
}
unset ($arItems);
$this->__component->SetResultCacheKeys(array('ITEMS_ID'));
$arResult['BASKET_ID'] = $arBasketItemsID;
$arResult['BASKET_QUANTITY'] = $arBasketItemsQuantity;








$arParams['TEMPLATE_THEME'] = (string)($arParams['TEMPLATE_THEME']);
if ('' != $arParams['TEMPLATE_THEME'])
{
	$arParams['TEMPLATE_THEME'] = preg_replace('/[^a-zA-Z0-9_\-\(\)\!]/', '', $arParams['TEMPLATE_THEME']);
	if ('site' == $arParams['TEMPLATE_THEME'])
	{
		$arParams['TEMPLATE_THEME'] = COption::GetOptionString('main', 'wizard_eshop_adapt_theme_id', 'blue', SITE_ID);
	}
	if ('' != $arParams['TEMPLATE_THEME'])
	{
		if (!is_file($_SERVER['DOCUMENT_ROOT'].$this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css'))
			$arParams['TEMPLATE_THEME'] = '';
	}
}
if ('' == $arParams['TEMPLATE_THEME'])
	$arParams['TEMPLATE_THEME'] = 'blue';

if (!empty($arResult['ITEMS']))
{
	$arEmptyPreview = false;
	$strEmptyPreview = $this->GetFolder() . '/images/no_photo.png';
	if (file_exists($_SERVER['DOCUMENT_ROOT'] . $strEmptyPreview))
	{
		$arSizes = getimagesize($_SERVER['DOCUMENT_ROOT'] . $strEmptyPreview);
		if (!empty($arSizes))
		{
			$arEmptyPreview = array(
				'SRC' => $strEmptyPreview,
				'WIDTH' => intval($arSizes[0]),
				'HEIGHT' => intval($arSizes[1])
			);
		}
		unset($arSizes);
	}
	unset($strEmptyPreview);

	$arSKUPropList = array();
	$arSKUPropIDs = array();
	$arSKUPropKeys = array();
	$boolSKU = false;
	$strBaseCurrency = '';
	$boolConvert = isset($arResult['CONVERT_CURRENCY']['CURRENCY_ID']);

	//
	$skuPropList = array(); // array("id_catalog" => array(...))
	$skuPropIds = array(); // array("id_catalog" => array(...))
	$skuPropKeys = array(); // array("id_catalog" => array(...))

	if (!$boolConvert)
		$strBaseCurrency = CCurrency::GetBaseCurrency();

	$catalogs = array();
	foreach($arResult['CATALOGS'] as $catalog)
	{
		$offersCatalogId = (int)$catalog['OFFERS_IBLOCK_ID'];
		$offersPropId = (int)$catalog['OFFERS_PROPERTY_ID'];
		$catalogId = (int)$catalog['IBLOCK_ID'];
		$sku = false;
		if($offersCatalogId > 0 && $offersPropId > 0)
			$sku = array("IBLOCK_ID" => $offersCatalogId, "SKU_PROPERTY_ID" => $offersPropId, "PRODUCT_IBLOCK_ID" => $catalogId);


		if (!empty($sku) && is_array($sku))
		{
			$skuPropList[$catalogId] = CIBlockPriceTools::getTreeProperties(
				$sku,
				$arParams['OFFER_TREE_PROPS'][$offersCatalogId],
				array(
					'PICT' => $arEmptyPreview,
					'NAME' => '-'
				)
			);

			$needValues = array();
			CIBlockPriceTools::getTreePropertyValues($skuPropList[$catalogId], $needValues);

			$skuPropIds[$catalogId] = array_keys($skuPropList[$catalogId]);
			if (!empty($skuPropIds[$catalogId]))
				$skuPropKeys[$catalogId] = array_fill_keys($skuPropIds[$catalogId], false);
		}
	}

	$arNewItemsList = array();

	foreach ($arResult['ITEMS'] as $key => $arItem)
	{

	$arItem['CATALOG_QUANTITY'] = (
		0 < $arItem['CATALOG_QUANTITY'] && is_float($arItem['CATALOG_MEASURE_RATIO'])
			? floatval($arItem['CATALOG_QUANTITY'])
			: intval($arItem['CATALOG_QUANTITY'])
		);
		$arItem['CATALOG'] = false;
		$arItem['LABEL'] = false;
		if (!isset($arItem['CATALOG_SUBSCRIPTION']) || 'Y' != $arItem['CATALOG_SUBSCRIPTION'])
			$arItem['CATALOG_SUBSCRIPTION'] = 'N';

		// Item Label Properties
		$itemIblockId = $arItem['IBLOCK_ID'];
		$propertyName = isset($arParams['LABEL_PROP'][$itemIblockId]) ? $arParams['LABEL_PROP'][$itemIblockId] : false;

		if ($propertyName && isset($arItem['PROPERTIES'][$propertyName]))
		{
			$property = $arItem['PROPERTIES'][$propertyName];

			if (!empty($property['VALUE']))
			{
				if ('N' == $property['MULTIPLE'] && 'L' == $property['PROPERTY_TYPE'] && 'C' == $property['LIST_TYPE'])
				{
					$arItem['LABEL_VALUE'] = $property['NAME'];
				}
				else
				{
					$arItem['LABEL_VALUE'] = (is_array($property['VALUE'])
						? implode(' / ', $property['VALUE'])
						: $property['VALUE']
					);
				}
				$arItem['LABEL'] = true;

				if (isset($arItem['DISPLAY_PROPERTIES'][$propertyName]))
					unset($arItem['DISPLAY_PROPERTIES'][$propertyName]);
			}
			unset($property);
		}
		// !Item Label Properties

		// item double images
		$productPictures = array(
			"PICT" => false,
			"SECOND_PICT" => false
		);

		if (isset($arParams['ADDITIONAL_PICT_PROP'][$itemIblockId]))
		{
			$productPictures = CIBlockPriceTools::getDoublePicturesForItem($arItem, $arParams['ADDITIONAL_PICT_PROP'][$itemIblockId]);
		}
		else
		{
			$productPictures = CIBlockPriceTools::getDoublePicturesForItem($arItem, false);
		}
		if (empty($productPictures['PICT']))
			$productPictures['PICT'] = $arEmptyPreview;
		if (empty($productPictures['SECOND_PICT']))
			$productPictures['SECOND_PICT'] = $productPictures['PICT'];
		$arItem['PREVIEW_PICTURE'] = $productPictures['PICT'];
		$arItem['PREVIEW_PICTURE_SECOND'] = $productPictures['SECOND_PICT'];
		$arItem['SECOND_PICT'] = true;
		$arItem['PRODUCT_PREVIEW'] = $productPictures['PICT'];
		$arItem['PRODUCT_PREVIEW_SECOND'] = $productPictures['SECOND_PICT'];
		// !item double images

		$arItem['CATALOG'] = true;
		if (!isset($arItem['CATALOG_TYPE']))
			$arItem['CATALOG_TYPE'] = CCatalogProduct::TYPE_PRODUCT;
		if (
			(CCatalogProduct::TYPE_PRODUCT == $arItem['CATALOG_TYPE'] || CCatalogProduct::TYPE_SKU == $arItem['CATALOG_TYPE'])
			&& !empty($arItem['OFFERS'])
		)
		{
			$arItem['CATALOG_TYPE'] = CCatalogProduct::TYPE_SKU;
		}
		switch ($arItem['CATALOG_TYPE'])
		{
			case CCatalogProduct::TYPE_SET:
				$arItem['OFFERS'] = array();
				$arItem['CATALOG_MEASURE_RATIO'] = 1;
				$arItem['CATALOG_QUANTITY'] = 0;
				$arItem['CHECK_QUANTITY'] = false;
				break;
			case CCatalogProduct::TYPE_SKU:
				break;
			case CCatalogProduct::TYPE_PRODUCT:
			default:
				$arItem['CHECK_QUANTITY'] = ('Y' == $arItem['CATALOG_QUANTITY_TRACE'] && 'N' == $arItem['CATALOG_CAN_BUY_ZERO']);
				break;
		}

		// Offers
		if ($arItem['CATALOG'] && isset($arItem['OFFERS']) && !empty($arItem['OFFERS']))
		{
			$arSKUPropIDs = isset($skuPropIds[$arItem['IBLOCK_ID']]) ? $skuPropIds[$arItem['IBLOCK_ID']] : array();
			$arSKUPropList = isset($skuPropList[$arItem['IBLOCK_ID']]) ? $skuPropList[$arItem['IBLOCK_ID']] : array();
			$arSKUPropKeys = isset($skuPropKeys[$arItem['IBLOCK_ID']]) ? $skuPropKeys[$arItem['IBLOCK_ID']] : array();

			$arMatrixFields = $arSKUPropKeys;
			$arMatrix = array();

			$arNewOffers = array();
			$boolSKUDisplayProperties = false;
			$arItem['OFFERS_PROP'] = false;

			foreach ($arItem['OFFERS'] as $keyOffer => $arOffer)
			{
				$arRow = array();
				foreach ($arSKUPropIDs as $propkey => $strOneCode)
				{
					$arCell = array(
						'VALUE' => 0,
						'SORT' => PHP_INT_MAX,
						'NA' => true
					);

					if (isset($arOffer['DISPLAY_PROPERTIES'][$strOneCode]))
					{
						$arMatrixFields[$strOneCode] = true;
						$arCell['NA'] = false;
						if ('directory' == $arSKUPropList[$strOneCode]['USER_TYPE'])
						{
							$intValue = $arSKUPropList[$strOneCode]['XML_MAP'][$arOffer['DISPLAY_PROPERTIES'][$strOneCode]['VALUE']];
							$arCell['VALUE'] = $intValue;
						}
						elseif ('L' == $arSKUPropList[$strOneCode]['PROPERTY_TYPE'])
						{
							$arCell['VALUE'] = intval($arOffer['DISPLAY_PROPERTIES'][$strOneCode]['VALUE_ENUM_ID']);
						}
						elseif ('E' == $arSKUPropList[$strOneCode]['PROPERTY_TYPE'])
						{
							$arCell['VALUE'] = intval($arOffer['DISPLAY_PROPERTIES'][$strOneCode]['VALUE']);
						}
						$arCell['SORT'] = $arSKUPropList[$strOneCode]['VALUES'][$arCell['VALUE']]['SORT'];
					}
					$arRow[$strOneCode] = $arCell;
				}
				$arMatrix[$keyOffer] = $arRow;


				$newOfferProps = array();
				if(!empty($arParams['PROPERTY_CODE'][$arOffer['IBLOCK_ID']]))
				{
					foreach($arParams['PROPERTY_CODE'][$arOffer['IBLOCK_ID']] as $propName)
						$newOfferProps[$propName] = $arOffer['DISPLAY_PROPERTIES'][$propName];
				}
				$arOffer['DISPLAY_PROPERTIES'] = $newOfferProps;

				$arOffer['CHECK_QUANTITY'] = ('Y' == $arOffer['CATALOG_QUANTITY_TRACE'] && 'N' == $arOffer['CATALOG_CAN_BUY_ZERO']);
				if (!isset($arOffer['CATALOG_MEASURE_RATIO']))
					$arOffer['CATALOG_MEASURE_RATIO'] = 1;
				if (!isset($arOffer['CATALOG_QUANTITY']))
					$arOffer['CATALOG_QUANTITY'] = 0;
				$arOffer['CATALOG_QUANTITY'] = (
				0 < $arOffer['CATALOG_QUANTITY'] && is_float($arOffer['CATALOG_MEASURE_RATIO'])
					? floatval($arOffer['CATALOG_QUANTITY'])
					: intval($arOffer['CATALOG_QUANTITY'])
				);
				$arOffer['CATALOG_TYPE'] = CCatalogProduct::TYPE_OFFER;
				CIBlockPriceTools::setRatioMinPrice($arOffer);

				$offerPictures = CIBlockPriceTools::getDoublePicturesForItem($arOffer, $arParams['ADDITIONAL_PICT_PROP'][$arOffer['IBLOCK_ID']]);
				$arOffer['OWNER_PICT'] = empty($offerPictures['PICT']);
				$arOffer['PREVIEW_PICTURE'] = false;
				$arOffer['PREVIEW_PICTURE_SECOND'] = false;
				$arOffer['SECOND_PICT'] = true;
				if (!$arOffer['OWNER_PICT'])
				{
					if (empty($offerPictures['SECOND_PICT']))
						$offerPictures['SECOND_PICT'] = $offerPictures['PICT'];
					$arOffer['PREVIEW_PICTURE'] = $offerPictures['PICT'];
					$arOffer['PREVIEW_PICTURE_SECOND'] = $offerPictures['SECOND_PICT'];
				}
				if ('' != $arParams['OFFER_ADD_PICT_PROP'] && isset($arOffer['DISPLAY_PROPERTIES'][$arParams['OFFER_ADD_PICT_PROP']]))
					unset($arOffer['DISPLAY_PROPERTIES'][$arParams['OFFER_ADD_PICT_PROP']]);
				$arNewOffers[$keyOffer] = $arOffer;
			}
			$arItem['OFFERS'] = $arNewOffers;

			$arUsedFields = array();
			$arSortFields = array();

			foreach ($arSKUPropIDs as $propkey => $strOneCode)
			{
				$boolExist = $arMatrixFields[$strOneCode];
				foreach ($arMatrix as $keyOffer => $arRow)
				{
					if ($boolExist)
					{
						if (!isset($arItem['OFFERS'][$keyOffer]['TREE']))
							$arItem['OFFERS'][$keyOffer]['TREE'] = array();
						$arItem['OFFERS'][$keyOffer]['TREE']['PROP_' . $arSKUPropList[$strOneCode]['ID']] = $arMatrix[$keyOffer][$strOneCode]['VALUE'];
						$arItem['OFFERS'][$keyOffer]['SKU_SORT_' . $strOneCode] = $arMatrix[$keyOffer][$strOneCode]['SORT'];
						$arUsedFields[$strOneCode] = true;
						$arSortFields['SKU_SORT_' . $strOneCode] = SORT_NUMERIC;
					}
					else
					{
						unset($arMatrix[$keyOffer][$strOneCode]);
					}
				}
			}
			$arItem['OFFERS_PROP'] = $arUsedFields;

			\Bitrix\Main\Type\Collection::sortByColumn($arItem['OFFERS'], $arSortFields);

			// Find Selected offer
			foreach($arItem['OFFERS']  as $ind => $offer)
				if($offer['SELECTED'])
				{
					$arItem['OFFERS_SELECTED'] = $ind;
					break;
				}

			$arMatrix = array();
			$intSelected = -1;
			//$arItem['MIN_PRICE'] = false;
			foreach ($arItem['OFFERS'] as $keyOffer => $arOffer)
			{
				if (empty($arItem['MIN_PRICE']) && $arOffer['CAN_BUY'])
				{
					$intSelected = $keyOffer;
					//$arItem['MIN_PRICE'] = (isset($arOffer['RATIO_PRICE']) ? $arOffer['RATIO_PRICE'] : $arOffer['MIN_PRICE']);
				}
				$arSKUProps = false;
				if (!empty($arOffer['DISPLAY_PROPERTIES']))
				{
					$boolSKUDisplayProperties = true;
					$arSKUProps = array();
					foreach ($arOffer['DISPLAY_PROPERTIES'] as &$arOneProp)
					{
						if ('F' == $arOneProp['PROPERTY_TYPE'])
							continue;
						$arSKUProps[] = array(
							'NAME' => $arOneProp['NAME'],
							'VALUE' => $arOneProp['DISPLAY_VALUE']
						);
					}
					unset($arOneProp);
				}

				$arOneRow = array(
					'ID' => $arOffer['ID'],
					'NAME' => $arOffer['~NAME'],
					'TREE' => $arOffer['TREE'],
					'DISPLAY_PROPERTIES' => $arSKUProps,
					'PRICE' => (isset($arOffer['RATIO_PRICE']) ? $arOffer['RATIO_PRICE'] : $arOffer['MIN_PRICE']),
					'SECOND_PICT' => $arOffer['SECOND_PICT'],
					'OWNER_PICT' => $arOffer['OWNER_PICT'],
					'PREVIEW_PICTURE' => $arOffer['PREVIEW_PICTURE'],
					'PREVIEW_PICTURE_SECOND' => $arOffer['PREVIEW_PICTURE_SECOND'],
					'CHECK_QUANTITY' => $arOffer['CHECK_QUANTITY'],
					'MAX_QUANTITY' => $arOffer['CATALOG_QUANTITY'],
					'STEP_QUANTITY' => $arOffer['CATALOG_MEASURE_RATIO'],
					'QUANTITY_FLOAT' => is_double($arOffer['CATALOG_MEASURE_RATIO']),
					'MEASURE' => $arOffer['~CATALOG_MEASURE_NAME'],
					'CAN_BUY' => $arOffer['CAN_BUY'],
					'BUY_URL' => $arOffer['~BUY_URL'],
					'ADD_URL' => $arOffer['~ADD_URL'],
				);
				$arMatrix[$keyOffer] = $arOneRow;
			}

			if (-1 == $intSelected)
				$intSelected = 0;
			if (!$arMatrix[$intSelected]['OWNER_PICT'])
			{
				$arItem['PREVIEW_PICTURE'] = $arMatrix[$intSelected]['PREVIEW_PICTURE'];
				$arItem['PREVIEW_PICTURE_SECOND'] = $arMatrix[$intSelected]['PREVIEW_PICTURE_SECOND'];
			}
			$arItem['JS_OFFERS'] = $arMatrix;
			//$arItem['OFFERS_SELECTED'] = $intSelected;
			$arItem['OFFERS_PROPS_DISPLAY'] = $boolSKUDisplayProperties;
		}

		if ($arItem['CATALOG'] && CCatalogProduct::TYPE_PRODUCT == $arItem['CATALOG_TYPE'])
		{
			CIBlockPriceTools::setRatioMinPrice($arItem, true);
		}

		if (!empty($arItem['DISPLAY_PROPERTIES']))
		{
			foreach ($arItem['DISPLAY_PROPERTIES'] as $propKey => $arDispProp)
			{
				if ('F' == $arDispProp['PROPERTY_TYPE'])
					unset($arItem['DISPLAY_PROPERTIES'][$propKey]);
			}
		}
		$arItem['LAST_ELEMENT'] = 'N';
		$arNewItemsList[$key] = $arItem;
	}

	$arNewItemsList[$key]['LAST_ELEMENT'] = 'Y';
	$arResult['ITEMS'] = $arNewItemsList;
	$arResult['SKU_PROPS'] = $skuPropList;
	$arResult['DEFAULT_PICTURE'] = $arEmptyPreview;
}

CModule::includeModule("sale");
$arBasketItemsID = array();
$dbBasketItems = CSaleBasket::GetList(
	array(
		"NAME" => "ASC",
		"ID" => "ASC"
	    ),
	array(
		"FUSER_ID" => CSaleBasket::GetBasketUserID(),
		"LID" => SITE_ID,
		"ORDER_ID" => "NULL",
	    ),
	false,
	false,
	array("ID", "CALLBACK_FUNC", "MODULE", 
	      "PRODUCT_ID", "QUANTITY", "DELAY", 
	      "CAN_BUY", "PRICE", "WEIGHT")
    );
while ($arItems = $dbBasketItems->Fetch())
{
	if($arItems['CAN_BUY'] != "Y"){
		continue;
	}
    /*if (strlen($arItems["CALLBACK_FUNC"]) > 0)
    {
	CSaleBasket::UpdatePrice($arItems["ID"], 
				 $arItems["CALLBACK_FUNC"], 
				 $arItems["MODULE"], 
				 $arItems["PRODUCT_ID"], 
				 $arItems["QUANTITY"]);
	$arItems2 = CSaleBasket::GetByID($arItems["ID"]);
    }*/

    $arBasketItemsID[] = $arItems['PRODUCT_ID'];
    $arBasketItemsQuantity[$arItems['PRODUCT_ID']] = $arItems["QUANTITY"];
}
unset ($arItems);
$this->__component->SetResultCacheKeys(array('ITEMS_ID'));
$arResult['BASKET_ID'] = $arBasketItemsID;
$arResult['BASKET_QUANTITY'] = $arBasketItemsQuantity;

?>
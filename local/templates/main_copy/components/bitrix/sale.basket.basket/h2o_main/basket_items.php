<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
echo ShowError($arResult["ERROR_MESSAGE"]);

$bDelayColumn  = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn  = false;
$bPriceType    = false;
GLOBAL $cartLine;
$cartLine = $normalCount;
if ($normalCount > 0):
?>
<div id="basket_items_list">
	<div class="head"> 
			<h2 style="letter-spacing: 2px; border-bottom: 2px solid #eee;padding-bottom: 12px;margin-right: 19px;">Корзина</h2>
	</div>
	<div class="cart_item">
		<table class="sort" id="basket_items">
			<thead>
				<tr>
					<td class="item" colspan="2" id="col_NAME">Наименование</td>
					<td class="custom" id="col_QUANTITY">Количество</td>
					<td class="price" id="col_PRICE">Цена</td>
					<td class="custom" id="col_DELETE">Удалить</td>
					<?
					foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):
						$arHeaders[] = $arHeader["id"];
					endforeach;
					?>
				</tr>
			</thead>
			<tbody>
				<?
				foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):
					if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y"):
						if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0):
							$url = $arItem["PREVIEW_PICTURE_SRC"];
						elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0):
							$url = $arItem["DETAIL_PICTURE_SRC"];
						else:
							$url = $templateFolder."/images/no_photo.png";
						endif;?>
						<tr id="<?=$arItem["ID"]?>">
							<td class="t_img">
								<img src="<?=$url?>" alt="">
							</td>
							<td class="description">
								<h4><?=$arItem['NAME']?></h4>
								<div class="artikul">Артикул: <span><?=$arItem['PRODUCT_XML_ID']?></span></div>
								<p>
								<?=$arItem["DETAIL_TEXT"]?>
								</p>
							</td>
							<td class="form">
								<div style="position: relative;">
									<?
									$ratio = isset($arItem["MEASURE_RATIO"]) ? $arItem["MEASURE_RATIO"] : 0;
									$max = isset($arItem["AVAILABLE_QUANTITY"]) ? "max=\"".$arItem["AVAILABLE_QUANTITY"]."\"" : "";
									$useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
									$useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
									?>
									<input
										type="text"
										size="3"
										id="QUANTITY_INPUT_<?=$arItem["ID"]?>"
										name="QUANTITY_INPUT_<?=$arItem["ID"]?>"
										size="2"
										maxlength="18"
										min="0"
										<?=$max?>
										step="<?=$ratio?>"
										style="max-width: 50px"
										value="<?=$arItem["QUANTITY"]?>"
										onchange="updateQuantity('QUANTITY_INPUT_<?=$arItem["ID"]?>', '<?=$arItem["ID"]?>', <?=$ratio?>, <?=$useFloatQuantityJS?>)"
									>

									<?
									if (!isset($arItem["MEASURE_RATIO"]))
									{
										$arItem["MEASURE_RATIO"] = 1;
									}
	
									if (
										floatval($arItem["MEASURE_RATIO"]) != 0
									):
									?>
										<div class="basket_quantity_control">
											<a href="javascript:void(0);" class="plus" onclick="setQuantity(<?=$arItem["ID"]?>, <?=$arItem["MEASURE_RATIO"]?>, 'up', <?=$useFloatQuantityJS?>);"></a>
											<a href="javascript:void(0);" class="minus" onclick="setQuantity(<?=$arItem["ID"]?>, <?=$arItem["MEASURE_RATIO"]?>, 'down', <?=$useFloatQuantityJS?>);"></a>
										</div>
									<?
									endif;
									?>
								</div>
								<?
								echo getMobileQuantityControl(
									"QUANTITY_SELECT_".$arItem["ID"],
									"QUANTITY_SELECT_".$arItem["ID"],
									$arItem["QUANTITY"],
									$arItem["AVAILABLE_QUANTITY"],
									$useFloatQuantityJS,
									$arItem["MEASURE_RATIO"],
									$arItem["MEASURE_TEXT"]
								);
								?>
								<input type="hidden" id="QUANTITY_<?=$arItem['ID']?>" name="QUANTITY_<?=$arItem['ID']?>" value="<?=$arItem["QUANTITY"]?>" />
							</td>
							<td class="prise">
								<span class="cost" style="text-decoration: line-through;"><?if (floatval($arItem["DISCOUNT_PRICE_PERCENT"]) > 0):?>
												<?=$arItem["FULL_PRICE_FORMATED"]?>
											<?endif;?></span><br>
								<span class="cost"><?=$arItem["PRICE_FORMATED"]?></span><br>
								<span class="sale"><?=$arItem["PRICE_CASHLESS_FORMATED"]?></span>
							</td>
							<td class="cart_delete">
								<a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delete"])?>"><img src="<?=$templateFolder."/images/cart_item_del.png"?>" alt=""></a>
							</td>
						</tr>
					<?
					endif;
				endforeach;
				?>
			</tbody>
		</table>
	</div>
	<div class="summa">
		<div class="sum_a">
			<a href="/catalog/">ПРОДОЛЖИТЬ ПОКУПКИ</a>
		</div>
		
		<div class="sum">
			<p class="beznal">Сумма заказа за безналичный расчет (в т.ч. НДС 18%): <span id="allSum_FORMATED"><?=$arResult["allSum_FORMATED"]?></span></p>
			<p class="nal" >Сумма заказа за наличный расчет: <span id="allSum_Cashless_Formated"><?=$arResult["allSum_Cashless_Formated"]?></span></p>
		</div>
	</div>
	<input type="hidden" id="column_headers" value="<?=CUtil::JSEscape(implode($arHeaders, ","))?>" />
	<input type="hidden" id="offers_props" value="<?=CUtil::JSEscape(implode($arParams["OFFERS_PROPS"], ","))?>" />
	<input type="hidden" id="action_var" value="<?=CUtil::JSEscape($arParams["ACTION_VARIABLE"])?>" />
	<input type="hidden" id="quantity_float" value="<?=$arParams["QUANTITY_FLOAT"]?>" />
	<input type="hidden" id="count_discount_4_all_quantity" value="<?=($arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="price_vat_show_value" value="<?=($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="hide_coupon" value="<?=($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="coupon_approved" value="N" />
	<input type="hidden" id="use_prepayment" value="<?=($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N"?>" />

	<div class="bx_ordercart_order_pay">

		<div class="bx_ordercart_order_pay_left">
			<div class="bx_ordercart_coupon">
				<?
				if ($arParams["HIDE_COUPON"] != "Y"):

					$couponClass = "";
					if (array_key_exists('COUPON_VALID', $arResult))
					{
						$couponClass = ($arResult["COUPON_VALID"] == "Y") ? "good" : "bad";
					}elseif (array_key_exists('COUPON', $arResult) && strlen($arResult["COUPON"]) > 0)
					{
						$couponClass = "good";
					}

				?>
					<span><?=GetMessage("STB_COUPON_PROMT")?></span>
					<input type="text" id="coupon" name="COUPON" value="<?=$arResult["COUPON"]?>" onchange="enterCoupon();" size="21" class="<?=$couponClass?>">
				<?else:?>
					&nbsp;
				<?endif;?>
			</div>
		</div>

		<!--div class="bx_ordercart_order_pay_right">
			<table class="bx_ordercart_order_sum">
				<?if ($bWeightColumn):?>
					<tr>
						<td class="custom_t1"><?=GetMessage("SALE_TOTAL_WEIGHT")?></td>
						<td class="custom_t2" id="allWeight_FORMATED"><?=$arResult["allWeight_FORMATED"]?></td>
					</tr>
				<?endif;?>
				<?if ($arParams["PRICE_VAT_SHOW_VALUE"] == "Y"):?>
					<tr>
						<td><?echo GetMessage('SALE_VAT_EXCLUDED')?></td>
						<td id="allSum_wVAT_FORMATED"><?=$arResult["allSum_wVAT_FORMATED"]?></td>
					</tr>
					<tr>
						<td><?echo GetMessage('SALE_VAT_INCLUDED')?></td>
						<td id="allVATSum_FORMATED"><?=$arResult["allVATSum_FORMATED"]?></td>
					</tr>
				<?endif;?>

					<tr>
						<td class="fwb"><?=GetMessage("SALE_TOTAL")?></td>
						<td class="fwb" id="allSum_FORMATED"><?=str_replace(" ", "&nbsp;", $arResult["allSum_FORMATED"])?></td>
					</tr>
					<tr>
						<td class="custom_t1"></td>
						<td class="custom_t2" style="text-decoration:line-through; color:#828282;" id="PRICE_WITHOUT_DISCOUNT">
							<?if (floatval($arResult["DISCOUNT_PRICE_ALL"]) > 0):?>
								<?=$arResult["PRICE_WITHOUT_DISCOUNT"]?>
							<?endif;?>
						</td>
					</tr>

			</table>
			<div style="clear:both;"></div>
		</div-->
		<div style="clear:both;"></div>
	</div>
</div>
<?
else:
?>
<!--div id="basket_items_list">
	<table>
		<tbody>
			<tr>
				<td colspan="<?=$numCells?>" style="text-align:center">
					<div class=""><?=GetMessage("SALE_NO_ITEMS");?></div>
				</td>
			</tr>
		</tbody>
	</table>
</div-->
<?
endif;
?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

//$isAjax = ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST["ajax_action"]) && $_POST["ajax_action"] == "Y");

$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
	'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME']
);

?> <div class="bx_compare <? echo $templateData['TEMPLATE_CLASS']; ?>" id="bx_catalog_compare_block"><?

$i = count($arResult["ITEMS"]);
$i--;
if($i>3)$i=3;
$j = 0;
$s = count($arResult["SHOW_PROPERTIES"]);
?>

<?
if (!empty($arResult["SHOW_FIELDS"]))
{
	foreach ($arResult["SHOW_FIELDS"] as $code => $arProp)
	{
		$showRow = true;
		if (!isset($arResult['FIELDS_REQUIRED'][$code]) || $arResult['DIFFERENT'])
		{
			$arCompare = array();
			foreach($arResult["ITEMS"] as &$arElement)
			{
				$arPropertyValue = $arElement["FIELDS"][$code];
				if (is_array($arPropertyValue))
				{
					sort($arPropertyValue);
					$arPropertyValue = implode(" / ", $arPropertyValue);
				}
				$arCompare[] = $arPropertyValue;
			}
			unset($arElement);
			$showRow = (count(array_unique($arCompare)) > 0);
		}


		if ($showRow)
		{
			?> <?
			foreach($arResult["ITEMS"] as &$arElement)
			{
		?>
				<td valign="top">
		<?
				switch($code)
				{
					case "NAME":
 $i = $i + 2;
 $name[$i] = "<a href='".$arElement["DETAIL_PAGE_URL"]."'>".$arElement[$code]."</a>";
						if($arElement["CAN_BUY"]): 
 $btn[$i] = "<noindex><br /><a class='bx_bt_button bx_small' href='".$arElement["BUY_URL"]."' rel='nofollow'>Купить</a></noindex>";
						 elseif(!empty($arResult["PRICES"]) || is_array($arElement["PRICE_MATRIX"])):?>
						<br /><?=GetMessage("CATALOG_NOT_AVAILABLE")?>
						<?endif;
						break;
					case "PREVIEW_PICTURE":
					case "DETAIL_PICTURE":
						if(is_array($arElement["FIELDS"][$code])):?>
<? $img[$i] = "<a href=".$arElement['DETAIL_PAGE_URL']."><img border='0' src=".$arElement['FIELDS'][$code]['SRC']." width='auto' height='150' alt=".$arElement['FIELDS'][$code]['ALT']." title=".$arElement['FIELDS'][$code]['TITLE']." /></a>"; ?>
						<?endif;
						break;
					default:
						echo $arElement["FIELDS"][$code];
						break;
				}
			?>

			<?
$i--;
			}
			unset($arElement);
		}
	?>

	<?
	}
}

$i = count($arResult["ITEMS"]);
$i--;

?>  



<?
$i = 0;
$i = count($arResult["ITEMS"]);
$i--; ?>

<div class="table_compare"> 
<div>
	<p style="height:200px;"></p>
<? foreach ($arResult["SHOW_PROPERTIES"] as $code => $arProperty)
{ ?> <p><?=$arProperty["NAME"]?></p> <? } ?>
</div>
	<div> 
				<? 
    while($i>=0) {
	foreach ($arResult["SHOW_PROPERTIES"] as $code => $arProperty)
	{   
		if($i > 2) { if($j==0)echo'<div class="compare_item" style="display:none;">'; }else{
			         if($j==0)echo'<div class="compare_item">'; }


foreach($arResult["ITEMS"] as $key=>$arElement)
{ 
    if($key==$i) {
		if($j==0){ !$arElement["FIELDS"]["DETAIL_PICTURE"]["SRC"]?$isrc=$arElement["FIELDS"]["PREVIEW_PICTURE"]["SRC"]:$isrc=$arElement["FIELDS"]["DETAIL_PICTURE"]["SRC"];
?>



<div id="success" class="success" style="display:none;">
<div class="goods-<?=$arElement['ID']?>"><a href="/compare/?action=COMPARE_ADD2BASKET&id=<?=$arElement["ID"]?>" class="close" onclick="closee()"></a>
	<?if(is_array($arElement["PREVIEW_PICTURE"])):?>
		<?$file = CFile::ResizeImageGet($arElement["PREVIEW_PICTURE"], array('width'=>213, 'height'=>140), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
		<a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="list_preview_image"><?if($APPLICATION->GetCurPage() == "/catalog/osnastka/buri-koronki-sds-plus/bury-sds-plus-5x/") ?>

			<img <?if(!empty($price_bas)) {?>itemprop="image"<? }?> style="<?if($this->GetEditAreaId($arElement['ID']) == "bx_1970176138_131632") {?> height:102px; <?}?>display: block; margin: 0 auto;" src="<?=$file['src']?>" alt=""></a>


	<?elseif(is_array($arElement["DETAIL_PICTURE"])):?>
		<?$file = CFile::ResizeImageGet($arElement["DETAIL_PICTURE"], array('width'=>213, 'height'=>140), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
		<a class="image_action" href="<?=$arElement["DETAIL_PAGE_URL"]?>"> <?if($APPLICATION->GetCurPage() == "/catalog/osnastka/buri-koronki-sds-plus/bury-sds-plus-5x/") ?><img <?if(!empty($price_bas)) {?>itemprop="image"<? }?> style="display: block; margin: 0 auto;" src="<?=$file['src']?>" alt=""></a>
	<?else:?>
		<a class="image_action" href="<?=$arElement["DETAIL_PAGE_URL"]?>"> <?if($APPLICATION->GetCurPage() == "/catalog/osnastka/buri-koronki-sds-plus/bury-sds-plus-5x/") ?><img <?if(!empty($price_bas)) {?>itemprop="image"<? }?> style="display: block; margin: 0 auto;" src="<?=SITE_TEMPLATE_PATH."/images/no_photo.jpg"?>" alt=""></a>
	<?endif?>
	<div class="description">
		<a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="pName"  style="color: inherit;"><p <?if(!empty($price_bas)) {?>itemprop="name"<?}?> ><?=$arElement["NAME"]?></p>
		<p class="add cart-test" style="display:none"><strong style="color:green;">Товар добавлен в корзину</strong></p>
		<p class="add favor" style="display:none"><strong style="color:green;">Товар добавлен в список</strong>
        <strong style="color:green;display:none">Для того, чтобы "запомнить" товар необходимо авторизоваться. Если у вас еще нет учетной записи, Вы можете зарегистрироваться прямо сейчас!"</strong></p>
		<p class="add compar" style="display:none"><strong style="color:green;">Товар добавлен в сравнение</strong></p>
		</a>
        <!--<div class="artikul">Артикул: <?//=$arElement["XML_ID"]?></div>-->
	</div> 
	<div class="compare-btn">
		<a style="cursor: pointer;
    display: block;
    width: 40%;
    float: left;
    background: #ebeded;
    border: none;
    height: 32px;
    line-height: 32px;
    text-align: center;
    font-size: .923em;
    font-weight: bold;
    color: black;
    text-decoration: none;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;" class="buttons grey_btn" onclick="closee()" href="/compare/?action=COMPARE_ADD2BASKET&id=<?=$arElement["ID"]?>" >Продолжить просмотр</a>
		<a class="cart-test" href="/personal/cart/" style="display:none">Перейти в корзину</a>
		<a class="favor" href="/favorites" style="display:none">Перейти в список</a>
		<a class="compar" href="/compare" style="display:none">Перейти к сравнению</a> 
        <a class="regno" style="display:none" href="/login">Зарегистрироваться/вход</a>
    </div> 
</div>
</div> 



<a class="close" onclick="CatalogCompareObj.MakeAjaxAction('<?=CUtil::JSEscape($arElement['~DELETE_URL'])?>');" href="javascript:void(0)"><?=GetMessage("CATALOG_REMOVE_PRODUCT")?></a>
		<? echo '<a style="display:block;width:100%;height:120px;" href="'.$arElement["DETAIL_PAGE_URL"].'"><img style="max-height:120px;max-width:120px;margin:0 auto;" src="'.$isrc.'" /></a>'; 
		   echo '<h5><a href="'.$arElement["DETAIL_PAGE_URL"].'">'.$arElement["NAME"].'</a></h5>';  
           echo '<a id="'.$arElement["ID"].'" href="/compare/?action=COMPARE_ADD2BASKET&id='.$arElement["ID"].'" onclick="BX.ajax.insertToNode("/compare/?action=COMPARE_ADD2BASKET&id='.$arElement["ID"].'&bxajaxid='.$arParams['AJAX_ID'].'", "comp_'.$arParams['AJAX_ID'].'"); return false;" id="'.$arElement["ID"].'" class="bx_bt_button bx_small" rel="nofollow">Купить</a>';

		   }?>

<p><?if($arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]):?>
	<strong><?=(is_array($arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"])? implode("/ ", $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]): $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"])?></strong>
<?else:?>-<?endif?></p>
<?}

}  unset($arElement);
                $j++;
		if($j==$s)echo'</div>';
	}$i--; $j=0;}  ?> 


<? $h = count($arResult["SHOW_PROPERTIES"])*34; ?>
<? $c = count($arResult["ITEMS"]); ?>

<? if($c == 1)echo '<div class="compare_item" style="min-height:'.$h.'px"><p>Подберите дополнительный инструмент для сравнения</p></div><div class="compare_item" style="min-height:'.$h.'px"><p>Подберите дополнительный инструмент для сравнения</p></div>'; ?>
<? if($c == 2)echo '<div class="compare_item" style="min-height:'.$h.'px"><p>Подберите дополнительный инструмент для сравнения</p></div>'; ?>
</div>  
</div> 


<div>
	<?/*
if ($isAjax)
{
	die();
}*/
?>
</div>
<script type="text/javascript">
	var CatalogCompareObj = new BX.Iblock.Catalog.CompareClass("bx_catalog_compare_block");
</script>

<script type="text/javascript">
		$(document).on('click', 'a.bx_bt_button', function (e) {
			e.preventDefault();
			var id = $(this).attr('id');

			if(!$(this).hasClass('already_in_cart')) {  

            $("#mask").remove(); 
            $(".goods-"+id).first().parent().css("display", "block"); 
            $(".cart-test").css("display", "block");
				$("body").append("<div id='mask'></div>");    }


			$(this).addClass('already_in_cart').html('Уже в корзине');  
   });
</script>
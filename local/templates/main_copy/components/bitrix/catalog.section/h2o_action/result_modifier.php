<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arParams['AJAX_ID'] = CH2oAjax::getComponentID($this->__component);
foreach($arResult['ITEMS'] as $k=>$arElement)
{
	$user_id = $arElement['DISPLAY_PROPERTIES']['USER_ID']['DISPLAY_VALUE'];
	if ($user_id)
	{
		$rsUSER = CUser::GetById($user_id);
		$f=$rsUSER->Fetch();
		$arResult['ITEMS'][$k]['DISPLAY_PROPERTIES']['USER_ID']['DISPLAY_VALUE'] = CUser::FormatName(CSite::GetNameFormat(false), array("NAME" => $f['NAME'], "LAST_NAME" => $f['LAST_NAME'], "SECOND_NAME" => $f['SECOND_NAME'], "LOGIN" => $f['LOGIN']));
	}
	
	$dbProductDiscounts = CCatalogDiscount::GetList(
		array("SORT" => "ASC"),
		array("+PRODUCT_ID" => $arElement["ID"],
            "ACTIVE" => "Y",),
		false,
		false,
		array(	"ID", "SITE_ID", "ACTIVE", "ACTIVE_FROM", "ACTIVE_TO", 
			"RENEWAL", "NAME", "SORT", "MAX_DISCOUNT", "VALUE_TYPE", 
			"VALUE", "CURRENCY", "PRODUCT_ID")
	);
	if ($arProductDiscounts = $dbProductDiscounts->Fetch())
	{
		$date = date_create($arProductDiscounts["ACTIVE_TO"]);
		$arProductDiscounts["ACTIVE_TO"] = date_format($date, 'd.m.Y');
		$arResult['ITEMS'][$k]["DISCOUNT"] = $arProductDiscounts;
	}
	
	$IBLOCK_ID = $arElement["IBLOCK_ID"]; 
	$ID = $arElement["ID"];
	$arInfo = CCatalogSKU::GetInfoByProductIBlock($IBLOCK_ID); 
	if (is_array($arInfo)){
		$minPrice = "";
		$minCurr = "";
		$rsOffers = CIBlockElement::GetList(array(),array('IBLOCK_ID' => $arInfo['IBLOCK_ID'], 'PROPERTY_'.$arInfo['SKU_PROPERTY_ID'] => $ID)); 
		while ($arOffer = $rsOffers->GetNext()){
			$arFilter = array("ID"=>$arOffer["ID"]);
			$arSelect = array("ID", "XML_ID", "NAME");
			$resof = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			if($ar_resof = $resof->GetNext()){
				$db_price = CPrice::GetList(array(),array("PRODUCT_ID" => $ar_resof['ID'],));
				if ($arPrice = $db_price->Fetch()){
					$ar_resof["PRICE"] = $arPrice;
					if ($minPrice==""){
						$minPrice = $arPrice["PRICE"];
						$minCurr = $arPrice["CURRENCY"];
					}elseif($minPrice>$arPrice["PRICE"]){
						$minPrice = $arPrice["PRICE"];
						$minCurr = $arPrice["CURRENCY"];
					}
				}
				$arResult['ITEMS'][$k]["OFFERS"][] = $ar_resof;
			}
		}
		$arResult['ITEMS'][$k]["MIN_PRICE"] = $minPrice;
		$arResult['ITEMS'][$k]["MIN_CURR"] = $minCurr;
	}
	$arResult['ITEMS_ID'][] = $ID;
}

$this->__component->SetResultCacheKeys(array('ITEMS_ID'));
?>

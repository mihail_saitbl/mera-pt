<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<h3 id="spec" style="margin-bottom: 15px;">Акции</h3>
		<pre><?//print_r($arResult['ITEMS'])?></pre>
	<div class="wrap_gallery">
		<div class="gallery" style="padding-right: 0px;">
			<div id="owl-demo" class="owl-carousel">
<?$n=0;?>
<?$t=0;?>

<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
	<?$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));?>
	<?$file=array();?>
	<?$n++;?>
	<?if($n==1){?>
		<?$t++;?>
		<div class="item">
	<?}?>					
	<div class="goods catalog_goods" style="margin-right: 15px; border: 2px solid #eee;float: none;" id="<?=$this->GetEditAreaId($arElement['ID']);?>">

		<?if (!empty($arElement["DISCOUNT"]["ACTIVE_TO"])):?><div class="act_date"><p>до <?=$arElement["DISCOUNT"]["ACTIVE_TO"]?></p></div><?endif;?>
		<?if(is_array($arElement["PREVIEW_PICTURE"])):?>
			<?$file = CFile::ResizeImageGet($arElement["PREVIEW_PICTURE"], array('width'=>200, 'height'=>140), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
			<a class="image_action" href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=$file['src']?>" alt=""></a>
		<?elseif(is_array($arElement["DETAIL_PICTURE"])):?>
			<?$file = CFile::ResizeImageGet($arElement["DETAIL_PICTURE"], array('width'=>200, 'height'=>140), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
			<a class="image_action" href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=$file['src']?>" alt=""></a>
		<?else:?>
			<a class="image_action" href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=SITE_TEMPLATE_PATH?>/images/no_photo.jpg" alt=""></a>
		<?endif?>
		<div class="description">
			<h4><?=$arElement["NAME"]?></h4>
			<div class="artikul">Артикул: <?=$arElement["XML_ID"]?></div>
		</div>
		<div class="prise">
			<?if(!empty($arElement["OFFERS"])):?>
				<?if($arElement["MIN_PRICE"]):?>
					<span class="cost">от</span><span class="sale"><?=FormatCurrency($arElement["MIN_PRICE"], $arElement['MIN_CURR'])?></span>
				<?endif;?>
			<?else:?>
				<?foreach($arElement["PRICES"] as $code=>$arPrice):?>
					<?if($arPrice["CAN_ACCESS"]):?>
						<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
							<span class="cost"><?=$arPrice["PRINT_VALUE"]?></span> <span class="sale"><?=$arPrice["PRINT_DISCOUNT_VALUE"]?></span>
						<?else:?><span class="cost"><?=$arPrice["PRINT_VALUE"]?></span><span class="sale"><?=FormatCurrency(($arPrice["VALUE"]*0.9), $arPrice['CURRENCY'])?></span><?endif;?>
			
					<?endif;?>
				<?endforeach;?>
				<?if(empty($arElement["PRICES"])):?>
				<span class="cost">Нет цены</span>
			<?endif;?>
			<?endif;?>
			<?if($arParams["DISPLAY_COMPARE"]):?>
					<noindex>
					<a ><!--href="<?echo $arElement['COMPARE_URL']?>" rel="nofollow">--><img src="<?=SITE_TEMPLATE_PATH?>/images/maincontent/grafic.png" alt=""></a>
					</noindex>
			<?endif?>
			
		</div>
		<div class="tocart">
			<?if($arElement["CAN_BUY"]):?>
				<?if($arParams["USE_PRODUCT_QUANTITY"] || count($arElement["PRODUCT_PROPERTIES"])):?>
					<form class="<?=$arParams['AJAX_ID']?>" id="<?=$arElement['ID']?>" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
					<input type="hidden" name="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" value="1" size="5">
					<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
					<input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?echo $arElement["ID"]?>">
					<button type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("CATALOG_ADD")?>"><?echo GetMessage("CATALOG_ADD")?></button>
					<!--input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("CATALOG_ADD")?>"-->
					</form>
				<?else:?>
					<noindex>
						<a href="<?echo $arElement['ADD_URL']?>" rel="nofollow"><?echo GetMessage("CATALOG_ADD")?></a>
					</noindex>
				<?endif?>
			<?elseif((count($arResult["PRICES"]) > 0) || is_array($arElement["PRICE_MATRIX"])):?>
				<a href="<?=$arElement['DETAIL_PAGE_URL']?>" rel="nofollow"><?echo GetMessage("CATALOG_ADD")?></a>
			<?endif?>
		</div>
	</div>
	<?if($n==2){?>
		<?$n=0;?>
		</div>
	<?}?>	
<?endforeach;?>
</div>
</div>
</div>
<div style="clear: both;"></div>

<div style="clear: both;"></div>
<script type="text/javascript">
	$(document).ready(function() {
		$("#owl-demo").owlCarousel({
			loop:true,
			margin:10,
			nav:true,
			navigation : true, // Show next and prev buttons
			slideSpeed : 800,
			paginationSpeed : 800,
			navText : true,
			singleItem:true,
			// "singleItem:true" is a shortcut for:
			items : 4,
			navigationText : ["prev","next"],
			// itemsDesktop : false,
			// itemsDesktopSmall : false,
			// itemsTablet: false,
			// itemsMobile : false
		     
		});
		/*
$(".gallery").jCarouselLite({
		btnNext: ".next",
		btnPrev: ".prev",
		speed: 1000,
		start: 0,
		visible: 4,
		scroll: 4,
	});*/
	});
	
		$(document).on('submit', 'div.tocart form.<?=$arParams['AJAX_ID']?>', function (e) {
			e.preventDefault();
			var id = $(this).attr('id');
			$('form[id="'+id+'"]').find('button[type="submit"]').addClass('already_in_cart').html('Купить еще');
			$(this).bitrixAjax(
				"<?=$arParams['AJAX_ID']?>",
				function (data) {
					update_basket_line();
					//$('div.tocart form#'+id).html(data.find('div.goods'+id).html());  
				},
				{
					post: {
						submit: 'Y'
					}
				}
			);
		});
	</script>

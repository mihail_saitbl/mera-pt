<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$style = 'bx_cart_block';

if ($arParams["SHOW_PRODUCTS"] == "Y")
	$style .= " bx_cart_sidebar";

if ($arParams["POSITION_FIXED"] == "Y")
{
	$style .= " bx_cart_fixed ".$arParams['POSITION_HORIZONTAL'].' '.$arParams["POSITION_VERTICAL"];
	if ($arParams["SHOW_PRODUCTS"] == "Y")
		$style .= " close";
}
?>
<a href="<?=$arParams['PATH_TO_BASKET']?>" style="text-decoration: none;">
	<div id="bx_cart_block">
		<pre><?//print_r($arResult)?></pre>
		<div class='number'>
			<img src="<?=SITE_TEMPLATE_PATH?>/images/logo/cart.png"  alt=""><?=GetMessage('TSB1_TO_CART')?> <b><?=$arResult['NUM_PRODUCTS']?> <?=$arResult['PRODUCT(S)']?></b>
		</div>
		<div class='sum'><?=GetMessage('TSB1_SUM')." <b>".$arResult['TOTAL_PRICE']?></b></div>
		<?if($arResult["NUM_PRODUCTS"]>0):?><a class="make_ord" href="<?=$arParams['PATH_TO_BASKET']?>"><?=GetMessage('TSB1_2ORDER')?></a><?endif;?>
	</div>
</a>
<script>
	sbbl.elemBlock = BX('bx_cart_block');

	sbbl.ajaxPath = '<?=$componentPath?>/ajax.php';
	sbbl.siteId = '<?=SITE_ID?>';
	sbbl.templateName = '<?=$templateName?>';
	sbbl.arParams = <?=CUtil::PhpToJSObject ($arParams)?>;

	BX.addCustomEvent(window, 'OnBasketChange', sbbl.refreshCart);

	<?if ($arParams["POSITION_FIXED"] == "Y"):?>
		sbbl.elemStatus = BX('bx_cart_block_status');
		sbbl.strCollapse = '<?=GetMessage('TSB1_COLLAPSE')?>';
		sbbl.strExpand = '<?=GetMessage('TSB1_EXPAND')?>';
		sbbl.bClosed = true;

		sbbl.elemProducts = BX('bx_cart_block_products');
		sbbl.bMaxHeight = false;
		sbbl.bVerticalTop = <?=$arParams["POSITION_VERTICAL"] == "top" ? 'true' : 'false'?>;

		<?if ($arParams["POSITION_VERTICAL"] == "top"):?>
			sbbl.fixCartTopPosition();
			BX.addCustomEvent(window, "onTopPanelCollapse", sbbl.fixCartTopPosition);
		<?endif?>

		sbbl.resizeTimer = null;
		BX.bind(window, 'resize', function() {
			clearTimeout(sbbl.resizeTimer);
			sbbl.resizeTimer = setTimeout(sbbl.toggleMaxHeight, 500);
		});
	<?endif?>

</script>
<script type="text/javascript">
	function update_basket_line() {
		console.log('ajax id basket <?=$arResult['AJAX_ID']?>');
		$(document).bitrixAjax(
			"<?=$arResult['AJAX_ID']?>",
			function (data) {
				$('#bx_cart_block').html(data.find("#bx_cart_block").html());
			}
		);
		
		
	}
</script>
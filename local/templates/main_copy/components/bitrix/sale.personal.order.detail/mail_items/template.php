<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?foreach($arResult["BASKET"] as $prod):?>
    <?$ID = $prod['PRODUCT_ID'];
    $arDelivery = AGGetDeliveryTime ($ID);
    if($_SESSION['CURRENT_CITY']['ID']==338){
        $del = "Отгрузка: ".$arDelivery["NSK"]." после оплаты";
    }else{
        $del = "Отгрузка: ".$arDelivery["OMSK"]." после оплаты";
    }
    $arPrices = AGFindTwoPrices($prod["PRODUCT_ID"], $prod["QUANTITY"]);

    if ($arResult["PAY_SYSTEM_ID"] == 8) // если оплата безнал, то скидка не применяется
    {
        if ($arPrices["DISC_TYPE"] != "always") // если тип скидки - для наличных
        {
            $arPrices["PRICE_DISC"] = $arPrices["PRICE"]; // убираем скидку
        }
    }
    else // если оплата налом, а цены одинаковые, то надо применить скидку
    {
        if ($arPrices["PRICE"] == $arPrices["PRICE_DISC"])
        {
            $arPrices["PRICE_DISC"] = ($arPrices["PRICE"] - $arPrices["PRICE"]*$basket_discount);
        }
    }

    $prod["PRICE"] = $arPrices["PRICE_DISC"]/$prod["QUANTITY"];

    ?>
    <?=$prod['NAME']?> <?=$prod["QUANTITY"]?> - <?=GetMessage('SPOD_DEFAULT_MEASURE')?>: <?=$prod["PRICE"]*$prod["QUANTITY"]?>р. <?=$del?><br />

<?endforeach;?>

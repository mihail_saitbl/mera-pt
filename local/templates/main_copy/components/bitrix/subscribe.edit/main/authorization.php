<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
//*************************************
//show current authorization section
//*************************************
?>
<?echo bitrix_sessid_post();?>
<h2><?echo GetMessage("subscr_title_auth")?></h2>
<div class="change_line">
	<p><?echo GetMessage("adm_auth_user")?>
	<?echo htmlspecialcharsbx($USER->GetFormattedName(false));?> [<?echo htmlspecialcharsbx($USER->GetLogin())?>].</p>
</div>



<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$APPLICATION->SetPageProperty("title", $arResult["NAME"]);
?>
<pre><?//print_r($arResult)?></pre>
<div class="catalog-element">
	<div style="display: inline-block; width: 100%;">
		<div class="connected-carousels">
			<div class="navigation">
				<div class="btn_slide_up"><a href="#" class="prev-navigation"></a></div>
				<div class="btn_slide_down"><a href="#" class="next-navigation"></a></div>
				<div class="carousel-wrapper">
					<div class="carousel-items" style="top: 0px;">
						<?$k=-1;?>
						<?if(!empty($arResult["DETAIL_PICTURE"])){?>
							<div class="carousel-block" data-index="<?=$k+1?>">
								<img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="">
							</div>
						<?}elseif(!empty($arResult["PREVIEW_PICTURE"])){?>
							<div class="carousel-block" data-index="<?=$k+1?>">
								<img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" alt="">
							</div>
						<?}?>
						<?foreach($arResult["PROPERTIES"]["IMAGES"]["VALUE"] as $k => $image):?>	
							<div class="carousel-block" data-index="<?=$k+1?>">
								<img src="<?=$image["URL"]?>" alt="">
							</div>
						<?endforeach;?>
					</div>
				</div>
			</div>
			<div class="stage">
				<div class="carousel carousel-stage">
					<ul style="left: 0px;">
						<?if(!empty($arResult["DETAIL_PICTURE"])){?>
							<li class="active_tab">
								<a href="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" rel="gallery1" class="gall block left fancybox" style="position: relative;">
									<div style="background-image: url('<?=$arResult["DETAIL_PICTURE"]["SRC"]?>');background-repeat: no-repeat;background-position: 50%;background-size: contain;"></div>
								</a>
							</li>
						<?}elseif(!empty($arResult["PREVIEW_PICTURE"])){?>
							<li class="active_tab">
								<a href="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" rel="gallery1" class="gall block left fancybox" style="position: relative;">
									<div style="background-image: url('<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>');background-repeat: no-repeat;background-position: 50%;background-size: contain;"></div>
								</a>
							</li>
						<?}?>
						<?foreach($arResult["PROPERTIES"]["IMAGES"]["VALUE"] as $k => $image):?>
						<li class="active_tab">
							<a href="<?=$image["URL"]?>" rel="gallery1" class="gall block left fancybox" style="position: relative;">
								<div style="background-image: url('<?=$image["URL"]?>');background-repeat: no-repeat;background-position: 50%;background-size: contain;"></div>
							</a>
						</li>
						<?endforeach;?>
					</ul>
				</div>
			</div>
		</div>
	
		<div class="left_side">
			<h1><?=$arResult["NAME"]?></h1>
			<div class="grey_block">
				<?if(!empty($arResult["OFFERS"])):?>
					<?foreach($arResult["OFFERS"] as $keyoff=>$off):?>
						<div class="artic hide tabs_<?=$off["ID"]?>">
							<p>Артикул:</p><span><?=$off["XML_ID"]?></span>
							<a href="http://www.bosch-pt.ru" target="_blank">Сайт производителя</a>
						</div>
					<?endforeach;?>
					<p>Комплектация:</p>
					<select class="offers">
					<?foreach($arResult["OFFERS"] as $ofkey => $offers):?>
						<option value="<?=$offers["ID"]?>">
							<?=$offers["NAME"]?>
						</option>
					<?endforeach;?>
					</select>
				<?else:?>
					<div class="artic">
						<p>Артикул:</p><span><?=$arResult["XML_ID"]?></span>
						<a href="http://www.bosch-pt.ru" target="_blank">Сайт производителя</a>
					</div>
				<?endif;?>
			</div>
			<?if(!empty($arResult["OFFERS"])):?>
				<p>Подробней о комплектациях читайте на вкладке <span>"Комплектация".</span></p>
			<?endif;?>
			<?if(!empty($arResult["OFFERS"])){?>
				<?foreach($arResult["OFFERS"] as $keyoff=>$off):?>
					<div class="hide tabs_<?=$off["ID"]?>">
						<div class="prise">
							<?foreach($off["PRICES"] as $code=>$arPrice):?>
								<span>
									<?if($arPrice["CAN_ACCESS"]):?>
										<?if($arParams["PRICE_VAT_SHOW_VALUE"] && ($arPrice["VATRATE_VALUE"] > 0)):?>
											<?if($arParams["PRICE_VAT_INCLUDE"]):?>
												(<?echo GetMessage("CATALOG_PRICE_VAT")?>)
											<?else:?>
												(<?echo GetMessage("CATALOG_PRICE_NOVAT")?>)
											<?endif?>
										<?endif;?>
										<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
											<?=$arPrice["PRINT_DISCOUNT_VALUE"]?>
										<?else:?>
											<?=$arPrice["PRINT_VALUE"]?>
										<?endif?>
									<?endif;?>
								</span>
							<?endforeach;?>
							<div class="red_prise"><?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]){ echo(FormatCurrency(ceil($arPrice["DISCOUNT_VALUE"]*0.9), $arPrice["CURRENCY"]));}else{ echo(FormatCurrency(ceil($arPrice["VALUE"]*0.9), $arPrice["CURRENCY"]));}?> -</div><p>цена при покупке 
							за наличный расчет</p>
						</div>
						<div id="tabs_<?=$off["ID"]?>" class="tocart hide tabs_<?=$off["ID"]?>">
							<?if($off["CAN_BUY"]):?>
								<?if($arParams["USE_PRODUCT_QUANTITY"] || count($arResult["PRODUCT_PROPERTIES"])):?>
									<form id="<?=$off["ID"]?>" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
									<input type="hidden" name="<? echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
									<input type="hidden" name="<? echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?=$off["ID"]?>">
									<!--input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."BUY"?>" value="<?echo GetMessage("CATALOG_BUY")?>"-->
									<input type="hidden" name="<? echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?=$off["ID"]?>">
									</form>
								<?else:?>
									<noindex>
									<a href="<?echo $arResult["BUY_URL"]?>" rel="nofollow"><?echo GetMessage("CATALOG_BUY")?></a>
									&nbsp;<a href="<?echo $arResult["ADD_URL"]?>" rel="nofollow"><?echo GetMessage("CATALOG_ADD_TO_BASKET")?></a>
									</noindex>
								<?endif;?>
							<?elseif((count($arResult["PRICES"]) > 0) || is_array($arResult["PRICE_MATRIX"])):?>
								<?=GetMessage("CATALOG_NOT_AVAILABLE")?>
								<?$APPLICATION->IncludeComponent("bitrix:sale.notice.product", ".default", array(
									"NOTIFY_ID" => $arResult['ID'],
									"NOTIFY_URL" => htmlspecialcharsback($arResult["SUBSCRIBE_URL"]),
									"NOTIFY_USE_CAPTHA" => "N"
									),
									$component
								);?>
							<?endif?>
							<a class="addtocart" href="#" id="<?=$off["ID"]?>">В КОРЗИНУ</a>
						</div>
						<a href="#" data-remodal-target="modal-oneclick-<?=$off["ID"]?>" class="buy1">купить в 1 клик</a>
						<!--<img src="<?=SITE_TEMPLATE_PATH?>/images/maincontent/grafic.png" alt="">-->
						<div class="remodal" data-remodal-id="modal-oneclick-<?=$off["ID"]?>">
							<?$APPLICATION->IncludeComponent(
								"h2o:buyoneclick",
								"main",
								Array(
									"ASD_ID" => $off["ID"],
									"ASD_TITLE" => $_REQUEST["title"],
									"ASD_URL" => $_REQUEST["url"],
									"ASD_PICTURE" => $_REQUEST["picture"],
									"ASD_TEXT" => $_REQUEST["text"],
									"ASD_LINK_TITLE" => "Расшарить в #SERVICE#",
									"ASD_SITE_NAME" => "",
									"ASD_INCLUDE_SCRIPTS" => array()
								)
							);?>
						</div>
						<p>Адрес сервисного центра BOSCH:</p>
						<?if($arResult['SECTION']["PATH"][0]['ID'] == 1797):?>
							<p>г. Новосибирск, ул. Сибревкома, д. 2, оф. 718</p>
						<?else:?>
							<p>г. Новосибирск, ул. Красноярская, д. 36, цокольный этаж</p>
						<?endif;?>
						<hr class="underline">
						<div class="status_block">
							<div class="status">
								<span class="status_left"><?=$off["PROPERTIES"]["NSK"]["NAME"]?></span>
								<?if($off["PROPERTIES"]["NSK"]["VALUE"]>0){?>
								<span class="status_right" style="color: #6a983c"><b>В наличии</b></span>
								<?}else{?>
								<span class="status_right" style="color: #e62239"><b>Нет в наличии</b></span>
								<?}?>
							</div>
							<div class="status">
								<span class="status_left"><?=$off["PROPERTIES"]["OMSK"]["NAME"]?></span>
								<?if($off["PROPERTIES"]["OMSK"]["VALUE"]>0){?>
								<span class="status_right" style="color: #6a983c"><b>В наличии</b></span>
								<?}else{?>
								<span class="status_right" style="color: #e62239"><b>Нет в наличии</b></span>
								<?}?>
							</div>
						</div>
					</div>
				<?endforeach;?>
			<?}else{?>
				<?if(!empty($arResult["PRICES"])){?>
					<div class="prise">
						<?foreach($arResult["PRICES"] as $code=>$arPrice):?>
							<span>
								<?if($arPrice["CAN_ACCESS"]):?>
									<?if($arParams["PRICE_VAT_SHOW_VALUE"] && ($arPrice["VATRATE_VALUE"] > 0)):?>
										<?if($arParams["PRICE_VAT_INCLUDE"]):?>
											(<?echo GetMessage("CATALOG_PRICE_VAT")?>)
										<?else:?>
											(<?echo GetMessage("CATALOG_PRICE_NOVAT")?>)
										<?endif?>
									<?endif;?>
									<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
										<?=$arPrice["PRINT_DISCOUNT_VALUE"]?>
									<?else:?>
										<?=$arPrice["PRINT_VALUE"]?>
									<?endif?>
								<?endif;?>
							</span>
						<?endforeach;?>
						<div class="red_prise"><?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]){ echo(FormatCurrency(ceil($arPrice["DISCOUNT_VALUE"]*0.9), $arPrice["CURRENCY"]));}else{ echo(FormatCurrency(ceil($arPrice["VALUE"]*0.9), $arPrice["CURRENCY"]));}?> -</div><p>цена при покупке 
						за наличный расчет</p>
					</div>
					<div class="tocart">
						<?if(($arResult["CAN_BUY"])!=""):?>
							<?if($arParams["USE_PRODUCT_QUANTITY"] || count($arResult["PRODUCT_PROPERTIES"])):?>
								<form id="<? if(empty($arResult["OFFERS"])){echo($arResult["ID"]);}else{echo($arResult["OFFERS"][0]["ID"]);}?>" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
								<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
								<input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<? if(empty($arResult["OFFERS"])){echo($arResult["ID"]);}else{echo($arResult["OFFERS"][0]["ID"]);}?>">
								<!--input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."BUY"?>" value="<?echo GetMessage("CATALOG_BUY")?>"-->
								<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("CATALOG_ADD_TO_BASKET")?>">
								</form>
							<?else:?>
								<noindex>
								<a href="<?echo $arResult["BUY_URL"]?>" rel="nofollow"><?echo GetMessage("CATALOG_BUY")?></a>
								&nbsp;<a href="<?echo $arResult["ADD_URL"]?>" rel="nofollow"><?echo GetMessage("CATALOG_ADD_TO_BASKET")?></a>
								</noindex>
							<?endif;?>
						<?elseif((count($arResult["PRICES"]) > 0) || is_array($arResult["PRICE_MATRIX"])):?>
							<?=GetMessage("CATALOG_NOT_AVAILABLE")?>
							<?$APPLICATION->IncludeComponent("bitrix:sale.notice.product", ".default", array(
								"NOTIFY_ID" => $arResult['ID'],
								"NOTIFY_URL" => htmlspecialcharsback($arResult["SUBSCRIBE_URL"]),
								"NOTIFY_USE_CAPTHA" => "N"
								),
								$component
							);?>
						<?endif?>
						<a class="addtocart" href="#" id="<? if(empty($arResult["OFFERS"])){echo($arResult["ID"]);}else{echo($arResult["OFFERS"][0]["ID"]);}?>">В КОРЗИНУ</a>
					</div>
					<a href="#" data-remodal-target="modal-oneclick-<?=$arResult["ID"]?>" class="buy1">купить в 1 клик</a>
					<!--<img src="<?=SITE_TEMPLATE_PATH?>/images/maincontent/grafic.png" alt="">-->
					<div class="remodal" data-remodal-id="modal-oneclick-<?=$arResult["ID"]?>">
						<?$APPLICATION->IncludeComponent(
							"h2o:buyoneclick",
							"main",
							Array(
								"ASD_ID" => $arResult["ID"],
								"ASD_TITLE" => $_REQUEST["title"],
								"ASD_URL" => $_REQUEST["url"],
								"ASD_PICTURE" => $_REQUEST["picture"],
								"ASD_TEXT" => $_REQUEST["text"],
								"ASD_LINK_TITLE" => "Расшарить в #SERVICE#",
								"ASD_SITE_NAME" => "",
								"ASD_INCLUDE_SCRIPTS" => array()
							)
						);?>
					</div>
				<?}?>
				<p>Адрес сервисного центра BOSCH:</p>
				<?if($arResult['SECTION']["PATH"][0]['ID'] == 1797):?>
					<p>г. Новосибирск, ул. Сибревкома, д. 2, оф. 718</p>
				<?else:?>
					<p>г. Новосибирск, ул. Красноярская, д. 36, цокольный этаж</p>
				<?endif;?>
				<hr class="underline">
				<div class="status_block">
					<div class="status">
						<span class="status_left"><?=$arResult["PROPERTIES"]["NSK"]["NAME"]?></span>
						<?if($arResult["PROPERTIES"]["NSK"]["VALUE"]>0){?>
						<span class="status_right" style="color: #6a983c"><b>В наличии</b></span>
						<?}else{?>
						<span class="status_right" style="color: #e62239"><b>Нет в наличии</b></span>
						<?}?>
					</div>
					<div class="status">
						<span class="status_left"><?=$arResult["PROPERTIES"]["OMSK"]["NAME"]?></span>
						<?if($arResult["PROPERTIES"]["OMSK"]["VALUE"]>0){?>
						<span class="status_right" style="color: #6a983c"><b>В наличии</b></span>
						<?}else{?>
						<span class="status_right" style="color: #e62239"><b>Нет в наличии</b></span>
						<?}?>
					</div>
				</div>
			<?}?>
			
		</div>
			<div class="tabs">
				<div class="menu_product">
					<ul>
						<li><a href="#fragment-1" class="active">Преимущества</a></li>
						<li><a href="#fragment-2">Характеристики</a></li>
						<li><a href="#fragment-3">Видео</a></li>
						<li><a href="#fragment-4">Руководства</a></li>
						<li><a href="#fragment-5">Гарантии</a></li>
						<?if(!empty($arResult["PROPERTIES"]["KOMPL_POST"]["VALUE"]) || !empty($arResult["OFFERS"])){?>
							<li ><a href="#fragment-6">Комплект поставки</a></li>
						<?}?>
					</ul>
				</div>
				<div class="content_menu" id="fragment-1">
					<pre><?//print_r($arResult["PROPERTIES"])?></pre>
					<?if($arResult["PROPERTIES"]["ADVANTAGES"]["VALUE"]["TEXT"]):?>
						<ul class="adv">
							<?$phrase  = $arResult["PROPERTIES"]["ADVANTAGES"]["~VALUE"]["TEXT"];
							$phrase  = unserialize($phrase)?>
							<?foreach($phrase as $elem){?>
								<li>
									<a><?=$elem?></a>
								</li>
							<?}?>
							<?if($arResult["PROPERTIES"]["DOP_PREIM"]["VALUE"]["TEXT"]):?>
								<?$phrase  = $arResult["PROPERTIES"]["DOP_PREIM"]["~VALUE"]["TEXT"];
								$phrase  = unserialize($phrase)?>
								<?foreach($phrase as $elem){?>
									<li>
										<a><?=$elem?></a>
									</li>
								<?}?>
							<?endif;?>
						</ul>
					<?endif;?>
				</div>
				<div class="content_menu" id="fragment-2">
					<table class="tab_table">
						<?foreach($arResult["PROP_CHAR"] as $propChar):?>
							<tr>
								<td><?=$arResult["PROPERTIES"][$propChar]["NAME"]?></td>
								<td><?=$arResult["PROPERTIES"][$propChar]["VALUE"]?></td>
							</tr>
						<?endforeach;?>
					</table>
				</div>
				<div class="content_menu" id="fragment-3">
				</div>
				<div class="content_menu" id="fragment-4">
					<?$rsFile = CFile::GetByID($arResult["PROPERTIES"]["LEADERSHIP"]["VALUE"]);
					$arFile = $rsFile->Fetch();
					$arFile["PATH"] = CFile::GetPath($arFile["ID"]);?>
					<a href="<?=$arFile["PATH"]?>"><?=$arFile["ORIGINAL_NAME"]?></a>
				</div>
				<div class="content_menu" id="fragment-5">
				</div>
				<?if(!empty($arResult["PROPERTIES"]["KOMPL_POST"]["VALUE"]) || !empty($arResult["OFFERS"])){?>
					<div class="content_menu" id="fragment-6">
						<?if(!empty($arResult["OFFERS"])){
							foreach($arResult["OFFERS"] as $keyoff=>$off):?>
								<div class="hide tabs_<?=$off["ID"]?>">
									<ul class="adv">
										<?if($off["PROPERTIES"]["KOMPLEKT"]["VALUE"]["TEXT"] !=""):?>
											<?$phrase  = $off["PROPERTIES"]["KOMPLEKT"]["~VALUE"]["TEXT"];
											$phrase  = unserialize($phrase)?>
											<?foreach($phrase as $elem){?>
												<li>
													<a><?=$elem?></a>
												</li>
											<?}?>	
										<?endif;?>
									</ul>
								</div>
							<?endforeach;
						}else{?>
							<?if($arResult["PROPERTIES"]["KOMPL_POST"]["VALUE"]["TEXT"] !=""):?>
								<ul class="adv">
									<?$phrase  = $arResult["PROPERTIES"]["KOMPL_POST"]["~VALUE"]["TEXT"];
									$phrase  = unserialize($phrase)?>
									<?foreach($phrase as $elem){?>
										<li>
											<a><?=$elem?></a>
										</li>
									<?}?>
								</ul>
							<?endif;?>
						<?}?>
					</div>
				<?}?>
			</div>
		<?if(!empty($arResult["PROPERTIES"]["NEEDED"]["VALUE"])){?>
			<div class="necessary">
				<p><?=$arResult["PROPERTIES"]["NEEDED"]["NAME"]?>:</p>
				<?foreach ($arResult["PROPERTIES"]["NEEDED"]["VALUE"] as $key=>$needed){?>
					
						<div class="product_necess">
							<a class="link" href="<?=$needed["DETAIL_PAGE_URL"]?>">
							<div style="display: inline-block; width: 110px; text-align: center; float: left;">
								<img src="<?=$needed["IMG_URL"]?>" alt="">
							</div>
							<div class="teod"><?=$needed["NAME"]?></div>
							<p>Артикул: <?=$needed["XML_ID"]?></p>
							<span class="grey_pr"><?=FormatCurrency($needed["PRICE"]['PRICE'], $needed["PRICE"]['CURRENCY'])?></span>
							<?$cur = $needed["PRICE"]['PRICE']*0.9;?>
							<div class="red_small_pr"><?=FormatCurrency($cur, $needed["PRICE"]['CURRENCY'])?></div>
							<?if($needed["PRICE"]['PRICE']>0):?>
								<form id="<?=($needed["ID"]);?>" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
								<input type="hidden" name="<?=$arParams["ACTION_VARIABLE"]?>" value="BUY">
								<input type="hidden" name="<?=$arParams["PRODUCT_ID_VARIABLE"]?>" value="<?=($needed["ID"]);?>">
								<!--input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."BUY"?>" value="<?echo GetMessage("CATALOG_BUY")?>"-->
								<input type="hidden" name="<?=$arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?=GetMessage("CATALOG_ADD_TO_BASKET")?>">
								</form>
								
							<?endif;?>
							</a>
							<?if($needed["PRICE"]['PRICE']>0):?>
								<a class="addtocart needed_buy" href="#" id="<?=($needed["ID"])?>">В корзину</a>
							<?endif;?>
						</div>
					
				<?}?>
			</div>
		<?}?>
		<?if(!empty($arResult["PROPERTIES"]["RECOMMEND"]["VALUE"])){?>
		<div class="head"> 
			<h2 style="letter-spacing: 2px;padding-bottom: 12px;margin-right: 19px;">Рекомендуем приобрести</h2>
		</div>
		<div class=" content" style="margin-left: 0px;padding-right: 0px;width: auto;">
			<?$GLOBALS['arrFilter'] = array("ID" => $arResult["PROPERTIES"]["RECOMMEND"]["VALUE"])?>
			<?$APPLICATION->IncludeComponent(
				"bitrix:catalog.section", 
				"h2o_salelider", 
				array(
					"IBLOCK_TYPE" => "catalog",
					"IBLOCK_ID" => "5",
					"SECTION_ID" => "",
					"SECTION_CODE" => "",
					"SECTION_USER_FIELDS" => array(
						0 => "",
						1 => "",
					),
					"ELEMENT_SORT_FIELD" => "RAND",
					"ELEMENT_SORT_ORDER" => "asc",
					"ELEMENT_SORT_FIELD2" => "id",
					"ELEMENT_SORT_ORDER2" => "desc",
					"FILTER_NAME" => "arrFilter",
					"INCLUDE_SUBSECTIONS" => "Y",
					"SHOW_ALL_WO_SECTION" => "Y",
					"HIDE_NOT_AVAILABLE" => "N",
					"PAGE_ELEMENT_COUNT" => "4",
					"LINE_ELEMENT_COUNT" => "3",
					"PROPERTY_CODE" => array(
						0 => "ARTNUMBER",
						1 => "",
					),
					"OFFERS_LIMIT" => "4",
					"SECTION_URL" => "",
					"DETAIL_URL" => "",
					"SECTION_ID_VARIABLE" => "SECTION_ID",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_GROUPS" => "Y",
					"SET_META_KEYWORDS" => "Y",
					"META_KEYWORDS" => "-",
					"SET_META_DESCRIPTION" => "N",
					"META_DESCRIPTION" => "-",
					"BROWSER_TITLE" => "-",
					"ADD_SECTIONS_CHAIN" => "N",
					"DISPLAY_COMPARE" => "N",
					"SET_TITLE" => "N",
					"SET_STATUS_404" => "N",
					"CACHE_FILTER" => "N",
					"PRICE_CODE" => array(
						0 => "базовая",
					),
					"USE_PRICE_COUNT" => "N",
					"SHOW_PRICE_COUNT" => "0",
					"PRICE_VAT_INCLUDE" => "Y",
					"CONVERT_CURRENCY" => "N",
					"BASKET_URL" => "/personal/cart/",
					"ACTION_VARIABLE" => "action",
					"PRODUCT_ID_VARIABLE" => "id",
					"USE_PRODUCT_QUANTITY" => "Y",
					"PRODUCT_QUANTITY_VARIABLE" => "quantity",
					"ADD_PROPERTIES_TO_BASKET" => "Y",
					"PRODUCT_PROPS_VARIABLE" => "prop",
					"PARTIAL_PRODUCT_PROPERTIES" => "N",
					"PRODUCT_PROPERTIES" => array(
					),
					"PAGER_TEMPLATE" => ".default",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "N",
					"PAGER_TITLE" => "Товары",
					"PAGER_SHOW_ALWAYS" => "Y",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "Y",
					"AJAX_OPTION_ADDITIONAL" => "",
					"SET_BROWSER_TITLE" => "Y",
					"COMPARE_PATH" => ""
				),
				false
			);?>
		</div>
		<?}?>
	</div>
			
		<pre><?//print_r($arResult)?></pre>	
			
				<?/*foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
					<b><?=$arProperty["NAME"]?>:</b>&nbsp;<?
					if(is_array($arProperty["DISPLAY_VALUE"])):
						echo implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);
					elseif($pid=="MANUAL"):
						?><a href="<?=$arProperty["VALUE"]?>"><?=GetMessage("CATALOG_DOWNLOAD")?></a><?
					else:
						echo $arProperty["DISPLAY_VALUE"];?>
					<?endif?><br />
				<?endforeach*/?>
				
	<?/*if($arResult["PREVIEW_TEXT"]):?>
		<br /><?=$arResult["PREVIEW_TEXT"]?><br />
	<?elseif($arResult["DETAIL_TEXT"]):?>
		<br /><?=$arResult["DETAIL_TEXT"]?><br />
	<?endif;*/?>
	<?if(count($arResult["LINKED_ELEMENTS"])>0):?>
		<br /><b><?=$arResult["LINKED_ELEMENTS"][0]["IBLOCK_NAME"]?>:</b>
		<ul>
	<?foreach($arResult["LINKED_ELEMENTS"] as $arElement):?>
		<li><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></li>
	<?endforeach;?>
		</ul>
	<?endif?>
	<?
	if(count($arResult["MORE_PHOTO"])>0):?>
		<a name="more_photo"></a>
		<?foreach($arResult["MORE_PHOTO"] as $PHOTO):?>
			<img border="0" src="<?=$PHOTO["SRC"]?>" width="<?=$PHOTO["WIDTH"]?>" height="<?=$PHOTO["HEIGHT"]?>" alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>" /><br />
		<?endforeach?>
	<?endif?>
	<?if(is_array($arResult["SECTION"])):?>
		<br /><a href="<?=$arResult["SECTION"]["SECTION_PAGE_URL"]?>"><?=GetMessage("CATALOG_BACK")?></a>
	<?endif?>
</div>
<script type="text/javascript">
		$(document).on('click', 'a.addtocart', function (e) {
			e.preventDefault();
			var id = $(this).attr('id');
			console.log(id);
			$('div.tocart').find('a[id="'+id+'"]').addClass('already_in_cart').html('Уже в корзине');
			$('form#'+id).bitrixAjax(
				"<?=$arParams['AJAX_ID']?>",
				function (data) {
					update_basket_line();
					//$('div.left_side').html(data.find('div.left_side').html());  
				},
				{
					post: {
						submit: 'Y',
					}
				}
			);
		});
</script>

<script>
	$(document).ready(function() {
		var select = $("select.offers").val();
		$(".hide").hide();
		$(".tabs_"+select).show();
		$('select.offers').change(function(){
			var select = $(this).val();
			$(".hide").hide();
			$(".tabs_"+select).show();
		});
	});
</script>
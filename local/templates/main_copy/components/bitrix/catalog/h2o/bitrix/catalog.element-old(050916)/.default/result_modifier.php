<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arParams['AJAX_ID'] = CH2oAjax::getComponentID($this->__component);
$user_id = $arResult['DISPLAY_PROPERTIES']['USER_ID']['DISPLAY_VALUE'];
if ($user_id)
{
	$rsUSER = CUser::GetById($user_id);
	$f=$rsUSER->Fetch();
	$arResult['DISPLAY_PROPERTIES']['USER_ID']['DISPLAY_VALUE'] = CUser::FormatName(CSite::GetNameFormat(false), array("NAME" => $f['NAME'], "LAST_NAME" => $f['LAST_NAME'], "SECOND_NAME" => $f['SECOND_NAME'], "LOGIN" => $f['LOGIN']));
}
?>
<?
$arImages = array();
//$db_props = CIBlockElement::GetProperty($arResult["IBLOCK_ID"], $arResult["ID"], array("sort" => "asc"), Array("CODE"=>"IMAGES"));
//while ($ar_props = $db_props->Fetch())
foreach($arResult["PROPERTIES"]["IMAGES"]["VALUE"] as $k => $imageId){
	$rsFile = CFile::GetByID($imageId);
	$arFile = $rsFile->Fetch();
	$arImages = $arFile;
	//$arImages["IMAGE_ID"] = $imageId;
	$arImages["URL"] = CFile::GetPath($imageId);
	$arResult["PROPERTIES"]["IMAGES"]["VALUE"][$k] = $arImages;
}

if(!empty($arResult["PROPERTIES"]["NEEDED"]["VALUE"])){
	foreach ($arResult["PROPERTIES"]["NEEDED"]["VALUE"] as $key=>$needed){
		//$arSelect = Array();
		//$arFilter = Array("ID"=>IntVal($needed), "ACTIVE"=>"Y");
		//$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

		$res = CIBlockElement::GetByID($needed);
		if($ar_res = $res->GetNext()){
			$arResult["PROPERTIES"]["NEEDED"]["VALUE"][$key] = $ar_res;
			if($ar_res['PREVIEW_PICTURE']!=""){
				$arResult["PROPERTIES"]["NEEDED"]["VALUE"][$key]["IMG_URL"] = CFile::GetPath($ar_res['PREVIEW_PICTURE']);
			}elseif ($ar_res['DETAIL_PICTURE']!=""){
				$arResult["PROPERTIES"]["NEEDED"]["VALUE"][$key]["IMG_URL"] = CFile::GetPath($ar_res['DETAIL_PICTURE']);
			}
		}
		
		$db_props = CIBlockElement::GetProperty($ar_res["IBLOCK_ID"], $ar_res["ID"], array("sort" => "asc"), Array());
		if($ar_props = $db_props->Fetch())
		    $arResult["PROPERTIES"]["NEEDED"]["VALUE"][$key][$ar_props['CODE']] = $ar_props;
		    
		//$arPrice = CPrice::GetByID($ar_res["ID"]);
		
		$db_price = CPrice::GetList(
			array(),
			array("PRODUCT_ID" => $ar_res['ID'],)
		);
		if ($arPrice = $db_price->Fetch()){
			$arResult["PROPERTIES"]["NEEDED"]["VALUE"][$key]['PRICE'] = $arPrice;
		}
		$arResult["ITEMS_ID"][]=$needed;
	}
}
?>
<?if(!empty($arResult["OFFERS"])):?>
	<?foreach($arResult["OFFERS"] as $ofkey => $offers):?>
		<?$arFilter = array("ID"=>$offers["ID"]);
		$arSelect = array("XML_ID", "NAME");
		$resof = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		if($ar_resof = $resof->GetNext())
			$arResult["OFFERS"][$ofkey]["NAME"] = $ar_resof['NAME'];?>
		<?foreach($offers["PROPERTIES"]["IMAGES"]["VALUE"] as $k => $imageId){
			$rsFile = CFile::GetByID($imageId);
			$arFile = $rsFile->Fetch();
			$arImages = $arFile;
			//$arImages["IMAGE_ID"] = $imageId;
			$arImages["URL"] = CFile::GetPath($imageId);
			$arResult["OFFERS"][$ofkey]["PROPERTIES"]["IMAGES"]["VALUE"][$k] = $arImages;
		}
		
		if(!empty($offers["PROPERTIES"]["NEEDED"]["VALUE"])){
			foreach ($offers["PROPERTIES"]["NEEDED"]["VALUE"] as $key=>$needed){
				//$arSelect = Array();
				//$arFilter = Array("ID"=>IntVal($needed), "ACTIVE"=>"Y");
				//$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		
				$res = CIBlockElement::GetByID($needed);
				if($ar_res = $res->GetNext()){
					$arResult["OFFERS"][$ofkey]["PROPERTIES"]["NEEDED"]["VALUE"][$key] = $ar_res;
					if($ar_res['PREVIEW_PICTURE']!=""){
						$arResult["OFFERS"][$ofkey]["PROPERTIES"]["NEEDED"]["VALUE"][$key]["IMG_URL"] = CFile::GetPath($ar_res['PREVIEW_PICTURE']);
					}elseif ($ar_res['DETAIL_PICTURE']!=""){
						$arResult["OFFERS"][$ofkey]["PROPERTIES"]["NEEDED"]["VALUE"][$key]["IMG_URL"] = CFile::GetPath($ar_res['DETAIL_PICTURE']);
					}
				}
				
				$db_props = CIBlockElement::GetProperty($ar_res["IBLOCK_ID"], $ar_res["ID"], array("sort" => "asc"), Array());
				if($ar_props = $db_props->Fetch())
				    $arResult["OFFERS"][$ofkey]["PROPERTIES"]["NEEDED"]["VALUE"][$key][$ar_props['CODE']] = $ar_props;
				    
				//$arPrice = CPrice::GetByID($ar_res["ID"]);
				
				$db_price = CPrice::GetList(
					array(),
					array("PRODUCT_ID" => $ar_res['ID'],)
				);
				if ($arPrice = $db_price->Fetch()){
					$arResult["OFFERS"][$ofkey]["PROPERTIES"]["NEEDED"]["VALUE"][$key]['PRICE'] = $arPrice;
				}
			}
		}
		?>
		
		<?if(!empty($offers["PROPERTIES"]["CONSIST"]["VALUE"])){
			foreach ($offers["PROPERTIES"]["CONSIST"]["VALUE"] as $key=>$needed){
				//$arSelect = Array();
				//$arFilter = Array("ID"=>IntVal($needed), "ACTIVE"=>"Y");
				//$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		
				$res = CIBlockElement::GetByID($needed);
				if($ar_res = $res->GetNext()){
					$arResult["OFFERS"][$ofkey]["PROPERTIES"]["CONSIST"]["VALUE"][$key] = $ar_res;
					if($ar_res['PREVIEW_PICTURE']!=""){
						$arResult["OFFERS"][$ofkey]["PROPERTIES"]["CONSIST"]["VALUE"][$key]["IMG_URL"] = CFile::GetPath($ar_res['PREVIEW_PICTURE']);
					}elseif ($ar_res['DETAIL_PICTURE']!=""){
						$arResult["OFFERS"][$ofkey]["PROPERTIES"]["CONSIST"]["VALUE"][$key]["IMG_URL"] = CFile::GetPath($ar_res['DETAIL_PICTURE']);
					}
				}
				
				$db_props = CIBlockElement::GetProperty($ar_res["IBLOCK_ID"], $ar_res["ID"], array("sort" => "asc"), Array());
				if($ar_props = $db_props->Fetch())
				    $arResult["OFFERS"][$ofkey]["PROPERTIES"]["CONSIST"]["VALUE"][$key][$ar_props['CODE']] = $ar_props;
				    
				//$arPrice = CPrice::GetByID($ar_res["ID"]);
				
				$db_price = CPrice::GetList(
					array(),
					array("PRODUCT_ID" => $ar_res['ID'],)
				);
				if ($arPrice = $db_price->Fetch()){
					$arResult["OFFERS"][$ofkey]["PROPERTIES"]["CONSIST"]["VALUE"][$key]['PRICE'] = $arPrice;
				}
			}
		}
		?>
	<?$arResult["ITEMS_ID"][]=$offers["ID"];?>
	<?endforeach;?>
<?endif;?>
<?$arResult["ITEMS_ID"][]=$arResult["ID"];?>

<?$arFilterProps = array("ARTNUMBER", "BLOG_POST_ID", "BLOG_COMMENTS_CNT", "FUNCTION", "IMAGES", "ADVANTAGES", "LEADERSHIP", "NEEDED", "RECOMMEND", "NSK", "OMSK", "LOGO", "MARK_POS", "DOP_PREIM", "KOMPL_POST", "SALELEADER", "SPECIALOFFER", "CML2_TRAITS", "CML2_BASE_UNIT", "CML2_ARTICLE", "CML2_TAXES", "EXTERNAL_XML_ID", "MAXIMUM_PRICE", "MINIMUM_PRICE", "VIDEO", "PRICE_1C");?>
<?foreach($arResult["PROPERTIES"] as $key_prop=>$prop_val):
	/*global $USER;
	if ($USER->IsAdmin())
	{
		echo $key_prop.": ".$prop_val["NAME"]." ".$prop_val["CODE"].": ".$prop_val["VALUE"]."<br />";
	}*/
	if(!in_array($key_prop, $arFilterProps)){
		if((!empty($prop_val["VALUE"]))&&($prop_val["VALUE"]!="Array")){
			$arResult["PROP_CHAR"][]=$key_prop;
		}
	}
endforeach;?>

<?

CModule::IncludeModule("sale");
$basket_discount = 1;
$ar = CSaleDiscount::GetByID(1);
$arar = unserialize($ar['ACTIONS']);
$basket_discount = 1 - $arar['CHILDREN'][0]['DATA']['Value']/100;


$arResult["basket_discount"] = $basket_discount;


?>




<?$this->__component->SetResultCacheKeys(array('ITEMS_ID'));?>

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
				</div> <!-- //bx_content_section-->
			</div>
		</div>
	<footer>
	<div class="remodal" data-remodal-id="modal-reg">
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.register",
			"h2o_main",
			Array(
				"SHOW_FIELDS" => array("EMAIL", "NAME", "PERSONAL_PHONE"),
				"REQUIRED_FIELDS" => array("EMAIL", "NAME"),
				"AUTH" => "Y",
				"USE_BACKURL" => "Y",
				"SUCCESS_PAGE" => "",
				"SET_TITLE" => "N",
				"USER_PROPERTY" => array(),
				"USER_PROPERTY_NAME" => ""
			)
		);?>
	</div>
<? /*
	<div class="remodal" data-remodal-id="modal-feedback">
		<?$APPLICATION->IncludeComponent("informunity:feedback", "main", Array(
	"USE_CAPTCHA" => "Y",	// Использовать защиту от автоматических сообщений (CAPTCHA) для не авторизованных пользователей
		"OK_TEXT" => "Спасибо, ваше сообщение принято.",	// Сообщение, выводимое пользователю после отправки
		"USE_IU_PAT" => "Y",	// Использовать почтовый шаблон компонента
		"USE_IU_IB" => "Y",	// Создавать элементы в инфоблоке
		"USE_ATTACH" => "N",	// Отправлять файлы
		"EMAIL_TO" => array(	// E-mail, на которые будет отправлено письмо
			0 => "",
			1 => "pawwwloff@yandex.ru",
			2 => "",
		),
		"EXT_FIELDS" => array(	// Поля для заполнения
			0 => "iu_0",
			1 => "iu_1",
			2 => "iu_2",
			3 => "Номер телефона",
			4 => "",
		),
		"FIELD_FOR_THEME" => "iu_none",	// Брать макрос #THEME# в теме письма из поля
		"EM_THEME" => "#SITE#: Сообщение из формы обратной связи",	// Тема письма
		"AFTER_TEXT" => "",	// Текст в конце письма
		"USE_EMAIL_USER" => "N",	// Отправитель письма - посетитель
		"REQUIRED_FIELDS" => array(	// Выберите обязательные поля для заполнения
			0 => "iu_0",
			1 => "Номер телефона",
		),
		"TEXTAREA_FIELDS" => "",	// Выберите поля с типом "Текст"
		"FIELD_FOR_NAME" => "iu_0",	// Выберите поле для имени
		"FIELD_FOR_EMAIL" => "iu_1",	// Выберите поле для E-mail
		"COPY_LETTER" => "N",	// Выводить checkbox "Отсылать себе копию письма"
		"USE_IU_IBC" => "Y",	// Использовать инфоблок компонента
		"IBLOCK_NAME" => "Сообщения из формы обратной связи",	// Название инфоблока
		"IB_ACT" => "N",	// Активность
		"IBE_NAME" => "iu_0",	// Поле для названия
		"IB_DET" => "iu_2",	// Поле для детального описания
		"IB_ANONS" => "iu_none",	// Поле для анонса
		"IB_PARAM" => "Y",	// Записывать остальные поля в свойства
		"WRIT_A" => "Y",	// Добавлять в письмо ссылку на элемент инфоблока
	),
	false
);?>
	</div>
		
*/ ?>		
		
		<div class="footerwrapper">
			
			<div class="footer_logo">
				<img src="<?=SITE_TEMPLATE_PATH?>/images/footer/logo.png" alt="">
			</div>

			<div class="leftli">
				<?$APPLICATION->IncludeComponent("bitrix:menu", "h2o_bottom_menu", array(
	"ROOT_MENU_TYPE" => "bottom_left",
	"MENU_CACHE_TYPE" => "A",
	"MENU_CACHE_TIME" => "36000000",
	"MENU_CACHE_USE_GROUPS" => "Y",
	"MENU_CACHE_GET_VARS" => array(
	),
	"MAX_LEVEL" => "1",
	"CHILD_MENU_TYPE" => "left",
	"USE_EXT" => "N",
	"DELAY" => "N",
	"ALLOW_MULTI_SELECT" => "N"
	),
	false
);?>
			</div>

			<div class="rightli">
				<?$APPLICATION->IncludeComponent("bitrix:menu", "h2o_top_menu", array(
	"ROOT_MENU_TYPE" => "top",
	"MENU_CACHE_TYPE" => "A",
	"MENU_CACHE_TIME" => "36000000",
	"MENU_CACHE_USE_GROUPS" => "Y",
	"MENU_CACHE_GET_VARS" => array(
	),
	"MAX_LEVEL" => "3",
	"CHILD_MENU_TYPE" => "left",
	"USE_EXT" => "Y",
	"DELAY" => "N",
	"ALLOW_MULTI_SELECT" => "N"
	),
	false
);?>
			</div>

			<div class="footer_adress">
				<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => "/include/bottom_contacts.php",
		"EDIT_TEMPLATE" => ""
	),
	false
);?>
			</div>
			<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => "/include/copyright.php",
		"EDIT_TEMPLATE" => ""
	),
	false
);?>
		</div>
	</footer>

	</div> <!-- //wrap -->
</body>
</html>
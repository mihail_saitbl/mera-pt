<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();
?>
<div class="ajax_wrap_reg">
		<div class="change_pass_forms">
<?if($USER->IsAuthorized()&&($_REQUEST['Register']=="Y")):?>
<div class="new remod_h1">
		<p>РЕГИСТРАЦИЯ</p>
		<div class="clear"></div>
		<hr class="under_menu hr_margin">
		<p><?echo GetMessage("MAIN_REGISTER_AUTH")?></p>
	</div>
<script type="text/javascript">
					window.location.hash = '#?';
					location.reload(true);
				</script>

<?elseif(!$USER->IsAuthorized()):?>


<?
if (count($arResult["ERRORS"]) > 0):
	foreach ($arResult["ERRORS"] as $key => $error)
		if (intval($key) == 0 && $key !== 0) 
			$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);

	ShowError(implode("<br />", $arResult["ERRORS"]));

elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):
?>
<p><?echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
<?endif?>
	<div class="new remod_h1">
		<p>РЕГИСТРАЦИЯ</p>
	</div>
	<div class="clear"></div>
	<hr class="under_menu hr_margin">
<form class="regform" method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data">
<?
if($arResult["BACKURL"] <> ''):
?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?
endif;
?>
<?foreach ($arResult["SHOW_FIELDS"] as $FIELD):?>
	<?if($FIELD == "AUTO_TIME_ZONE" && $arResult["TIME_ZONE_ENABLED"] == true):?>
	<div class="change_line">
		<div class="before_input"><?echo GetMessage("main_profile_time_zones_auto")?><?if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y"):?>*<?endif?>:</div>
		<select class="input_line" name="REGISTER[AUTO_TIME_ZONE]" onchange="this.form.elements['REGISTER[TIME_ZONE]'].disabled=(this.value != 'N')">
			<option value=""><?echo GetMessage("main_profile_time_zones_auto_def")?></option>
			<option value="Y"<?=$arResult["VALUES"][$FIELD] == "Y" ? " selected=\"selected\"" : ""?>><?echo GetMessage("main_profile_time_zones_auto_yes")?></option>
			<option value="N"<?=$arResult["VALUES"][$FIELD] == "N" ? " selected=\"selected\"" : ""?>><?echo GetMessage("main_profile_time_zones_auto_no")?></option>
		</select>
	</div>
	<div class="change_line">
		<div class="before_input"><?echo GetMessage("main_profile_time_zones_zones")?>:</div>
		<select class="input_line" name="REGISTER[TIME_ZONE]"<?if(!isset($_REQUEST["REGISTER"]["TIME_ZONE"])) echo 'disabled="disabled"'?>>
		<?foreach($arResult["TIME_ZONE_LIST"] as $tz=>$tz_name):?>
			<option value="<?=htmlspecialcharsbx($tz)?>"<?=$arResult["VALUES"]["TIME_ZONE"] == $tz ? " selected=\"selected\"" : ""?>><?=htmlspecialcharsbx($tz_name)?></option>
		<?endforeach?>
		</select>
	</div>
	<?else:?>
	<div class="change_line">
		<div class="before_input"><?=GetMessage("REGISTER_FIELD_".$FIELD)?><?if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y"):?>*<?endif?>:</div>
		<?
		switch ($FIELD)
		{
			case "LOGIN":
				?><input class="input_line" type="text" name="REGISTER[<?=$FIELD?>]" value="">
					<?
				break;
			case "PASSWORD":
				?><input type="password" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>" autocomplete="off" class="input_line" /><?
				break;
			case "CONFIRM_PASSWORD":
				?><input class="input_line" type="password" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>" autocomplete="off" />
				<?/*if(!empty($FIELD['ERROR'])):?>
						<span style="color: red;"><?=$FIELD['ERROR'];?></span>
				<?endif;*/?>
				<?
				break;
			case "PERSONAL_GENDER":
				?><select class="input_line" name="REGISTER[<?=$FIELD?>]">
					<option value=""><?=GetMessage("USER_DONT_KNOW")?></option>
					<option value="M"<?=$arResult["VALUES"][$FIELD] == "M" ? " selected=\"selected\"" : ""?>><?=GetMessage("USER_MALE")?></option>
					<option value="F"<?=$arResult["VALUES"][$FIELD] == "F" ? " selected=\"selected\"" : ""?>><?=GetMessage("USER_FEMALE")?></option>
				</select><?
				break;
			case "PERSONAL_COUNTRY":
			case "WORK_COUNTRY":
				?><select class="input_line" name="REGISTER[<?=$FIELD?>]"><?
				foreach ($arResult["COUNTRIES"]["reference_id"] as $key => $value)
				{
					?><option value="<?=$value?>"<?if ($value == $arResult["VALUES"][$FIELD]):?> selected="selected"<?endif?>><?=$arResult["COUNTRIES"]["reference"][$key]?></option>
				<?
				}
				?></select><?
				break;
			case "PERSONAL_PHOTO":
			case "WORK_LOGO":
				?><input class="input_line" type="file" name="REGISTER_FILES_<?=$FIELD?>" /><?
				break;
			case "PERSONAL_NOTES":
			case "WORK_NOTES":
				?><textarea class="input_line" cols="30" rows="5" name="REGISTER[<?=$FIELD?>]"><?=$arResult["VALUES"][$FIELD]?></textarea><?
				break;
			case "PERSONAL_MOBILE":
				?><input class="input_line phone" type="text" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>" /><?
				break;
			default:
				if ($FIELD == "PERSONAL_BIRTHDAY"):?><small><?=$arResult["DATE_FORMAT"]?></small><br /><?endif;
				?><input class="input_line" type="text" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>" /><?
					if ($FIELD == "PERSONAL_BIRTHDAY")
						$APPLICATION->IncludeComponent(
							'bitrix:main.calendar',
							'',
							array(
								'SHOW_INPUT' => 'N',
								'FORM_NAME' => 'regform',
								'INPUT_NAME' => 'REGISTER[PERSONAL_BIRTHDAY]',
								'SHOW_TIME' => 'N'
							),
							null,
							array("HIDE_ICONS"=>"Y")
						);
					
		}
		?>
		<?if(!empty($arResult['ERRORS'][$FIELD])):?>
			<br/><span style="color: red;"><?=$arResult['ERRORS'][$FIELD];?></span>
		<?endif;?>
	</div>
	<?endif;?>
<?endforeach;?>
<?// ********************* User properties ***************************************************?>
<?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
	<?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>
		<div class="change_line">
			<div class="before_input"><?=$arUserField["EDIT_FORM_LABEL"]?>:<?if ($arUserField["MANDATORY"]=="Y"):?>*<?endif;?>:</div>
			<?$APPLICATION->IncludeComponent(
					"bitrix:system.field.edit",
					$arUserField["USER_TYPE"]["USER_TYPE_ID"],
					array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "form_name" => "regform"), null, array("HIDE_ICONS"=>"Y"));?>
		</div>
	<?endforeach;?>
<?endif;?>
<?// ******************** /User properties ***************************************************?>
<?
/* CAPTCHA */
if ($arResult["USE_CAPTCHA"] == "Y")
{
	?>
	<div class="change_line">
		<div class="before_input"><?=GetMessage("REGISTER_CAPTCHA_TITLE")?>:</div>
		<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
		<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
	</div>
	<div class="change_line">
		<div class="before_input"><?=GetMessage("REGISTER_CAPTCHA_PROMT")?>*:</div>
		<input class="input_line" type="text" name="captcha_word" maxlength="50" value="" />
	</div>
		
	<?
}
/* !CAPTCHA */
?>
	

	<span class="required_forms">*Обязательные поля для заполнения</span>
	<div class="clear"></div>
	<hr class="under_catalog">
	
	<div class="konfident">Нажимая на кнопку, вы даете согласие на обработку своих персональных данных и соглашаетесь с <a href="/about/politika-konfidentsialnosti/">политикой конфиденциальности</a></div>
	
	<div class="move_button">
		<?/*<a href="#" class="continue button_enter">
			ЗАРЕГИСТРИРОВАТЬСЯ
		</a>*/?>
		<input class="button_enter" type="submit" name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER")?>" />
	</div>
</form>

<script type="text/javascript">
	$(function () {
		$(document).on('submit', 'form.regform', function (e) {
			e.preventDefault();
			$(this).closest('form').bitrixAjax("<?=$arResult['AJAX_ID']?>", function (data) {
				$('div.ajax_wrap_reg').html(data.find('div.ajax_wrap_reg').html());
				
			},{
				post:{
					register_submit_button: 'Y',
					Register: 'Y'
				}
			});
		});
	});
</script>

<?endif?>
</div>
</div>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$bDefaultColumns = $arResult["GRID"]["DEFAULT_COLUMNS"];
$colspan = ($bDefaultColumns) ? count($arResult["GRID"]["HEADERS"]) : count($arResult["GRID"]["HEADERS"]) - 1;
$bPropsColumn = false;
$bUseDiscount = false;
$bPriceType = false;
$bShowNameWithPicture = ($bDefaultColumns) ? true : false; // flat to show name and picture column in one column
?>
<div class="bx_ordercart">
	<div class="bx_ordercart_order_pay">
		<div class="itog"><?=GetMessage("SOA_TEMPL_SUM_IT")?></div>
		<div class="total price"><?=$arResult["ORDER_TOTAL_PRICE_FORMATED"]?></div>
		
		<div style="clear:both;"></div>

	</div>
</div>

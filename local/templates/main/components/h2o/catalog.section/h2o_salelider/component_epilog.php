<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $templateData */
/** @var @global CMain $APPLICATION */
global $APPLICATION;
if (isset($templateData['TEMPLATE_THEME']))
{
	$APPLICATION->SetAdditionalCSS($templateData['TEMPLATE_THEME']);
}

//$APPLICATION->AddHeadScript (SITE_TEMPLATE_PATH."/js/jquery.tinycarousel.min.js");
//$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/style/tinycarousel.css");

$APPLICATION->AddHeadScript (SITE_TEMPLATE_PATH."/js/jquery.jcarousel.js");
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/style/jcarousel.basic.css");

CJSCore::Init(array("popup"));
?>
<script type="text/javascript">
<?
	CModule::IncludeModule('sale');
	$list = array();
	$dbBasketItems = CSaleBasket::GetList(
		array(
			"NAME" => "ASC",
			"ID" => "ASC"
		),
		array(
			"FUSER_ID" => CSaleBasket::GetBasketUserID(),
			"LID" => SITE_ID,
			"ORDER_ID" => "NULL"
		),
		false,
		false,
		array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "PRODUCT_PROVIDER_CLASS")
	);
	while ($arItems = $dbBasketItems->Fetch()) {
		$list[] = $arItems['PRODUCT_ID'];
	}
	//print_r($arResult['ITEMS_ID']);
	foreach ($arResult['ITEMS_ID'] as $id) {
		if (in_array($id, $list)):?>
			$('form[id="<?=$id;?>"]').find('button[type="submit"]').addClass('already_in_cart').html('Уже в корзине');
		<?endif;
	}
?>
</script>

<?
$text = "";
foreach ($arResult["PATH"] as $arSection)
{
	$text = $arSection["DESCRIPTION"];
}
$url = $APPLICATION->GetCurPage();;
$urlRes = explode("/", $url);
global $arSeoInfo;
if (!empty($arSeoInfo["TEXT"]))
{
	$text = $arSeoInfo["TEXT"];
}
//if($urlRes[2] == "cat" && !empty($text))
if ($arParams["DISPLAY_BOTTOM_PAGER"] && (int)$arResult['NAV_RESULT']->NavPageNomer <= 1)
{
	?>
<div class="right_block_dscrp" >
	<p><?=$text;?></p>
</div>
<?}?>
<?if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();?>
<div class="mfeed">
	<?$auto_focus = 0;
	$FL_req = 0;
	if(!empty($arResult["ERROR_MESSAGE"])) {
		foreach($arResult["ERROR_MESSAGE"] as $v)
			ShowError(implode("<br />", $v));
		$auto_focus = 1;}?>
	<div class="change_pass_forms">
		<?if(strlen($arResult["OK_MESSAGE"]) > 0):?>
			<div class="new remod_h1">
				<h2>ЗАКАЗАТЬ ОБРАТНЫЙ ЗВОНОК</h2>
				<div class="clear"></div>
				<hr class="under_menu hr_margin">
				<p><?=$arResult["OK_MESSAGE"]?></p>
			</div>
			<!--<script type="text/javascript">
				window.location.hash = '#?';
				location.reload(true);
			</script>-->
			<script type="text/javascript">
                    ga('send', 'event', 'FEEDBACK', 'FEEDBACK', 'submit', {
                        'hitCallback': function() {
                        console.log('ГОТОВО!');}
                      });
			</script>

		<?else:
			if(!empty($arResult["ERROR_MESSAGE"])) {
				foreach($arResult["ERROR_MESSAGE"] as $v)
					ShowError($v);
				$auto_focus = 1;}?>
			<div class="new remod_h1">
				<h2>Оставьте Ваш телефон и менеджер Вам перезвонит.</h2>
			</div>
			<div class="clear"></div>
			<hr class="under_menu hr_margin">
			<form method="POST" class="feedback" enctype="multipart/form-data" onsubmit="yaCounter28820320.reachGoal('QZVONOKS');">
				<?=bitrix_sessid_post()?>
				<?$cp=0;?>
				<input style="display:none;" name="CAPCHA">
				<?foreach($arParams["NEW_EXT_FIELDS"] as $i => $ext_field):?>  
					<?
					if ($i == "iu_4")
					{
						?><input type="hidden" name="custom[<?=$i?>]" value="http://mera-pt.ru<?=$APPLICATION->GetCurPage (false);?>" /><?
					}
					elseif(!($i == "iu_2" && $arParams["USE_IU_PAT"] == "Y"))
					{?>
						<div class="change_line">
							<div class="before_input" ><?=$ext_field[0]?><?if($ext_field[1]): if($FL_req==0)$FL_req=1;?><?=GetMessage("MFT_REQ")?><?endif?>:</div>
							<?if($ext_field[3]):?>
								<input type="hidden" value="<?=($arParams["MAX_SIZE_FILE"] * 1000)?>" name="MAX_FILE_SIZE">
								<input class="input_line" name="<?="file_".$i?>" type="file" size="37">
							<?else:?>
								<?if($ext_field[2]):?>
								<textarea class="input_line" name="custom[<?=$i?>]" rows="5" cols="40" style="resize: none;" <?if($cp==0 && $arResult["custom_$i"]=='' && $auto_focus): $cp=1;?>autofocus <?endif?>><?=$arResult["custom_$i"]?></textarea>
								<?else:?>
								<input type="text" name="custom[<?=$i?>]" value="<?=$arResult["custom_$i"]?>" class="input_line" <?if($cp==0 && $arResult["custom_$i"]=='' && $auto_focus): $cp=1;?>autofocus <?endif?>/>
								<?endif?>
							<?endif?>
						</div>
					<?
					}
					else
					{
						$FL_mes = $ext_field[1];
					}?>
				<?endforeach;?>
				<?if(isset($FL_mes)):?>
					<div class="change_line">
						<div class="before_input" style=" height: 100%;"><?=GetMessage("MFT_MESS")?><?if($FL_mes): if($FL_req==0)$FL_req=1;?><?=GetMessage("MFT_REQ")?><?endif?>:</div>
						<textarea class="input_line" name="custom[iu_2]" rows="5" cols="40" style="resize: none;" <?if($cp==0 && $auto_focus): $cp=1;?>autofocus <?endif?>><?=$arResult["custom_iu_2"]?></textarea>
					</div>
				<?endif?>
				<?if($arParams["USE_CAPTCHA"] == "Y"): if($FL_req==0)$FL_req=1;?>
					<div class="change_line">
						<input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
						<div class="before_input" style=" height: 100%;"><img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA"></div>
						<input class="input_line" type="text" name="captcha_word" size="30" maxlength="50" value="">
					</div>
				<?endif;?>
				<?if($arParams["COPY_LETTER"] == "Y" && isset($arParams['FIELD_FOR_EMAIL']) && $arParams['FIELD_FOR_EMAIL'] !== "iu_none"):?>
					<div><input type="checkbox" name="copy_letter" value="Y" /> <?=GetMessage("MFT_COPY_LETTER")?></div>
				<?endif;?>
				<span class="required_forms">*Обязательные поля для заполнения</span>
				<div class="clear"></div>
				<hr class="under_catalog">
				
				<div class="konfident">Нажимая на кнопку, вы даете согласие на обработку своих персональных данных и соглашаетесь с <a href="/about/politika-konfidentsialnosti/">политикой конфиденциальности</a></div>
				
				<div class="move_button">
					<input class="button_enter" type="submit" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>" onclick="yaCounter28820320.reachGoal('OBR_ZVON'); return true;"/>
				</div>
			</form>
		<?endif;?>
	</div>
</div>


<script type="text/javascript">
	$(function () {
		$(document).on('submit', 'form.feedback', function (e) {
			e.preventDefault();
			$(this).closest('form').bitrixAjax("<?=$arResult['AJAX_ID']?>", function (data) {
				$('div.mfeed').html(data.find('div.mfeed').html());
				$("[name = capcha]").val("");
				
			},{
				post:{
					submit: 'Y',
				}
			});
		});
	});
</script>
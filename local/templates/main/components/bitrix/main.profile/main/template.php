<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
?>
<?=ShowError($arResult["strProfileError"]);?>
<?
if ($arResult['DATA_SAVED'] == 'Y')
	echo ShowNote(GetMessage('PROFILE_DATA_SAVED'));
?>
<div class="bx_profile">
	<form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>?" enctype="multipart/form-data">
	<?=$arResult["BX_SESSION_CHECK"]?>
	<input type="hidden" name="lang" value="<?=LANG?>" />
	<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
	<input type="hidden" name="LOGIN" value=<?=$arResult["arUser"]["LOGIN"]?> />
		<h2><?=GetMessage("LEGEND_PROFILE")?></h2>
		<div class="change_line">
			<div class="before_input"><?=GetMessage('NAME')?>:</div>
			<input class="input_line" type="text" name="NAME" maxlength="50" value="<?=$arResult["arUser"]["NAME"]?>" />
		</div>
		<div class="change_line">
			<div class="before_input"><?=GetMessage('LAST_NAME')?>:</div>
			<input class="input_line" type="text" name="LAST_NAME" maxlength="50" value="<?=$arResult["arUser"]["LAST_NAME"]?>" />
		</div>
		<div class="change_line">
			<div class="before_input"><?=GetMessage('SECOND_NAME')?>:</div>
			<input class="input_line" type="text" name="SECOND_NAME" maxlength="50"  value="<?=$arResult["arUser"]["SECOND_NAME"]?>" />
		</div>
        <div class="change_line">
			<div class="before_input"><?=GetMessage('EMAIL')?>:</div>
            <input maxlength="50" class="input_line" type="text" name="EMAIL" value=<?=$arResult["arUser"]["EMAIL"]?> />
		</div>
        <div class="change_line">
			<div class="before_input"><?=GetMessage('PERSONAL_PHONE')?>:</div>
            <input maxlength="20" class="input_line" type="text" name="PERSONAL_PHONE" value=<?=$arResult["arUser"]["PERSONAL_PHONE"]?> />
		</div>
		<h2><?=GetMessage("MAIN_PSWD")?></h2>
		<div class="change_line">
			<div class="before_input"><?=GetMessage('NEW_PASSWORD_REQ')?>:</div>
			<input class="input_line" type="password" name="NEW_PASSWORD" maxlength="50" value="" autocomplete="off" />
		</div>
		<div class="change_line">
			<div class="before_input"><?=GetMessage('NEW_PASSWORD_CONFIRM')?>:</div>
			<input class="input_line" type="password" name="NEW_PASSWORD_CONFIRM" maxlength="50" value="" autocomplete="off" />
		</div>
		<input name="save" value="<?=GetMessage("MAIN_SAVE")?>" class="button_enter" type="submit">
	</form>
</div>
<br>
<?
/* if($arResult["SOCSERV_ENABLED"])
{
	$APPLICATION->IncludeComponent("bitrix:socserv.auth.split", ".default", array(
			"SHOW_PROFILES" => "Y",
			"ALLOW_DELETE" => "Y"
		),
		false
	);
} */
?>

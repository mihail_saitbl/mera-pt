<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?if(!empty($arResult["ITEMS"])){?>
	<div class="head"> 
		<h2 style="letter-spacing: 2px; border-bottom: 2px solid #eee;padding-bottom: 12px;margin-right: 19px;">Вместе с данным товаром часто покупают</h2>
	</div>
<?}?>
<div class=" content" style="margin-left: 0px;padding-right: 0px;width: auto;">
<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
	<?$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));?>
	<?$file=array();?>
<div class="goods" id="<?=$this->GetEditAreaId($arElement['ID']);?>">
	<pre><?//print_r($arElement)?></pre>
	<?if(is_array($arElement["PREVIEW_PICTURE"])):?>
		<?$file = CFile::ResizeImageGet($arElement["PREVIEW_PICTURE"]['ID'], array('width'=>213, 'height'=>140), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
		<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=$file['src']?>" alt=""></a>
	<?elseif(is_array($arElement["DETAIL_PICTURE"])):?>
		<?$file = CFile::ResizeImageGet($arElement["DETAIL_PICTURE"]['ID'], array('width'=>213, 'height'=>140), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
		<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=$file['src']?>" alt=""></a>
	<?endif?>
	<div class="description">
		<h4><?=$arElement["NAME"]?></h4>
		<div class="artikul"><?=$arElement["PROPERTIES"]['ARTNUMBER']['NAME'].": ".$arElement["PROPERTIES"]['ARTNUMBER']['VALUE']?></div>
	</div>
	<div class="price">
		<?foreach($arElement["PRICES"] as $code=>$arPrice):?>
			<?if($arPrice["CAN_ACCESS"]):?>
				<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
					<span class="cost"><?=$arPrice["PRINT_VALUE"]?></span> <span class="sale"><?=$arPrice["PRINT_DISCOUNT_VALUE"]?></span>
				<?else:?><span class="cost"><?=$arPrice["PRINT_VALUE"]?></span><span class="sale"><?=FormatCurrency(($arPrice["VALUE"]*0.9), $arPrice['CURRENCY'])?></span><?endif;?>
	
			<?endif;?>
		<?endforeach;?>
				<noindex>
				<a href="<?echo $arElement['COMPARE_URL']?>" rel="nofollow"><img src="<?=SITE_TEMPLATE_PATH?>/images/maincontent/grafic.png" alt=""></a>
				</noindex>
		
	</div>
	<div class="tocart">
			<?if($arElement["CAN_BUY"]):?>
				<?if($arParams["USE_PRODUCT_QUANTITY"] || count($arElement["PRODUCT_PROPERTIES"])):?>
					<form id="<?=$arElement['ID']?>" class="<?=$arParams['AJAX_ID']?>" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
					<input type="hidden" name="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" value="1" size="5">
					<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
					<input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?echo $arElement["ID"]?>">
					<button type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("CATALOG_ADD")?>"><img src="<?=SITE_TEMPLATE_PATH?>/images/maincontent/cart.png" alt="" ><?echo GetMessage("CATALOG_ADD")?></button>
					<!--input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("CATALOG_ADD")?>"-->
					</form>
				<?else:?>
					<noindex>
						<a href="<?echo $arElement['ADD_URL']?>" rel="nofollow"><img src="<?=SITE_TEMPLATE_PATH?>/images/maincontent/cart.png" alt="" ><?echo GetMessage("CATALOG_ADD")?></a>
					</noindex>
				<?endif?>
			<?elseif((count($arResult["PRICES"]) > 0) || is_array($arElement["PRICE_MATRIX"])):?>
				<?=GetMessage("CATALOG_NOT_AVAILABLE")?>
				<?$APPLICATION->IncludeComponent("bitrix:sale.notice.product", ".default", array(
							"NOTIFY_ID" => $arElement['ID'],
							"NOTIFY_URL" => htmlspecialcharsback($arElement["SUBSCRIBE_URL"]),
							"NOTIFY_USE_CAPTHA" => "N"
							),
							$component
						);?>
			<?endif?>
			&nbsp;
	</div>
</div>
		<?endforeach; // foreach($arResult["ITEMS"] as $arElement):?>
</div>
<div style="clear: both;"></div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
<div style=" border-bottom: 1px solid #eee;"></div>
<div class="pagination">
	<?=$arResult["NAV_STRING"]?>
</div>
<?endif;?>
<div style="clear: both;"></div>
<script type="text/javascript">
	$(document).on('submit', 'div.tocart form.<?=$arParams['AJAX_ID']?>', function (e) {
		e.preventDefault();
		var id = $(this).attr('id');
		$(this).bitrixAjax(
			"<?=$arParams['AJAX_ID']?>",
			function (data) {
				update_basket_line();
				$('div.tocart form#'+id).html(data.find('div.goods'+id).html());  
			},
			{
				post: {
					submit: 'Y'
				}
			}
		);
	});
</script>

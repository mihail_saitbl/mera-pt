<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CDatabase $DB */
 ?>
<?CModule::IncludeModule("sale");
$basket_discount = 1;
$ar = CSaleDiscount::GetByID(1);
$arar = unserialize($ar['ACTIONS']);
$basket_discount = 1 - $arar['CHILDREN'][0]['DATA']['Value']/100;
?>


<div class="head viewed_gods"> 
	<p><i class="fa fa-eye media_mobile" aria-hidden="true"></i>Недавно смотрели</p>  
</div> 


<div class="content wishlist-content div_for_owl_carousel" style="width:100%;">
 <? 

foreach ($arResult['ITEMS'] as $cell=>$arElement): 

if ($arParams["ADD_PRODUCT_NAME"] == "Y")
{
	$arElement["NAME"] .= " Bosch";
}

    $pri = 0;
    $price_bas = false;
    $price_disc = false;
    $price_id_1 = false;
    foreach($arElement["PRICES"] as $pri){
        if($pri["PRICE_ID"]==2)
            $price_bas = $pri;
        elseif($pri["PRICE_ID"]==3)   
            $price_disc = $pri;
        elseif ($pri["PRICE_ID"] == 1)
        {
        	$price_id_1 = $pri;
        }
    }
    if (!$price_bas && $price_id_1)
    {
    	$price_bas = $price_id_1;
    }
    ?>
    <?$multi = $arElement['PROPERTIES']['MULTI']['VALUE'];
    if(intval($multi)==0){
        $multi=1;
    }?>
	<?$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));?>
	<?$file=array();?>
	
	
<div class="goods" <?if(!empty($price_bas)) {?>itemscope="" itemtype="http://schema.org/Product"<?}?> id="<?=$this->GetEditAreaId($arElement['ID']);?>">
	<meta <?if(!empty($price_bas)) {?>itemprop="description"<?}?> <?if(!empty($price_bas)) {?>content="<?=$arElement["IPROPERTY_VALUES"]["ELEMENT_META_DESCRIPTION"]?>"<? }?>>
	<?if (!empty($arElement["DISCOUNT"]["ACTIVE_TO"])):?><div class="act_date_main"><p>до <?=$arElement["DISCOUNT"]["ACTIVE_TO"]?></p></div><?endif;?>
	<div id="success" class="success" style="display:none;">
		<div class="goods-<?=$arElement['ID']?>"><a class="close" onclick="closee()"></a>


			<?if($arElement["DETAIL_PICTURE"]):?>   
			<?$file = CFile::ResizeImageGet($arElement['DETAIL_PICTURE'], array('width'=>200, 'height'=>140), BX_RESIZE_IMAGE_PROPORTIONAL, true);
						?> 
			<a href="<?=$arElement['DETAIL_PAGE_URL']?>" class="list_preview_image">
			<?if($APPLICATION->GetCurPage() == "/catalog/osnastka/buri-koronki-sds-plus/bury-sds-plus-5x/") ?>
			<img <?if(!empty($price_bas)) {?>itemprop="image"<? }?> style="<?if($this->GetEditAreaId($arElement['ID']) == "bx_1970176138_131632") {?> height:102px; <?}?>display: block; margin: 0 auto;" 
			src="<?=$file['src']?>" alt=""></a>


			<?elseif($arElement["PREVIEW_PICTURE"]):?>    
			<?$file = CFile::ResizeImageGet($arElement['PREVIEW_PICTURE']['ID'], array('width'=>200, 'height'=>140), BX_RESIZE_IMAGE_PROPORTIONAL, true); 
					   ?> 
			<a href="<?=$arElement['DETAIL_PAGE_URL']?>" class="list_preview_image">
			<?if($APPLICATION->GetCurPage() == "/catalog/osnastka/buri-koronki-sds-plus/bury-sds-plus-5x/") ?>
			<img <?if(!empty($price_bas)) {?>itemprop="image"<? }?> style="<?if($this->GetEditAreaId($arElement['ID']) == "bx_1970176138_131632") {?> height:102px; <?}?>display: block; margin: 0 auto;" 
			src="<?=$file['src']?>" alt=""></a>


			<?else:?> 
				<a class="image_action" href="<?=$arElement["DETAIL_PAGE_URL"]?>"> <?if($APPLICATION->GetCurPage() == "/catalog/osnastka/buri-koronki-sds-plus/bury-sds-plus-5x/") ?><img <?if(!empty($price_bas)) {?>itemprop="image"<? }?> style="display: block; margin: 0 auto;" src="<?=SITE_TEMPLATE_PATH."/images/no_photo.jpg"?>" alt=""></a>
			<?endif?>

			<div class="description">
				<a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="pName"  style="color: inherit;"><p <?if(!empty($price_bas)) {?>itemprop="name"<?}?> ><?=$arElement["NAME"]?></p>
				<p class="add cart-test" style="display:none"><strong style="color:green;">Товар добавлен в корзину</strong></p>
				<p class="add favor" style="display:none"><strong style="color:green;">Товар добавлен в список</strong>
				<strong style="color:green;display:none">Для того, чтобы "запомнить" товар необходимо авторизоваться. Если у вас еще нет учетной записи, Вы можете зарегистрироваться прямо сейчас!"</strong></p>
				<p class="add compar" style="display:none"><strong style="color:green;">Товар добавлен в сравнение</strong></p></a>
				<!--<div class="artikul">Артикул: <?=$arElement["XML_ID"]?></div>-->
			</div> 
			<div class="compare-btn">
				<span class="buttons" onclick="closee()">Продолжить просмотр</span>
				<a class="cart-test" href="/personal/cart/" style="display:none">Перейти в корзину</a>
				<a class="favor" href="/favorites" style="display:none">Перейти в список</a>
				<a class="compar" href="/compare" style="display:none">Перейти к сравнению</a> 
				<a class="regno" style="display:none" href="/login">Зарегистрироваться/вход</a>
			</div> 
		</div>
	</div>
	<!---------------------------->

    <?if($arElement["DETAIL_PICTURE"]):?>   
	<?$file = CFile::ResizeImageGet($arElement['DETAIL_PICTURE'], array('width'=>200, 'height'=>140), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                ?> 
    <a href="<?=$arElement['DETAIL_PAGE_URL']?>" class="list_preview_image">
    <?if($APPLICATION->GetCurPage() == "/catalog/osnastka/buri-koronki-sds-plus/bury-sds-plus-5x/") ?>
	<img <?if(!empty($price_bas)) {?>itemprop="image"<? }?> style="<?if($this->GetEditAreaId($arElement['ID']) == "bx_1970176138_131632") {?> height:102px; <?}?>display: block; margin: 0 auto;" 
    src="<?=$file['src']?>" alt=""></a>


	<?elseif($arElement["PREVIEW_PICTURE"]):?>    
	<?$file = CFile::ResizeImageGet($arElement['PREVIEW_PICTURE']['ID'], array('width'=>200, 'height'=>140), BX_RESIZE_IMAGE_PROPORTIONAL, true); 
               ?> 
    <a href="<?=$arElement['DETAIL_PAGE_URL']?>" class="list_preview_image">
    <?if($APPLICATION->GetCurPage() == "/catalog/osnastka/buri-koronki-sds-plus/bury-sds-plus-5x/") ?>
	<img <?if(!empty($price_bas)) {?>itemprop="image"<? }?> style="<?if($this->GetEditAreaId($arElement['ID']) == "bx_1970176138_131632") {?> height:102px; <?}?>display: block; margin: 0 auto;" 
    src="<?=$file['src']?>" alt=""></a>


    <?else:?> 
		<a class="image_action" href="<?=$arElement["DETAIL_PAGE_URL"]?>"> <?if($APPLICATION->GetCurPage() == "/catalog/osnastka/buri-koronki-sds-plus/bury-sds-plus-5x/") ?><img <?if(!empty($price_bas)) {?>itemprop="image"<? }?> style="display: block; margin: 0 auto;" src="<?=SITE_TEMPLATE_PATH."/images/no_photo.jpg"?>" alt=""></a>
	<?endif?>

	<div class="description">
		<a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="pName"  style="color: inherit;"><p <?if(!empty($price_bas)) {?>itemprop="name"<?}?> ><?=$arElement["NAME"]?></p>
		</a> 
	</div><div class="artikul">Артикул: <?=$arElement["PROPERTIES"]["ARTNUMBER"]["VALUE"]?></div>
<!---------------------------->
	<div class="wishlist">
		<div>
		<?
		$iblockid = $arElement['IBLOCK_ID'];
		$id=$arElement['ID'];
		if(isset($_SESSION["CATALOG_COMPARE_LIST"][$iblockid]["ITEMS"][$id]))
		{
		$checked='checked';
		}
		else
		{
		$checked='';
		}
		?>
		<div class="compare">
		<input <?=$checked;?> type="checkbox" id="compareid_<?=$arElement['ID'];?>" onchange="compare_tov(<?=$arElement['ID'];?>);"> 
		<label for="compareid_<?=$arElement['ID'];?>"><i class="fa fa-circle-o media_mobile" aria-hidden="true"></i><i class="fa fa-check-circle-o media_mobile" aria-hidden="true"></i> сравнить</label></div>
		<div class="media_mobile product_availability"><i class="fa fa-check" aria-hidden="true"></i> <span>В наличии</span></div>
		<div class="media_no_mobile">Наличие: <span>есть</span></div>
		</div>
		<div>
		
		
			<?/*if($USER->isAuthorized()):*/?>
		<div class="favorites_container media_no_mobile">
			<p>Запомнить <br />товар: <!--favorites-<?=$arElement['ID']?>--></p>
			<a class="favorites inline_block" onclick="favorites(this)" data-element_id="<?=$arElement['ID']?>">
				<div class="added" style="<?=(!$arElement['IN_FAVORITES']) ? 'display:none' : ''?>" title="В избранном"></div>
				<div class="add" style="<?=($arElement['IN_FAVORITES']) ? 'display:none' : ''?>" title="Добавить в избранное"></div>
				<div class="loading"></div>
			</a>
			<div> 
				<? 
				foreach($arResult["COMMENTS"] as $res) {
					if($arElement["NAME"] == $res["TITLE"]) { $k = $res["NUM_COMMENTS"]; }
				}
				?> 
				<p>Отзывы: <? if(!empty($k)) {echo $k;}else{echo '0';} unset($k);?></p>
				<? $votesValue = round($arElement["PROPERTIES"]["vote_sum"]["VALUE"]/$arElement["PROPERTIES"]["vote_count"]["VALUE"], 0);  ?>
				<?if(!empty($arElement["PROPERTIES"]["vote_count"]["VALUE"])) { ?>
				<?
				if($votesValue == 1) echo '<span class="vote"><span class="active"></span><span class="not"></span><span class="not"></span><span class="not"></span><span class="not"></span></span>';
				if($votesValue == 2) echo '<span class="vote"><span class="active"></span><span class="active"></span><span class="not"></span><span class="not"></span><span class="not"></span></span>';
				if($votesValue == 3) echo '<span class="vote"><span class="active"></span><span class="active"></span><span class="active"></span><span class="not"></span><span class="not"></span></span>';
				if($votesValue == 4) echo '<span class="vote"><span class="active"></span><span class="active"></span><span class="active"></span><span class="active"></span><span class="not"></span></span>';
				if($votesValue == 5) echo '<span class="vote"><span class="active"></span><span class="active"></span><span class="active"></span><span class="active"></span><span class="active"></span></span>';
				}else{?>
				<span class="vote"><span class="not"></span><span class="not"></span><span class="not"></span><span class="not"></span><span class="not"></span></span>
				<?}?> 
			</div>
		</div>
		
		<div class="favorites_container media_mobile">    
		<a class="favorites inline_block" onclick="favorites(this)" data-element_id="<?=$arElement['ID']?>">
		 <div class="added" style="<?=(!$arElement['IN_FAVORITES']) ? 'display:none' : ''?>" title="В избранном"><i class="fa fa-heart" aria-hidden="true"></i></div>
		 <div class="add" style="<?=($arElement['IN_FAVORITES']) ? 'display:none' : ''?>" title="Добавить в избранное"><i class="fa fa-heart" aria-hidden="true"></i></div>
		 <span>В избранное</span>
		</a>
		<div class="votes_and_comments_mobile"> 
			<i class="fa fa-star" aria-hidden="true"></i>
			<?if(!empty($arElement["PROPERTIES"]["vote_count"]["VALUE"])) { ?>
			<?
			  if($votesValue == 1) echo '<span>(1.0)</span>';
			  if($votesValue == 2) echo '<span>(2.0)</span>';
			  if($votesValue == 3) echo '<span>(3.0)</span>';
			  if($votesValue == 4) echo '<span>(4.0)</span>';
			  if($votesValue == 5) echo '<span>(5.0)</span>';
			 }else{?>
			 <span>(0.0)</span>
			<?}?>
		
		 <? foreach($arResult["COMMENTS"] as $res) {if($arElement["NAME"] == $res["TITLE"]) { $k = $res["NUM_COMMENTS"]; }} ?> 
		 <p><a style="text-decoration:none;" href="<?=$arElement["DETAIL_PAGE_URL"]?>?reviews=true">Отзывы: <? if(!empty($k)) {echo $k;}else{echo '0';} unset($k);?></a></p>
		 <? $votesValue = round($arElement["PROPERTIES"]["vote_sum"]["VALUE"]/$arElement["PROPERTIES"]["vote_count"]["VALUE"], 0);?>
		 
		</div>
	   </div>
		
			<?/*endif;*/?>		
			<div></div>
		</div>
	</div> 
	<?/*------------------------------?>
                   ЦЕНА
	<?------------------------------*/?>
	<div class="price" <?if(!empty($price_bas)) {?> itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" <?}?> >
			<meta <?if(!empty($price_bas)) {?>itemprop="priceCurrency" content="RUB" <?}?>>
		<?
		if(!empty($arElement["OFFERS"])):?>
			<?if($arElement["MIN_PRICE"]):?>

			<div class="wrap-pr">
                           <p class="name_price" style="width:auto;color: #555555;">Цена от <span class="sale " style="position:relative; left:10px;">
                           	<?$res = FormatCurrency(ceil($arElement["MIN_PRICE"]/**$basket_discount*/), $arElement['MIN_CURR'])?> 
                           	<?=str_replace("р.", "руб.", $res)?></span></p>
                           <p class="new_sale"></p>
                           <p class="grey-price"> </p>

             </div>	
             <meta itemprop="price" content="<?=trim(str_replace("р.", "", $res))?>?>">

			<?endif;?>
		<?else:?>
			<?/*foreach($arElement["PRICES"] as $code=>$arPrice):*/?>
				<?if($price_bas["CAN_ACCESS"]):?>
					<?if($price_bas["DISCOUNT_VALUE"] < $price_bas["VALUE"]):?>

						
						
						<meta itemprop="price" content="<?=trim(str_replace("р.","", $price_bas["PRINT_VALUE"]))?>">

						<div class="wrap-pr">
                            	<p class="name_price">Цена</p>
                            	<div class="new_sale">
                            		<p class="sale"><?=str_replace("р.","руб.", $price_bas["PRINT_DISCOUNT_VALUE"])?></p>
                            		 <p class="cost new_cost" style="text-decoration: line-through;"><?=str_replace("р.","руб.", $price_bas["PRINT_VALUE"])?></p>
                            	</div>
                            	 <p class="grey-price">  </p>
                            </div>
                        
					<?else:?>
                        <?if($price_disc["VALUE"] > 0){?> 
                        	<meta itemprop="price" content="<?=trim(str_replace("р.","", $price_bas["PRINT_VALUE"]))?>">
             
                           <div class="wrap-pr">
                            	<p class="name_price">Цена</p>
                            	<div class="new_sale">
                            		<p class="sale "> <?=str_replace("р.","руб.", $price_disc["PRINT_VALUE"])?></p>
                            		<p class="cost new_cost"><?=str_replace("р.","руб.", $price_bas["PRINT_VALUE"])?></p>
                            	</div>
                            	 <p class="grey-price">  </p>
                            </div>

                        <?}else{?>  
                        	<?$cur = $price_bas["VALUE"]*$basket_discount;?>
                        	<meta itemprop="price" content="<?=trim(str_replace("р.","", $price_bas["PRINT_VALUE"]))?>">
                        	<div class="wrap-pr">
                            	<p class="name_price">Цена</p>
                            	<div class="new_sale">
                            		<p class="sale "> <?=str_replace("р.","руб.", FormatCurrency($cur, $price_bas['CURRENCY']))?></p>
                            		 <p class="cost new_cost"><?=str_replace("р.","руб.", $price_bas["PRINT_VALUE"])?></p> 
                            	</div>
                            	<p class="grey-price"></p>
                            </div>	

                            
                        <?}?>
                    <?endif;?>
				<?else:?>
					<p></p>
				<?endif;?>
			<?//endforeach;?>
			<?if(empty($price_bas)):?>

                <div class="wrap-pr">
                    <p class="name_price"></p>
	                    <div class="new_sale">
	                      	<p class="sale "></p>
	                      	<p class="cost new_cost" style="position:relative;top: 20px;">Спецзаказ </p> 
	                    </div>
                    <p class="grey-price"></p>
                </div>	
                             
			<?endif;?>
		<?endif;?>
		<?if($arParams["DISPLAY_COMPARE"]):?>
				<noindex>
				<a ><!--href="<?echo $arElement['COMPARE_URL']?>" rel="nofollow">--><img src="<?=SITE_TEMPLATE_PATH?>/images/maincontent/grafic.png" alt=""></a>
				</noindex>
		<?endif?>
		
	</div>
	<div class="tocart">

		<?
		$url = $APPLICATION->GetCurPage();

		$goal = 'V_KORZINU';
		if ($arParams["PLACE"] == "DETAIL")
		{
			$goal = 'VK_RECOMMEND';
		}
		?>
		<?/*if($arElement["CAN_BUY"] && (empty($arElement["OFFERS"]))):*/?>


		<?/*if(empty($arElement["OFFERS"])):*/?>
				<form class="<?=$arParams['AJAX_ID']?>" id="<?=$arElement['ID']?>" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" value="<?=$multi?>" size="5">
				<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
				<input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?echo $arElement["ID"]?>">
				<?if($arResult['BASKET_QUANTITY'][$arElement["ID"]]>0):?>

					<button type="submit" class="already_in_cart" onclick="yaCounter28820320.reachGoal('<?=$goal;?>');" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("IN_CART")?>"><?echo GetMessage("IN_CART")?></button>
				<?else:?>
					<button type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" onclick="yaCounter28820320.reachGoal('<?=$goal;?>');" value="<?echo GetMessage("CATALOG_ADD")?>"><?//echo GetMessage("CATALOG_ADD")?>Купить</button>
				<?endif;?>
				<!--input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("CATALOG_ADD")?>"-->
				</form>
		<?/*elseif((count($arResult["PRICES"]) > 0) || is_array($arElement["PRICE_MATRIX"])):*/?>
		<?/*else:?>
			<a href="<?=$arElement['DETAIL_PAGE_URL']?>" onclick="yaCounter28820320.reachGoal('VK_KART');" rel="nofollow"><?//echo GetMessage("CATALOG_ADD")?> Купить</a>
<?endif*/?>

		<?/*if($arElement["CAN_BUY"]):?>
			<?if($arParams["USE_PRODUCT_QUANTITY"] || count($arElement["PRODUCT_PROPERTIES"])):?>
				<form class="<?=$arParams['AJAX_ID']?>" id="<?=$arElement['ID']?>" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" value="<?=$multi?>" size="5">
				<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
				<input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?echo $arElement["ID"]?>">
				<?if($arResult['BASKET_QUANTITY'][$arElement["ID"]]>0):?>

					<button type="submit" class="already_in_cart" onclick="yaCounter28820320.reachGoal('<?=$goal;?>');" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("IN_CART")?>"><?echo GetMessage("IN_CART")?></button>
				<?else:?>
					<button type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" onclick="yaCounter28820320.reachGoal('<?=$goal;?>');" value="<?echo GetMessage("CATALOG_ADD")?>"><?//echo GetMessage("CATALOG_ADD")?>Купить</button>
				<?endif;?>
				<!--input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("CATALOG_ADD")?>"-->
				</form>
			<?else:?>
				<noindex>
					<a href="<?=$arElement['ADD_URL']?>" rel="nofollow"> <?//echo GetMessage("CATALOG_ADD")?> Купить</a>
				</noindex>
			<?endif?>
		<?elseif((count($arResult["PRICES"]) > 0) || is_array($arElement["PRICE_MATRIX"])):?>
			<a href="<?=$arElement['DETAIL_PAGE_URL']?>" onclick="yaCounter28820320.reachGoal('VK_KART');" rel="nofollow"><?//echo GetMessage("CATALOG_ADD")?> Купить</a>
		<?endif*/?>
		 
	</div>
	<?/*------------------------------?>
                 END ЦЕНА
	<?------------------------------*/?>
</div>   

<?endforeach;?>

	</div> 

<script type="text/javascript">
		$(document).on('submit', 'div.tocart form.<?=$arParams['AJAX_ID']?>', function (e) {
			e.preventDefault();
			var id = $(this).attr('id');

			if(!$('form[id="'+id+'"]').find('button[type="submit"]').hasClass('already_in_cart')) {  

            $("#mask").remove(); 
            $(".goods-"+id).first().parent().css("display", "block"); 
            $(".cart-test").css("display", "block");
			$("body").append("<div id='mask'></div>");     }

			$('form[id="'+id+'"]').find('button[type="submit"]').addClass('already_in_cart').html('Уже в корзине');
			$(this).bitrixAjax( 
				"<?=$arParams['AJAX_ID']?>",
				function (data) {
					update_basket_line();
					//$('div.tocart form#'+id).html(data.find('div.goods'+id).html());  
				},
				{
					post: {
						submit: 'Y'
					}
				}
			);

		});
</script>
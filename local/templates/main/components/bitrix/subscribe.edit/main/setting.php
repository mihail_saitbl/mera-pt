<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
//***********************************
//setting section
//***********************************
?>
<form action="<?=$arResult["FORM_ACTION"]?>" method="post">
	<?echo bitrix_sessid_post();?>
	<h2><?echo GetMessage("subscr_title_settings")?></h2>
	<div class="change_line">
		<div class="before_input media_no_mobile"><?echo GetMessage("subscr_email")?>*</div>
		<input type="text" class="input_line" name="EMAIL" value="<?=$arResult["SUBSCRIPTION"]["EMAIL"]!=""?$arResult["SUBSCRIPTION"]["EMAIL"]:$arResult["REQUEST"]["EMAIL"];?>" size="30" maxlength="255" placeholder="<?echo GetMessage("subscr_email")?>*"/></p>
	</div>	
	<div class="change_line">
		<div class="before_input"><?echo GetMessage("subscr_rub")?>*</div>
		<?foreach($arResult["RUBRICS"] as $itemID => $itemValue):?>
			<input type="checkbox" name="RUB_ID[]" id="RUB_ID_INP" value="<?=$itemValue["ID"]?>"<?if($itemValue["CHECKED"]) echo " checked"?> />
			<label for="RUB_ID_INP"><?=$itemValue["NAME"]?></label><br />
		<?endforeach;?>
	</div>
	<div class="change_line">
		<div class="before_input"><?echo GetMessage("subscr_fmt")?></div>
		<input type="radio" name="FORMAT" id="FORMAT_INP_TEXT" value="text"<?if($arResult["SUBSCRIPTION"]["FORMAT"] == "text") echo " checked"?> />		
		<label for="FORMAT_INP_TEXT"><?echo GetMessage("subscr_text")?></label>
		<b>&nbsp;/&nbsp;</b>
		<input type="radio" name="FORMAT" id="FORMAT_INP_HTML" value="html"<?if($arResult["SUBSCRIPTION"]["FORMAT"] == "html") echo " checked"?> />
		<label for="FORMAT_INP_HTML">HTML</label>
	</div>
	<input class="button_enter" type="submit" name="Save" value="<?echo ($arResult["ID"] > 0? GetMessage("subscr_upd"):GetMessage("subscr_add"))?>" />
	<input class="button_enter" type="reset" value="<?echo GetMessage("subscr_reset")?>" name="reset" />
<input type="hidden" name="PostAction" value="<?echo ($arResult["ID"]>0? "Update":"Add")?>" />
<input type="hidden" name="ID" value="<?echo $arResult["SUBSCRIPTION"]["ID"];?>" />
<?if($_REQUEST["register"] == "YES"):?>
	<input type="hidden" name="register" value="YES" />
<?endif;?>
<?if($_REQUEST["authorize"]=="YES"):?>
	<input type="hidden" name="authorize" value="YES" />
<?endif;?>
</form>
<p><?echo GetMessage("subscr_settings_note1")?></p>
<p><?echo GetMessage("subscr_settings_note2")?></p>
</br>

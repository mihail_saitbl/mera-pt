<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="bx-system-auth-form">
<?if($arResult["FORM_TYPE"] == "login"):?>
<img src="<?=SITE_TEMPLATE_PATH?>/images/logo/login.png" alt="">
<a href="" class="sigin" data-remodal-target="modal-auth" onclick="yaCounter28820320.reachGoal('VHOD');" >Вход</a> /
<a href="" class="checkin" data-remodal-target="modal-reg" onclick="yaCounter28820320.reachGoal('REG');" >Регистрация</a>


<div class="remodal" data-remodal-id="modal-auth">
	<div class="change_pass_forms">
		<div class="remod_h1">
			<p>ВХОД</p>
		</div>
		<hr class="under_menu">
		<? if(($USER->IsAuthorized())&&($_REQUEST['Login']=="Y")):?>
			<script type="text/javascript">
				//$(document).ready(function() {
					window.location.hash = '#?';
					location.reload(true);
				//});
			</script>
			<p>Вход выполнен, сейчас произойдет автоматическая перезагрузка страницы</p>
		<?endif;?>
		<form name="system_auth_form<?=$arResult["RND"]?>" id="form_auth" method="post" class="continue_<?=$arResult["RND"]?>" target="_top" action="<?=$arResult["AUTH_URL"]?>">
			<?if(!empty($arResult['ERROR_MESSAGE'])):?>
				<div class="form-row">
					<div class="form-cell-3"> &nbsp; </div>
					<div class="form-cell-9">
						<span style="color: red;"><?=strip_tags($arResult['ERROR_MESSAGE']['MESSAGE']);?></span>
					</div>
				</div>
			<?endif;?>
			<?if($arResult["BACKURL"] <> ''):?>
				<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
			<?endif?>
			<?foreach ($arResult["POST"] as $key => $value):?>
				<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
			<?endforeach?>
			<input type="hidden" name="AUTH_FORM" value="Y" />
			<input type="hidden" name="TYPE" value="AUTH" />
			<input type="hidden" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>" />
			<div class="change_line">
				<div class="before_input">Почта:</div>
				<input class="input_line" id='txtEmail' type="text" name="USER_LOGIN" maxlength="50" value="<?=$arResult["USER_LOGIN"]?>" size="17" />
			</div>
			<div class="change_line">
				<div class="before_input">Пароль:</div>
				<input class="input_line" type="password" name="USER_PASSWORD" maxlength="50" size="17" />
			</div>
		
			<ul class="remember_check">
				<li>
					<input type="checkbox" id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y" />Запомнить меня
				</li>
			</ul>
			<a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" class="forgot_pass" <?/*data-remodal-target="modal-forgot"*/?>>Забыли пароль?</a>
			<div class="clear"></div>
			<hr class="under_catalog">
		</form>
		<div class="move_button">
			<a href="#" class="continue" id="continue_<?=$arResult["RND"]?>">
				ВОЙТИ
			</a>
		</div>
	</div>
</div>
<?elseif($arResult["FORM_TYPE"] == "otp"):?>
	<div class="change_pass_forms" style="display: none;">
		<? if(($USER->IsAuthorized())&&($_REQUEST['Login']=="Y")):?>
			<script type="text/javascript">
				//$(document).ready(function() {
					window.location.hash = '#?';
					location.reload(true);
				//});
			</script>
			<div class="remod_h1">
				<h1>ВХОД</h1>
			</div>
			<hr class="under_menu">
			<p>Вход выполнен, сейчас произойдет автоматическая перезагрузка страницы</p>
		<?endif;?>
	</div>
<form name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
	<?if($arResult["BACKURL"] <> ''):?>
		<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
	<?endif?>
	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="OTP" />
	<table width="95%">
		<tr>
			<td colspan="2">
			<?echo GetMessage("auth_form_comp_otp")?><br />
			<input type="text" name="USER_OTP" maxlength="50" value="" size="17" autocomplete="off" /></td>
		</tr>
		<?if ($arResult["CAPTCHA_CODE"]):?>
			<tr>
				<td colspan="2">
				<?echo GetMessage("AUTH_CAPTCHA_PROMT")?>:<br />
				<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
				<img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /><br /><br />
				<input type="text" name="captcha_word" maxlength="50" value="" /></td>
			</tr>
		<?endif?>
		<?if ($arResult["REMEMBER_OTP"] == "Y"):?>
			<tr>
				<td valign="top"><input type="checkbox" id="OTP_REMEMBER_frm" name="OTP_REMEMBER" value="Y" /></td>
				<td width="100%"><label for="OTP_REMEMBER_frm" title="<?echo GetMessage("auth_form_comp_otp_remember_title")?>"><?echo GetMessage("auth_form_comp_otp_remember")?></label></td>
			</tr>
		<?endif?>
		<tr>
			<td colspan="2"><input type="submit" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>" /></td>
		</tr>
		<tr>
			<td colspan="2"><noindex><a href="<?=$arResult["AUTH_LOGIN_URL"]?>" rel="nofollow"><?echo GetMessage("auth_form_comp_auth")?></a></noindex><br /></td>
		</tr>
	</table>
</form>
<?else:?>
	<div>
		<div class="change_pass_forms" style="display: none;">
			<? if(($USER->IsAuthorized())&&($_REQUEST['Login']=="Y")):?>
				<script type="text/javascript">
					//$(document).ready(function() {
						window.location.hash = '#?';
						location.reload(true);
					//});
				</script>
				<div class="remod_h1">
					<h1>ВХОД</h1>
				</div>
				<hr class="under_menu">
				<p>Вход выполнен, сейчас произойдет автоматическая перезагрузка страницы</p>
			<?endif;?>
		</div>
		<form action="<?=$arResult["AUTH_URL"]?>">
			<input type="submit" name="logout_butt" value="" class="logout_btn" title="Выйти"/>
			<!-- <?=$arResult["USER_NAME"]?> -->
			<a href="/personal/"><?=$arResult["USER_LOGIN"]?></a>
			<!--a href="<?=$arResult["PROFILE_URL"]?>" title="<?=GetMessage("AUTH_PROFILE")?>"><?=GetMessage("AUTH_PROFILE")?></a><br /-->
			<?foreach ($arResult["GET"] as $key => $value):?>
				<input type="hidden" name="<?=$key?>" value="<?=$value?>"/>
			<?endforeach?>
			<input type="hidden" name="logout" value="yes" />			
		</form>
	</div>




<?endif?>
</div>
<script>
$(document).ready(function() {
	 $(document).on('click','.continue', function(e){
		e.preventDefault();
		var this_ID = $(this).attr("id");
		console.log(this_ID);
		/*$("form."+this_ID).bitrixAjax('<?=$arParams['AJAX_ID'];?>', function (data) {
			$(".change_pass_forms").html(data.find(".change_pass_forms").html());
		}, {
			post: {
				'Login': 'Y',
				submit: 'Y'
			}
		});
		*/
		$("#form_auth").submit();
		console.log("submit");
		
		//$("form[name='"+this_ID+"']").submit();
	});
	 $(document).on('submit',"#form_auth",function(e){
		e.preventDefault();
		$(this).bitrixAjax(
			"<?=$arParams['AJAX_ID']?>",
			function (data) {
				console.log(data);
				$("[data-remodal-id='modal-auth'] .change_pass_forms").html(data.find(".change_pass_forms").html());
			},
			{
				post: {
					submit: 'Y',
					'Login': 'Y'
				}
			}
		);
	})
	 
});
</script>

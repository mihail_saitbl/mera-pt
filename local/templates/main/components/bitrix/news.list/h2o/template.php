<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="newswrapper">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<h3><i class="fa fa-comment media_mobile" aria-hidden="true"></i>Новости</h3>
</div>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<article id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
		<?$date_arr = explode("-", $arItem["DISPLAY_ACTIVE_FROM"]);?>
		<?$mount = array ('01' => 'ЯНВ',
				'02' => 'ФЕВ',
				'03' => 'МАР',
				'04' => 'АПР',
				'05' => 'МАЯ',
				'06' => 'ИЮН',
				'07' => 'ИЮЛ',
				'08' => 'АВГ',
				'09' => 'СЕН',
				'10' => 'ОКТ',
				'11' => 'НОЯ',
				'12' => 'ДЕК'
		    )?>
		<div class="date">
			<div class="day"><?=$date_arr[0]?></div>
	     		<div class = 'mouth'><?=$mount[$date_arr[1]]?>'<?=$date_arr[2]?></div>
		</div>
		<?endif?>
		<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
			<div class="preview">
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?echo $arItem["PREVIEW_TEXT"];?></a>
			</div>
		<?endif;?>
	</article>
<?endforeach;?>
<div class="all_news_link_wrapper"><a href="/news/" class="all_news media_no_mobile">Читать все новости</a><a href="/news/" class="all_news media_mobile">Читать еще</a></div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

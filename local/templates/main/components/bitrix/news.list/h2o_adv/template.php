<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<h2><i class="fa fa-trophy media_mobile" aria-hidden="true"></i>Наши преимущества</h2>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<pre><?//print_r($arItem)?></pre>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	$strMainID = $this->GetEditAreaId($arItem['ID']);
        ?>
        <div class="diler" id="<? echo $strMainID; ?>">
            <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="">
            <div class="desc"><?=$arItem["NAME"]?></div>
            <div class="diler_hover">
                    <img src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>" alt="">
                    <div class="desc color_desk"><?=$arItem["NAME"]?></div>
            </div>
	</div>
	
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
<script>
    $(document).ready(function() {
	$('.diler').hover(function () {
			$(this).find('.diler_hover').stop(true, false).animate({
				opacity: 1
			});
		}, function () {
			$(this).find('.diler_hover').stop(true, false).animate({
				opacity: 0
			});
		});
});
</script>

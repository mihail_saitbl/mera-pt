<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="newswrapper">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<h3><i class="fa fa-graduation-cap media_mobile" aria-hidden="true"></i>Сертификаты</h3>
<div style="padding-bottom: 6px;"></div>
<?if(count($arResult["ITEMS"])%2 == 0){?>
    <?$i=0;}else{$i=1;}?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?$i++;
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<?


	?>
        <?if (!empty($arItem["DETAIL_PICTURE"])):?>
            <?$href = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"], array('width'=>600, 'height'=>850), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
        <?else:?>
            <?$href = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width'=>600, 'height'=>850), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
        <?endif;?>
		<div class="sertif_thumb_wrapper">
        <a <? if($i<3){?>style="display: none;"<?}?> data-fancybox-group="thumb" href="<?=$href["src"]?>" rel="gallery1" class="gallery1">
			<div class="sertif_thumb_img_wrapper"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt=""></div>
			<div class="desc media_mobile"><?=$arItem["PREVIEW_TEXT"]?></div>
        </a>
		</div>

<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
<script>
    $(document).ready(function () {
        $(".gallery1").fancybox({
	    "helpers":  {
		"thumbs" : {
		    width: 50,
		    height: 50,
		    position: 'top',
		}
	    }
	});
    });
</script>
<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
CModule::IncludeModule("sale");
$ar = CSaleDiscount::GetByID(1);
$arar = unserialize($ar['ACTIONS']);
$basket_discount = $arar['CHILDREN'][0]['DATA']['Value']/100;
$Cashless_AllSum = 0;
$Cashless_disc_AllSum = 0;

//error_reporting(E_ERROR);

$arItemIDs = Array();

foreach ($arResult["GRID"]["ROWS"] as $k => &$arItem):
    if ($arItem["CAN_BUY"] != "Y"){
        continue;
    }
    $arItemIDs[$arItem["PRODUCT_ID"]] = $k;

    $res = CIBlockElement::GetByID($arItem["PRODUCT_ID"]);
    if($ar_res = $res->GetNext())
        $IBlock_id=$ar_res['IBLOCK_ID'];
    $res = CIBlockElement::GetProperty($IBlock_id, $arItem["PRODUCT_ID"], "sort", "asc", array("CODE" => "ARTNUMBER"));
    while ($ob = $res->GetNext())
    {
        $arResult["GRID"]["ROWS"][$k]["PROPS"][$ob['CODE']] = $ob;
    }

    $arPrices = AGFindTwoPrices ($arItem["PRODUCT_ID"], $arItem["QUANTITY"]);
    $Cashless = $arPrices["PRICE"];
    $Cashless_disc = $arPrices["PRICE_DISC"];

    /*global $USER;
    if ($USER->IsAdmin())
    {
        echo '<pre>'; print_r($arPrices); echo "#".__LINE__."@".__FILE__; echo '</pre>';
    }*/

    /*if ($Cashless == $Cashless_disc && $arPrices["DISC_TYPE"] != "always")
    {
        $Cashless_disc = ($Cashless - $Cashless*$basket_discount);
    }*/

    if ($arPrices["DISC_TYPE"] == "always")
    {
        //echo $Cashless_disc;
        // всегда считаем по этой цене, безнал не влияет
        $arResult["GRID"]["ROWS"][$k]["PRICE_CASHLESS"] = $Cashless;
        $arResult["GRID"]["ROWS"][$k]["PRICE_CASHLESS_FORMATED"] = FormatCurrency($Cashless, $arItem["CURRENCY"]);
        $Cashless_AllSum = $Cashless_AllSum + $Cashless_disc;
        $arResult["GRID"]["ROWS"][$k]["PRICE_CASHLESS_DISC_TYPE"] = "always";
        $arResult["GRID"]["ROWS"][$k]["PRICE_CASHLESS_DISC"] = $Cashless_disc;
        $arResult["GRID"]["ROWS"][$k]["PRICE_CASHLESS_DISC_FORMATED"] = FormatCurrency($Cashless_disc, $arItem["CURRENCY"]);
        $Cashless_disc_AllSum = $Cashless_disc_AllSum + $Cashless_disc;
    }
    else
    {
        $arResult["GRID"]["ROWS"][$k]["PRICE_CASHLESS"] = $Cashless;
        $arResult["GRID"]["ROWS"][$k]["PRICE_CASHLESS_FORMATED"] = FormatCurrency($Cashless, $arItem["CURRENCY"]);
        $Cashless_AllSum = $Cashless_AllSum + $Cashless;
        $arResult["GRID"]["ROWS"][$k]["PRICE_CASHLESS_DISC"] = $Cashless_disc;
        $arResult["GRID"]["ROWS"][$k]["PRICE_CASHLESS_DISC_FORMATED"] = FormatCurrency($Cashless_disc, $arItem["CURRENCY"]);
        $Cashless_disc_AllSum = $Cashless_disc_AllSum + $Cashless_disc;
    }

    $pri = 0;
    /*$db_price = CPrice::GetList(array(),array("PRODUCT_ID" => $arItem['PRODUCT_ID'],));
	while ($arPrice = $db_price->Fetch()){
	   if($arPrice["CATALOG_GROUP_ID"]==3){
            $arPriceR = CCatalogProduct::GetOptimalPrice($arItem['PRODUCT_ID'], 1, $USER->GetUserGroupArray(), "N");
            if($arPriceR["RESULT_PRICE"]["BASE_PRICE"]>$arPriceR["RESULT_PRICE"]["DISCOUNT_PRICE"]){
               $arItem["PRICES_DISC"] = $arPriceR["RESULT_PRICE"]["DISCOUNT_PRICE"];
               $Cashless_disc = $arItem["PRICES_DISC"];
            }else{
               $arItem["PRICES_DISC"] = $arPriceR["RESULT_PRICE"]["BASE_PRICE"];
               $Cashless_disc =  $arPriceR["RESULT_PRICE"]["BASE_PRICE"];
            }
        }elseif($arPrice["CATALOG_GROUP_ID"]==2){
            $arItem["PRICES_BAS"] = $arPrice['PRICE'];
            $Cashless = $arPrice['PRICE'];
        }
    }

    $arResult["GRID"]["ROWS"][$k]["PRICE_CASHLESS"] = $Cashless;
    $arResult["GRID"]["ROWS"][$k]["PRICE_CASHLESS_FORMATED"] = FormatCurrency($Cashless, $arItem["CURRENCY"]);
    $Cashless_AllSum = $Cashless_AllSum + $Cashless;
    $arResult["GRID"]["ROWS"][$k]["PRICE_CASHLESS_DISC"] = $Cashless_disc;
    $arResult["GRID"]["ROWS"][$k]["PRICE_CASHLESS_DISC_FORMATED"] = FormatCurrency($Cashless_disc, $arItem["CURRENCY"]);
    $Cashless_disc_AllSum = $Cashless_disc_AllSum + $Cashless_disc;*/
endforeach;

unset ($arItem);
unset ($items);

$items = CIBlockElement::GetList (Array(), Array ("IBLOCK_ID" => 9, "ID" => array_keys($arItemIDs)), false, false, Array ("IBLOCK_ID", "ID", "NAME", "PROPERTY_CML2_LINK"));
while ($arItem = $items->Fetch())
{
    // $arItemIDs - ключ - это ИД из корзины, значение - индекс в резулте
    $arItemIDs2[$arItem["PROPERTY_CML2_LINK_VALUE"]][] = $arItem["ID"];
}
unset ($arItem);
unset ($items);
$items = CIBlockElement::GetList (Array(), Array ("IBLOCK_ID" => 5, "ID" => array_merge(array_keys($arItemIDs), array_keys($arItemIDs2))), false, false, Array ("IBLOCK_ID", "ID", "NAME", "DETAIL_PAGE_URL"));
while ($arItem = $items->GetNext (true, false))
{
    /*if (is_array($arResult["GRID"]["ROWS"][$arItemIDs[$arItem["ID"]]]))
    {
        $arResult["GRID"]["ROWS"][$arItemIDs[$arItem["ID"]]]["DETAIL_PAGE_URL"] = $arItem["DETAIL_PAGE_URL"];
    }
    else*/if (is_array($arItemIDs2[$arItem["ID"]]))
    {
        //$arResult["GRID"]["ROWS"][$arItemIDs[$arItemIDs2[$arItem["ID"]]]]["DETAIL_PAGE_URL"] = $arItem["DETAIL_PAGE_URL"];
        foreach ($arItemIDs2[$arItem["ID"]] as $id)
        {
            $arResult["GRID"]["ROWS"][$arItemIDs[$id]]["DETAIL_PAGE_URL"] = $arItem["DETAIL_PAGE_URL"];
        }
    }
}
unset ($arItem);
unset ($items);
//$Cashless_AllSum = intval($arResult["allSum"]*$ct);
$arResult["allSum_Cashless"] = $Cashless_AllSum;
$arResult["allSum_Cashless_Formated"] = FormatCurrency($Cashless_AllSum, "RUB");
$arResult["allSum_Cashless_disc"] = $Cashless_disc_AllSum;
$arResult["allSum_Cashless_disc_Formated"] = FormatCurrency($Cashless_disc_AllSum, "RUB");
?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?CModule::IncludeModule("sale");
$ar = CSaleDiscount::GetByID(1);
$arar = unserialize($ar['ACTIONS']);
$ct = 1 - $arar['CHILDREN'][0]['DATA']['Value']/100?>
<p id="spec">Спецпредложения</p>
<div class="slider_goods">
	<div class="slide-list">
		<div class="slide-wrap">
<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
    <?$pri = 0;
    $price_bas = array();
    $price_disc = array();
    foreach($arElement["PRICES"] as $pri){
        if($pri["PRICE_ID"]==2)
            $price_bas = $pri;
        elseif($pri["PRICE_ID"]==3)   
            $price_disc = $pri;
    }?>
	<?$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));?>
	<?$multi = $arElement['PROPERTIES']['MULTI']['VALUE'];
    if(intval($multi)==0){
        $multi=1;
    }?>
    <?$file=array();?>
<div class="slide-item">
	<div class="goods">
	<pre><?//print_r($arElement)?></pre>
	<?if(is_array($arElement["PREVIEW_PICTURE"])):?>
		<?$file = CFile::ResizeImageGet($arElement["PREVIEW_PICTURE"], array('width'=>213, 'height'=>140), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
		<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=$file['src']?>" alt=""></a>
	<?elseif(is_array($arElement["DETAIL_PICTURE"])):?>
		<?$file = CFile::ResizeImageGet($arElement["DETAIL_PICTURE"], array('width'=>213, 'height'=>140), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
		<a class="image_action" href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=$file['src']?>" alt=""></a>
	<?else:?>
		<a class="image_action" href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=SITE_TEMPLATE_PATH."/images/no_photo.jpg"?>" alt=""></a>
	<?endif?>
	<div class="description">
		<a href="<?=$arElement["DETAIL_PAGE_URL"]?>" style="color: inherit;text-decoration: none;"><p class="goods-txt"><?=$arElement["NAME"]?></p></a>
		<div class="artikul">Артикул: <?=$arElement["XML_ID"]?></div>
	</div>
	<div class="price">
		<?if(!empty($arElement["OFFERS"])):?>
			<?if($arElement["MIN_PRICE"]):?>
				<div class="wrap-pr">
                    <p class="name_price">Цена</p>
                    <div class="new_sale">
                        <p class="sale "><?=str_replace("р.","руб.", FormatCurrency(ceil($arElement["MIN_PRICE"]*$ct)), $arElement['MIN_CURR'])?></p>
                        <p class="cost new_cost"></p>
                    </div>
                    <p class="grey-price">  </p>
               </div>
			<?endif;?>
		<?else:?>
			<?//foreach($arElement["PRICES"] as $code=>$arPrice):?>
				<?if($price_bas["CAN_ACCESS"]):?>
					<?if($price_bas["DISCOUNT_VALUE"] < $price_bas["VALUE"]):?>
					<?/*цена в спец предложении слева*/?>
						<div class="wrap-pr">
                            	<p class="name_price">Цена</p>
                            	<div class="new_sale" style="width:auto; position:relative; top:7px;">
                            		<p class="sale "><?=str_replace("р.","руб.", $price_bas["PRINT_DISCOUNT_VALUE"])?></p>
                            		<p class="cost new_cost" style="text-decoration: line-through; margin:0;padding:0;text-align:right;"><?=str_replace("р.","руб.", $price_bas["PRINT_VALUE"])?></p>
                            	</div>
                            	 <p class="grey-price">  </p>
                            </div>



					<?else:?>
                        <?if($price_disc["VALUE"] > 0){?> 
                            <div class="wrap-pr">
                            	<p class="name_price">Цена</p>
                            	<div class="new_sale">
                            		<p class="sale "><?=str_replace("р.","руб.", $price_disc["PRINT_VALUE"])?></p>
                            		<p class="cost new_cost"><?=str_replace("р.","руб.", $price_bas["PRINT_VALUE"])?></p>
                            	</div>
                            	 <p class="grey-price"></p>
                            </div>
                        <?}else{?>
                            <div class="wrap-pr">
                            	<p class="name_price">Цена</p>
	                            <div class="new_sale">
	                            	<p class="sale "><?=str_replace("р.","руб.", FormatCurrency($cur, $price_bas['CURRENCY']))?><</p>
	                            	<p class="cost new_cost"><?=str_replace("р.","руб.", $price_bas["PRINT_VALUE"])?></p>
	                           	</div>
                            	 <p class="grey-price"></p>
                            </div>
                        <?}?>
                    <?endif;?>
				<?else:?>
					<p></p>
				<?endif;?>
			<?//endforeach;?>
			<?if(empty($price_bas)):?>
				<span class="cost">Спецзаказ</span>
			<?endif;?>
		<?endif;?>
		<?if($arParams["DISPLAY_COMPARE"]):?>
				<noindex>
				<a ><!--href="<?echo $arElement['COMPARE_URL']?>" rel="nofollow">--><img src="<?=SITE_TEMPLATE_PATH?>/images/maincontent/grafic.png" alt=""></a>
				</noindex>
		<?endif?>
		
	</div>
	<div class="tocart">
			<?if($arElement["CAN_BUY"] && (empty($arElement["OFFERS"]))):?>
				<?if($arParams["USE_PRODUCT_QUANTITY"] || count($arElement["PRODUCT_PROPERTIES"])):?>
					<form class="<?=$arParams['AJAX_ID']?>" id="<?=$arElement['ID']?>" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
					<input type="hidden" name="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" value="<?=$multi?>" size="5">
					<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
					<input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?echo $arElement["ID"]?>">
						<button type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?//echo GetMessage("CATALOG_ADD")?> Купить" onclick="yaCounter28820320.reachGoal('V_KORZINU');"><?//echo GetMessage("CATALOG_ADD")?>Купить</button>
						<!--input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?//echo GetMessage("CATALOG_ADD")?> Купить"-->
					</form>
				<?else:?>
					<noindex>
						<a href="<?echo $arElement['ADD_URL']?>" rel="nofollow"><?//echo GetMessage("CATALOG_ADD")?>Купить</a>
					</noindex>
				<?endif?>
			<?elseif((count($arResult["PRICES"]) > 0) || is_array($arElement["PRICE_MATRIX"])):?>
		<a href="<?=$arElement['DETAIL_PAGE_URL']?>" rel="nofollow"><?//echo GetMessage("CATALOG_ADD")?>Купить</a>
			<?endif?>
			&nbsp;
	</div>
	</div>
</div>
		<?endforeach; // foreach($arResult["ITEMS"] as $arElement):?>
		</div></div>
	<div name="prev" class="navy prev-slide"><</div>
	<div name="next" class="navy next-slide">></div>
</div>
<div style="clear: both;"></div>
<script type="text/javascript">
		$(document).on('submit', 'div.tocart form.<?=$arParams['AJAX_ID']?>', function (e) {
			e.preventDefault();
			var id = $(this).attr('id');
			$('form[id="'+id+'"]').find('button[type="submit"]').addClass('already_in_cart').html('Уже в корзине');
			$(this).bitrixAjax(
				"<?=$arParams['AJAX_ID']?>",
				function (data) {
					update_basket_line();
					$('div.tocart form#'+id).html(data.find('div.goods'+id).html());  
				},
				{
					post: {
						submit: 'Y'
					}
				}
			);
		});
</script>

<script>
	function htmSlider(){
    /* Зададим следующие параметры */
    /* обертка слайдера */
    var slideWrap = $('.slide-wrap');
    /* кнопки вперед/назад и старт/пауза */
    var nextLink = $('.next-slide');
    var prevLink = $('.prev-slide');
    var playLink = $('.auto');
    /* Проверка на анимацию */
    var is_animate = false;
    /* ширина слайда с отступами */
    var slideWidth = $('.slide-item').outerWidth();
    /* смещение слайдера */
    var scrollSlider = slideWrap.position().left - slideWidth;
   
    /* Клик по ссылке на следующий слайд */
    nextLink.click(function(){
     if(!slideWrap.is(':animated')) {
      slideWrap.animate({left: scrollSlider}, 500, function(){
       slideWrap
        .find('.slide-item:first')
        .appendTo(slideWrap)
        .parent()
        .css({'left': 0});
      });
     }
    });
   
    /* Клик по ссылке на предыдующий слайд */
    prevLink.click(function(){
     if(!slideWrap.is(':animated')) {
      slideWrap
       .css({'left': scrollSlider})
       .find('.slide-item:last')
       .prependTo(slideWrap)
       .parent()
       .animate({left: 0}, 500);
     }
    });
   
    /* Функция автоматической прокрутки слайдера */
    function autoplay(){
     if(!is_animate){
      is_animate = true;
      slideWrap.animate({left: scrollSlider}, 500, function(){
       slideWrap
        .find('.slide-item:first')
        .appendTo(slideWrap)
        .parent()
        .css({'left': 0});
       is_animate = false;
      });
     }
    }
   
    timer = setInterval(function(){
     slideWrap.animate({left: scrollSlider}, 500, function(){
      slideWrap
       .find('.slide-item:first')
       .appendTo(slideWrap)
       .parent()
       .css({'left': 0});
      });
    }, 114000);
 }
 htmSlider();	
</script>

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?CModule::IncludeModule("sale");
$ar = CSaleDiscount::GetByID(1);
$arar = unserialize($ar['ACTIONS']);
$basket_discount = $ct = 1 - $arar['CHILDREN'][0]['DATA']['Value']/100;
?>
<div class="spec_block_in_catalog_wrapper">
<p id="spec">Спецпредложения</p>
<div class="slider_goods">
	<div class="slide-list">
		<div class="slide-wrap">
<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
    <?$pri = 0;
    $price_bas = array();
    $price_disc = array();
    foreach($arElement["PRICES"] as $pri){
        if($pri["PRICE_ID"]==2)
            $price_bas = $pri;
        elseif($pri["PRICE_ID"]==3)   
            $price_disc = $pri;
    }?>
	<?$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));?>
	<?$multi = $arElement['PROPERTIES']['MULTI']['VALUE'];
    if(intval($multi)==0){
        $multi=1;
    }?>
    <?$file=array();?>
<div class="slide-item">
	<div class="goods">
		<div id="success" class="success" style="display:none;">
<div class="goods-<?=$arElement['ID']?>"><a class="close" onclick="closee()"></a>
	<?if(is_array($arElement["PREVIEW_PICTURE"])):?>
		<?$file = CFile::ResizeImageGet($arElement["PREVIEW_PICTURE"], array('width'=>213, 'height'=>140), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
		<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=$file['src']?>" alt=""></a>
	<?elseif(is_array($arElement["DETAIL_PICTURE"])):?>
		<?$file = CFile::ResizeImageGet($arElement["DETAIL_PICTURE"], array('width'=>213, 'height'=>140), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
		<a class="image_action" href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=$file['src']?>" alt=""></a>
	<?else:?>
		<a class="image_action" href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=SITE_TEMPLATE_PATH."/images/no_photo.jpg"?>" alt=""></a>
	<?endif?>
	<div class="description">
		<a href="<?=$arElement["DETAIL_PAGE_URL"]?>" style="color: inherit;text-decoration: none;"><p class="goods-txt"><?=$arElement["NAME"]?></p>
		<p class="add cart-test" style="display:none"><strong style="color:green;">Товар добавлен в корзину</strong></p>
		<p class="add favor" style="display:none"><strong style="color:green;">Товар добавлен в список</strong></p>
		<p class="add compar" style="display:none"><strong style="color:green;">Товар добавлен в сравнение</strong></p></a> 
	</div>	 
	<div class="compare-btn">
		<span class="buttons" onclick="closee()">Продолжить просмотр</span>
		<a class="cart-test" href="/personal/cart/" style="display:none">Перейти в корзину</a>
		<a class="favor" href="/favorites" style="display:none">Перейти в список</a>
		<a class="compar" href="/compare" style="display:none">Перейти к сравнению</a>  
    </div> 
    <?if(!empty($arElement["PROPERTIES"]["NEEDED"]["VALUE"])){?>
   <div class="necessary cart-test jcarousel_ sl_<?=$arElement["ID"];?>">
    <h3 style="font-size:16px;margin-bottom:10px;">Дополнительная комплектация к выбранному товару:</h3>
    <div class="jcarousel-wrapper">
     <ul class="bxslider" id="sl_tiny_<?=$arElement["ID"];?>" >
      <?foreach ($arElement["PROPERTIES"]["NEEDED"]["VALUE"] as $key=>$needed){?>
      <li class="product_necess">
       <a class="link" href="<?=$needed["DETAIL_PAGE_URL"]?>">
        <div class="image_div"><img src="<?=$needed["IMG_URL"];?>"></div>
        <div class="teod"><?=$needed["NAME"]?></div>
        <p class="article">Артикул: <?=$needed["ARTNUMBER"];?></p>
        <span class="grey_pr"><?=FormatCurrency($needed["PRICE"]['PRICE'], $needed["PRICE"]['CURRENCY'])?></span>
        <?$cur = $needed["PRICE"]['PRICE']*$basket_discount;?>
        <div class="red_small_pr"><?=FormatCurrency($cur, $needed["PRICE"]['CURRENCY'])?></div>
        <?if($needed["PRICE"]['PRICE']>0):?>
        <form id="<?=($needed["ID"]);?>" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
         <input type="hidden" name="<?=$arParams["ACTION_VARIABLE"]?>" value="BUY">
         <input type="hidden" name="<?=$arParams["PRODUCT_ID_VARIABLE"]?>" value="<?=($needed["ID"]);?>">
         <!--input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."BUY"?>" value="<?echo GetMessage("CATALOG_BUY")?>"-->
         <input type="hidden" name="<?=$arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?=GetMessage("CATALOG_ADD_TO_BASKET")?>">
        </form>
        <?endif;?>
       </a>
       <?if($needed["PRICE"]['PRICE']>0):?>
       <a class="addtocart needed_buy" onclick="yaCounter28820320.reachGoal('VK_KART');" href="#" ids="need" id="<?=($needed["ID"])?>" onclick="yaCounter28820320.reachGoal('VK_DOP_KART');">Купить</a>
       <?endif;?>
      </li>
      <?}?>
     </ul>     
    </div><!--end jcarousel-wrapper-->
   </div>
   <?}?>
</div>
</div>
<!---------------------------->
	<?if(is_array($arElement["PREVIEW_PICTURE"])):?>
		<?$file = CFile::ResizeImageGet($arElement["PREVIEW_PICTURE"], array('width'=>213, 'height'=>140), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
		<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=$file['src']?>" alt=""></a>
	<?elseif(is_array($arElement["DETAIL_PICTURE"])):?>
		<?$file = CFile::ResizeImageGet($arElement["DETAIL_PICTURE"], array('width'=>213, 'height'=>140), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
		<a class="image_action" href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=$file['src']?>" alt=""></a>
	<?else:?>
		<a class="image_action" href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=SITE_TEMPLATE_PATH."/images/no_photo.jpg"?>" alt=""></a>
	<?endif?>
	<div class="description">
		<a href="<?=$arElement["DETAIL_PAGE_URL"]?>" style="color: inherit;text-decoration: none;"><p class="goods-txt"><?=$arElement["NAME"]?></p></a>

	</div>	 <div class="artikul">Артикул: <?=$arElement["PROPERTIES"]["ARTNUMBER"]["VALUE"];?></div> 
<!---------------------------->
<div class="wishlist">
<div>
<?
$iblockid = $arElement['IBLOCK_ID'];
$id=$arElement['ID'];
if(isset($_SESSION["CATALOG_COMPARE_LIST"][$iblockid]["ITEMS"][$id]))
{
$checked='checked';
}
else
{
$checked='';
}
?>
<div class="compare">
<input <?=$checked;?> type="checkbox" id="compareid_<?=$arElement['ID'];?>" onchange="compare_tov(<?=$arElement['ID'];?>);"> 
<label for="compareid_<?=$arElement['ID'];?>">сравнить</label></div>
<div>Наличие: <span>есть</span></div>
</div>
<div>
	<?/*if($USER->isAuthorized()):*/?>
			<div class="favorites_container">
				<p>Запомнить <br />товар: <!--favorites-<?=$arElement['ID']?>--></p>
				<a class="favorites inline_block" onclick="favorites(this)" data-element_id="<?=$arElement['ID']?>">
					<div class="added" style="<?=(!$arElement['IN_FAVORITES']) ? 'display:none' : ''?>" title="В избранном"></div>
					<div class="add" style="<?=($arElement['IN_FAVORITES']) ? 'display:none' : ''?>" title="Добавить в избранное"></div>
					<div class="loading"></div>
				</a>
				<div>
<? 
	foreach($arResult["COMMENTS"] as $res) {

		if($arElement["NAME"] == $res["TITLE"]) { $k = $res["NUM_COMMENTS"]; }

	}
?> 
					<p>Отзывы: <? if(!empty($k)) {echo $k;}else{echo '0';} unset($k);?></p>
<? $votesValue = round($arElement["PROPERTIES"]["vote_sum"]["VALUE"]/$arElement["PROPERTIES"]["vote_count"]["VALUE"], 0);
  ?>
					<?if(!empty($arElement["PROPERTIES"]["vote_count"]["VALUE"])) { ?>
<?
if($votesValue == 1) echo '<span class="vote"><span class="active"></span><span class="not"></span><span class="not"></span><span class="not"></span><span class="not"></span></span>';
if($votesValue == 2) echo '<span class="vote"><span class="active"></span><span class="active"></span><span class="not"></span><span class="not"></span><span class="not"></span></span>';
if($votesValue == 3) echo '<span class="vote"><span class="active"></span><span class="active"></span><span class="active"></span><span class="not"></span><span class="not"></span></span>';
if($votesValue == 4) echo '<span class="vote"><span class="active"></span><span class="active"></span><span class="active"></span><span class="active"></span><span class="not"></span></span>';
if($votesValue == 5) echo '<span class="vote"><span class="active"></span><span class="active"></span><span class="active"></span><span class="active"></span><span class="active"></span></span>';

					}else{?>
<span class="vote"><span class="not"></span><span class="not"></span><span class="not"></span><span class="not"></span><span class="not"></span></span>
					<?}?>   </div>
			</div>
	<?/*endif;*/?>
	<div></div>
</div>
</div>
<!---------------------------->
	<div class="price">
		<?if(!empty($arElement["OFFERS"])):?>
			<?if($arElement["MIN_PRICE"]):?>
				<div class="wrap-pr">
                    <p class="name_price">Цена</p>
                    <div class="new_sale">
                        <p class="sale "><?=str_replace("р.","руб.", FormatCurrency(ceil($arElement["MIN_PRICE"]*$ct)), $arElement['MIN_CURR'])?></p>
                        <p class="cost new_cost"></p>
                    </div>
                    <p class="grey-price">  </p>
               </div>
			<?endif;?>
		<?else:?>
			<?//foreach($arElement["PRICES"] as $code=>$arPrice):?>
				<?if($price_bas["CAN_ACCESS"]):?>
					<?if($price_bas["DISCOUNT_VALUE"] < $price_bas["VALUE"]):?>
					<?/*цена в спец предложении слева*/?>
						<div class="wrap-pr">
                            	<p class="name_price">Цена</p>
                            	<div class="new_sale" style="width:auto; position:relative; top:7px;">
                            		<p class="sale "><?=str_replace("р.","руб.", $price_bas["PRINT_DISCOUNT_VALUE"])?></p>
                            		<p class="cost new_cost" style="text-decoration: line-through; margin:0;padding:0;text-align:right;"><?=str_replace("р.","руб.", $price_bas["PRINT_VALUE"])?></p>
                            	</div>
                            	 <p class="grey-price">  </p>
                            </div>



					<?else:?>
                        <?if($price_disc["VALUE"] > 0){?> 
                            <div class="wrap-pr">
                            	<p class="name_price">Цена</p>
                            	<div class="new_sale">
                            		<p class="sale "><?=str_replace("р.","руб.", $price_disc["PRINT_VALUE"])?></p>
                            		<p class="cost new_cost"><?=str_replace("р.","руб.", $price_bas["PRINT_VALUE"])?></p>
                            	</div>
                            	 <p class="grey-price"></p>
                            </div>
                        <?}else{?>
                            <div class="wrap-pr">
                            	<p class="name_price">Цена</p>
	                            <div class="new_sale">
	                            	<p class="sale "><?=str_replace("р.","руб.", FormatCurrency($cur, $price_bas['CURRENCY']))?><</p>
	                            	<p class="cost new_cost"><?=str_replace("р.","руб.", $price_bas["PRINT_VALUE"])?></p>
	                           	</div>
                            	 <p class="grey-price"></p>
                            </div>
                        <?}?>
                    <?endif;?>
				<?else:?>
					<p></p>
				<?endif;?>
			<?//endforeach;?>
			<?if(empty($price_bas)):?>
				<span class="cost">Спецзаказ</span>
			<?endif;?>
		<?endif;?>
		<?if($arParams["DISPLAY_COMPARE"]):?>
				<noindex>
				<a ><!--href="<?echo $arElement['COMPARE_URL']?>" rel="nofollow">--><img src="<?=SITE_TEMPLATE_PATH?>/images/maincontent/grafic.png" alt=""></a>
				</noindex>
		<?endif?>
		
	</div>
	<div class="tocart">
		<?if($arElement["CAN_BUY"] && (empty($arElement["OFFERS"]))):?>
			<?if($arParams["USE_PRODUCT_QUANTITY"] || count($arElement["PRODUCT_PROPERTIES"])):?>
				<form url="http://mera-pt.ru/<?=$arElement['DETAIL_PAGE_URL']?>" class="<?=$arParams['AJAX_ID']?>" id="<?=$arElement['ID']?>" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
					<input type="hidden" name="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" value="<?=$multi?>" size="5">
					<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
					<input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?echo $arElement["ID"]?>">
					<?if($arResult['BASKET_QUANTITY'][$arElement["ID"]]>0):?>

						<button type="submit" class="already_in_cart" onclick="yaCounter28820320.reachGoal('<?=$goal;?>');" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("IN_CART")?>"><?echo GetMessage("IN_CART")?></button>
					<?else:?>
						<button type="submit" class="not_cart" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" onclick="initSlider('#sl_tiny_<?=$arElement["ID"];?>'); yaCounter28820320.reachGoal('<?=$goal;?>');" value="<?echo GetMessage("CATALOG_ADD")?>"><?//echo GetMessage("CATALOG_ADD")?>Купить</button>
					<?endif;?>
					<!--input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("CATALOG_ADD")?>"-->
				</form>
			<?else:?>
				<noindex>
					<a class="addtocart" href="<?=$arElement['ADD_URL']?>" rel="nofollow" url="<?=$arElement['ADD_URL']?>"><?echo GetMessage("CATALOG_ADD")?></a>
				</noindex>
			<?endif?>
		<?elseif((count($arResult["PRICES"]) > 0) || is_array($arElement["PRICE_MATRIX"])):?>

			<form url="http://mera-pt.ru/<?=$arElement['DETAIL_PAGE_URL']?>" class="<?=$arParams['AJAX_ID']?>" id="<?=$arElement['ID']?>" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
				<input type="hidden" name="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" value="<?=$multi?>" size="5">
				<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
				<input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?echo $arElement["ID"]?>">
				<?if($arResult['BASKET_QUANTITY'][$arElement["ID"]]>0):?>

					<button type="submit" class="already_in_cart" onclick="yaCounter28820320.reachGoal('<?=$goal;?>');" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("IN_CART")?>"><?echo GetMessage("IN_CART")?></button>
				<?else:?>
					<button type="submit" class="not_cart" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" onclick="initSlider('#sl_tiny_<?=$arElement["ID"];?>'); yaCounter28820320.reachGoal('<?=$goal;?>');" value="<?echo GetMessage("CATALOG_ADD")?>"><?//echo GetMessage("CATALOG_ADD")?>Купить</button>
				<?endif;?>
				<!--input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("CATALOG_ADD")?>"-->
			</form>
			<?/*
			<a class="addtocart" url="http://mera-pt.ru/<?=$arElement['DETAIL_PAGE_URL']?>" href="<?=$arElement['DETAIL_PAGE_URL']?>" onclick="yaCounter28820320.reachGoal('VK_KART');" rel="nofollow"><?//echo GetMessage("CATALOG_ADD")?> Купить</a>
*/?>
		<?endif?>
			 
	</div>
	</div>
</div>
		<?endforeach; // foreach($arResult["ITEMS"] as $arElement):?>
		</div></div>
	<div name="prev" class="navy prev-slide"><</div>
	<div name="next" class="navy next-slide">></div>
</div>
<div style="clear: both;"></div>
</div>
<script type="text/javascript">
		$(document).on('submit', 'div.tocart form.<?=$arParams['AJAX_ID']?>', function (e) {
			e.preventDefault();
			var id = $(this).attr('id');

			if(!$('form[id="'+id+'"]').find('button[type="submit"]').hasClass('already_in_cart')) {
	            $("#mask").remove();
	            $(".goods-"+id).first().parent().css("display", "block");
	            $(".cart-test").css("display", "block");
				$("body").append("<div id='mask'></div>");
				$('form[id="'+id+'"]').find('button[type="submit"]').addClass('already_in_cart').html('Уже в корзине');
				$(this).bitrixAjax(
					"<?=$arParams['AJAX_ID']?>",
					function (data) {
						update_basket_line();
						$('div.tocart form#'+id).html(data.find('div.goods'+id).html());
					},
					{
						post: {
							submit: 'Y'
						}
					}
				);
			} else {
				return false;
			}


		});
</script>
<script type="text/javascript">
      function initSlider(el) {
       var sl = $(el)
       sl.bxSlider({
        mode: 'vertical',
        adaptiveHeight: true,
        maxSlides: 3,
        minSlides: 3,
        moveSlides: 1,
        pager: false,
        preloadImages: "all"
        });
       
        var child = $(el + " .product_necess:not(.bx-clone)");
        if(child.length > 0)
        {
         var px = child.length < 3 ? (child.length*97) + "px" : "291px";
         sl.parent().css("minHeight", px)
         sl.css('transform', 'translate3d(0px, -' + px + ', 0px)');
         if(child.length == 2)
          $(".bx-next").css("top", "195px !important;");
        }        
       }
</script>
<script>
	function htmSlider(){
    /* Зададим следующие параметры */
    /* обертка слайдера */
    var slideWrap = $('.slide-wrap');
    /* кнопки вперед/назад и старт/пауза */
    var nextLink = $('.next-slide');
    var prevLink = $('.prev-slide');
    var playLink = $('.auto');
    /* Проверка на анимацию */
    var is_animate = false;
    /* ширина слайда с отступами */
    var slideWidth = $('.slide-item').outerWidth();
    /* смещение слайдера */
    var scrollSlider = slideWrap.position().left - slideWidth;
   
    /* Клик по ссылке на следующий слайд */
    nextLink.click(function(){
     if(!slideWrap.is(':animated')) {
      slideWrap.animate({left: scrollSlider}, 500, function(){
       slideWrap
        .find('.slide-item:first')
        .appendTo(slideWrap)
        .parent()
        .css({'left': 0});
      });
     }
    });
   
    /* Клик по ссылке на предыдующий слайд */
    prevLink.click(function(){
     if(!slideWrap.is(':animated')) {
      slideWrap
       .css({'left': scrollSlider})
       .find('.slide-item:last')
       .prependTo(slideWrap)
       .parent()
       .animate({left: 0}, 500);
     }
    });
   
    /* Функция автоматической прокрутки слайдера */
    function autoplay(){
     if(!is_animate){
      is_animate = true;
      slideWrap.animate({left: scrollSlider}, 500, function(){
       slideWrap
        .find('.slide-item:first')
        .appendTo(slideWrap)
        .parent()
        .css({'left': 0});
       is_animate = false;
      });
     }
    }
   
    timer = setInterval(function(){
     slideWrap.animate({left: scrollSlider}, 500, function(){
      slideWrap
       .find('.slide-item:first')
       .appendTo(slideWrap)
       .parent()
       .css({'left': 0});
      });
    }, 114000);
 }
 htmSlider();	
</script>

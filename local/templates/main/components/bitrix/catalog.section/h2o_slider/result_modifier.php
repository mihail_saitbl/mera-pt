<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arParams['AJAX_ID'] = CH2oAjax::getComponentID($this->__component);


if (CModule::IncludeModule("blog"))
{
// выберем все опубликованные сообщения 
$SORT = Array("NAME" => "ASC");
$arFilter = Array(
    "PUBLISH_STATUS" => BLOG_PUBLISH_STATUS_PUBLISH,
    ">NUM_COMMENTS" => 0 
    );	 
$SELECT = array();  

$dbPosts = CBlogPost::GetList(
        $SORT,
        $arFilter
    );

$i = 0;
while($arPost = $dbPosts->Fetch()) { 
	$arResult["COMMENTS"][$i] = $arPost;
$i++;
}

	/*
	foreach ($arResult["ITEMS"] as &$arElement) {
	while($arPost = $dbPosts->Fetch()) { 
		if($arElement["NAME"] == $arPost["TITLE"]) { 
 $arElement["COMMENTS"][$arElement["ID"]] = $arPost["NUM_COMMENTS"]; }else{
$arElement["COMMENTS"][$arElement["ID"]] = 0;
		} 
 
      }
	}
*/
	//print_r($arResult["COMMENTS"]);

}

foreach($arResult['ITEMS'] as $k=>&$arElement)
{
	if(!empty($arElement["PROPERTIES"]["NEEDED"]["VALUE"])){
		$res = CIBlockElement::GetList (array(), array ("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE" => "Y", "ID" => $arElement["PROPERTIES"]["NEEDED"]["VALUE"]), false, false, array ("IBLOCK_ID", "ID", "NAME", "DETAIL_PAGE_URL", "PREVIEW_PICTURE", "DETAIL_PICTURE", "PROPERTY_ARTNUMBER"));
		while ($ar_res = $res->GetNext (true, false))
		{
			$key = array_search($ar_res["ID"], $arElement["PROPERTIES"]["NEEDED"]["VALUE"]);
		//foreach ($arElement["PROPERTIES"]["NEEDED"]["VALUE"] as $key=>$needed){ 

			//$res = CIBlockElement::GetByID($needed);
			//if($ar_res = $res->GetNext()){
				$arElement["PROPERTIES"]["NEEDED"]["VALUE"][$key] = $ar_res;
				if($ar_res['PREVIEW_PICTURE']!=""){
					$file = CFile::ResizeImageGet($ar_res["PREVIEW_PICTURE"], array('width'=>110, 'height'=>75), BX_RESIZE_IMAGE_PROPORTIONAL, true); 
					$arElement["PROPERTIES"]["NEEDED"]["VALUE"][$key]["IMG_URL"] = $file["src"];
				}elseif ($ar_res['DETAIL_PICTURE']!=""){
					$file = CFile::ResizeImageGet($ar_res['DETAIL_PICTURE'], array('width'=>110, 'height'=>75), BX_RESIZE_IMAGE_PROPORTIONAL, true); 
					$arElement["PROPERTIES"]["NEEDED"]["VALUE"][$key]["IMG_URL"] = $file["src"];
				}
			//}
			
			/*$db_props = CIBlockElement::GetProperty($ar_res["IBLOCK_ID"], $ar_res["ID"], array("sort" => "asc"), Array());
			if($ar_props = $db_props->Fetch())
			    $arElement["PROPERTIES"]["NEEDED"]["VALUE"][$key][$ar_props['CODE']] = $ar_props;*/
			    
			$arElement["PROPERTIES"]["NEEDED"]["VALUE"][$key]["ARTNUMBER"] = $ar_res["PROPERTY_ARTNUMBER_VALUE"];

			//$arPrice = CPrice::GetByID($ar_res["ID"]);
			
			$db_price = CPrice::GetList(
				array(),
				array("PRODUCT_ID" => $ar_res['ID'],)
			);
			if ($arPrice = $db_price->Fetch()){
				$arElement["PROPERTIES"]["NEEDED"]["VALUE"][$key]['PRICE'] = $arPrice;
			}
			$arElement["ITEMS_ID"][]=$needed;
		}
	}
}

foreach($arResult['ITEMS'] as $k=>&$arElement)
{
    $arPrice = array();
    $date = "";
    $arPrice = CCatalogProduct::GetOptimalPrice($arElement['ID'], 1, $USER->GetUserGroupArray(), "N");

	if(!empty($arPrice['DISCOUNT'])){
    	$date = date_create($arPrice['DISCOUNT']["ACTIVE_TO"]);
    	$arProductDiscounts["ACTIVE_TO"] = date_format($date, 'd.m.Y');
    	$arElement["DISCOUNT"] = $arProductDiscounts;
    }

	$user_id = $arElement['DISPLAY_PROPERTIES']['USER_ID']['DISPLAY_VALUE'];
	if ($user_id)
	{
		$rsUSER = CUser::GetById($user_id);
		$f=$rsUSER->Fetch();
		$arResult['ITEMS'][$k]['DISPLAY_PROPERTIES']['USER_ID']['DISPLAY_VALUE'] = CUser::FormatName(CSite::GetNameFormat(false), array("NAME" => $f['NAME'], "LAST_NAME" => $f['LAST_NAME'], "SECOND_NAME" => $f['SECOND_NAME'], "LOGIN" => $f['LOGIN']));
	}


// Определяем, находятся ли товары в избранном
if ($USER->isAuthorized())
{
	$rsUser = CUser::GetByID($USER->GetID());
	$arUser = $rsUser->Fetch();
	foreach ($arResult["ITEMS"] as &$arElement)
		$arElement['IN_FAVORITES'] = in_array($arElement['ID'],$arUser['UF_FAVORITES']);
}

	$IBLOCK_ID = $arElement["IBLOCK_ID"]; 
	$ID = $arElement["ID"];
	$arInfo = CCatalogSKU::GetInfoByProductIBlock($IBLOCK_ID); 
	if (is_array($arInfo)){
		$minPrice = "";
		$minCurr = "";
		$rsOffers = CIBlockElement::GetList(array(),array('IBLOCK_ID' => $arInfo['IBLOCK_ID'], 'PROPERTY_'.$arInfo['SKU_PROPERTY_ID'] => $ID)); 
		while ($arOffer = $rsOffers->GetNext()){
			$arFilter = array("ID"=>$arOffer["ID"]);
			$arSelect = array("ID", "XML_ID", "NAME");
			$resof = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			if($ar_resof = $resof->GetNext()){
				$db_price = CPrice::GetList(array(),array("PRODUCT_ID" => $ar_resof['ID'],));
				if ($arPrice = $db_price->Fetch()){
					$ar_resof["PRICE"] = $arPrice;
					if ($minPrice==""){
						$minPrice = $arPrice["PRICE"];
						$minCurr = $arPrice["CURRENCY"];
					}elseif($minPrice>$arPrice["PRICE"]){
						$minPrice = $arPrice["PRICE"];
						$minCurr = $arPrice["CURRENCY"];
					}
				}
				$arResult['ITEMS'][$k]["OFFERS"][] = $ar_resof;
			}
		}
		$arResult['ITEMS'][$k]["MIN_PRICE"] = $minPrice;
		$arResult['ITEMS'][$k]["MIN_CURR"] = $minCurr;
	}
	$arResult['ITEMS_ID'][] = $ID;

}
$this->__component->SetResultCacheKeys(array('ITEMS_ID'));

?>

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script type="text/javascript">
        ga('send', 'event', 'MAKE_ORDER', 'MAKE_ORDER', 'submit', {
            'hitCallback': function() {
            console.log('������!');}
          });
</script>
<?
function getTransactionJs(&$trans) {
  return <<<HTML
ga('ecommerce:addTransaction', {
  'id': '{$trans['ACCOUNT_NUMBER']}',
  'affiliation': '{$trans['LID']}',
  'revenue': '{$trans['PRICE']}',
  'shipping': '{$trans['PRICE_DELIVERY']}',
  'tax': '{$trans['TAX_VALUE']}'
});
HTML;
}

// Function to return the JavaScript representation of an ItemData object.
function getItemJs(&$transId, &$item) {
  return <<<HTML
ga('ecommerce:addItem', {
  'id': '$transId',
  'name': '{$item['NAME']}',
  'price': '{$item['PRICE']}',
  'quantity': '{$item['QUANTITY']}'
});
HTML;
}
?>
<?
if (!empty($arResult["ORDER"]))
{
	?>
	<h2><?=GetMessage("SOA_TEMPL_ORDER_COMPLETE")?></h2>
	<p><?= GetMessage("SOA_TEMPL_ORDER_SUC", Array("#ORDER_DATE#" => $arResult["ORDER"]["DATE_INSERT"], "#ORDER_ID#" => $arResult["ORDER"]["ACCOUNT_NUMBER"]))?></p>
	<p><?= GetMessage("SOA_TEMPL_ORDER_SUC1", Array("#LINK#" => $arParams["PATH_TO_PERSONAL"])) ?></p>
	
	<?$dbBasketItems = CSaleBasket::GetList(
			array(),
			array(
				"LID" => SITE_ID,
				"ORDER_ID" => $arResult["ORDER"]["ACCOUNT_NUMBER"]
			    ),
			false,
			false,
			array()
	);?>
	<? while ($arItems = $dbBasketItems->Fetch()){?>
		<? $arItemsAr[] = $arItems;?>
	<?}?>
	<script>
		var yaParams = {
			order_id: "<?=$arResult["ORDER"]["ACCOUNT_NUMBER"]?>",
			order_price: <?=$arResult["ORDER"]["PRICE"]?>, 
			currency: "<?=$arResult["ORDER"]["CURRENCY"]?>",
			exchange_rate: 1,
			goods: 
			   [ <? foreach ($arItemsAr as &$item){?>
			      {
				id: "<?=$item["ID"]?>", 
				name: "<?=$item["NAME"]?>", 
				price: <?=$item["PRICE"]?>,
				quantity: <?=$item["QUANTITY"]?>
			      },
			      <?}?>
			    ]
		};
		ga('require', 'ecommerce');

		<?
		echo getTransactionJs($arResult["ORDER"]);
		
		foreach ($arItemsAr as &$item) {
		  echo getItemJs($arResult["ORDER"]['ACCOUNT_NUMBER'], $item);
		}
		?>
		ga('ecommerce:send');
		window.onload = function() {
			yaCounter28820320.reachGoal('MAKE_ORDER', yaParams);
			console.log("MAKE_ORDER");
		};
	</script>
	
	<?
	if (!empty($arResult["PAY_SYSTEM"]))
	{
		?>
		<h2><?=GetMessage("SOA_TEMPL_PAY")?></h2>
		<p><?=CFile::ShowImage($arResult["PAY_SYSTEM"]["LOGOTIP"], 100, 100, "border=0", "", false);?></br>
		<?= $arResult["PAY_SYSTEM"]["NAME"] ?></p></br>
			<?
			if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0)
			{
						if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y")
						{
							?>
							<script language="JavaScript">
								window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))?>');
							</script>
							<p><?= GetMessage("SOA_TEMPL_PAY_LINK", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))))?></p>
							<?
							if (CSalePdf::isPdfAvailable() && CSalePaySystemsHelper::isPSActionAffordPdf($arResult['PAY_SYSTEM']['ACTION_FILE']))
							{
								?><br />
								<p><?= GetMessage("SOA_TEMPL_PAY_PDF", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))."&pdf=1&DOWNLOAD=Y")) ?></p>
								<?
							}
						}
						else
						{
							if (strlen($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"])>0)
							{?><p><?
								//include($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]);
							}?></p><?
						}
						?>
				<?
			}
			?>
		<?
	}
}
else
{
	?>
	<b><?=GetMessage("SOA_TEMPL_ERROR_ORDER")?></b><br /><br />

	<table class="sale_order_full_table">
		<tr>
			<td>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $arResult["ACCOUNT_NUMBER"]))?>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1")?>
			</td>
		</tr>
	</table>
	<?
}
?>
<pre><?//print_r($arItemsAr)?></pre>

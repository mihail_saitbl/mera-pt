<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="bx_redetpassword_page">
<?
ShowMessage(str_replace("сменен", "изменен", $arParams["~AUTH_RESULT"]));
if ($arParams["~AUTH_RESULT"]["TYPE"] != "OK"){
?>
	<form method="post" action="<?=$arResult["AUTH_FORM"]?>" name="bform">
		<?if (strlen($arResult["BACKURL"]) > 0):?>
		<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
		<?endif?>
		<input type="hidden" name="AUTH_FORM" value="Y">
		<input type="hidden" name="TYPE" value="CHANGE_PWD">


		<h2><?=GetMessage("AUTH_CHANGE_PASSWORD")?></h2>
		<div class="change_line">
			<div class="before_input">Почта*:</div>
			<input class="input_line" id='txtEmail' type="text" name="USER_LOGIN" maxlength="50" value="<?=$arResult["LAST_LOGIN"]?>" size="17" />
		</div>
		<div class="change_line">
			<div class="before_input"><?=GetMessage("AUTH_CHECKWORD")?>*:</div>
			<input class="input_line" id='txtEmail' type="text" name="USER_CHECKWORD" maxlength="50" value="<?=$arResult["USER_CHECKWORD"]?>" size="17" />
		</div>
		<div class="change_line">
			<div class="before_input"><?=GetMessage("AUTH_NEW_PASSWORD_REQ")?>*:</div>
			<input class="input_line" id='txtEmail' type="password" name="USER_PASSWORD" maxlength="50" value="<?=$arResult["USER_PASSWORD"]?>" size="17" />
		</div>
		<div class="change_line">
			<div class="before_input"><?=GetMessage("AUTH_NEW_PASSWORD_CONFIRM")?>*:</div>
			<input class="input_line" id='txtEmail' type="password" name="USER_CONFIRM_PASSWORD" maxlength="50" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" size="17" />
		</div>
		<div class="move_button">
			<input type="submit" name="change_pwd" value="<?=GetMessage("AUTH_CHANGE")?>" class="forgot"/>
		</div>
		<p><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?><br/>
		<span class="starrequired">*</span><?=GetMessage("AUTH_REQ")?></p>

		<p><a href="<?=$arResult["AUTH_AUTH_URL"]?>"><b><?=GetMessage("AUTH_AUTH")?></b></a></p>
	</form>
<? } else {?>
Теперь Вы можете <a href data-remodal-target="modal-auth">авторизоваться</a> и продолжить Ваши действия.
<? }?>
</div>

<script type="text/javascript">
	document.bform.USER_LOGIN.focus();
</script>
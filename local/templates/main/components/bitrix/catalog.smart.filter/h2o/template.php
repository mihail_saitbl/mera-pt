<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$show = false;
foreach ($arResult["ITEMS"] as $arItem)
{
	if (
		(
			$arItem["PROPERTY_TYPE"] == "N" || isset($arItem["PRICE"])
		)
		 ||
		(
			(
				$arItem["PROPERTY_TYPE"] == "Y" || empty($arItem["PRICE"]) && !empty($arItem["VALUES"]) && $arItem["PROPERTY_TYPE"] != "N"
			)
		)
	)
	{
		$show = true;
		break;
	}
}


if ($show)
{
	?>
	<div class="mobile_filter_modal_open_btn_wrapper media_mobile"><a href="#" class="mobile_filter_modal_open_btn"><i class="fa fa-sliders" aria-hidden="true"></i>Фильтр</a></div>
	<div class="filter_wrapper_for_mobile">		
		<div class="filter">
			<div class="filter_modal_header media_mobile">
				<a href="#" class="mobile_filter_modal_close_btn media_mobile"><i class="fa fa-times" aria-hidden="true"></i></a>
				<a href="#" class="mobile_filter_modal_close_btn media_mobile"><i class="fa fa-sliders" aria-hidden="true"></i> Применить</a>
			</div>
			<p style="font-size: 16px; margin: 10px 10px 20px 10px;">Фильтр</p>
			<form name="<?=$arResult['FILTER_NAME'].'_form'?>" action="<?=$arResult['FORM_ACTION']?>" method="get" class="smartfilter range_cost">
				<?foreach($arResult["HIDDEN"] as $arItem):?>
					<input
						type="hidden"
						name="<?echo $arItem["CONTROL_NAME"]?>"
						id="<?echo $arItem["CONTROL_ID"]?>"
						value="<?echo $arItem["HTML_VALUE"]?>"
					/>
				<?endforeach;?>
					<?$priceMin = 0;?>
					<?$priceMax = 0;?>
					<?foreach($arResult["ITEMS"] as $arItem):?>
						<?if($arItem["PROPERTY_TYPE"] == "N" || isset($arItem["PRICE"])):?>
							<?$priceMin = floor($arItem["VALUES"]["MIN"]["VALUE"]);?>
							<?$priceMax = ceil($arItem["VALUES"]["MAX"]["VALUE"]);?>
							<p style="font-size: 14px; margin: 10px 10px 20px 10px;">Цена</p>
							<div class="slider_range">
								<div id="slider-range"></div>
							</div>
							<div class="border">
								<label for="mincost">от</label>
								<input
									class="min-price"
									type="text"
									name="<?=$arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
									id="mincost"
									value="<?=$priceMin?>"
									size="5"
								/>
								<label for="maxcost">до</label>
								<input
									class="max-price"
									type="text"
									name="<?=$arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
									id="maxcost"
									value="<?=$priceMax?>"
									size="5"
								/>
							</div>
						<?endif;?>
					<?endforeach;?>
					<?
					$dop_class = "";
					$display_style = "";
					if ($arParams["FILTER_CLOSED"] == "Y")
					{
						$dop_class = " up";
						$display_style = ' style="display: none;"';
					}
					?>
					<div class="check_block">
						<?foreach($arResult["ITEMS"] as $keys => $arItem):?>
							<?if($arItem["PROPERTY_TYPE"] == "Y" || empty($arItem["PRICE"]) && !empty($arItem["VALUES"]) && $arItem["PROPERTY_TYPE"] != "N"):?>
								<a href="#" class="role<?=$dop_class;?>"><?=$arItem["NAME"]?></a>
								<div class="first_box"<?=$display_style;?>>
									<?foreach($arItem["VALUES"] as $val => $ar):?>
										<div>
											<input
												type="checkbox"
												value="<?=$ar["HTML_VALUE"]?>"
												name="<?=$ar["CONTROL_NAME"]?>"
												id="<?=$ar["CONTROL_ID"]?>"
												<?=$ar["CHECKED"]? 'checked="checked"': ''?>
												/><label for="<?echo $ar["CONTROL_ID"]?>"><?echo $ar["VALUE"];?></label>
										</div>
									<?endforeach;?>
								</div>
							<?endif;?>
						<?endforeach;?>
						<div class="modef" id="modef" <?if(!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"';?>>
							<?echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">'.intval($arResult["ELEMENT_COUNT"]).'</span>'));?>
							<a href="<?echo $arResult["FILTER_URL"]?>" class="showchild"><?echo GetMessage("CT_BCSF_FILTER_SHOW")?></a>
						</div>
						<div class="end_filter">
							<button type="reset" class="reset_filter" id="reset" name="del_filter" value="<?=GetMessage("CT_BCSF_DEL_FILTER")?>"><?=GetMessage("CT_BCSF_DEL_FILTER")?></button>
						</div>
						
					</div>

			</form>
			<!--script>
				var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>');
			</script-->
			<script>
				$(document).ready(function() {
					$(document).on('click','a.role',function(e){
						e.preventDefault();
						console.log("click");
						$(this).toggleClass('up').next().toggle();
					})
					
					$(document).on('click','.end_filter button',function(e){
						//e.preventDefault();
						console.log($(this));
						$("form.smartfilter").append('<input type="hidden" name="del_filter" value="Y"/>');
						$(this).closest('form').submit();
						$('input[name="del_filter"]').remove();
						$('#slider-range').slider('values', 0, <?=$priceMin?>);
						$('#slider-range').slider('values', 1, <?=$priceMax?>);
						//$(this).closest('form').reset();
					});
					
					$(document).on('click','.sort_date',function(e){
						e.preventDefault();
						console.log($(this));
						var method = $(this).attr('data-method');
						var sort = $(this).attr('data-sort');
						$('input[name="met"]').remove();
						$('input[name="sort"]').remove();
						$("form.smartfilter").append('<input type="hidden" name="sort" value="'+sort+'"/>');
						$("form.smartfilter").append('<input type="hidden" name="met" value="'+method+'"/>');
						$("form.smartfilter").submit();
					});
					
					$(document).on('change','.check_block input',function(){
						console.log($(this));
						$(this).closest('form').submit();
					});
					
				//var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>');
				//Ползунок фильтра     
					$(function() {
						
						$('#mincost').change(function() {
						var mincost = $(this).val();
						$('#slider-range').slider('values', 0, mincost);
						$(this).closest('form').submit();
						});
						$('#maxcost').change(function() {
						var maxcost = $(this).val();
						$('#slider-range').slider('values', 1, maxcost);
						$(this).closest('form').submit();
						});
					
						$( "#slider-range" ).slider({
						  range: true,
						  min: <?=$priceMin?>,
						  max: <?=$priceMax?>,
						  values: [<?if($_REQUEST['arrFilter_P1_MIN']>0){echo $_REQUEST['arrFilter_P1_MIN'];}else{echo($priceMin);}?>, <?if($_REQUEST['arrFilter_P1_MAX']>0){echo $_REQUEST['arrFilter_P1_MAX'];}else{echo ($priceMax);}?> ],
						  slide: function( event, ui ) {
						$( "#mincost" ).val( ui.values[ 0 ] );
						$( "#maxcost" ).val( ui.values[ 1 ] );
						//$(this).closest('form').submit();
						//smartFilter.mess(this);
						  },
						  stop: function( event, ui ) {
						$(this).closest('form').submit();
						  }
						});
						$('#mincost').val($('#slider-range').slider('values',0));
						$('#maxcost').val($('#slider-range').slider('values',1));
					
					});
				});
			</script>
		</div>
	</div>
<?}?>
<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?global $menu_serv;
foreach ($arResult as $key=>$link):
    $arLink[0] = $link["TEXT"];
    $arLink[1] = $link["LINK"];
    $arLink[2][0] = $link["LINK"];
    $arLink[3]["FROM_IBLOCK"] = 1;
    $arLink[3]["IS_PARENT"] = $link["IS_PARENT"];
    $arLink[3]["DEPTH_LEVEL"] = $link["DEPTH_LEVEL"];
    $arLink[4] = "SERVICE";
    $arResult[$key] = $arLink;
endforeach;
$menu_serv = $arResult;?>

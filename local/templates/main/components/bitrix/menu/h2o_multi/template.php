<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<!-- <pre><?/* print_r($arResult) */?></pre> -->
<ul id="horizontal-multilevel-menu">

<?
$previousLevel = 0;
$i=0;
foreach($arResult as $arItem):?>
	<?//if ($USER->IsAdmin()) print_r($arItem)?>
	<?$file = "";?>
	<?//$arr = explode("/", $arItem["LINK"]);?>
	<?//$arFilter = array('CODE' => ($arr[(count($arr)-2)]));
	//$res = CIBlockSection::GetList(array(),$arFilter, false, array("PICTURE", "ID"));
	//if($ar_res = $res->GetNext())
	if($arItem['PARAMS']['PICTURE'])
	    $file = CFile::ResizeImageGet($arItem['PARAMS']['PICTURE']/*$ar_res['PICTURE']*/, array('width'=>120, 'height'=>50), BX_RESIZE_IMAGE_EXACT, true);?>
	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</div></ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>
	<?//$ar_fil[] = $file;?>
	<?if ($arItem["IS_PARENT"]):?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
		<?$i++;?>
			<li class="first <? if($i==6){?>services_menu<?}?> ">
				<a href="<?=$arItem["LINK"]?>" class="<? if ($arItem["SELECTED"]):?>root-item-selected<? else:?>root-item<? endif?>"><?=$arItem["TEXT"]?></a>
				<ul class="drop-wrap">
					<div class="drop-menu">
		<?elseif (($arItem["DEPTH_LEVEL"] == 2) && (isset($file))):?>
			<li class="list_drop hover2">
				<a href="<?=$arItem["LINK"]?>" class="parent-img" style="width: 115px;"><img src="<?=$file["src"]?>"></a>
				<a href="<?=$arItem["LINK"]?>" class="parent" style="position:relative; top: 30px;"><?=$arItem["TEXT"]?></a>
				<ul class="drop-wrap2">
					<div class="drop-menu2">
		<?elseif (($arItem["DEPTH_LEVEL"] == 2)):?>
			<li class="list_drop hover2">
				<a href="<?=$arItem["LINK"]?>" class="parent"><?=$arItem["TEXT"]?></a>
				<ul class="drop-wrap2">
					<div class="drop-menu2">
		<?else:?>
			<li class="hover2">
				<a href="<?=$arItem["LINK"]?>" class="parent"><?=$arItem["TEXT"]?></a>
				<ul class="drop-wrap2">
					<div class="drop-menu2">
		<?endif?>

	<?else:?>
		<?if ($arItem["PERMISSION"] > "D"):?>
			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
			<?$i++;?>
				<li class="first <? if($arItem['TEXT']=='Акции'){?>red<?} elseif($arItem["TEXT"]=="Услуги"){?>services_menu<?}?>">
					<a href="<?=$arItem["LINK"]?>" class="<?if ($arItem["SELECTED"]):?>root-item-selected<?else:?>root-item<?endif?>"><?=$arItem["TEXT"]?></a>
				</li>
			<?else:?>
				<li class="<? 	if ($arItem["DEPTH_LEVEL"] == 2):?>
							list_drop
						<? endif;?>
						<? if($arItem["SELECTED"]):?>
							item-selected
						<? endif?>
				">
					<?if (($arItem["DEPTH_LEVEL"] == 2) && (isset($file["src"]))):?>
						<img src="<?=$file["src"]?>" style="width:120px; height:50px;">
					<?endif;?>
					<a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
				</li>
			<?endif?>
		<?else:?>
			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
			<?$i++;?>
				<li class="first <? if($i==5){?>red<?}elseif($i==6){?>services_menu<?}?>">
					<a href="" class="<?if($arItem['SELECTED']):?>root-item-selected<?else:?>root-item<?endif?>" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a>
				</li>
			<?else:?>
				<li class="">
					<!--img src="<?=$file["src"]?>" style="width:120px; height:50px;"-->
					<a href="" class="denied" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a>
				</li>
			<?endif?>

		<?endif?>

	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
<?endif?>

</ul>
<?if ($USER->IsAdmin()){?><pre><?//print_r($ar_fil)?></pre><?}?>
<div class="menu-clear-left"></div>
<?endif?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arSection_curr = false;
$arSections = array();
$arFilter = array(
	"ACTIVE" => "Y",
	"GLOBAL_ACTIVE" => "Y",
	"IBLOCK_ID" => $arParams["IBLOCK_ID"],
	"CNT_ACTIVE" => "Y",
);
$arSelect = array (
	"ID",
	"NAME",
	"UF_FALSE_SECTION",
	"LEFT_MARGIN",
	"RIGHT_MARGIN",
	"DEPTH_LEVEL",
	"IBLOCK_SECTION_ID",
);
if(intval($arResult["VARIABLES"]["SECTION_ID"])>0)
{
	$arFilter["ID"] = intval($arResult["VARIABLES"]["SECTION_ID"]);
	$rsSections = CIBlockSection::GetList(array(), $arFilter, false, $arSelect);
	$arSection_curr = $rsSections->GetNext();
}
elseif('' != $arResult["VARIABLES"]["SECTION_CODE"])
{
	$arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];
	$rsSections = CIBlockSection::GetList(array(), $arFilter, false, $arSelect);
	$arSection_curr = $rsSections->GetNext();
}
if(is_array($arSection_curr))
{
	unset($arFilter["ID"]);
	unset($arFilter["=CODE"]);
	$arFilter["LEFT_MARGIN"]=$arSection_curr["LEFT_MARGIN"]+1;
	$arFilter["RIGHT_MARGIN"]=$arSection_curr["RIGHT_MARGIN"];
	//$arFilter["<="."DEPTH_LEVEL"] = $arSection_curr["DEPTH_LEVEL"] + $arParams["SECTION_TOP_DEPTH"];
	$arFilter["<="."DEPTH_LEVEL"] = $arParams["SECTION_TOP_DEPTH"];
	$rsSections = CIBlockSection::GetList($arSort, $arFilter, false, $arSelect);
	while($arSection = $rsSections->GetNext())
	{
		if ($arSection["UF_FALSE_SECTION"] == 1)
		{
			continue;
		}
		$arSections[] = $arSection;
	}

}

//echo '<pre>'; print_r($arSection_curr); echo '</pre>@'.__FILE__.'#'.__LINE__;

$bFalseSection = "N";
if ($arSection_curr["UF_FALSE_SECTION"] == 1)
{
	$arResult["VARIABLES"]["SECTION_ID"] = $arSection_curr["IBLOCK_SECTION_ID"];
	$bFalseSection = "Y";
}

$FILTER_CLOSED = "N";
if(count($arSections)>0)
{
	$FILTER_CLOSED = "Y";
}

if (!$arParams['FILTER_VIEW_MODE'])
	$arParams['FILTER_VIEW_MODE'] = 'VERTICAL';
$arParams['USE_FILTER'] = (isset($arParams['USE_FILTER']) && $arParams['USE_FILTER'] == 'Y' ? 'Y' : 'N');
$verticalGrid = ('Y' == $arParams['USE_FILTER'] && $arParams["FILTER_VIEW_MODE"] == "VERTICAL");

/*global $USER;
if ($APPLICATION->GetCurDir() == "/catalog/osnastka/otrezanie-obdirka-kratsevanie/otreznye-krugi/")
{
	?><ul class="related"><?
		?><li><a href="/catalog/otreznye-krugi-po-metallu/230-mm/">Круги 230 мм</a></li><?
	?></ul><?
}elseif($APPLICATION->GetCurDir() == "/catalog/osnastka/almaznaya-rezka-shlifovanie-i-sverlenie/"){
	?><ul class="related"><?
		?><li><a href="/catalog/osnastka/almaznaya-rezka-shlifovanie-i-sverlenie/125-mm/">Круги 125 мм</a></li><?
		?><li><a href="/catalog/osnastka/almaznaya-rezka-shlifovanie-i-sverlenie/230-mm/">Круги 230 мм</a></li><?
		?><li><a href="/catalog/osnastka/almaznaya-rezka-shlifovanie-i-sverlenie/350-mm/">Круги 230 мм</a></li><?
	?></ul><?
}*/

if ($arResult["VARIABLES"]["SECTION_ID"] > 0)
{
	$urls = CIBlockElement::GetList (array ("SORT" => "ASC", "NAME" => "ASC"), array ("IBLOCK_ID" => 28, "ACITVE" => "Y", "PROPERTY_LINK_CAT" => $arResult["VARIABLES"]["SECTION_ID"]), false, false, array ("IBLOCK_ID", "ID", "NAME", "PROPERTY_NAME", "PROPERTY_LINK_CAT"));
	$bShow = false;
	while ($arUrl = $urls->Fetch())
	{
		if (!$bShow)
		{
			?><ul class="related"><?
			$bShow = true;
		}
		?><li><a href="<?=$arUrl["NAME"];?>"><?=$arUrl["PROPERTY_NAME_VALUE"];?></a></li><?
	}
	if ($bShow)
	{
		?></ul><?
	}
}

if ($verticalGrid)
{
?><div class="workarea grid2x1">

	<h1><?$APPLICATION->ShowProperty ("H1");?></h1>


	<?
	}
	if ($arParams['USE_FILTER'] == 'Y')
	{

		$arFilter = array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ACTIVE" => "Y",
			"GLOBAL_ACTIVE" => "Y",
		);
		if (0 < intval($arResult["VARIABLES"]["SECTION_ID"])){
			$arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
		}elseif ('' != $arResult["VARIABLES"]["SECTION_CODE"]){
			$arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];
		}

		$obCache = new CPHPCache();
		if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog"))
		{
			$arCurSection = $obCache->GetVars();
		}
		elseif ($obCache->StartDataCache())
		{
			$arCurSection = array();
			if (\Bitrix\Main\Loader::includeModule("iblock"))
			{
				$dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID"));

				if(defined("BX_COMP_MANAGED_CACHE"))
				{
					global $CACHE_MANAGER;
					$CACHE_MANAGER->StartTagCache("/iblock/catalog");

					if ($arCurSection = $dbRes->Fetch())
					{
						$CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"]);
					}
					$CACHE_MANAGER->EndTagCache();
				}
				else
				{
					if(!$arCurSection = $dbRes->Fetch())
						$arCurSection = array();
				}
			}
			$obCache->EndDataCache($arCurSection);
		}
		if (!isset($arCurSection)){
			$arCurSection = array();
		}
		if ($verticalGrid){?>
			<div class="left_block">
			<?
			if(count($arSections)>0)
			{
				?><?$APPLICATION->IncludeComponent(
				"bitrix:catalog.section.list",
				"left_list",
				array(
					"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"CACHE_TYPE" => $arParams["CACHE_TYPE"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
					"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
					"TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
					"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
					"VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
					"SECTION_CODE" => $arResult["VARIABLES"]['SECTION_CODE'],
					"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
					"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
					"ADD_SECTIONS_CHAIN" => "N",
					"PRICE_CODE" => $arParams["PRICE_CODE"]
				),
				$component
			);
				?><?
			}
			?>
		<?}?>




		<?$APPLICATION->IncludeComponent(
		"bitrix:catalog.smart.filter",
		"h2o",
		Array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"SECTION_ID" => $arCurSection['ID'],
			"FILTER_NAME" => $arParams["FILTER_NAME"],
			"PRICE_CODE" => $arParams["FILTER_PRICE_CODE"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"SAVE_IN_SESSION" => "N",
			"XML_EXPORT" => "Y",
			"SECTION_TITLE" => "NAME",
			"SECTION_DESCRIPTION" => "DESCRIPTION",
			'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
			"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
			"FILTER_CLOSED" => $FILTER_CLOSED,
		),
		$component,
		array('HIDE_ICONS' => 'Y')
	);


		?>
		<?GLOBAL $arrFilterTop;
		$arrFilterTop = array('PROPERTY_SPECIALOFFER_VALUE' => "Y");?>
		<?/***SLIDER***/?>
		<?$APPLICATION->IncludeComponent(
		"bitrix:catalog.section",
		"h2o_slider",
		array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
			"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
			"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
			"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
			"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
			"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
			"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
			"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
			"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
			"BASKET_URL" => $arParams["BASKET_URL"],
			"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
			"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
			"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
			"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
			"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
			"FILTER_NAME" =>  "arrFilterTop",//$arParams["FILTER_NAME"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_FILTER" => $arParams["CACHE_FILTER"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"SET_TITLE" => $arParams["SET_TITLE"],
			"SET_STATUS_404" => $arParams["SET_STATUS_404"],
			"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
			"PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
			"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
			"PRICE_CODE" => $arParams["PRICE_CODE"],
			"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
			"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

			"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
			"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
			"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
			"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
			"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

			"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
			"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
			"PAGER_TITLE" => $arParams["PAGER_TITLE"],
			"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
			"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
			"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
			"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
			"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],

			"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
			"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
			"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
			"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
			"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
			"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
			"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
			"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

			"SECTION_ID" => "",//$arResult["VARIABLES"]["SECTION_ID"],
			"SECTION_CODE" => "",//$arResult["VARIABLES"]["SECTION_CODE"],
			"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
			"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
			'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
			'CURRENCY_ID' => $arParams['CURRENCY_ID'],
			'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],

			'LABEL_PROP' => $arParams['LABEL_PROP'],
			'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
			'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

			'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
			'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
			'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
			'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
			'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
			'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
			'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
			'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
			'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
			'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],

			'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
			"ADD_SECTIONS_CHAIN" => "N",
			"SHOW_ALL_WO_SECTION" => "Y",

			"USE_PRODUCT_QUANTITY" => "Y"
		),
		$component
	);
		$dbSectionMain = CIBlockSection::GetList(
			array(),
			array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ID" => $arResult["VARIABLES"]["SECTION_ID"], "!UF_LINK_SECTIONS" => false),
			false,
			array("UF_LINK_SECTIONS")
		);
		if($arSectionMain = $dbSectionMain->GetNext()){
			$dbSections = CIBlockSection::GetList(array(),array("ID"=>$arSectionMain["UF_LINK_SECTIONS"], "IBLOCK_ID" => $arParams["IBLOCK_ID"]));
			?>
			<div class="sub_sections_box">
				<span>Популярные товары</span>
				<div class="items">
					<?
					while($arSection = $dbSections->GetNext(true, false)){
						?>
						<div>
							<a href="<?=$arSection['SECTION_PAGE_URL']?>"><?=$arSection['NAME']?></a>
						</div>
						<?
					}
					?>
				</div>
			</div>
			<?
		}


		?>
		</div>

	<? }?>

	<?if($arParams["USE_COMPARE"]=="Y"){?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:catalog.compare.list",
			"",
			array(
				"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"NAME" => $arParams["COMPARE_NAME"],
				"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
				"COMPARE_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["compare"],
			),
			$component
		);?>
	<?}
	$intSectionID = 0;?>

	<?/***SORT***/?>

	<?foreach($_REQUEST as $key => $value):
		if($key != ""):
			$_SESSION[$key]=$value;
		endif;
	endforeach;?>
	<?
	if(count($arSections)>0)
	{
		?><?$APPLICATION->IncludeComponent(
		"bitrix:catalog.section.list",
		"center_list",
		array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
			"TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
			"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
			"VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
			"SECTION_CODE" => $arResult["VARIABLES"]['SECTION_CODE'],
			"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
			"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
			"ADD_SECTIONS_CHAIN" => "N",
			"PRICE_CODE" => $arParams["PRICE_CODE"]
		),
		$component
	);
		?><?
	}?>

	<?/*if($_SESSION["count"]){$elementCount = $_SESSION["count"];} else $elementCount=18;*/?>

	<div class="right_block">
		<div class="filter2">
			<div class="filterwrapper">
				<div class="sort">
					<?if($_SESSION['met']=='asc,nulls'){$mt='desc,nulls';}else{$mt='asc,nulls';}?>
					<span>Сортировать по:</span>
					<a class="sort_date" data-sort="PROPERTY_MINIMUM_PRICE" data-method="<?=$mt?>" href="<?=$APPLICATION->GetCurPageParam('sort=PROPERTY_MINIMUM_PRICE&method='.$mt, array('sort', 'method'), false)?>">цене</a>
					<a class="sort_date" data-sort="show_counter" data-method="<?=$mt?>" href="<?=$APPLICATION->GetCurPageParam("sort=show_counter&method=".$mt, array('sort', 'method'), false)?>">популяности</a>
					<a class="sort_date" data-sort="name" data-method="<?=$mt?>" href="<?=$APPLICATION->GetCurPageParam("sort=name&method=".$mt, array('sort', 'method'), false)?>">названию</a>
				</div>

				<div class="vivod">
					<span>Выводить по:</span>
					<a href="<?=$APPLICATION->GetCurPageParam("count=20", array("count"), false)?>">20</a>
					<a href="<?=$APPLICATION->GetCurPageParam("count=50", array("count"), false)?>">50</a>
					<a href="<?=$APPLICATION->GetCurPageParam("count=100", array("count"), false)?>">100</a>
				</div>
			</div>
		</div>

		<div class=" content" style="margin-left: 0px;padding-right: 0px;">
			<img class="ajax-loader" src="/bitrix/templates/main/images/ajax-loader.gif" alt="">

			<?$urlParse = $APPLICATION->GetCurPage();?>
			<?if($urlParse == "/catalog/izmeritelny-instrument/") {
				/*подключаю шаблон в разделе "Измерительный инструмент" из-за того что шаблон который ниже ломает верстку из-за микро разметки именно на этой странице*/
				$tpl_copy_slider = "h2o_salelider_copy";

			}else {

				$tpl_copy_slider = "h2o_salelider";
			}?>


			<?$intSectionID = $APPLICATION->IncludeComponent(
				"h2o:catalog.section",
				$tpl_copy_slider,
				array(
					"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"ELEMENT_SORT_FIELD" => $_SESSION["sort"],//$arParams["ELEMENT_SORT_FIELD"],
					"ELEMENT_SORT_ORDER" => $_SESSION["met"],//$arParams["ELEMENT_SORT_ORDER"],
					"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
					"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
					"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
					"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
					"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
					"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
					"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
					"BASKET_URL" => $arParams["BASKET_URL"],
					"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
					"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
					"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
					"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
					"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
					"FILTER_NAME" => $arParams["FILTER_NAME"],
					"CACHE_TYPE" => $arParams["CACHE_TYPE"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"CACHE_FILTER" => $arParams["CACHE_FILTER"],
					"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
					"SET_TITLE" => $arParams["SET_TITLE"],
					"SET_STATUS_404" => $arParams["SET_STATUS_404"],
					"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
					/* "PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"], */
					"PAGE_ELEMENT_COUNT" => ($GLOBALS['isMobile']) ? 5 : $arParams["PAGE_ELEMENT_COUNT"],
					"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
					"PRICE_CODE" => $arParams["PRICE_CODE"],
					"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
					"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

					"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
					"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
					"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
					"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
					"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

					"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
					"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
					"PAGER_TITLE" => $arParams["PAGER_TITLE"],
					"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
					"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
					"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
					"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
					"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],

					"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
					"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
					"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
					"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
					"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
					"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
					"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
					"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

					"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
					"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
					"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
					"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
					'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
					'CURRENCY_ID' => $arParams['CURRENCY_ID'],
					'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],

					'LABEL_PROP' => $arParams['LABEL_PROP'],
					'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
					'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

					'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
					'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
					'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
					'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
					'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
					'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
					'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
					'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
					'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
					'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],

					'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
					"ADD_SECTIONS_CHAIN" => "Y",
					"ADD_PRODUCT_NAME" => $bFalseSection,
				),
				$component
			);?>


		</div>
		<?

		if (!($intSectionID > 0))
		{
			define ("ERROR_404", "Y");
		}

		if ($bFalseSection == "Y")
		{
			$APPLICATION->AddChainItem ($arSection_curr["NAME"]);
		}
		?><?
		if ($verticalGrid){?>
	</div>

	<div style="clear: both;"></div>
</div>
<?}?>






<?
if (\Bitrix\Main\ModuleManager::isModuleInstalled("sale"))
{
	$arRecomData = array();
	$recomCacheID = array('IBLOCK_ID' => $arParams['IBLOCK_ID']);
	$obCache = new CPHPCache();
	if ($obCache->InitCache(36000, serialize($recomCacheID), "/sale/bestsellers"))
	{
		$arRecomData = $obCache->GetVars();
	}
	elseif ($obCache->StartDataCache())
	{
		if (\Bitrix\Main\Loader::includeModule("catalog"))
		{
			$arSKU = CCatalogSKU::GetInfoByProductIBlock($arParams['IBLOCK_ID']);
			$arRecomData['OFFER_IBLOCK_ID'] = (!empty($arSKU) ? $arSKU['IBLOCK_ID'] : 0);
		}
		$obCache->EndDataCache($arRecomData);
	}
}
?>

<script>
	$( document ).ready(function() {
		var arCheck = $('input[type="checkbox"]');
		arCheck.each(function (i) {
			if($(this).prop("checked")==true){
				$('div.right_block div.content').addClass('ajax-loading');
				$('form.smartfilter').bitrixAjax("<?=$arResult['AJAX_ID']?>", function (data) {
						$('div.right_block div.content').removeClass('ajax-loading');
						$('div.right_block div.content').html(data.find('div.right_block div.content').html());
						$('div.right_block div.filterwrapper').html(data.find('div.right_block div.filterwrapper').html());
					},
					{
						get: {
							set_filter: 'Y'
						}
					});
				return false;
			}
		});
	});

	$(function () {
		$(document).on('submit', 'form.smartfilter', function (e) {
			e.preventDefault();
			$('div.right_block div.content').addClass('ajax-loading');
			$(this).bitrixAjax("<?=$arResult['AJAX_ID']?>", function (data) {
					$('div.right_block div.content').removeClass('ajax-loading');
					$('div.right_block div.content').html(data.find('div.right_block div.content').html());
					$('div.right_block div.filterwrapper').html(data.find('div.right_block div.filterwrapper').html());
				},
				{
					get: {
						set_filter: 'Y'
					}
				});
		});
	});
</script>

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
$APPLICATION->SetPageProperty("title", $arResult["NAME"]);

/*global $USER;
if ($USER->IsAdmin())
{
	echo '<pre>'; print_r($arResult["OFFERS"]); echo "#".__LINE__."@".__FILE__; echo '</pre>';
}*/
 
?> 
<?foreach($arResult["PRICES"] as $pri){
    if($pri["PRICE_ID"]==2)
        $price_bas = $pri;
    elseif($pri["PRICE_ID"]==3)   
        $price_disc = $pri;
    elseif($pri["PRICE_ID"]==1)   
        $price_id_1 = $pri;
}

if (empty($price_bas) && !empty($price_id_1))
{
	$price_bas = $price_id_1;
}

/*global $USER;
if ($USER->IsAdmin())
{

}*/

/*if ($price_bas["VALUE"] == $price_disc["VALUE"])
{
	unset ($price_disc);
}*/

$multi = $arResult['PROPERTIES']['MULTI']['VALUE'];
if(intval($multi)==0){
    $multi=1;
}
$ID = $arResult['ID'];
/*$amount = 0;
$mult = $arResult['PROPERTIES']['STATUS']['VALUE'];
if($mult>0)
    $amount = 2;
foreach($arResult["PRICES"] as $code=>$arPrice):
    if($arPrice["VALUE"]>0 && $amount==0){
        $amount = 2;
    }
endforeach;
if($arResult['CATALOG_QUANTITY']>0)
        $amount = 1;
$rsStoreNsk = CCatalogStoreProduct::GetList(array(), array('PRODUCT_ID' =>$ID, 'STORE_ID' => 1), false, false, array()); 
$rsStoreBsh = CCatalogStoreProduct::GetList(array(), array('PRODUCT_ID' =>$ID, 'STORE_ID' => 2), false, false, array());
if ($arStoreNsk = $rsStoreNsk->Fetch())
    if($arStoreNsk['AMOUNT']>0 || $arResult['CATALOG_QUANTITY']>0)
        $amount = 1;
if ($arStoreBsh = $rsStoreBsh->Fetch())
    if($arStoreBsh['AMOUNT']>0 && $amount == 0)
        $amount = 2;
*/
?>
<?CModule::IncludeModule("sale");
$basket_discount = 1;
$ar = CSaleDiscount::GetByID(1);
$arar = unserialize($ar['ACTIONS']);
$basket_discount = 1 - $arar['CHILDREN'][0]['DATA']['Value']/100;
?>
<div class="catalog-element" itemscope="" itemtype="http://schema.org/Product">


	<div style="display: inline-block; width: 100%;">
		<div class="connected-carousels">
			<div class="navigation">
				<div class="btn_slide_up media_no_mobile"><a href="#" class="prev-navigation"></a></div>
				<div class="btn_slide_down media_no_mobile"><a href="#" class="next-navigation"></a></div>
				<div class="btn_slide_left media_mobile"><a href="#" class="prev-navigation"><i class="fa fa-caret-left" aria-hidden="true"></i></a></div>
				<div class="btn_slide_right media_mobile"><a href="#" class="next-navigation"><i class="fa fa-caret-right" aria-hidden="true"></i></a></div>
				<div class="carousel-wrapper">
					<div class="carousel-items" style="top: 0px;">
						<?$k=-1;?>
						<?if(!empty($arResult["DETAIL_PICTURE"])){?>
							<div class="carousel-block" data-index="<?=$k+1?>">
								<img itemprop="image" src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="">
							</div>
						<?}elseif(!empty($arResult["PREVIEW_PICTURE"])){?>
							<div class="carousel-block" data-index="<?=$k+1?>">
								<img itemprop="image" src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" alt="">
							</div>
						<?}?>
						<?foreach($arResult["PROPERTIES"]["IMAGES"]["VALUE"] as $k => $image):?>	
							<div class="carousel-block" data-index="<?=$k+1?>">
								<img itemprop="image" src="<?=$image["URL"]?>" alt="">
							</div>
						<?endforeach;?>
					</div>
				</div>
			</div>
			<div class="stage">
				<div class="carousel carousel-stage">
					<ul style="left: 0px;">
						<?if(!empty($arResult["DETAIL_PICTURE"])){?>
							<li class="active_tab">
								<a href="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" rel="gallery1" class="gall block left fancybox" style="position: relative;">
									<div style="background-image: url('<?=$arResult["DETAIL_PICTURE"]["SRC"]?>');background-repeat: no-repeat;background-position: 50%;background-size: contain;"></div>
								</a>
							</li>
						<?}elseif(!empty($arResult["PREVIEW_PICTURE"])){?>
							<li class="active_tab">
								<a href="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" rel="gallery1" class="gall block left fancybox" style="position: relative;">
									<div style="background-image: url('<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>');background-repeat: no-repeat;background-position: 50%;background-size: contain;"></div>
								</a>
							</li>
						<?}?>
						<?foreach($arResult["PROPERTIES"]["IMAGES"]["VALUE"] as $k => $image):?>
						<li class="active_tab">
							<a href="<?=$image["URL"]?>" rel="gallery1" class="gall block left fancybox" style="position: relative;">
								<div style="background-image: url('<?=$image["URL"]?>');background-repeat: no-repeat;background-position: 50%;background-size: contain;"></div>
							</a>
						</li>
						<?endforeach;?>
					</ul>
				</div>
			</div>
		</div>
		
		<?
			$price = CPrice::GetList(
				array(),
				array("PRODUCT_ID" => $arResult["ID"],)
			);
			if ($ar_res = $price->Fetch())
			{
			    $pric = CurrencyFormat($ar_res["PRICE"], $ar_res["CURRENCY"]);
			}
		?>

		<div class="left_side" >
			<meta itemprop="description" content="<?=$arResult["IPROPERTY_VALUES"]["ELEMENT_META_TITLE"];?>">

			<div id="success" class="success" <?if(!empty($arResult["PROPERTIES"]["NEEDED"]["VALUE"])){?> style="height:400px;display:none;" <?}else{?>  style="display:none;" <?}?>>
				<div class="goods-<?=$arResult["ID"]?>"><a class="close" onclick="closee()"></a>
					<?if(is_array($arResult["PREVIEW_PICTURE"])):?>
						<?$file = CFile::ResizeImageGet($arResult["PREVIEW_PICTURE"], array('width'=>213, 'height'=>140), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
						<a href="<?=$arResult["DETAIL_PAGE_URL"]?>" class="list_preview_image">
							<?if($APPLICATION->GetCurPage() == "/catalog/osnastka/buri-koronki-sds-plus/bury-sds-plus-5x/") ?>
								<img <?if(!empty($price_bas)) {?>itemprop="image"<? }?> style="<?if($this->GetEditAreaId($arElement['ID']) == "bx_1970176138_131632") {?> height:102px; <?}?>display: block; margin: 0 auto;" src="<?=$file['src']?>" alt=""></a>
							<?elseif(is_array($arResult["DETAIL_PICTURE"])):?>
								<?$file = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"], array('width'=>213, 'height'=>140), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
								<a class="image_action" href="<?=$arResult["DETAIL_PAGE_URL"]?>"> <?if($APPLICATION->GetCurPage() == "/catalog/osnastka/buri-koronki-sds-plus/bury-sds-plus-5x/") ?><img <?if(!empty($price_bas)) {?>itemprop="image"<? }?> style="display: block; margin: 0 auto;" src="<?=$file['src']?>" alt=""></a>
							<?else:?>
								<a class="image_action" href="<?=$arResult["DETAIL_PAGE_URL"]?>"> <?if($APPLICATION->GetCurPage() == "/catalog/osnastka/buri-koronki-sds-plus/bury-sds-plus-5x/") ?><img <?if(!empty($price_bas)) {?>itemprop="image"<? }?> style="display: block; margin: 0 auto;" src="<?=SITE_TEMPLATE_PATH."/images/no_photo.jpg"?>" alt=""></a>
							<?endif?>
						<div class="description">
							<a href="<?=$arResult["DETAIL_PAGE_URL"]?>" class="pName"  style="color: inherit;"><p><?=$arResult["NAME"]?></p>
							<p class="add cart-test" style="display:none"><strong style="color:green;">Товар добавлен в корзину</strong></p>
							<p class="add favor" style="display:none"><strong style="color:green;">Товар добавлен в список</strong><strong style="color:green;display:none">Для того, чтобы "запомнить" товар необходимо авторизоваться. Если у вас еще нет учетной записи, Вы можете зарегистрироваться прямо сейчас!"</strong></p>
							<p class="add compar" style="display:none"><strong style="color:green;">Товар добавлен в сравнение</strong></p></a> 
						</div> 
						<div class="compare-btn">
							<span class="buttons grey_btn" onclick="closee()">Продолжить просмотр</span>
							<a class="cart-test" href="/personal/cart" style="display:none"><span class="buttons">Перейти в корзину</span></a>
							<a class="favor" href="/favorites" style="display:none">Перейти в список</a>
							<a class="compar" href="/compare" style="display:none">Перейти к сравнению</a>
							<a class="regno" style="display:none" href="/login">Зарегистрироваться/вход</a>
						</div>

						<?if(!empty($arResult["PROPERTIES"]["NEEDED"]["VALUE"])){?>					
							<div class="necessary cart-test jcarousel sl_<?=$arResult["ID"];?>">  
								<h3 style="font-size:16px;margin-bottom:10px;">Дополнительная комплектация к выбранному товару:</h3>									
								<div class="jcarousel-wrapper">
									<ul class="bxslider" id="tiny_<?=$arElement["ID"];?>" >
										<?foreach ($arResult["PROPERTIES"]["NEEDED"]["VALUE"] as $key=>$needed){?>
											<li class="product_necess">
												<a class="link" href="<?=$needed["DETAIL_PAGE_URL"]?>">
													<div class="image_div">
														<img src="<?=$needed["IMG_URL"];?>">
													</div>
													<div class="teod"><?=$needed["NAME"]?></div> 
													<p class="article">Артикул: <?=$needed["ARTNUMBER"]["VALUE"];?></p>
													<span class="grey_pr"><?=FormatCurrency($needed["PRICE"]['PRICE'], $needed["PRICE"]['CURRENCY'])?></span>
													<?$cur = $needed["PRICE"]['PRICE']*$basket_discount;?>
													<div class="red_small_pr"><?=FormatCurrency($cur, $needed["PRICE"]['CURRENCY'])?></div>
													<?if($needed["PRICE"]['PRICE']>0):?>
														<form id="<?=($needed["ID"]);?>" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
														<input type="hidden" name="<?=$arParams["ACTION_VARIABLE"]?>" value="BUY">
														<input type="hidden" name="<?=$arParams["PRODUCT_ID_VARIABLE"]?>" value="<?=($needed["ID"]);?>">
														<!--input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."BUY"?>" value="<?echo GetMessage("CATALOG_BUY")?>"-->
														<input type="hidden" name="<?=$arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?=GetMessage("CATALOG_ADD_TO_BASKET")?>">
														</form>														
													<?endif;?>
												</a>
												<?if($needed["PRICE"]['PRICE']>0):?>
													<a class="addtocart needed_buy" onclick="yaCounter28820320.reachGoal('VK_KART');" href="#" ids="need" id="<?=($needed["ID"])?>" onclick="yaCounter28820320.reachGoal('VK_DOP_KART');">Купить</a>
												<?endif;?>
											</li>									
										<?}?>
									</ul>					
								</div>								
							</div>

							<script type="text/javascript">
								function initSlider(el) {
									var sl = $(el)
									sl.bxSlider({
										mode: 'vertical',
										adaptiveHeight: true,
										maxSlides: 3,
										minSlides: 3,
										moveSlides: 1,
										pager: false,
										preloadImages: "all"
									});								   
									var child = $(el + " .product_necess:not(.bx-clone)");
									if(child.length > 0)
									{
										var px = child.length < 3 ? (child.length*97) + "px" : "291px";
										sl.parent().css("minHeight", px)
										sl.css('transform', 'translate3d(0px, -' + px + ', 0px)');
										if(child.length == 2) 
										{											
											$(".bx-next").css("top", "195px !important;");
										}
									}        
								}
							</script>					
						<?}?>
				</div>
			</div>

			<?/*------ отзывы и запомнить ------*/?>
			<div class="wishlist">
				<div class="compare">
					<?
					$iblockid = $arElement['IBLOCK_ID'];
					$id=$arElement['ID'];
					if(isset($_SESSION["CATALOG_COMPARE_LIST"][$iblockid]["ITEMS"][$id]))
					{
						$checked='checked';
					}
					else
					{
						$checked='';
					}
					?>
					<input <?=$checked;?> type="checkbox" id="compareid_<?=$arResult['ID'];?>" onchange="compare_tov(<?=$arResult['ID'];?>);"> 
					<label for="compareid_<?=$arResult['ID'];?>"><i class="fa fa-circle-o media_mobile" aria-hidden="true"></i><i class="fa fa-check-circle-o media_mobile" aria-hidden="true"></i> Сравнить</label>
			   </div>			   
				<div class="favorites_container media_no_mobile"><p>запомнить товар</p>
					<a class="favorites inline_block" onclick="favorites(this)" data-element_id="<?=$arResult["ID"]?>">
						<div class="added" style="<?=(!$arResult['IN_FAVORITES']) ? 'display:none' : ''?>" title="В избранном"></div>
						<div class="add" style="<?=($arResult['IN_FAVORITES']) ? 'display:none' : ''?>" title="Добавить в избранное"></div>
						<div class="loading"></div>
					</a>
				</div>
				<div class="favorites_container media_mobile">    
					<a class="favorites inline_block" onclick="favorites(this)" data-element_id="<?=$arResult['ID']?>">
						<div class="added" style="<?=(!$arElement['IN_FAVORITES']) ? 'display:none' : ''?>" title="В избранном"><i class="fa fa-heart" aria-hidden="true"></i></div>
						<div class="add" style="<?=($arElement['IN_FAVORITES']) ? 'display:none' : ''?>" title="Добавить в избранное"><i class="fa fa-heart" aria-hidden="true"></i></div>
						<span>В избранное</span>
					</a>     
				</div>
			</div>
<?/*------ отзывы и запомнить ------*/?>

	<h1 itemprop="name"><?=$arResult["NAME"]?></h1>
	
	<div style="display:none;"itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
		<meta itemprop="priceCurrency" content="RUB">
		<meta itemprop="price" content="<?=$pric?>">
	</div>
	
	<div class="grey_block">
		<?if(!empty($arResult["OFFERS"])):?>
			<?foreach($arResult["OFFERS"] as $keyoff=>$off):?>  
				<div class="artic hide tabs_<?=$off["ID"]?>">
					<p class="media_no_mobile">Артикул:</p><p class="media_mobile">Арт:</p><span><?=$off["PROPERTIES"]["ARTNUMBER"]["VALUE"]?></span>
					<a href="http://www.bosch-pt.ru" target="_blank">Сайт производителя</a>
				</div>
			<?endforeach;?>
			<p>Комплектация:</p>
			<select class="offers_hide">
				<?foreach($arResult["OFFERS"] as $ofkey => $offers):?>
					<option value="<?=$offers["ID"]?>">
						<?if($offers["PROPERTIES"]["NAME"]["VALUE"]!=""){?>
							<?=$offers["PROPERTIES"]["NAME"]["VALUE"];?>
						<?}else{?>
							<?=$offers["NAME"]?>
						<?}?>
					</option>
				<?endforeach;?>
			</select>
		<?else:?>
			<div class="artic">
				<p class="media_no_mobile">Артикул:</p><p class="media_mobile">Арт:</p><span><?=$arResult["PROPERTIES"]["ARTNUMBER"]["VALUE"];?></span>
				<a href="http://www.bosch-pt.ru" target="_blank">Сайт производителя</a>
			</div>
		<?endif;?>
	</div>
	
	<?if(!empty($arResult["PROPERTIES"]["KOMPL_POST"]["VALUE"])):?>
		<p style="color: #333;" class="media_no_mobile">Подробнее о комплектации читайте на вкладке <span>"Комплект поставки".</span></p>
	<?endif;?>
	
	<?if(!empty($arResult["OFFERS"])){?>
		<?foreach($arResult["OFFERS"] as $keyoff=>$off):?>
			<?$ID = $off['ID'];
			/*$amount = 0;
			$mult = $arResult['PROPERTIES']['STATUS']['VALUE'];
			if($mult>0)
				$amount = 2;
			foreach($arResult["PRICES"] as $code=>$arPrice):
				if($arPrice["VALUE"]>0 && $amount==0){
					$amount = 2;
				}
			endforeach;
			$rsStoreNsk = CCatalogStoreProduct::GetList(array(), array('PRODUCT_ID' =>$ID, 'STORE_ID' => 1), false, false, array()); 
			$rsStoreBsh = CCatalogStoreProduct::GetList(array(), array('PRODUCT_ID' =>$ID, 'STORE_ID' => 2), false, false, array());
			if ($arStoreNsk = $rsStoreNsk->Fetch())
				if($arStoreNsk['AMOUNT']>0 || $off['CATALOG_QUANTITY']>0)
					$amount = 1;
			if ($arStoreBsh = $rsStoreBsh->Fetch())
				if($arStoreBsh['AMOUNT']>0 && $amount == 0)
					$amount = 2;
			*/
			?>
			<div class="hide tabs_<?=$off["ID"]?>">
				<div class="prise"  >					
					<?
					$price_id_1 = false;
					$price_bas_off = false;
					$price_disc_off = false;
					foreach($off["PRICES"] as $pri_off){
						if($pri_off["PRICE_ID"]==2)
							$price_bas_off = $pri_off;
						elseif($pri_off["PRICE_ID"]==3)   
							$price_disc_off = $pri_off;
						elseif($pri_off["PRICE_ID"] == 1)
						{
							$price_id_1 = $pri_off;
						}
					}
					if (!$price_bas_off && $price_id_1)
					{
						$price_bas_off = $price_id_1;
					}
					/*if ($price_disc_off["VALUE"] == $price_bas_off["VALUE"])
					{
						unset ($price_disc_off);
					}*/
					?>
					<?//foreach($off["PRICES"] as $code=>$arPrice):?>
					<span>
						<?if($price_bas_off["CAN_ACCESS"]):?>
							<?if($arParams["PRICE_VAT_SHOW_VALUE"] && ($price_bas_off["VATRATE_VALUE"] > 0)):?>
								<?if($arParams["PRICE_VAT_INCLUDE"]):?>
									(<?echo GetMessage("CATALOG_PRICE_VAT")?>)
								<?else:?>
									(<?echo GetMessage("CATALOG_PRICE_NOVAT")?>)
								<?endif?>
							<?endif;?>
							<?if($price_bas_off["DISCOUNT_VALUE"] < $price_bas_off["VALUE"]):?>
								<span class="cost" style="text-decoration: line-through;"><?=str_replace("р.","руб.", $price_bas_off["PRINT_VALUE"])?></span></br>
								</span>
								<div class="red_prise"><?=str_replace("р.","руб.", $price_bas_off["PRINT_DISCOUNT_VALUE"])?> -</div><p>цена<br />со скидкой</p>
								<?/*	<span class="cost"><?=$arPrice["PRINT_DISCOUNT_VALUE"]?></span>*/?>
							<?else:?>
								<?if($price_disc_off["VALUE"] > 0){?> 
									<?=str_replace("р.","руб.", $price_bas_off["PRINT_VALUE"])?>
									</span>
									<div class="red_prise"><?echo(str_replace("р.","руб.", $price_disc_off["PRINT_VALUE"]));?> -</div><p>цена при покупке за наличный расчет</p>
								<?}else{?>
									<?echo(str_replace("р.","руб.",$price_bas_off["PRINT_VALUE"]));?>
									</span>
									<div class="red_prise"><?echo str_replace("р.","руб.", FormatCurrency($price_bas_off["VALUE"] * $basket_discount, $price_bas_off["CURRENCY"]));?></div>
								<?}?>
							<?endif?>
						<?endif;?>
					<?//endforeach;?>					
				</div>
				
				<?if (intval($arResult["PROPERTIES"]["MULTI"]["VALUE"])>1){?>
					<p>Кратность товара в заказе <?=$arResult["PROPERTIES"]["MULTI"]["VALUE"]?> шт.</p>
				<?}?>
				
				<div id="tabs_<?=$off["ID"]?>" class="tocart hide tabs_<?=$off["ID"]?>">
					<?if($off["CAN_BUY"]):?>
						<?if($arParams["USE_PRODUCT_QUANTITY"] || count($arResult["PRODUCT_PROPERTIES"])):?>
							<form id="<?=$off["ID"]?>" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
							<input type="hidden" name="quantity" value="<?=$multi?>" class="quantity">
							<input type="hidden" name="<? echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
							<input type="hidden" name="<? echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?=$off["ID"]?>">
							<!--input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."BUY"?>" value="<?echo GetMessage("CATALOG_BUY")?>"-->
							<input type="hidden" name="<? echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?=$off["ID"]?>">
							</form>
						<?else:?>
							<noindex>
							<a href="<?echo $arResult["BUY_URL"]?>" rel="nofollow"><?echo GetMessage("CATALOG_BUY")?></a>
							 <a href="<?echo $arResult["ADD_URL"]?>" rel="nofollow"><?echo GetMessage("CATALOG_ADD_TO_BASKET")?></a>
							</noindex>
						<?endif;?>
					<?elseif((count($arResult["PRICES"]) > 0) || is_array($arResult["PRICE_MATRIX"])):?>
						<?=GetMessage("CATALOG_NOT_AVAILABLE")?>
						<?$APPLICATION->IncludeComponent("bitrix:sale.notice.product", ".default", array(
							"NOTIFY_ID" => $arResult['ID'],
							"NOTIFY_URL" => htmlspecialcharsback($arResult["SUBSCRIBE_URL"]),
							"NOTIFY_USE_CAPTHA" => "N"
							),
							$component
						);?>
					<?endif?>
					<a class="addtocart" href="#" onclick="initSlider('#tiny_<?=$arElement["ID"];?>');yaCounter28820320.reachGoal('VK_KART');" id="<?=$off["ID"]?>">Купить</a>
				</div>
				<a href="#" data-remodal-target="modal-oneclick-<?=$off["ID"]?>" class="buy1" onclick="yaCounter28820320.reachGoal('START_BUY_ONE_CLICK'); return true;">купить в 1 клик</a>
				<!--<img src="<?=SITE_TEMPLATE_PATH?>/images/maincontent/grafic.png" alt="">-->
				<div class="remodal" data-remodal-id="modal-oneclick-<?=$off["ID"]?>">
					<?$APPLICATION->IncludeComponent(
						"h2o:buyoneclick",
						"main",
						Array(
							"ASD_ID" => $off["ID"],
							"ASD_TITLE" => $_REQUEST["title"],
							"ASD_URL" => $_REQUEST["url"],
							"ASD_PICTURE" => $_REQUEST["picture"],
							"ASD_TEXT" => $_REQUEST["text"],
							"ASD_LINK_TITLE" => "Расшарить в #SERVICE#",
							"ASD_SITE_NAME" => "",
							"ASD_INCLUDE_SCRIPTS" => array()
						)
					);?>
				</div>
				<?/*if($arResult['SECTION']["PATH"][0]['ID'] == 1797):*/?>
					<?$APPLICATION->IncludeFile ("/include/service_address.php");?>
				<?/*else:?>
					<p>г. Новосибирск, ул. Красноярская, д. 36, цокольный этаж</p>
				<?endif;*/?>
				<hr class="underline">
				<div class="status_block">
					<?
					$arDelivery = AGGetDeliveryTime($off["ID"]);
					if ($_SESSION["CURRENT_CITY"]["NAME"] == "Новосибирск")
					{?>
					<div class="status">
						<span class="status_left"><?=$off["PROPERTIES"]["NSK"]["NAME"]?></span>
						<span class="status_right" style="color: #6a983c"><b><?=$arDelivery["NSK"];?></b></span>
					</div>
					<?}?>
					<div class="status">
						<span class="status_left"><?=$off["PROPERTIES"]["OMSK"]["NAME"]?></span>
						<span class="status_right" style="color: #6a983c"><b><?=$arDelivery["OMSK"];?></b></span>
					</div>
				</div>
			</div>
		<?endforeach;?>
		
	<?}else{?>
		<?if(!empty($price_bas)){?>
			<div class="prise">
				<div class="media_no_mobile">
					<?//foreach($arResult["PRICES"] as $code=>$arPrice):?>
					<span style="max-width: 110px;">
						<?if($price_bas["CAN_ACCESS"]):?>
							<?if($arParams["PRICE_VAT_SHOW_VALUE"] && ($price_bas["VATRATE_VALUE"] > 0)):?>
								<?if($arParams["PRICE_VAT_INCLUDE"]):?>
									(<?echo str_replace("р.","руб.",GetMessage("CATALOG_PRICE_VAT"))?>)
								<?else:?>
									(<?echo str_replace("р.","руб.",GetMessage("CATALOG_PRICE_NOVAT"))?>)
								<?endif?>
							<?endif;?>
							<?if($price_bas["DISCOUNT_VALUE"] < $price_bas["VALUE"]):?>
								<span class="cost" style="text-decoration: line-through;"><?=str_replace("р.", "руб.", $price_bas["PRINT_VALUE"])?></span></br>
								</span>
								<div class="red_prise"><?=str_replace("р.", "руб.", $price_bas["PRINT_DISCOUNT_VALUE"])?> -</div><p>цена<br />со скидкой</p>
							<?else:?>
								<?if($price_disc["VALUE"] > 0){?> 
									<?=str_replace("р.", "руб.",$price_bas["PRINT_VALUE"])?>
									</span>
									<div class="red_prise">
										<?echo(str_replace("р.","руб.",$price_disc["PRINT_VALUE"]));?> -
									</div>
									<p>цена при покупке за наличный расчет</p>
								<?}else{?>
									<?echo(str_replace("р.","руб.",$price_bas["PRINT_VALUE"]));?>
									</span>
									<?$cur = $price_bas["VALUE"]*$basket_discount;?>
									<div class="red_prise">
										<?=str_replace("р.","руб.",FormatCurrency($cur, $price_bas['CURRENCY']))?>
									</div>
									<p>цена при покупке за наличный расчет</p>
								<?}?>
							<?endif?>
						<?endif;?>
					<?//endforeach;?>
				</div>
				<div class="media_mobile">
					<?//foreach($arResult["PRICES"] as $code=>$arPrice):?>
					
						<?if($price_bas["CAN_ACCESS"]):?>
							<?if($arParams["PRICE_VAT_SHOW_VALUE"] && ($price_bas["VATRATE_VALUE"] > 0)):?>
								<?if($arParams["PRICE_VAT_INCLUDE"]):?>
									(<?echo str_replace("р.","руб.",GetMessage("CATALOG_PRICE_VAT"))?>)
								<?else:?>
									(<?echo str_replace("р.","руб.",GetMessage("CATALOG_PRICE_NOVAT"))?>)
								<?endif?>
							<?endif;?>
							<?if($price_bas["DISCOUNT_VALUE"] < $price_bas["VALUE"]):?>
								<span>
									<span class="cost" style="text-decoration: line-through;"><?=str_replace("р.", "руб.", $price_bas["PRINT_VALUE"])?></span></br>
								</span>
								<div class="red_prise"><?=str_replace("р.", "руб.", $price_bas["PRINT_DISCOUNT_VALUE"])?> -</div><p>цена<br />со скидкой</p>
							<?else:?>
								<?if($price_disc["VALUE"] > 0){?> 
									<span>Цена:</span>									
									<div class="red_prise">
										<?echo(str_replace("р.","<span>руб.</span>",$price_disc["PRINT_VALUE"]));?>
									</div>
									<span class="gray_prise"><?=str_replace("р.", "руб.",$price_bas["PRINT_VALUE"])?></span>
									<p>цена при покупке за наличный расчет</p>
								<?}else{?>
									<span>Цена:</span>	
									<?$cur = $price_bas["VALUE"]*$basket_discount;?>
									<div class="red_prise">
										<?=str_replace("р.","<span>руб.</span>",FormatCurrency($cur, $price_bas['CURRENCY']))?>
									</div>
									<span class="gray_prise"><?echo(str_replace("р.","руб.",$price_bas["PRINT_VALUE"]));?></span>									
									<p>цена при покупке за наличный расчет</p>
								<?}?>
							<?endif?>
						<?endif;?>
					<?//endforeach;?>
				</div>
			</div>
			
			<?if (intval($arResult["PROPERTIES"]["MULTI"]["VALUE"])>1){?>
				<p>Кратность товара в заказе <?=$arResult["PROPERTIES"]["MULTI"]["VALUE"]?> шт.</p>
		<?}?>
			
			<div class="tocart">
				<?if(($arResult["CAN_BUY"])!=""):?>
					<?//if($arParams["USE_PRODUCT_QUANTITY"] || count($arResult["PRODUCT_PROPERTIES"])):?>
						<form id="<? if(empty($arResult["OFFERS"])){echo($arResult["ID"]);}else{echo($arResult["OFFERS"][0]["ID"]);}?>" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
						<input type="hidden" name="quantity" value="<?=$multi?>" class="quantity">
						<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
						<input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<? if(empty($arResult["OFFERS"])){echo($arResult["ID"]);}else{echo($arResult["OFFERS"][0]["ID"]);}?>">
						<!--input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."BUY"?>" value="<?echo GetMessage("CATALOG_BUY")?>"-->
						<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("CATALOG_ADD_TO_BASKET")?>">
						</form>
					<?/*else:?>
						<noindex>
						<a href="<?echo $arResult["BUY_URL"]?>" rel="nofollow"><?echo GetMessage("CATALOG_BUY")?></a>
						 <a href="<?echo $arResult["ADD_URL"]?>" rel="nofollow"><?echo GetMessage("CATALOG_ADD_TO_BASKET")?></a>
						</noindex>
					<?endif;*/?>
				<?elseif((count($arResult["PRICES"]) > 0) || is_array($arResult["PRICE_MATRIX"])):?>
					<?=GetMessage("CATALOG_NOT_AVAILABLE")?>
					<?$APPLICATION->IncludeComponent("bitrix:sale.notice.product", ".default", array(
						"NOTIFY_ID" => $arResult['ID'],
						"NOTIFY_URL" => htmlspecialcharsback($arResult["SUBSCRIBE_URL"]),
						"NOTIFY_USE_CAPTHA" => "N"
						),
						$component
					);?>
				<?endif?>
				<a class="addtocart" href="#" onclick="initSlider('#tiny_<?=$arElement["ID"];?>');yaCounter28820320.reachGoal('VK_KART');" id="<? if(empty($arResult["OFFERS"])){echo($arResult["ID"]);}else{echo($arResult["OFFERS"][0]["ID"]);}?>">Купить</a>
				
				<!-- <a href="#" data-remodal-target="modal-oneclick-<?=$arResult["ID"]?>" class="buy1">купить в 1 клик</a> -->				
				<!-- <div class="remodal" data-remodal-id="modal-oneclick-<?=$arResult["ID"]?>">
					<?/* $APPLICATION->IncludeComponent(
						"h2o:buyoneclick",
						"main",
						Array(
							"ASD_ID" => $arResult["ID"],
							"ASD_TITLE" => $_REQUEST["title"],
							"ASD_URL" => $_REQUEST["url"],
							"ASD_PICTURE" => $_REQUEST["picture"],
							"ASD_TEXT" => $_REQUEST["text"],
							"ASD_LINK_TITLE" => "Расшарить в #SERVICE#",
							"ASD_SITE_NAME" => "",
							"ASD_INCLUDE_SCRIPTS" => array()
						)
					); */?>
				</div> -->
			</div>
			<div class="cardIcon">
				<div class="ico-visa"><a href="/about/delivery/#cardPayment" target="_blank"></a></div>
				<div class="ico-masterCard"><a href="/about/delivery/#cardPayment" target="_blank"></a></div>
				<div class="ico-mir"><a href="/about/delivery/#cardPayment" target="_blank"></a></div>
			</div>
			
			
		<?}?>
		
		<?/*if($arResult['SECTION']["PATH"][0]['ID'] == 1797):*/?>
		<div class="media_no_mobile">
			<?$APPLICATION->IncludeFile ("/include/service_address.php");?>
			<?/*else:?>
				<p>г. Новосибирск, ул. Красноярская, д. 36, цокольный этаж</p>
			<?endif;*/?>
			<hr class="underline">
		</div>
		
		<div class="status_block">
			<p class="media_mobile">Наличие на складе:</p>
			<?
			$arDelivery = AGGetDeliveryTime ($arResult["ID"]);
			if ($_SESSION["CURRENT_CITY"]["NAME"] == "Новосибирск")
			{?>
			<div class="status">
				<span class="status_left media_no_mobile"><?=$arResult["PROPERTIES"]["NSK"]["NAME"]?></span>
				<span class="status_left media_mobile">г. Новосибирск</span>
				<span class="status_right" style="color: #6a983c"><b><?=$arDelivery["NSK"];?></b></span>
			</div>
			<?}?>
			<div class="status">
				<span class="status_left media_no_mobile"><?=$arResult["PROPERTIES"]["OMSK"]["NAME"]?></span>
				<span class="status_left media_mobile">г. Омск</span>
				<span class="status_right" style="color: #6a983c"><b><?=$arDelivery["OMSK"];?></b></span>
			</div>
		</div>
	<?}?>			
	</div>
	
	
	<? $tab_counter = 0 ?>	
	<div class="tabs">
		<div class="menu_product">
			<ul>
				<?if ($arResult["DETAIL_TEXT"]) {?>
					<li><a href="#fragment-0">Описание</a></li>
				<?}?>
				<?if ($arResult["PROPERTIES"]["ADVANTAGES"]["VALUE"]["TEXT"] || $arResult["PROPERTIES"]["DOP_PREIM"]["VALUE"]["TEXT"]){?>
					<li><a href="#fragment-1">Преимущества</a></li>
				<?}?>
				<li><a href="#fragment-2">Характеристики</a></li>
				<?if (!empty($arResult["PROPERTIES"]["VIDEO"]["~VALUE"])){?>
					<li><a href="#fragment-3">Видео</a></li>
				<?}
				if ($arResult["PROPERTIES"]["LEADERSHIP"]["VALUE"][0] > 0){?>
					<li><a href="#fragment-4">Руководства</a></li>
				<?}?>
				<li><a href="#fragment-5">Гарантии</a></li>
				<?if(!empty($arResult["PROPERTIES"]["KOMPL_POST"]["VALUE"]) || !empty($arResult["OFFERS"])){?>
					<li ><a href="#fragment-6" >Комплект поставки</a></li>
				<?}?>
					<li id="reviews"><a href="#fragment-7">Отзывы</a></li>
			</ul>			
		</div>
		
		<?if ($arResult["DETAIL_TEXT"]) {?>
			<span class="media_mobile mobile_tabs_btn" data-mobiletab-id="<? echo $tab_counter ?>">Описание</span>
			<div class="content_menu" id="fragment-0">
				<?=$arResult["DETAIL_TEXT"]?>
			</div>
			<? $tab_counter++ ?>
		<?}?>
		
		<?if ($arResult["PROPERTIES"]["ADVANTAGES"]["VALUE"]["TEXT"] || $arResult["PROPERTIES"]["DOP_PREIM"]["VALUE"]["TEXT"]){?>
			<span class="media_mobile mobile_tabs_btn" data-mobiletab-id="<? echo $tab_counter ?>">Преимущества</span>
			<div class="content_menu" id="fragment-1">
				<?if($arResult["PROPERTIES"]["ADVANTAGES"]["VALUE"]["TEXT"]):?>
					<?$onlyconsonants = str_replace("&amp;amp;amp;amp;", "&", $arResult["PROPERTIES"]["ADVANTAGES"]["VALUE"]["TEXT"]);
					$onlyconsonants = str_replace("&amp;amp;amp;", "&", $onlyconsonants);
					$onlyconsonants = str_replace("&amp;amp;", "&", $onlyconsonants);
					$onlyconsonants = str_replace("&amp;", "&", $onlyconsonants);
					$onlyconsonants=(htmlspecialchars_decode($onlyconsonants));?>
					<?=htmlspecialchars_decode($onlyconsonants);?>
				<?endif;?>
				<?if($arResult["PROPERTIES"]["DOP_PREIM"]["VALUE"]["TEXT"]):?>
					<?$onlyconsonants = str_replace("&amp;amp;amp;amp;", "&", $arResult["PROPERTIES"]["DOP_PREIM"]["VALUE"]["TEXT"]);
					$onlyconsonants = str_replace("&amp;amp;amp;", "&", $onlyconsonants);
					$onlyconsonants = str_replace("&amp;amp;", "&", $onlyconsonants);
					$onlyconsonants = str_replace("&amp;", "&", $onlyconsonants);
					$onlyconsonants=(htmlspecialchars_decode($onlyconsonants));?>
					<?=htmlspecialchars_decode($onlyconsonants);?>
				<?endif;?>
			</div>
			
			<? $tab_counter++ ?>
		<?}?>
			<span class="media_mobile mobile_tabs_btn" data-mobiletab-id="<? echo $tab_counter ?>">Характеристики</span>
			<div class="content_menu" id="fragment-2">
				<table class="tab_table">
					<?
					$bComplectShow = true;
					foreach($arResult["PROP_CHAR"] as $propChar):
						if($arResult["PROPERTIES"][$propChar]["NAME"]=='Артикул') continue;
						if($arResult["PROPERTIES"][$propChar]["NAME"]=='ean') continue;
						if($arResult["PROPERTIES"][$propChar]["NAME"]=='Статус') continue;
						$bComplectShow = false;
						?>
						<tr>
							<td><?=$arResult["PROPERTIES"][$propChar]["NAME"]?></td>
							<td><?=$arResult["PROPERTIES"][$propChar]["VALUE"]?></td>
						</tr>
					<?endforeach;?>
				</table>
			</div>
			<? $tab_counter++ ?>
		<?if (!empty($arResult["PROPERTIES"]["VIDEO"]["~VALUE"])){?>	
			<span class="media_mobile mobile_tabs_btn" data-mobiletab-id="<? echo $tab_counter ?>">Видео</span>
			<div class="content_menu" id="fragment-3">
				<?foreach($arResult["PROPERTIES"]["VIDEO"]["~VALUE"] as $video):?>
						<div style="margin: 15px;"><?=htmlspecialchars_decode($video["TEXT"]);?></div>
					<?endforeach;?>
			</div>
			<? $tab_counter++ ?>
		<?}
		if ($arResult["PROPERTIES"]["LEADERSHIP"]["VALUE"][0] > 0){?>			
			<span class="media_mobile mobile_tabs_btn" data-mobiletab-id="<? echo $tab_counter ?>">Руководства</span>
			<div class="content_menu" id="fragment-4">
				<?$rsFile = CFile::GetByID($arResult["PROPERTIES"]["LEADERSHIP"]["VALUE"][0]);
				$arFile = $rsFile->Fetch();
				$arFile["PATH"] = CFile::GetPath($arFile["ID"]);?>
				<ul class="adv">
					<li>
						<a href="<?=$arFile["PATH"]?>" target="_blank"><?if ($arFile["DESCRIPTION"] != "") echo $arFile["DESCRIPTION"]; else echo $arFile['ORIGINAL_NAME']?> (<?=round($arFile['FILE_SIZE']/1000000, 1)?> MB)</a>
					</li>
				</ul>
			</div>
			<? $tab_counter++ ?>
		<?}?>			
			<span class="media_mobile mobile_tabs_btn" data-mobiletab-id="<? echo $tab_counter ?>">Гарантии</span>
			<div class="content_menu" id="fragment-5">
				<? $APPLICATION->IncludeFile( '/include/garantii.php', array(),	array()); ?>
			</div>
			<? $tab_counter++ ?>
			<?if(!empty($arResult["PROPERTIES"]["KOMPL_POST"]["VALUE"]) || !empty($arResult["OFFERS"])){?>
				<span class="media_mobile mobile_tabs_btn" data-mobiletab-id="<? echo $tab_counter ?>">Комплект поставки</span>
				<div class="content_menu" id="fragment-6">
					<?if(!empty($arResult["OFFERS"])){
						foreach($arResult["OFFERS"] as $keyoff=>$off):?>
							<div class="hide tabs_<?=$off["ID"]?>">
								<?$onlyconsonants = str_replace("&amp;amp;amp;amp;", "&", $off["PROPERTIES"]["KOMPLEKT"]["VALUE"]["TEXT"]);
								$onlyconsonants = str_replace("&amp;amp;amp;", "&", $onlyconsonants);
								$onlyconsonants = str_replace("&amp;amp;", "&", $onlyconsonants);
								$onlyconsonants = str_replace("&amp;", "&", $onlyconsonants);
								$onlyconsonants=(htmlspecialchars_decode($onlyconsonants));?>
								<?=htmlspecialchars_decode($onlyconsonants);?>
							</div>
						<?endforeach;
					}else{?>
						<?if($arResult["PROPERTIES"]["KOMPL_POST"]["VALUE"]["TEXT"] !=""):?>
							<?$onlyconsonants = str_replace("&amp;amp;amp;amp;", "&", $arResult["PROPERTIES"]["KOMPL_POST"]["VALUE"]["TEXT"]);
							$onlyconsonants = str_replace("&amp;amp;amp;", "&", $onlyconsonants);
							$onlyconsonants = str_replace("&amp;amp;", "&", $onlyconsonants);
							$onlyconsonants = str_replace("&amp;", "&", $onlyconsonants);
							$onlyconsonants=(htmlspecialchars_decode($onlyconsonants));?>
							<?=htmlspecialchars_decode($onlyconsonants);?>
						<?endif;?>
					<?}?>
				</div>
				<? $tab_counter++ ?>
			<?if ($bComplectShow){?>
			<script>
				document.getElementById("ui-id-7").onclick();
			</script>
			<?}
		}?>
		<span class="media_mobile mobile_tabs_btn" data-mobiletab-id="<? echo $tab_counter ?>">Отзывы</span>
		<div class="content_menu" id="fragment-7"> 
			<div class="b-new-review">
				<div class="yes-review">  
					<?$APPLICATION->IncludeComponent(
					"bitrix:catalog.comments",
					"",
					array(
					"ELEMENT_ID" => $arResult['ID'],
					"ELEMENT_CODE" => "",
					"IBLOCK_ID" => $arParams['IBLOCK_ID'],
					"SHOW_DEACTIVATED" => "N",
					"URL_TO_COMMENT" => "",
					"WIDTH" => "",
					"COMMENTS_COUNT" => "5",
					"BLOG_USE" => $arParams['BLOG_USE'],
					"FB_USE" => "N",
					"FB_APP_ID" => $arParams['FB_APP_ID'],
					"VK_USE" => "N",
					"VK_API_ID" => $arParams['VK_API_ID'],
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600",
					'CACHE_GROUPS' => "N",
					"BLOG_TITLE" => "",
					"BLOG_URL" => $arParams['BLOG_URL'],
					"PATH_TO_SMILE" => "",
					"EMAIL_NOTIFY" => $arParams['BLOG_EMAIL_NOTIFY'],
					"AJAX_POST" => "N",
					"SHOW_SPAM" => "Y",
					"SHOW_RATING" => "N",
					"FB_TITLE" => "",
					"FB_USER_ADMIN_ID" => "",
					"FB_COLORSCHEME" => "light",
					"FB_ORDER_BY" => "reverse_time",
					"VK_TITLE" => "",
					"TEMPLATE_THEME" => $arParams['~TEMPLATE_THEME']
					),
					$component,
					array("HIDE_ICONS" => "Y")
					);?>
				</div><!--end yes-review-->
			</div><!--end b-new-review-->

			<?$APPLICATION->IncludeComponent(
			"bitrix:iblock.vote", 
			"stars1", 
			array(
			"IBLOCK_TYPE" => "catalog",
			"IBLOCK_ID" => "5",
			"ELEMENT_ID" => $arResult["ID"],
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "3600",
			"MAX_VOTE" => "5",
			"VOTE_NAMES" => array(
				0 => "1",
				1 => "2",
				2 => "3",
				3 => "4",
				4 => "5",
				5 => "",
			),
			"SET_STATUS_404" => "Y",
			"DISPLAY_AS_RATING" => "vote_avg",
			"COMPONENT_TEMPLATE" => "stars1",
			"ELEMENT_CODE" => $_REQUEST["code"]
			),
			false
			);?>
		</div>
		<? $tab_counter++ ?>
	</div>
	
	
		<?if(!empty($arResult["PROPERTIES"]["NEEDED"]["VALUE"])){?>
			<div class="necessary">
				<p class="media_no_mobile"><?=$arResult["PROPERTIES"]["NEEDED"]["NAME"]?>:</p>
				<p class="media_mobile"><i class="fa fa-plus-square" aria-hidden="true"></i> Сопутствующие товары</p>
				
				<div class="necessary_wrapper div_for_owl_carousel">
					<?foreach ($arResult["PROPERTIES"]["NEEDED"]["VALUE"] as $key=>$needed){?>
						<?
						if($needed["NAME"] == "Аккумуляторная дрель-шуруповёрт GSR 14,4 V-EC FC2") 
							{$needed["PRICE"]["PRICE"] = "28500.00";}
						if($needed["NAME"] == "Аккумуляторная дрель-шуруповёрт GSR 18 V-EC FC2") 
							{$needed["PRICE"]["PRICE"] = "31300.00";}
						?>
						<?if (!$GLOBALS['isMobile']):?>
							<div id="success" class="success" style="display:none;">
								<div class="goods-<?=$needed["ID"]?>"><a class="close" onclick="closee()"></a>
									<?$file = CFile::ResizeImageGet($needed["DETAIL_PICTURE"], array('width'=>200, 'height'=>150), BX_RESIZE_IMAGE_PROPORTIONAL, true);?> 
									<a class="image_action" href="<?=$needed["DETAIL_PAGE_URL"]?>"><img style="display: block; margin: 0 auto;" src="<?=$file["src"]?>" alt=""></a>
									<div class="description">
										<a href="<?=$needed["DETAIL_PAGE_URL"]?>" class="pName"  style="color: inherit;"><p><?=$needed["NAME"]?></p>
										<p class="add"><strong style="color:green;">Товар добавлен в корзину</strong></p></a> 
									</div> 
									<div class="compare-btn">
										<span class="buttons" onclick="closee()">Продолжить просмотр</span>
										<a href="/personal/cart/">Перейти в корзину</a>
									</div> 
								</div>
							</div>
						<?endif;?>						
						
						<div class="product_necess">
							<a class="link" href="<?=$needed["DETAIL_PAGE_URL"]?>">
								<div class="product_necess_row">
									<div class="product_necess_image">
										<img src="<?=$needed["IMG_URL"]?>" alt="">
									</div>
									<div class="teod"><?=$needed["NAME"]?></div>
									<p>Артикул: <?=$needed["ARTNUMBER"]["VALUE"]?></p>
								</div>		
							</a>
							<div class="product_necess_row media_mobile">								
								
								<div class="wishlist">
									<div>
										<?
											$iblockid = $arElement['IBLOCK_ID'];
											$id=$arElement['ID'];
											if(isset($_SESSION["CATALOG_COMPARE_LIST"][$iblockid]["ITEMS"][$id]))
											{
												$checked='checked';
											}
											else
											{
												$checked='';
											}
										?>
										<div class="compare">
											<input <?=$checked;?> type="checkbox" id="compareid_<?=$id;?>" onchange="compare_tov(<?=$arElement['ID'];?>);"> 
											<label for="compareid_<?=$arElement['ID'];?>"><i class="fa fa-circle-o media_mobile" aria-hidden="true"></i><i class="fa fa-check-circle-o media_mobile" aria-hidden="true"></i> Сравнить</label>
										</div>
										<div class="media_mobile product_availability"><i class="fa fa-check" aria-hidden="true"></i> <span>В наличии</span></div>
										<div class="media_no_mobile">Наличие: <span>есть</span></div>
									</div>
									<div>
										<div class="favorites_container media_mobile">
											<p class="media_no_mobile">Запомнить <br />товар: <!--favorites-<?=$arElement['ID']?>--></p>
											<?
											$arElement_ID = $arElement['ID'];
											?>
											<a class="favorites inline_block media_no_mobile" onclick="favorites(this)" data-element_id="<?=$arElement_ID?>">
												<div class="added" style="<?=(!$arElement['IN_FAVORITES']) ? 'display:none' : ''?>" title="В избранном"></div>
												<div class="add" style="<?=($arElement['IN_FAVORITES']) ? 'display:none' : ''?>" title="Добавить в избранное"></div>
												<div class="loading"></div>
											</a>
											<a class="favorites inline_block media_mobile" onclick="favorites(this)" data-element_id="<?=$arElement_ID?>">
												<div class="added" style="<?=(!$arElement['IN_FAVORITES']) ? 'display:none' : ''?>" title="В избранном"><i class="fa fa-heart" aria-hidden="true"></i></div>
												<div class="add" style="<?=($arElement['IN_FAVORITES']) ? 'display:none' : ''?>" title="Добавить в избранное"><i class="fa fa-heart" aria-hidden="true"></i></div>
												<span>В избранное</span>
											</a>
											<div class="media_no_mobile"> 
												<? foreach($arResult["COMMENTS"] as $res) {if($arElement["NAME"] == $res["TITLE"]) { $k = $res["NUM_COMMENTS"]; }} ?> 
												<p><a style="text-decoration:none;" href="<?=$arElement["DETAIL_PAGE_URL"]?>?reviews=true">Отзывы: <? if(!empty($k)) {echo $k;}else{echo '0';} unset($k);?></a></p>
												<? $votesValue = round($arElement["PROPERTIES"]["vote_sum"]["VALUE"]/$arElement["PROPERTIES"]["vote_count"]["VALUE"], 0);?>
												<?if(!empty($arElement["PROPERTIES"]["vote_count"]["VALUE"])) { ?>
													<?
													if($votesValue == 1) echo '<span class="vote"><span class="active"></span><span class="not"></span><span class="not"></span><span class="not"></span><span class="not"></span></span>';
													if($votesValue == 2) echo '<span class="vote"><span class="active"></span><span class="active"></span><span class="not"></span><span class="not"></span><span class="not"></span></span>';
													if($votesValue == 3) echo '<span class="vote"><span class="active"></span><span class="active"></span><span class="active"></span><span class="not"></span><span class="not"></span></span>';
													if($votesValue == 4) echo '<span class="vote"><span class="active"></span><span class="active"></span><span class="active"></span><span class="active"></span><span class="not"></span></span>';
													if($votesValue == 5) echo '<span class="vote"><span class="active"></span><span class="active"></span><span class="active"></span><span class="active"></span><span class="active"></span></span>';
												}else{?>
													<span class="vote"><span class="not"></span><span class="not"></span><span class="not"></span><span class="not"></span><span class="not"></span></span>
												<?}?>
											</div>
											<div class="votes_and_comments_mobile media_mobile"> 
												<i class="fa fa-star" aria-hidden="true"></i>
												<?if(!empty($arElement["PROPERTIES"]["vote_count"]["VALUE"])) { ?>
												<?
													if($votesValue == 1) echo '<span>(1.0)</span>';
													if($votesValue == 2) echo '<span>(2.0)</span>';
													if($votesValue == 3) echo '<span>(3.0)</span>';
													if($votesValue == 4) echo '<span>(4.0)</span>';
													if($votesValue == 5) echo '<span>(5.0)</span>';
												}else{?>
													<span>(0.0)</span>
												<?}?>
												<? foreach($arResult["COMMENTS"] as $res) {if($arElement["NAME"] == $res["TITLE"]) { $k = $res["NUM_COMMENTS"]; }} ?> 
												<p><a style="text-decoration:none;" href="<?=$arElement["DETAIL_PAGE_URL"]?>?reviews=true">Отзывы: <? if(!empty($k)) {echo $k;}else{echo '0';} unset($k);?></a></p>
												<? $votesValue = round($arElement["PROPERTIES"]["vote_sum"]["VALUE"]/$arElement["PROPERTIES"]["vote_count"]["VALUE"], 0);?>
				 							</div>
										</div>																			
									</div>
								</div>							
							</div>
							<div class="product_necess_row price">
								<?
									$grey_price_str = FormatCurrency($needed["PRICE"]['PRICE'], $needed["PRICE"]['CURRENCY']);
									$cur = $needed["PRICE"]['PRICE']*$basket_discount;
									$red_price_str = FormatCurrency($cur, $needed["PRICE"]['CURRENCY']);
								?>
								<div class="wrap-pr media_no_mobile">
									<span class="grey_pr"><?=$grey_price_str?></span>									
									<div class="red_small_pr"><?=$red_price_str?></div>
								</div>
								<div class="wrap-pr media_mobile">
									<p class="name_price">Цена:</p>
									<div class="new_sale">										
										<p class="sale "><?=$red_price_str?></p>
										<p class="cost new_cost"><?=$grey_price_str?></p>
									</div> 
								</div>							
							</div>
							<div class="tocart">
								<?if($needed["PRICE"]['PRICE']>0):?>
									<form id="<?=($needed["ID"]);?>" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
									<input type="hidden" name="<?=$arParams["ACTION_VARIABLE"]?>" value="BUY">
									<input type="hidden" name="<?=$arParams["PRODUCT_ID_VARIABLE"]?>" value="<?=($needed["ID"]);?>">
									<!--input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."BUY"?>" value="<?echo GetMessage("CATALOG_BUY")?>"-->
									<input type="hidden" name="<?=$arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?=GetMessage("CATALOG_ADD_TO_BASKET")?>">
									</form>
									
								<?endif;?>
							
								<?if($needed["PRICE"]['PRICE']>0):?>
									<a class="addtocart needed_buy" onclick="yaCounter28820320.reachGoal('VK_KART');" href="#" ids="need" id="<?=($needed["ID"])?>" onclick="yaCounter28820320.reachGoal('VK_DOP_KART');">Купить</a>
								<?endif;?>
							</div>								
						</div>						
					<?}?>					
				</div>	
			</div>
		<?}?>
<?
$arSelectRec = Array("ID");
$arFilterRec = Array("IBLOCK_ID"=>IntVal(5), "ACTIVE"=>"Y", "PROPERTY_RECOMMENDED_ALL_VALUE" => "Да");
$resRec = CIBlockElement::GetList(Array(), $arFilterRec, false, Array("nPageSize"=>5), $arSelectRec);
$REC_ID = array();
while($obRec = $resRec->GetNext())
{
 $REC_ID[] = $obRec['ID'];
}
?>
		<?//if(!empty($arResult["PROPERTIES"]["RECOMMEND"]["VALUE"])){?>
	<?if(count($REC_ID) > 0){?>
		<div class="head recomend-head"> 
			<p><i class="fa fa-exclamation-circle media_mobile" aria-hidden="true"></i>Рекомендуем приобрести</p>
		</div>
		<div class="content div_for_owl_carousel" id="recomend">
			<!-- <pre><?// print_r($arResult["PROPERTIES"]["RECOMMEND"]["VALUE"])?></pre> -->
			<?$GLOBALS['arrFilter'] = array("ID" => $REC_ID/*$arResult["PROPERTIES"]["RECOMMEND"]["VALUE"]*/)?>
			<?$APPLICATION->IncludeComponent(
			"bitrix:catalog.section", 
			"h2o_salelider", 
			array(
				"IBLOCK_TYPE" => "catalog",
				"IBLOCK_ID" => "5",
				"SECTION_ID" => "",
				"SECTION_CODE" => "",
				"SECTION_USER_FIELDS" => array(
					0 => "",
					1 => "",
				),
				"ELEMENT_SORT_FIELD" => "RAND",
				"ELEMENT_SORT_ORDER" => "asc",
				"ELEMENT_SORT_FIELD2" => "id",
				"ELEMENT_SORT_ORDER2" => "desc",
				"FILTER_NAME" => "arrFilter",
				"INCLUDE_SUBSECTIONS" => "Y",
				"SHOW_ALL_WO_SECTION" => "Y",
				"HIDE_NOT_AVAILABLE" => "N",
				"PAGE_ELEMENT_COUNT" => "4",
				"LINE_ELEMENT_COUNT" => "3",
				"PROPERTY_CODE" => array(
					0 => "ARTNUMBER",
					1 => "SPECIALOFFER",
					2 => "",
				),
				"OFFERS_LIMIT" => "4",
				"SECTION_URL" => "",
				"DETAIL_URL" => "",
				"SECTION_ID_VARIABLE" => "SECTION_ID",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_GROUPS" => "Y",
				"SET_META_KEYWORDS" => "Y",
				"META_KEYWORDS" => "-",
				"SET_META_DESCRIPTION" => "N",
				"META_DESCRIPTION" => "-",
				"BROWSER_TITLE" => "-",
				"ADD_SECTIONS_CHAIN" => "N",
				"DISPLAY_COMPARE" => "N",
				"SET_TITLE" => "N",
				"SET_STATUS_404" => "N",
				"CACHE_FILTER" => "N",
				"PRICE_CODE" => array(
					0 => "базовая",
				),
				"USE_PRICE_COUNT" => "N",
				"SHOW_PRICE_COUNT" => "0",
				"PRICE_VAT_INCLUDE" => "Y",
				"CONVERT_CURRENCY" => "N",
				"BASKET_URL" => "/personal/cart/",
				"ACTION_VARIABLE" => "action",
				"PRODUCT_ID_VARIABLE" => "id",
				"USE_PRODUCT_QUANTITY" => "Y",
				"PRODUCT_QUANTITY_VARIABLE" => "quantity",
				"ADD_PROPERTIES_TO_BASKET" => "Y",
				"PRODUCT_PROPS_VARIABLE" => "prop",
				"PARTIAL_PRODUCT_PROPERTIES" => "N",
				"PRODUCT_PROPERTIES" => array(
				),
				"PAGER_TEMPLATE" => ".default",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "N",
				"PAGER_TITLE" => "Товары",
				"PAGER_SHOW_ALWAYS" => "Y",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "Y",
				"AJAX_OPTION_ADDITIONAL" => "",
				"SET_BROWSER_TITLE" => "Y",
				"COMPARE_PATH" => "",
				"OFFERS_FIELD_CODE" => array(
					0 => "",
					1 => "",
				),
				"OFFERS_PROPERTY_CODE" => array(
					0 => "SPECIALOFFER",
					1 => "",
				),
				"OFFERS_SORT_FIELD" => "sort",
				"OFFERS_SORT_ORDER" => "asc",
				"OFFERS_SORT_FIELD2" => "id",
				"OFFERS_SORT_ORDER2" => "desc",
				"PLACE" => "DETAIL",
				"OFFERS_CART_PROPERTIES" => array(
				)
			),
			false
			);?>
		</div>

		<?}?>
	</div>


			
				<?/*foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
					<b><?=$arProperty["NAME"]?>:</b> <?
					if(is_array($arProperty["DISPLAY_VALUE"])):
						echo implode(" / ", $arProperty["DISPLAY_VALUE"]);
					elseif($pid=="MANUAL"):
						?><a href="<?=$arProperty["VALUE"]?>"><?=GetMessage("CATALOG_DOWNLOAD")?></a><?
					else:
						echo $arProperty["DISPLAY_VALUE"];?>
					<?endif?><br />
				<?endforeach*/?>
				
	<?/*if($arResult["PREVIEW_TEXT"]):?>
		<br /><?=$arResult["PREVIEW_TEXT"]?><br />
	<?elseif($arResult["DETAIL_TEXT"]):?>
		<br /><?=$arResult["DETAIL_TEXT"]?><br />
	<?endif;*/?>
	<?if(count($arResult["LINKED_ELEMENTS"])>0):?>
		<br /><b><?=$arResult["LINKED_ELEMENTS"][0]["IBLOCK_NAME"]?>:</b>
		<ul>
	<?foreach($arResult["LINKED_ELEMENTS"] as $arElement):?>
		<li><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></li>
	<?endforeach;?>
		</ul>
	<?endif?>
	<?
	if(count($arResult["MORE_PHOTO"])>0):?>
		<a name="more_photo"></a>
		<?foreach($arResult["MORE_PHOTO"] as $PHOTO):?>
			<img border="0" src="<?=$PHOTO["SRC"]?>" width="<?=$PHOTO["WIDTH"]?>" height="<?=$PHOTO["HEIGHT"]?>" alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>" /><br />
		<?endforeach?>
	<?endif?>
	<?if(is_array($arResult["SECTION"])):?>
		<br /><a href="<?=$arResult["SECTION"]["SECTION_PAGE_URL"]?>"><?=GetMessage("CATALOG_BACK")?></a>
	<?endif?>
</div>
<?
		if($_GET["reviews"] == 'true') { ?>
<script type="text/javascript">
	$(document).ready(function() { setTimeout(function(){

		$("#reviews a").click();
		var top = $(".tabs").offset().top;
		$('body,html').animate({scrollTop: top}, 300);
		console.log("!! "+top);
		}, 200);
		

	});
</script>
<?
		}
?>
<script type="text/javascript">
		$(document).on('click', 'a.addtocart', function (e) {
			addtocart (this, '<?=$arParams['AJAX_ID']?>');
		});
</script>
<script>
	$(document).ready(function() {
		var val = $(".menu_product ul li").last();
		var res = val.find("a");
		var txt = res.text();
		if(txt == "Комплект поставки") 
		{
			res.trigger('click');	
		}
		 
		var select = $("select.offers_hide").val();
		console.log(select);
		$(".hide").hide();
		$(".tabs_"+select).show();
		$('select.offers_hide').change(function(e){
			e.preventDefault();
			var select = $(this).val();
			console.log(select);
			$(".hide").hide();
			$(".tabs_"+select).show();
		});
	});
</script> 
<script type="text/javascript">
		$(document).on('click', 'a.addtocart', function (e) {
			addtocart (this, '<?=$arParams['AJAX_ID']?>');
		});
</script>
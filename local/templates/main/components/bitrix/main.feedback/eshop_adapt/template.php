<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<div class="bx_mfeedback">
	<?if(!empty($arResult["ERROR_MESSAGE"]))
	{
		foreach($arResult["ERROR_MESSAGE"] as $v)
			ShowError($v);
	}
	if(strlen($arResult["OK_MESSAGE"]) > 0)
	{
		?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div><?
	}
	?>
	<form action="<?=POST_FORM_ACTION_URI?>" method="POST">
		<?=bitrix_sessid_post()?>
		<div class="change_line">
			<div class="before_input"><?=GetMessage("MFT_NAME")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])):?>*<?endif?>:</div>
			<input class="input_line" type="text" name="user_name" value="<?=$arResult["AUTHOR_NAME"]?>" placeholder="<?=GetMessage("MFT_NAME")?>"/><br/>
		</div>
		<div class="change_line">
			<div class="before_input"><?=GetMessage("MFT_EMAIL")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("EMAIL", $arParams["REQUIRED_FIELDS"])):?>*<?endif?>:</div>
			<input class="input_line" type="text" name="user_email" value="<?=$arResult["AUTHOR_EMAIL"]?>" placeholder="<?=GetMessage("MFT_EMAIL")?>"/><br/>
		</div>
		<div class="change_line">
			<div class="before_input" style=" height: 100%;"><?=GetMessage("MFT_MESSAGE")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])):?>*<?endif?>:</div>
			<textarea class="input_line" style="resize: none;" name="MESSAGE" rows="5" cols="40" placeholder="<?=GetMessage("MFT_MESSAGE")?>"><?=$arResult["MESSAGE"]?></textarea><br/>
		</div>
		<?if($arParams["USE_CAPTCHA"] == "Y"):?>
			<div class="change_line captcha_wrapp">
				<input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
				<div class="before_input"><img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA"></div>
				<input class="input_line" type="text" name="captcha_word" size="30" maxlength="50" value="" placeholder="Введите символы справа"/><br/>
			</div>
		<?endif;?>

		<input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
		
		<div class="konfident">Нажимая на кнопку, вы даете согласие на обработку своих персональных данных и соглашаетесь с <a href="/about/politika-konfidentsialnosti/">политикой конфиденциальности</a></div>
		
		<input type="submit" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>" class="button_enter">
	</form>
</div>
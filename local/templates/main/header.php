<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".SITE_TEMPLATE_ID."/header.php");
$APPLICATION->SetTitle("");
$wizTemplateId = COption::GetOptionString("main", "wizard_template_id", "eshop_adapt_horizontal", SITE_ID);
CUtil::InitJSCore();
CJSCore::Init(array("fx"));
$curPage = $APPLICATION->GetCurPage(true);
?><?CModule::IncludeModule('mcart.souvenirs');?>
<?
$bIndexPage = ($_SERVER["SCRIPT_NAME"] == "/index.php" ? true : false);
define("PATH_TO_404", "/404.php");
?>
<?
  function bclass() {
    global $APPLICATION;
	global $USER;
	$var_body_class = "";
	$DirExp = substr($APPLICATION->GetCurDir(), 1);
	$DirExp = str_replace("/", "_", $DirExp);
	if($DirExp[1] !== '') {
		$var_body_class = $DirExp;
	}
	if($USER->isAuthorized()) {
		$var_body_class .= " user_authorized";
	} else {
		$var_body_class .= " user_not_authorized";
	}
	return ' class="'.$var_body_class.'"';
  }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<head>
	<title><?$APPLICATION->ShowTitle()?></title>
	<?$APPLICATION->ShowProperty ("AC_CANONICAL");?>
    <meta name="copyright" content="Сайт переработан и поддерживается в студии Pround Group"/>
	<meta name='yandex-verification' content='7c2a16121fdf444c' />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />

 

	<?//$APPLICATION->ShowHead();
	echo '<meta http-equiv="Content-Type" content="text/html; charset='.LANG_CHARSET.'"'.(true ? ' /':'').'>'."\n";
	$APPLICATION->ShowMeta("robots", false, true);
	$APPLICATION->ShowMeta("keywords", false, true);
	$APPLICATION->ShowMeta("description", false, true);
	$APPLICATION->ShowCSS(true, true);
	$APPLICATION->SetAdditionalCSS (SITE_TEMPLATE_PATH."/colors.css");
	$APPLICATION->SetAdditionalCSS (SITE_TEMPLATE_PATH."/style/screen.css");
	$APPLICATION->SetAdditionalCSS (SITE_TEMPLATE_PATH."/style/jquery.fancybox.css");
	$APPLICATION->SetAdditionalCSS (SITE_TEMPLATE_PATH."/style/jquery.bxslider.css");
	$APPLICATION->SetAdditionalCSS (SITE_TEMPLATE_PATH."/style/owl.carousel.css");
	$APPLICATION->SetAdditionalCSS (SITE_TEMPLATE_PATH."/style/jquery.remodal.css");
	$APPLICATION->SetAdditionalCSS (SITE_TEMPLATE_PATH."/style/uniform.default.css");
	$APPLICATION->SetAdditionalCSS (SITE_TEMPLATE_PATH."/style/styleSlider.css");
	$APPLICATION->SetAdditionalCSS (SITE_TEMPLATE_PATH."/font-awesome/css/font-awesome.min.css");
	?>

	<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquey.js'); ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery-migrate-1.2.1.min.js'); ?>
	<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery-ui.min.js'); ?>
	<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/js.cookie.js'); ?>

	<?
	//$APPLICATION->ShowProperty ("CANONICAL_URL");
	$APPLICATION->ShowHeadStrings();
	$APPLICATION->ShowHeadScripts();
	//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/script.js");
	?>

	<link href='<?=SITE_TEMPLATE_PATH;?>/style/styles.css' type='text/css' rel='stylesheet' />

<!-- Google Tag Manager -->
<script data-skip-moving="true">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TK94FVV');</script>
<!-- End Google Tag Manager -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
var yaParams = {/*Здесь параметры визита*/};
</script>



<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter28820320 = new Ya.Metrika({id:28820320,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    ecommerce:"dataLayer",
                    accurateTrackBounce:true,params:window.yaParams||{ }});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/28820320" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- Google Analytics -->
<script data-skip-moving="true">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    window.ga_debug = {trace: true};
    ga('create', 'UA-60347677-1', 'auto');
    ga('require', 'displayfeatures');
    ga('require', 'linkid', 'linkid.js');
    ga('send', 'pageview');

</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-60347677-1']);
  _gaq.push(['_setDomainName', 'mera-pt.ru']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- End Google Analytics -->
</head>
<body <?echo bclass();?>>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TK94FVV"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<div id="panel"><?$APPLICATION->ShowPanel();?></div>

	<div class="main">
		<div class="header_top">
			<header>
				<div class="city media_no_mobile">
					<form method="post" class="city_change">
						<input type="hidden" name="SET_CITY" value="Y">
						<select class="change_city" name="CURRENT_CITY">
							<?foreach($_SESSION["ALL_CITY"] as $city):?>
								<option value="<?=$city["ID"]?>"<?if($city["ID"]==$_SESSION["CURRENT_CITY"]["ID"]){echo(' selected');}?>> <?=$city["NAME"]?></option>
							<?endforeach;?>
						</select>
					</form>
				</div>
				<div class="topmenu media_no_mobile">
					<?$APPLICATION->IncludeComponent("bitrix:menu", "h2o_top_menu", Array(
						"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
						"MENU_CACHE_TYPE" => "A",	// Тип кеширования
						"MENU_CACHE_TIME" => "36000000",	// Время кеширования (сек.)
						"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
						"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
						"MAX_LEVEL" => "3",	// Уровень вложенности меню
						"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
						"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
						"DELAY" => "N",	// Откладывать выполнение шаблона меню
						"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
						),
						false
					);?>
				</div>
				<div class="mobile_topmenu media_mobile">
					<ul>
						<li>
							<a href="" data-remodal-target="modal-mobile_menu"><i class="fa fa-bars" aria-hidden="true"></i></a>
						</li>
						<li>
							<?
							$is_user_auth_class = "";
							if ($USER->isAuthorized()) {?>
								<a href="/personal/" class="user_authorized"><i class="fa fa-unlock" aria-hidden="true"></i></a>
							<?} else {?>
								<a href="/login/" ><i class="fa fa-lock" aria-hidden="true"></i></a>
							<?}?>
						</li>
						<li>
							<div class="cart">
								<i class="fa fa-shopping-cart" aria-hidden="true"></i>
								<? $APPLICATION->IncludeComponent(
									"bitrix:sale.basket.basket.line",
									"h2o",
									array(
										"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
										"PATH_TO_PERSONAL" => SITE_DIR."personal/",
										"SHOW_PERSONAL_LINK" => "N",
										"SHOW_NUM_PRODUCTS" => "Y",
										"SHOW_TOTAL_PRICE" => "Y",
										"SHOW_PRODUCTS" => "N",
										"POSITION_FIXED" => "N",
										"COMPONENT_TEMPLATE" => "h2o",
										"SHOW_EMPTY_VALUES" => "Y",
										"SHOW_AUTHOR" => "N",
										"PATH_TO_REGISTER" => SITE_DIR."login/",
										"PATH_TO_PROFILE" => SITE_DIR."personal/"
									),
									false
								);?>
							</div>
						</li>
						<li>
							<?
							$var_phone_no = $_SESSION['CURRENT_CITY']['CITY_PHONE_CODE'].$_SESSION['CURRENT_CITY']['CITY_PHONE'][0].$_SESSION['CURRENT_CITY']['CITY_PHONE'][1];
							?>

							<a href="tel:<?=$var_phone_no?>"><i class="fa fa-phone" aria-hidden="true"></i></a>
						</li>
					</ul>
				</div>
				<div class="remodal modal-mobile_menu" data-remodal-id="modal-mobile_menu">
					<div class="search">
						<? $APPLICATION->IncludeComponent(
							"bitrix:search.title",
							"h2o",
							array(
								"NUM_CATEGORIES" => "1",
								"TOP_COUNT" => "5",
								"ORDER" => "date",
								"USE_LANGUAGE_GUESS" => "N",
								"CHECK_DATES" => "N",
								"SHOW_OTHERS" => "N",
								"PAGE" => SITE_DIR."catalog/",
								"CATEGORY_0_TITLE" => GetMessage("SEARCH_GOODS"),
								"CATEGORY_0" => array(
									0 => "iblock_catalog",
								),
								"CATEGORY_0_iblock_catalog" => array(
									0 => "5",
								),
								"SHOW_INPUT" => "Y",
								"INPUT_ID" => "title-search-input_2",
								"CONTAINER_ID" => "search_2",
								"PRICE_CODE" => array(
									0 => "BASE",
								),
								"PRICE_VAT_INCLUDE" => "Y",
								"PREVIEW_TRUNCATE_LEN" => "",
								"SHOW_PREVIEW" => "Y",
								"PREVIEW_WIDTH" => "75",
								"PREVIEW_HEIGHT" => "75",
								"CONVERT_CURRENCY" => "Y",
								"CURRENCY_ID" => "RUB"
							),
							false
						);  ?>
					</div>
					<div class="mobile_menu">
						<ul>
							<li><a href="/catalog/">Каталог</a></li>
							<li><a href="/action/">Акции</a></li>
							<li><a href="/uslugi/">Услуги</a></li>
							<li><a href="/about/">О компании</a></li>
							<li><a href="/reviews/">Отзывы</a></li>
							<li><a href="/about/delivery/">Оплата и доставка</a></li>
							<li><a href="/kompaniyam/">Компаниям</a></li>
							<li><a href="/about/howto/">Как заказать</a></li>
							<li><a href="/about/contacts/">Контакты</a></li>
						</ul>
					</div>
				</div>
			</header>
		</div>
		<div class="header_2-nd_block">
			<div class="logo">
					<div class="logotype">
						<a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/images/logo/Logo.png" alt=""></a>
						<!-- <a href="/" class="media_mobile"><img src="<?=SITE_TEMPLATE_PATH?>/images/logo/mobile_logo.png" alt=""></a> -->
						<div>
						Компания МЕРА - специализированный дилер корпорации Bosch
						</div>
					</div>
			</div>
			<div class="city media_mobile">
				<form method="post" class="city_change">
					<input type="hidden" name="SET_CITY" value="Y">
					<select class="change_city" name="CURRENT_CITY">
						<?foreach($_SESSION["ALL_CITY"] as $city):?>
							<option value="<?=$city["ID"]?>"<?if($city["ID"]==$_SESSION["CURRENT_CITY"]["ID"]){echo(' selected');}?>> <?=$city["NAME"]?></option>
						<?endforeach;?>
					</select>
				</form>
			</div>
			<div class="contact">
				<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
					"AREA_FILE_SHOW" => "sect",
					"AREA_FILE_SUFFIX" => "incl",
					"AREA_FILE_RECURSIVE" => "Y",
					"EDIT_TEMPLATE" => ""
					),
					false
				);?>
				<div class="new-time">
					<img src="/local/templates/main/images/logo/clock_ico.png" alt="">
					пн-пт 09:00 - 18:00
				</div>
			</div>
		</div>

		<!-- <div class="mainmenu">
			<ul>
	     		<li><img src="<?=SITE_TEMPLATE_PATH?>/images/logo/darr.png" alt="">Весь каталог</li>
	     		<li>Измерительный инструмент</li>
	     		<li>Электроинструмент</li>
	     		<li>Оснастка</li>
	     		<li class='red'>Акции</li>
	     		<li><img src="<?=SITE_TEMPLATE_PATH?>/images/logo/darr.png" alt="">Услуги</li>
	     	</ul>
		</div> -->
		<div class="mainmenu media_no_mobile">
			<?$APPLICATION->IncludeComponent("bitrix:menu", "h2o_multi", Array(
			"ROOT_MENU_TYPE" => "left",	// Тип меню для первого уровня
			"MENU_CACHE_TYPE" => "A",	// Тип кеширования
			"MENU_CACHE_TIME" => "36000000",	// Время кеширования (сек.)
			"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
			"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
			"MAX_LEVEL" => "3",	// Уровень вложенности меню
			"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
			"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
			"DELAY" => "N",	// Откладывать выполнение шаблона меню
			"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
			),
			false
		);?>
			<div class="right_block_in_mainmenu">
				<div class="search">
					<a href="" class="search_toggle"><img src="/local/templates/main/images/search.png" alt=""><span>Поиск</span></a>
					<? $APPLICATION->IncludeComponent(
						"bitrix:search.title",
						"h2o",
						array(
							"NUM_CATEGORIES" => "1",
							"TOP_COUNT" => "5",
							"ORDER" => "date",
							"USE_LANGUAGE_GUESS" => "N",
							"CHECK_DATES" => "N",
							"SHOW_OTHERS" => "N",
							"PAGE" => SITE_DIR."catalog/",
							"CATEGORY_0_TITLE" => GetMessage("SEARCH_GOODS"),
							"CATEGORY_0" => array(
								0 => "iblock_catalog",
							),
							"CATEGORY_0_iblock_catalog" => array(
								0 => "5",
							),
							"SHOW_INPUT" => "Y",
							"INPUT_ID" => "title-search-input",
							"CONTAINER_ID" => "search",
							"PRICE_CODE" => array(
								0 => "BASE",
							),
							"PRICE_VAT_INCLUDE" => "Y",
							"PREVIEW_TRUNCATE_LEN" => "",
							"SHOW_PREVIEW" => "Y",
							"PREVIEW_WIDTH" => "75",
							"PREVIEW_HEIGHT" => "75",
							"CONVERT_CURRENCY" => "Y",
							"CURRENCY_ID" => "RUB"
						),
						false
					);  ?>
				</div>
				<div class="log_cart">
					<div class="login">
						<?$APPLICATION->IncludeComponent(
							"bitrix:system.auth.form",
							"h2o_main",
							array(
								"REGISTER_URL" => "",
								"FORGOT_PASSWORD_URL" => "/personal/profile/?forgot_password=yes",
								"PROFILE_URL" => "",
								"SHOW_ERRORS" => "N"
							),
							false
						);?>
					</div>
					<div class="cart">
						<? $APPLICATION->IncludeComponent(
							"bitrix:sale.basket.basket.line",
							"h2o",
							array(
								"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
								"PATH_TO_PERSONAL" => SITE_DIR."personal/",
								"SHOW_PERSONAL_LINK" => "N",
								"SHOW_NUM_PRODUCTS" => "Y",
								"SHOW_TOTAL_PRICE" => "Y",
								"SHOW_PRODUCTS" => "N",
								"POSITION_FIXED" => "N",
								"COMPONENT_TEMPLATE" => "h2o",
								"SHOW_EMPTY_VALUES" => "Y",
								"SHOW_AUTHOR" => "N",
								"PATH_TO_REGISTER" => SITE_DIR."login/",
								"PATH_TO_PROFILE" => SITE_DIR."personal/"
							),
							false
						);?>
					</div>
				</div>
			</div>
		</div>
	<div class="maincontent">
		<div class="worakarea_wrap_container workarea <?if ($wizTemplateId == "eshop_adapt_vertical"):?>grid1x3<?else:?>grid<?endif?>">
			<div id="navigation">
				<?/* if($APPLICATION->GetCurPage() !== '/'){ */?>
				<?if(($APPLICATION->GetCurPage() !== '/')and($APPLICATION->GetCurPage() !== '/catalog/')){?>
					<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
							"START_FROM" => "0",
							"PATH" => "",
							"SITE_ID" => "-"
						),
						false,
						Array('HIDE_ICONS' => 'Y')
					);?>
				<?}?>
			</div>
			<div class="bx_content_section">


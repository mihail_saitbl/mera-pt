jQuery(document).ready(function(){
	jQuery("#order_form_zakaz_zwonka2").submit(function(){
		var a = jQuery("[name = capcha]").val("");
		var error = false;
		jQuery(this).find(".classOk").each(function(){
			if ((jQuery(this).val() == "" && jQuery(this).hasClass("requied")))
			{
				jQuery(this).addClass("bad");
				error = true;
			}
			else
			{
				jQuery(this).removeClass("bad");
			}
		});

		if (!error)
		{
			jQuery(".error").hide();
			var form = this;
			xhr = new XMLHttpRequest();
			xhr.open("POST", "form_order.php");
			xhr.onload = function (e) {
				data = xhr.responseText;
			    var arResult = JSON.parse(data);
				if (arResult["RESULT"] == "OK")
				{   //yaCounter31777796.reachGoal ("BWCALLBACKS");
					if (arResult["HREF"])
					{
						location.href = arResult["HREF"];
					}
					else
					{
						jQuery(form).find("classOk").each(function(){
							if	(jQuery(this).attr('type')!="submit")
							{
								jQuery(this).val("");
							}
						});
						jQuery.fancybox.close();
						if (arResult["TEXT"])
						{
							alert (arResult["TEXT"]);
						}
						else
						{
							alert ("Спасибо! Мы свяжемся с вами в ближайшее время!");
						}
					}
				}
				else
				{
					alert (arResult["ERROR"]);
				}
			}
			xhr.send(new FormData(form));
		}
		else
		{
			//jQuery(".error").show();
			alert ("Не все поля заполнены");
		}
		return false;
	});
});


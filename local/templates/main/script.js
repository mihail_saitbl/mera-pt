﻿/*Disclaimer*/
/*$(document).ready(function() { 
	var v_cookie = $.cookie('mera_fz_152_agree');
	if (v_cookie!='agree') { 
		$('#Disclaimer').css("display", "block");
	}
	
	$('#Disclaimer .close').click(function() {
		$('#Disclaimer').fadeOut();	
		$.cookie('mera_fz_152_agree', 'agree');
	});
	
});*/




function closee() { 
			$("#mask").remove(); 
            $(".success").css("display", "none"); 
            $(".cart-test").css("display", "none");
            $(".favor").css("display", "none");
            $(".compar").css("display", "none");
            $(".regno").css("display", "none");
}

function favorites(e){    
		var $link = $(e); 
		var v_good_link = $(".goods-"+$link.data().element_id).first().find('.image_action').attr('href');
		var v_image_src = $(".goods-"+$link.data().element_id).first().find('.image_action').find('img').attr('src');
		var v_good_title = $(".goods-"+$link.data().element_id).first().find('.description').find('a').find('p').first().text();
		
		$link.find('.add, .added').hide();
		$link.find('.loading').show();
		
				
		$.post('/favorites/favorites_ajax.php', {id: $link.data().element_id}, function(r)
		{   
			$link.find('.loading').hide();
			if (r.status=='added') {  
				$link.find('.added').fadeIn('fast');
				$("#mask").remove(); 
				$("body").append("<div id='mask'></div>"); 
				
				if ($(window).width() >= 1000) {					
					$(".goods-"+$link.data().element_id).first().parent().css("display", "block");
					$(".favor").css("display", "block");					
				} else {				
					var v_str = '<a href="'+v_good_link+'" class="mobile_success_img"><img src="'+v_image_src+'" alt=""></a><a href="'+v_good_link+'" class="mobile_success_desc"><span>'+v_good_title+'</span><span>Товар добавлен в список</span></a>';
					$('#mask').append('<div class="mobile_modal_wrapper"><a class="close" onclick="closee()"></a>'+v_str+'<span class="buttons grey_btn" onclick="closee()">Продолжить просмотр</span><a class="buttons" href="/favorites">Перейти в список</a></div>');
				}
				
				/* $link.find('.add').css('display', 'block');
				$link.find('.added').css('display', 'block'); */				
			}
			else if(r.status == 'no') { 
				$link.find('.add').fadeIn('fast');  				
				$("#mask").remove(); 
				$("body").append("<div id='mask'></div>");  
				
				if ($(window).width() >= 1000) {					
					$(".goods-"+$link.data().element_id).first().parent().css("display", "block");
					$(".favor").css("display", "block");				 
					//$("#success .image_action img").css('display','none');
					//$("#success .list_preview_image").css('display','none');
					//$(".add .description p").html('');					
					$(".goods-"+$link.data().element_id).first().parent().css("display", "block");
					$(".favor strong:first-child").css("display", "none");
					$(".favor strong:last-child").css("display", "block");
					$(".compare-btn .favor").css("display", "none");
					$(".compare-btn .regno").css("display", "block");
				} else {					
					var v_str = '<a href="'+v_good_link+'" class="mobile_success_img"><img src="'+v_image_src+'" alt=""></a><a href="'+v_good_link+'" class="mobile_success_desc"><span>'+v_good_title+'</span><span>Для того, чтобы "запомнить" товар необходимо авторизоваться. Если у вас еще нет учетной записи, Вы можете зарегистрироваться прямо сейчас!</span></a>';
					$('#mask').append('<div class="mobile_modal_wrapper"><a class="close" onclick="closee()"></a>'+v_str+'<span class="buttons grey_btn" onclick="closee()">Продолжить просмотр</span><a class="buttons" href="/login">Зарегистрироваться/вход</a></div>');
				}
			}
			else if(r.status == 'removed'){  
				$link.find('.add').fadeIn('fast');  
			    //$('.favorites_total_count').text(r.count); 
			}
		}); 
}

function compare_tov(id)
{
    var chek = document.getElementById('compareid_'+id);	
    if (chek.checked) {
        //Добавить
        var AddedGoodId = id;		
		var v_good_link = $(".goods-"+AddedGoodId).first().find('.image_action').attr('href');
		var v_image_src = $(".goods-"+AddedGoodId).first().find('.image_action').find('img').attr('src');
		var v_good_title = $(".goods-"+AddedGoodId).first().find('.description').find('a').find('p').first().text();
		
		
		
        $.get("/compare/ajax/list_compare.php", { 
            action: "ADD_TO_COMPARE_LIST", id: AddedGoodId},
            function(data) {
				$("#compare_list_count").html(data); 
			}
        );
		
		$("#mask").remove(); 
		$("body").append("<div id='mask'></div>");			
		
		if ($(window).width() >= 1000) {            
			$(".compar").css("display", "block");
            $(".goods-"+id).first().parent().css("display", "block"); 
            $(".goods-"+id).first().parent().find ('.necessary').hide();
            $(".goods-"+id).first().parent().css("height", "200px"); 				
		} else { 
			var v_str = '<a href="'+v_good_link+'" class="mobile_success_img"><img src="'+v_image_src+'" alt=""></a><a href="'+v_good_link+'" class="mobile_success_desc"><span>'+v_good_title+'</span><span>Товар добавлен в сравнение</span></a>';
			$('#mask').append('<div class="mobile_modal_wrapper"><a class="close" onclick="closee()"></a>'+v_str+'<span class="buttons grey_btn" onclick="closee()">Продолжить просмотр</span><a class="buttons" href="/compare">Перейти к сравнению</a></div>');
		} 
    }
    else
       {
        //Удалить
        var AddedGoodId = id;
            $.get("/compare/ajax/list_compare.php",
            { 
                action: "DELETE_FROM_COMPARE_LIST", id: AddedGoodId},
                function(data) {
	        $("#compare_list_count").html(data);
            }
            );
    }
}

function eshopOpenNativeMenu()
{
	var native_menu = BX("bx_native_menu");
	var is_menu_active = BX.hasClass(native_menu, "active");

	if (is_menu_active)
	{
		BX.removeClass(native_menu, "active");
		BX.removeClass(BX('bx_menu_bg'), "active");
		BX("bx_eshop_wrap").style.position = "";
		BX("bx_eshop_wrap").style.top = "";
		BX("bx_eshop_wrap").style.overflow = "";
	}
	else
	{
		BX.addClass(native_menu, "active");
		BX.addClass(BX('bx_menu_bg'), "active");
		var topHeight = document.body.scrollTop;
		BX("bx_eshop_wrap").style.position = "fixed";
		BX("bx_eshop_wrap").style.top = -topHeight+"px";
		BX("bx_eshop_wrap").style.overflow = "hidden";
	}

	var easing = new BX.easing({
		duration : 300,
		start : { left : (is_menu_active) ? 0 : -100 },
		finish : { left : (is_menu_active) ? -100 : 0 },
		transition : BX.easing.transitions.quart,
		step : function(state){
			native_menu.style.left = state.left + "%";
		}
	});
	easing.animate();
}

window.addEventListener('resize',
	function() {
		if (window.innerWidth >= 640 && BX.hasClass(BX("bx_native_menu"), "active"))
			eshopOpenNativeMenu();
	},
	false
);


$(document).ready(function() {  
	var select = $("select.change_city");
	/*
    var parent = select.parents("form.city_change");
	select.change(function() {
		parent.submit();
		//window.location.search = 'SET_CITY=Y&CURRENT_CITY='+sea;
		//location.reload(true);
	});
	*/
	$('select.change_city').selectmenu({
		change: function( event, ui ) {
            var city = $(this).attr('class');
            $.get('/include/set_city.php', {SET_CITY: "Y", CURRENT_CITY: ui.item.value}, function(res){
                location.reload();
            });
		}
	});
});


$(document).ready(function() {
	if ($(window).width() >= 1000) {
		var start_pos=$('.mainmenu').offset().top;
		$(window).scroll(function(){
			if ($(window).scrollTop()>start_pos) {
				  if ($('.mainmenu').hasClass()==false) {
					$('.mainmenu').addClass('to_top');
					$('.maincontent').addClass('to_top');		
				  } 
			}
			else {
				$('.mainmenu').removeClass('to_top');
				$('.maincontent').removeClass('to_top');  	
			}   
		});
	}
});


function mobile_addtocart(id) {
	var v_good_link = $(".goods-"+id).first().find('.image_action').attr('href');
	var v_image_src = $(".goods-"+id).first().find('.image_action').find('img').attr('src');
	var v_good_title = $(".goods-"+id).first().find('.description').find('a').find('p').first().text();
	var v_str = '<a href="'+v_good_link+'" class="mobile_success_img"><img src="'+v_image_src+'" alt=""></a><a href="'+v_good_link+'" class="mobile_success_desc"><span>'+v_good_title+'</span><span>Товар добавлен в корзину</span></a>';
	$('#mask').append('<div class="mobile_modal_wrapper"><a class="close" onclick="closee()"></a>'+v_str+'<span class="buttons grey_btn" onclick="closee()">Продолжить просмотр</span><a class="buttons" href="/personal/cart/">Перейти в корзину</a></div>');
	
}	
 
$(document).ready(function() { 
    $(".search_toggle").toggle(function() {       
		$(".search").toggleClass('active');
        return false; 
    },  
    function() {       
		$(".search").toggleClass('active');
        return false; 
    }); 
}); 

/*карусели*/
$(document).ready(function(){
	if ($(window).width() <= 1000) {
		$(".div_for_owl_carousel").owlCarousel({			
			items:1,
			loop:true,
			nav:true,
			responsive:{
				0:{
					items:1					
				},
				768:{
					items:2
				}
			}
		});	
		$(".necessary .necessary_wrapper").owlCarousel({
			loop:true,
			nav:true,
			responsive:{
				0:{
					items:1					
				},
				768:{
					items:2
				}
			}
		});	
		if ($(".content.wishlist-content").children('.goods').length > 1) {
			$(".content.wishlist-content").owlCarousel({
				loop:true,
				nav:true,
				responsive:{
					0:{
						items:1					
					},
					768:{
						items:2
					}
				}
			});	
		}
		
					
	}	
}); 

/*открытие фильтра в модальном окне*/
$(document).ready(function() { 
	$(".mobile_filter_modal_open_btn").click(function() { 
		$("body").addClass("modal_opened");
		$(".filter_wrapper_for_mobile").fadeIn();	
		
		$(".mobile_filter_modal_close_btn").click(function() {
			$("body").removeClass("modal_opened");
			$(".filter_wrapper_for_mobile").fadeOut();	
		});	
	});
}); 









/**/
  

  
/* !!?????!! */
var cart_adding = false;

function addtocart (th, ajax_id)
{
	if (cart_adding == false)
	{
		cart_adding = true;
		var id = $(th).attr('id');
		if ($(th).is('.already_in_cart')) {
			return false;
		}		
		$('div.tocart').find('a[id="'+id+'"]').addClass('already_in_cart').html('Уже в корзине');
		$('a.needed_buy[id="'+id+'"]').addClass('already_in_cart').html('Уже в корзине');
		$('form#'+id).bitrixAjax(
			ajax_id,
			function (data) {
				cart_adding = false;
				update_basket_line();
				//$('div.left_side').html(data.find('div.left_side').html());  
			},
			{
				post: {
					submit: 'Y'
				}
			}
		);
		/* if ($(window).width() >= 1000) { */
			$("#mask").remove(); 
			$(".goods-"+id).first().parent().css("display", "block"); 
			$(".cart-test").css("display", "block");
			$("body").append("<div id='mask'></div>");  
		/* } */
	}
}
/* end !!?????!! */
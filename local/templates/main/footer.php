<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>				</div> <!-- //bx_content_section-->
			</div>
		</div>
	<footer>

	<div class="remodal" data-remodal-id="modal-reg">
		<?$APPLICATION->IncludeComponent(
			"h2o:main.register",
			"h2o_main",
			Array(
				"SHOW_FIELDS" => array("EMAIL", "NAME", "PERSONAL_PHONE"),
				"REQUIRED_FIELDS" => array("EMAIL", "NAME"),
				"AUTH" => "Y",
				"USE_BACKURL" => "Y",
				"SUCCESS_PAGE" => "",
				"SET_TITLE" => "N",
				"USER_PROPERTY" => array(),
				"USER_PROPERTY_NAME" => ""
			)
		);?>
	</div>
	<? /*
	<div class="remodal" data-remodal-id="modal-feedback">
		<?$APPLICATION->IncludeComponent(
	"informunity:feedback",
	"main",
	array(
		"USE_CAPTCHA" => "N",
		"OK_TEXT" => "Спасибо, ваше сообщение принято.",
		"USE_IU_PAT" => "Y",
		"USE_IU_IB" => "Y",
		"USE_ATTACH" => "N",
		"EMAIL_TO" => array(
			0 => "info@mera-pt.ru",
			1 => "",
		),
		"EXT_FIELDS" => array(
			0 => "iu_0",
			1 => "iu_1",
			2 => "iu_2",
			3 => "Номер телефона",
			4 => "Страница заявки",
			5 => "",
		),
		"FIELD_FOR_THEME" => "iu_none",
		"EM_THEME" => "#SITE#: Сообщение из формы обратной связи",
		"AFTER_TEXT" => "",
		"USE_EMAIL_USER" => "N",
		"REQUIRED_FIELDS" => array(
			0 => "iu_0",
			1 => "Номер телефона",
		),
		"TEXTAREA_FIELDS" => array(
		),
		"FIELD_FOR_NAME" => "iu_0",
		"FIELD_FOR_EMAIL" => "iu_none",
		"COPY_LETTER" => "N",
		"USE_IU_IBC" => "Y",
		"IBLOCK_NAME" => "Сообщения из формы обратной связи",
		"IB_ACT" => "N",
		"IBE_NAME" => "iu_0",
		"IB_DET" => "iu_none",
		"IB_ANONS" => "iu_none",
		"IB_PARAM" => "Y",
		"WRIT_A" => "Y",
		"COMPONENT_TEMPLATE" => "main"
	),
	false
);?>
	</div>
*/ ?>


		<div class="footerwrapper">

			<div class="footer_logo">
				<img width="177" src="<?=SITE_TEMPLATE_PATH?>/images/logo/Logo.png" alt="" class="media_no_mobile">
				<img width="182" src="<?=SITE_TEMPLATE_PATH?>/images/logo/Logo.png" alt="" class="media_mobile">
			</div>

			<div class="leftli">
				<?$APPLICATION->IncludeComponent("bitrix:menu", "h2o_bottom_menu", array(
	"ROOT_MENU_TYPE" => "bottom_left",
	"MENU_CACHE_TYPE" => "A",
	"MENU_CACHE_TIME" => "36000000",
	"MENU_CACHE_USE_GROUPS" => "Y",
	"MENU_CACHE_GET_VARS" => array(
	),
	"MAX_LEVEL" => "1",
	"CHILD_MENU_TYPE" => "left",
	"USE_EXT" => "N",
	"DELAY" => "N",
	"ALLOW_MULTI_SELECT" => "N"
	),
	false
);?>
			</div>

			<div class="rightli">
				<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"simple",
	array(
		"ROOT_MENU_TYPE" => "bottom_right",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "3",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N"
	),
	false
);?>
			</div>

			<div class="footer_adress">
				<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => "/include/bottom_contacts.php",
		"EDIT_TEMPLATE" => ""
	),
	false
);?>
			</div>
			<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => "/include/copyright.php",
		"EDIT_TEMPLATE" => ""
	),
	false
);?><?CModule::IncludeModule('mcart.souvenirs');?>

		</div>
<!--LiveInternet counter--><script type="text/javascript"><!--
new Image().src = "//counter.yadro.ru/hit?r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random();//--></script><!--/LiveInternet-->

	</footer>

	</div> <!-- //wrap -->
<div id="fx_menu">
<a style="
	margin-right: 30px;
    text-decoration: none;
    font-size: 14px; " href="/compare/">Перейти к списку сравниваемых товаров</a>
<a style="
    font-size: 14px;
    text-decoration: none;
    margin: 5px;" href="/favorites/">Перейти к списку избранных товаров</a>
</div>



  <div id="Disclaimer" style = "display:none; width: 100%; height: 7%;">
	<span><i class="fa fa-info-circle" aria-hidden="true"></i></span>
	<div> 
		<p style = "margin-top:-14px;"> Ознакомьтесь с нашей <a href="/about/politika-konfidentsialnosti/" title="Политика конфиденциальности ООО МЕРА">Политикой конфиденциальности</a> </p> </br>
	</div>
	<span class="close" style = "height: 33%;  text-align: center; ">ok</span>
   </div>  
 
 





<?/*

<!--<script type='text/javascript'>

(function(){ var widget_id = '8aGJ9aneN6';

var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>-->

<?*/?>
<?
$h1 = $APPLICATION->GetPageProperty ("H1");
if (empty($h1))
{
	$title = $APPLICATION->GetTitle ();
	$APPLICATION->SetPageProperty ("H1", $title);
}

$goal == false;

if($_POST["CURRENT_CITY"] == "339")
{
	$goal = 'CLICK_OMSK';
}elseif($_POST["CURRENT_CITY"] == "338") {
	$goal = 'CLICK_NSK';
}
if ($goal)
{
?>
	<script type="text/javascript">
	window.onload = function() {
		setTimeout (function(){yaCounter28820320.reachGoal('<?=$goal;?>');}, 500);
	};
	</script>
<?}?>
<?
//global $USER;
//if ($USER->IsAdmin())
//if ($_SERVER["REMOTE_ADDR"] == "195.208.134.152")
{ //print_r(get_included_files());
	/*?>
	<a class="fancybox" href="#city_popup" id="show_city_popup">выбрать город</a>
	<?
	$APPLICATION->IncludeFile ("/include/city_popup.php");*/
}
$APPLICATION->IncludeFile ("/include/city_popup.php");
?>
<a class="fancybox" href="#city_popup" id="show_city_popup" style="display: none;">выбрать город</a>


<script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.bitrix.ajax.js" defer></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.fancybox.js" defer></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.remodal.min.js" defer></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/owl.carousel.min.js" defer></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.session.js" defer></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/form_zwonok.js" defer></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.bxslider.min.js" defer></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/js.cookie.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/script.js"></script>


	<? /* $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquey.js'); */ ?>
    <? /* $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery-migrate-1.2.1.min.js'); */ ?>
	<?/*  $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery-ui.js'); */ ?>
	<? /* $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery-ui.min.js'); */ ?>
	<? /* $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.bitrix.ajax.js'); */ ?>
	<? /* $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.fancybox.js'); */ ?>
	<? /* $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.remodal.min.js'); */ ?>
	<? /* $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/owl.carousel.min.js'); */ ?>
    <? /* $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.session.js'); */ ?>
    <? /* $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/form_zwonok.js'); */ ?>
    <? /* $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.maskedinput.js'); */ ?>
	<? /* $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.bxslider.min.js'); */ ?>


<script>

$(document).ready(function(){

	let the_cookie5 = $.cookie('the_cookie5');   

	console.log(the_cookie5 );


if ( the_cookie5 != 'test1')  {

		setTimeout(func, 60000);

			function func(){
				$('#Disclaimer').css('display', 'block');
			} 
    
        

 		$('.close').click(function(){
		
				$('#Disclaimer').css('display', 'none');

				$.cookie('the_cookie5', 'test1', { expires: 30 });
		});
 
    
    
    } else {
        
        $('#Disclaimer').css('display', 'none');
 
        
    }


});


</script>

 

</body>
</html>

 
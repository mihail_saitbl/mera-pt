(function($) {
	
	$.fn.serializeObject = function(){

        var self = this,
            json = {},
            push_counters = {},
            patterns = {
                "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
                "push":     /^$/,
                "fixed":    /^\d+$/,
                "named":    /^[a-zA-Z0-9_]+$/
            };


        this.build = function(base, key, value){
            base[key] = value;
            return base;
        };

        this.push_counter = function(key){
            if(push_counters[key] === undefined){
                push_counters[key] = 0;
            }
            return push_counters[key]++;
        };

        $.each($(this).serializeArray(), function(){

            // skip invalid keys
            if(!patterns.validate.test(this.name)){
                return;
            }

            var k,
                keys = this.name.match(patterns.key),
                merge = this.value,
                reverse_key = this.name;

            while((k = keys.pop()) !== undefined){

                // adjust reverse_key
                reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

                // push
                if(k.match(patterns.push)){
                    merge = self.build([], self.push_counter(reverse_key), merge);
                }

                // fixed
                else if(k.match(patterns.fixed)){
                    merge = self.build([], k, merge);
                }

                // named
                else if(k.match(patterns.named)){
                    merge = self.build({}, k, merge);
                }
            }

            json = $.extend(true, json, merge);
        });

        return json;
    };

	var xhr = new Array;
	
	$.fn.bitrixAjax = function(ajax_call, container, params )
	{
		if (container === undefined)
			container = true;
		if (ajax_call.length == 32) {
			var link = '?ajax_call=' + ajax_call;
			var postArray = {};
			if (($(this).prop("tagName") == "FORM")&&($(this).prop("method") == "get")) {
				link = link + "&" + $(this).serialize();
			} else if (($(this).prop("tagName") == "FORM")&&($(this).prop("method") == "post")) {
				postArray = $(this).serializeObject();
			}
			
			if ((params != undefined)&&(params.get != undefined)&&(typeof params.get == 'object')) {
				link = link + "&" + $.param(params.get);
			}
			
			if ((params != undefined)&&(params.post != undefined)) {
				for (var v in params.post) {
					postArray[v] = params.post[v];
				}
			}
			
			if ((params != undefined)&&(params.component != undefined)) {
				postArray["ajax_component_params"] = params.component;
			}
			if ((params != undefined)&&(params.link != undefined)) {
				if (params.link.indexOf("?") >= 0)
					link = params.link + "&" + link.substring(1);
				else
					link = params.link + "?" + link.substring(1);
			}
			if (xhr[ajax_call] != null) {
				xhr[ajax_call].abort();
			}
			xhr[ajax_call] = $.ajax(link, {
				type: "POST",
				data: postArray,
				success: function (data) {
					xhr[ajax_call] = null;
					// var obj = $($.parseHTML(data));
					var obj = $("<div />").html(data);
					if ($.isFunction(container)) {
						container(obj);
					} else if ((container === true)||(container == "")) {
						$('.ajax_'+ajax_call).html(obj.find('.ajax_'+ajax_call).html());
					} else if (container) {
						$(container).html(data);
					}
				},
				dataType: "html"
			});
		} else {
			if (console !== undefined) {
				console.log("BITRIX.H2O.AJAX: Error! Ajax id is incorrect = \"" + ajax_call + "\"");
			}
		}
		
	};

})(jQuery);
<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):

    $res = CIBlockElement::GetByID($arItem["PRODUCT_ID"]);
    if($ar_res = $res->GetNext())
        $IBlock_id=$ar_res['IBLOCK_ID'];
    $res = CIBlockElement::GetProperty($IBlock_id, $arItem["PRODUCT_ID"], "sort", "asc", array("CODE" => "ARTNUMBER"));
    while ($ob = $res->GetNext())
    {
        $arResult["GRID"]["ROWS"][$k]["PROPS"][$ob['CODE']] = $ob;
    }
    $Cashless = intval($arItem["PRICE"]*0.9);
    $arResult["GRID"]["ROWS"][$k]["PRICE_CASHLESS"] = $Cashless;
    $arResult["GRID"]["ROWS"][$k]["PRICE_CASHLESS_FORMATED"] = FormatCurrency($Cashless, $arItem["CURRENCY"]);
endforeach;
$Cashless_AllSum = intval($arResult["allSum"]*0.9);
$arResult["allSum_Cashless"] = $Cashless_AllSum;
$arResult["allSum_Cashless_Formated"] = FormatCurrency($Cashless_AllSum, "RUB");
?>
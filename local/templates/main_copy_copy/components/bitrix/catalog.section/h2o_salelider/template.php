<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$APPLICATION->SetPageProperty("title", $arResult["NAME"]);?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?global $USER;
		if ($USER->IsAdmin()){?><pre><?//print_r($arResult)?></pre><?}?>
<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
	<?$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));?>
	<?$file=array();?>
<div class="goods" id="<?=$this->GetEditAreaId($arElement['ID']);?>">
	<?if(is_array($arElement["PREVIEW_PICTURE"])):?>
		<?$file = CFile::ResizeImageGet($arElement["PREVIEW_PICTURE"], array('width'=>213, 'height'=>140), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
		<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=$file['src']?>" alt=""></a>
	<?elseif(is_array($arElement["DETAIL_PICTURE"])):?>
		<?$file = CFile::ResizeImageGet($arElement["DETAIL_PICTURE"], array('width'=>213, 'height'=>140), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
		<a class="image_action" href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=$file['src']?>" alt=""></a>
	<?endif?>
	<div class="description">
		<h4><?=$arElement["NAME"]?></h4>
		<div class="artikul">Артикул: <?=$arElement["XML_ID"]?></div>
	</div>
	<div class="price">
		<?if(!empty($arElement["OFFERS"])):?>
			<?if($arElement["MIN_PRICE"]):?>
				<span class="cost">от</span><span class="sale"><?=FormatCurrency($arElement["MIN_PRICE"], $arElement['MIN_CURR'])?></span>
			<?endif;?>
		<?else:?>
			<?foreach($arElement["PRICES"] as $code=>$arPrice):?>
				<?if($arPrice["CAN_ACCESS"]):?>
					<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
						<span class="cost" style="text-decoration: line-through;"><?=$arPrice["PRINT_VALUE"]?></span> <span class="sale"><?=$arPrice["PRINT_DISCOUNT_VALUE"]?></span>
					<?else:?><span class="cost"><?=$arPrice["PRINT_VALUE"]?></span><span class="sale"><?=FormatCurrency(($arPrice["VALUE"]*0.9), $arPrice['CURRENCY'])?></span><?endif;?>
				<?else:?>
					<p></p>
				<?endif;?>
			<?endforeach;?>
			<?if(empty($arElement["PRICES"])):?>
				<span class="cost">Нет цены</span>
			<?endif;?>
		<?endif;?>
		<?if($arParams["DISPLAY_COMPARE"]):?>
				<noindex>
				<a ><!--href="<?echo $arElement['COMPARE_URL']?>" rel="nofollow">--><img src="<?=SITE_TEMPLATE_PATH?>/images/maincontent/grafic.png" alt=""></a>
				</noindex>
		<?endif?>
		
	</div>
	<div class="tocart">
		<?if($arElement["CAN_BUY"]):?>
			<?if($arParams["USE_PRODUCT_QUANTITY"] || count($arElement["PRODUCT_PROPERTIES"])):?>
				<form class="<?=$arParams['AJAX_ID']?>" id="<?=$arElement['ID']?>" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
				<input type="hidden" name="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" value="1" size="5">
				<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
				<input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?echo $arElement["ID"]?>">
				<?if($arResult['BASKET_QUANTITY'][$arElement["ID"]]>0):?>
					<button type="submit" class="already_in_cart" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("IN_CART")?>"><?echo GetMessage("IN_CART")?></button>
				<?else:?>
					<button type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("CATALOG_ADD")?>"><?echo GetMessage("CATALOG_ADD")?></button>
				<?endif;?>
				<!--input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("CATALOG_ADD")?>"-->
				</form>
			<?else:?>
				<noindex>
					<a href="<?=$arElement['ADD_URL']?>" rel="nofollow"><img src="<?=SITE_TEMPLATE_PATH?>/images/maincontent/cart.png" alt="" ><?echo GetMessage("CATALOG_ADD")?></a>
				</noindex>
			<?endif?>
		<?elseif((count($arResult["PRICES"]) > 0) || is_array($arElement["PRICE_MATRIX"])):?>
			<a href="<?=$arElement['DETAIL_PAGE_URL']?>" rel="nofollow"><?echo GetMessage("CATALOG_ADD")?></a>
		<?endif?>
		&nbsp;
	</div>
</div>
		<?endforeach; // foreach($arResult["ITEMS"] as $arElement):?>
<div style="clear: both;"></div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
<div style=" border-bottom: 1px solid #eee;"></div>
<div class="pagination">
	<?=$arResult["NAV_STRING"]?>
</div>
<?endif;?>
<div style="clear: both;"></div>
<script type="text/javascript">
		$(document).on('submit', 'div.tocart form.<?=$arParams['AJAX_ID']?>', function (e) {
			e.preventDefault();
			var id = $(this).attr('id');
			$('form[id="'+id+'"]').find('button[type="submit"]').addClass('already_in_cart').html('Купить еще');
			$(this).bitrixAjax(
				"<?=$arParams['AJAX_ID']?>",
				function (data) {
					update_basket_line();
					//$('div.tocart form#'+id).html(data.find('div.tocart form#'+id).html());  
				},
				{
					post: {
						submit: 'Y'
					}
				}
			);
		});
	</script>

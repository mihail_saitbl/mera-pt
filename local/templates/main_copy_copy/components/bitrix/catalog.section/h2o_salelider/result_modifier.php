<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arParams['AJAX_ID'] = CH2oAjax::getComponentID($this->__component);
//CModule::IncludeModule("iblock");
foreach($arResult['ITEMS'] as $k=>$arElement)
{
	$user_id = $arElement['DISPLAY_PROPERTIES']['USER_ID']['DISPLAY_VALUE'];
	if ($user_id)
	{
		$rsUSER = CUser::GetById($user_id);
		$f=$rsUSER->Fetch();
		$arResult['ITEMS'][$k]['DISPLAY_PROPERTIES']['USER_ID']['DISPLAY_VALUE'] = CUser::FormatName(CSite::GetNameFormat(false), array("NAME" => $f['NAME'], "LAST_NAME" => $f['LAST_NAME'], "SECOND_NAME" => $f['SECOND_NAME'], "LOGIN" => $f['LOGIN']));
	}
	
	$IBLOCK_ID = $arElement["IBLOCK_ID"]; 
	$ID = $arElement["ID"];
	$arInfo = CCatalogSKU::GetInfoByProductIBlock($IBLOCK_ID); 
	if (is_array($arInfo)){
		$minPrice = "";
		$minCurr = "";
		$rsOffers = CIBlockElement::GetList(array(),array('IBLOCK_ID' => $arInfo['IBLOCK_ID'], 'PROPERTY_'.$arInfo['SKU_PROPERTY_ID'] => $ID)); 
		while ($arOffer = $rsOffers->GetNext()){
			$arFilter = array("ID"=>$arOffer["ID"]);
			$arSelect = array("ID", "XML_ID", "NAME");
			$resof = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			if($ar_resof = $resof->GetNext()){
				$db_price = CPrice::GetList(array(),array("PRODUCT_ID" => $ar_resof['ID'],));
				if ($arPrice = $db_price->Fetch()){
					$ar_resof["PRICE"] = $arPrice;
					if ($minPrice==""){
						$minPrice = $arPrice["PRICE"];
						$minCurr = $arPrice["CURRENCY"];
					}elseif($minPrice>$arPrice["PRICE"]){
						$minPrice = $arPrice["PRICE"];
						$minCurr = $arPrice["CURRENCY"];
					}
				}
				$arResult['ITEMS'][$k]["OFFERS"][] = $ar_resof;
			}
		}
		$arResult['ITEMS'][$k]["MIN_PRICE"] = $minPrice;
		$arResult['ITEMS'][$k]["MIN_CURR"] = $minCurr;
	}
	$arResult['ITEMS_ID'][] = $ID;
}

CModule::includeModule("sale");
$arBasketItemsID = array();
$dbBasketItems = CSaleBasket::GetList(
	array(
		"NAME" => "ASC",
		"ID" => "ASC"
	    ),
	array(
		"FUSER_ID" => CSaleBasket::GetBasketUserID(),
		"LID" => SITE_ID,
		"ORDER_ID" => "NULL",
	    ),
	false,
	false,
	array("ID", "CALLBACK_FUNC", "MODULE", 
	      "PRODUCT_ID", "QUANTITY", "DELAY", 
	      "CAN_BUY", "PRICE", "WEIGHT")
    );
while ($arItems = $dbBasketItems->Fetch())
{
	if($arItems['CAN_BUY'] != "Y"){
		continue;
	}
    /*if (strlen($arItems["CALLBACK_FUNC"]) > 0)
    {
	CSaleBasket::UpdatePrice($arItems["ID"], 
				 $arItems["CALLBACK_FUNC"], 
				 $arItems["MODULE"], 
				 $arItems["PRODUCT_ID"], 
				 $arItems["QUANTITY"]);
	$arItems2 = CSaleBasket::GetByID($arItems["ID"]);
    }*/

    $arBasketItemsID[] = $arItems['PRODUCT_ID'];
    $arBasketItemsQuantity[$arItems['PRODUCT_ID']] = $arItems["QUANTITY"];
}
unset ($arItems);
$this->__component->SetResultCacheKeys(array('ITEMS_ID'));
$arResult['BASKET_ID'] = $arBasketItemsID;
$arResult['BASKET_QUANTITY'] = $arBasketItemsQuantity;

?>

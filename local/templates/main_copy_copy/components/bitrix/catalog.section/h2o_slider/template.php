<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<h3 id="spec">Спецпредложения</h3>
<div class="slider_goods">
	<div class="slide-list">
		<div class="slide-wrap">
<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
	<?$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));?>
	<?$file=array();?>
<div class="slide-item">
	<div class="goods">
	<pre><?//print_r($arElement)?></pre>
	<?if(is_array($arElement["PREVIEW_PICTURE"])):?>
		<?$file = CFile::ResizeImageGet($arElement["PREVIEW_PICTURE"], array('width'=>213, 'height'=>140), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
		<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=$file['src']?>" alt=""></a>
	<?elseif(is_array($arElement["DETAIL_PICTURE"])):?>
		<?$file = CFile::ResizeImageGet($arElement["DETAIL_PICTURE"], array('width'=>213, 'height'=>140), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
		<a class="image_action" href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=$file['src']?>" alt=""></a>
	<?endif?>
	<div class="description">
		<h4><?=$arElement["NAME"]?></h4>
		<div class="artikul">Артикул: <?=$arElement["XML_ID"]?></div>
	</div>
	<div class="prise">
		<?if(!empty($arElement["OFFERS"])):?>
			<?if($arElement["MIN_PRICE"]):?>
				<span class="cost">от</span><span class="sale"><?=FormatCurrency($arElement["MIN_PRICE"], $arElement['MIN_CURR'])?></span>
			<?endif;?>
		<?else:?>
			<?foreach($arElement["PRICES"] as $code=>$arPrice):?>
				<?if($arPrice["CAN_ACCESS"]):?>
					<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
						<span class="cost" style="text-decoration: line-through;"><?=$arPrice["PRINT_VALUE"]?></span> <span class="sale"><?=$arPrice["PRINT_DISCOUNT_VALUE"]?></span>
					<?else:?><span class="cost"><?=$arPrice["PRINT_VALUE"]?></span><span class="sale"><?=FormatCurrency(($arPrice["VALUE"]*0.9), $arPrice['CURRENCY'])?></span><?endif;?>
		
				<?endif;?>
			<?endforeach;?>
			<?if(empty($arElement["PRICES"])):?>
				<span class="cost">Нет цены</span>
			<?endif;?>
		<?endif;?>
		<?if($arParams["DISPLAY_COMPARE"]):?>
				<noindex>
				<a><!--href="<?echo $arElement['COMPARE_URL']?>" rel="nofollow"--><img src="<?=SITE_TEMPLATE_PATH?>/images/maincontent/grafic.png" alt=""></a>
				</noindex>
		<?endif?>
		
	</div>
	<div class="tocart">
			<?if($arElement["CAN_BUY"]):?>
				<?if($arParams["USE_PRODUCT_QUANTITY"] || count($arElement["PRODUCT_PROPERTIES"])):?>
					<form class="<?=$arParams['AJAX_ID']?>" id="<?=$arElement['ID']?>" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
					<input type="hidden" name="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" value="1" size="5">
					<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
					<input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?echo $arElement["ID"]?>">
					<button type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("CATALOG_ADD")?>"><?echo GetMessage("CATALOG_ADD")?></button>
					<!--input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("CATALOG_ADD")?>"-->
					</form>
				<?else:?>
					<noindex>
						<a href="<?echo $arElement['ADD_URL']?>" rel="nofollow"><?echo GetMessage("CATALOG_ADD")?></a>
					</noindex>
				<?endif?>
			<?elseif((count($arResult["PRICES"]) > 0) || is_array($arElement["PRICE_MATRIX"])):?>
				<a href="<?=$arElement['DETAIL_PAGE_URL']?>" rel="nofollow"><?echo GetMessage("CATALOG_ADD")?></a>
			<?endif?>
			&nbsp;
	</div>
	</div>
</div>
		<?endforeach; // foreach($arResult["ITEMS"] as $arElement):?>
		</div></div>
	<div name="prev" class="navy prev-slide"><</div>
	<div name="next" class="navy next-slide">></div>
</div>
<div style="clear: both;"></div>
<script type="text/javascript">
		$(document).on('submit', 'div.tocart form.<?=$arParams['AJAX_ID']?>', function (e) {
			e.preventDefault();
			var id = $(this).attr('id');
			$('form[id="'+id+'"]').find('button[type="submit"]').addClass('already_in_cart').html('Купить еще');
			$(this).bitrixAjax(
				"<?=$arParams['AJAX_ID']?>",
				function (data) {
					update_basket_line();
					$('div.tocart form#'+id).html(data.find('div.goods'+id).html());  
				},
				{
					post: {
						submit: 'Y'
					}
				}
			);
		});
</script>

<script>
	function htmSlider(){
    /* Зададим следующие параметры */
    /* обертка слайдера */
    var slideWrap = $('.slide-wrap');
    /* кнопки вперед/назад и старт/пауза */
    var nextLink = $('.next-slide');
    var prevLink = $('.prev-slide');
    var playLink = $('.auto');
    /* Проверка на анимацию */
    var is_animate = false;
    /* ширина слайда с отступами */
    var slideWidth = $('.slide-item').outerWidth();
    /* смещение слайдера */
    var scrollSlider = slideWrap.position().left - slideWidth;
   
    /* Клик по ссылке на следующий слайд */
    nextLink.click(function(){
     if(!slideWrap.is(':animated')) {
      slideWrap.animate({left: scrollSlider}, 500, function(){
       slideWrap
        .find('.slide-item:first')
        .appendTo(slideWrap)
        .parent()
        .css({'left': 0});
      });
     }
    });
   
    /* Клик по ссылке на предыдующий слайд */
    prevLink.click(function(){
     if(!slideWrap.is(':animated')) {
      slideWrap
       .css({'left': scrollSlider})
       .find('.slide-item:last')
       .prependTo(slideWrap)
       .parent()
       .animate({left: 0}, 500);
     }
    });
   
    /* Функция автоматической прокрутки слайдера */
    function autoplay(){
     if(!is_animate){
      is_animate = true;
      slideWrap.animate({left: scrollSlider}, 500, function(){
       slideWrap
        .find('.slide-item:first')
        .appendTo(slideWrap)
        .parent()
        .css({'left': 0});
       is_animate = false;
      });
     }
    }
   
    timer = setInterval(function(){
     slideWrap.animate({left: scrollSlider}, 500, function(){
      slideWrap
       .find('.slide-item:first')
       .appendTo(slideWrap)
       .parent()
       .css({'left': 0});
      });
    }, 114000);
 }
 htmSlider();	
</script>
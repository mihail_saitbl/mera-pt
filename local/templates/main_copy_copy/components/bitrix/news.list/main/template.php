<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-list">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="free_deliev" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<?if(is_array($arItem["PREVIEW_PICTURE"])):?>
				<?$file = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width'=>193, 'height'=>193), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
				<img src="<?=$file['src']?>" alt="">
			<?elseif(is_array($arItem["DETAIL_PICTURE"])):?>
				<?$file = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"], array('width'=>193, 'height'=>193), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
				<img src="<?=$file['src']?>" alt="">
			<?endif?>
		<?endif?>
		<div class="text_free">
			<h3><?=$arItem["NAME"]?></h3>
			<?if(!empty($arItem["PREVIEW_TEXT"])):?>
				<p><?echo $arItem["PREVIEW_TEXT"];?></p>
			<?elseif(!empty($arItem["DETAIL_TEXT"])):?>
				<p><?echo $arItem["DETAIL_TEXT"];?></p>
			<?endif;?>
		</div>
	</div>
<?endforeach;?>
</div>

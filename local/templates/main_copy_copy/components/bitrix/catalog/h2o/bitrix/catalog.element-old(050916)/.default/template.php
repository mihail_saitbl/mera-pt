<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
$APPLICATION->SetPageProperty("title", $arResult["NAME"]);

/*global $USER;
if ($USER->IsAdmin())
{
	echo '<pre>'; print_r($arResult["OFFERS"]); echo "#".__LINE__."@".__FILE__; echo '</pre>';
}*/

?>
<?foreach($arResult["PRICES"] as $pri){

    if($pri["PRICE_ID"]==2)
        $price_bas = $pri;
    elseif($pri["PRICE_ID"]==3)   
        $price_disc = $pri;
    elseif($pri["PRICE_ID"]==1)   
        $price_id_1 = $pri;
}


if (empty($price_bas) && !empty($price_id_1))
{
	$price_bas = $price_id_1;
}

/*global $USER;
if ($USER->IsAdmin())
{

}*/

/*if ($price_bas["VALUE"] == $price_disc["VALUE"])
{
	unset ($price_disc);
}*/

$multi = $arResult['PROPERTIES']['MULTI']['VALUE'];
if(intval($multi)==0){
    $multi=1;
}
$ID = $arResult['ID'];
/*$amount = 0;
$mult = $arResult['PROPERTIES']['STATUS']['VALUE'];
if($mult>0)
    $amount = 2;
foreach($arResult["PRICES"] as $code=>$arPrice):
    if($arPrice["VALUE"]>0 && $amount==0){
        $amount = 2;
    }
endforeach;
if($arResult['CATALOG_QUANTITY']>0)
        $amount = 1;
$rsStoreNsk = CCatalogStoreProduct::GetList(array(), array('PRODUCT_ID' =>$ID, 'STORE_ID' => 1), false, false, array()); 
$rsStoreBsh = CCatalogStoreProduct::GetList(array(), array('PRODUCT_ID' =>$ID, 'STORE_ID' => 2), false, false, array());
if ($arStoreNsk = $rsStoreNsk->Fetch())
    if($arStoreNsk['AMOUNT']>0 || $arResult['CATALOG_QUANTITY']>0)
        $amount = 1;
if ($arStoreBsh = $rsStoreBsh->Fetch())
    if($arStoreBsh['AMOUNT']>0 && $amount == 0)
        $amount = 2;
*/
?>
<?


?>




<div class="catalog-element" itemscope="" itemtype="http://schema.org/Product">


	<div style="display: inline-block; width: 100%;">
		<div class="connected-carousels">
			<div class="navigation">
				<div class="btn_slide_up"><a href="#" class="prev-navigation"></a></div>
				<div class="btn_slide_down"><a href="#" class="next-navigation"></a></div>
				<div class="carousel-wrapper">
					<div class="carousel-items" style="top: 0px;">
						<?$k=-1;?>
						<?if(!empty($arResult["DETAIL_PICTURE"])){

								$detail = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"], array('width'=>384,'height'=>340), BX_RESIZE_IMAGE_PROPORTIONAL, true);

							?>
							<div class="carousel-block" data-index="<?=$k+1?>">
								<img itemprop="image" src="<?=$detail['src'];?>" alt="">
							</div>
						<?}elseif(!empty($arResult["PREVIEW_PICTURE"])){

									$prew = CFile::ResizeImageGet($arResult["PREVIEW_PICTURE"], array('width'=>384,'height'=>340), BX_RESIZE_IMAGE_PROPORTIONAL, true);

							?>
							<div class="carousel-block" data-index="<?=$k+1?>">
								<img itemprop="image" src="<?=$prew['src'];?>" alt="">
							</div>
						<?}?>
						<?foreach($arResult["PROPERTIES"]["IMAGES"]["VALUE"] as $k => $image):?>	

							<?
							$img = CFile::ResizeImageGet($image["URL"], array('width'=>384,'height'=>340), BX_RESIZE_IMAGE_PROPORTIONAL, true);	

							?>
							<div class="carousel-block" data-index="<?=$k+1?>">
								<img itemprop="image" src="<?=$img["src"]?>" alt="">
							</div>
						<?endforeach;?>
					</div>
				</div>
			</div>

			<div class="stage">
				<div class="carousel carousel-stage">
					<ul style="left: 0px;">
						<?if(!empty($arResult["DETAIL_PICTURE"])){

						
							?>
							<li class="active_tab">
								<a href="<?=$detail["src"]?>" rel="gallery1" class="gall block left fancybox" style="position: relative;">
									<div style="background-image: url('<?=$detail["src"];?>');background-repeat: no-repeat;background-position: 50%;background-size: contain;"></div>
								</a>
							</li>
						<?}elseif(!empty($arResult["PREVIEW_PICTURE"])){
							
							?>
							<li class="active_tab">
								<a href="<?=$prew['src']?>" rel="gallery1" class="gall block left fancybox" style="position: relative;">
									<div style="background-image: url('<?=$prew['src']?>');background-repeat: no-repeat;background-position: 50%;background-size: contain;"></div>
								</a>
							</li>
						<?}?>
						<?foreach($arResult["PROPERTIES"]["IMAGES"]["VALUE"] as $k => $image):?>
						<?

						?>
						<li class="active_tab">
							<a href="<?=$img["src"]?>" rel="gallery1" class="gall block left fancybox" style="position: relative;">
								<div style="background-image: url('<?=$img["src"];?>');background-repeat: no-repeat;background-position: 50%;background-size: contain;"></div>
							</a>
						</li>
						<?endforeach;?>
					</ul>
				</div>
			</div>
		</div>
							<?

								$price = CPrice::GetList(
									array(),
									array("PRODUCT_ID" => $arResult["ID"],)
								);
								if ($ar_res = $price->Fetch())
								{
								    $pric = CurrencyFormat($ar_res["PRICE"], $ar_res["CURRENCY"]);
								}
							?>
	


		<div class="left_side" >
			<meta itemprop="description" content="<?=$arResult["IPROPERTY_VALUES"]["ELEMENT_META_TITLE"];?>">
			<h1 itemprop="name"><?=$arResult["NAME"]?></h1>
			<div style="display:none;"itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">

							<meta itemprop="priceCurrency" content="RUB">
							<meta itemprop="price" content="<?=$pric?>">
						</div>
			<div class="grey_block">
				<?if(!empty($arResult["OFFERS"])):?>
					<?foreach($arResult["OFFERS"] as $keyoff=>$off):?>
						<div class="artic hide tabs_<?=$off["ID"]?>">
							<p>Артикул:</p><span><?=$off["XML_ID"]?></span>
							<a href="http://www.bosch-pt.ru" target="_blank">Сайт производителя</a>
						</div>
					<?endforeach;?>
					<p>Комплектация:</p>
					<select class="offers_hide">
					<?foreach($arResult["OFFERS"] as $ofkey => $offers):?>
						<option value="<?=$offers["ID"]?>">
							<?if($offers["PROPERTIES"]["NAME"]["VALUE"]!=""){?>
								<?=$offers["PROPERTIES"]["NAME"]["VALUE"];?>
							<?}else{?>
								<?=$offers["NAME"]?>
							<?}?>
						</option>
					<?endforeach;?>
					</select>
				<?else:?>
					<div class="artic">
						<p>Артикул:</p><span><?=$arResult["XML_ID"]?></span>
						<a href="http://www.bosch-pt.ru" target="_blank">Сайт производителя</a>
					</div>
				<?endif;?>
			</div>
			<?if(!empty($arResult["PROPERTIES"]["KOMPL_POST"]["VALUE"])):?>
				<p style="color: #333;" >Подробнее о комплектации читайте на вкладке <span>"Комплект поставки".</span></p>
			<?endif;?>
			<?if(!empty($arResult["OFFERS"])){?>
				<?foreach($arResult["OFFERS"] as $keyoff=>$off):?>
                    <?$ID = $off['ID'];
                    /*$amount = 0;
                    $mult = $arResult['PROPERTIES']['STATUS']['VALUE'];
                    if($mult>0)
                        $amount = 2;
                    foreach($arResult["PRICES"] as $code=>$arPrice):
                        if($arPrice["VALUE"]>0 && $amount==0){
                            $amount = 2;
                        }
                    endforeach;
                    $rsStoreNsk = CCatalogStoreProduct::GetList(array(), array('PRODUCT_ID' =>$ID, 'STORE_ID' => 1), false, false, array()); 
                    $rsStoreBsh = CCatalogStoreProduct::GetList(array(), array('PRODUCT_ID' =>$ID, 'STORE_ID' => 2), false, false, array());
                    if ($arStoreNsk = $rsStoreNsk->Fetch())
                        if($arStoreNsk['AMOUNT']>0 || $off['CATALOG_QUANTITY']>0)
                            $amount = 1;
                    if ($arStoreBsh = $rsStoreBsh->Fetch())
                        if($arStoreBsh['AMOUNT']>0 && $amount == 0)
                            $amount = 2;
					*/
                    ?>
					<div class="hide tabs_<?=$off["ID"]?>">
				
						

						<div class="prise"  >
							
                            <?
                            $price_id_1 = false;
                            $price_bas_off = false;
                            $price_disc_off = false;
                            foreach($off["PRICES"] as $pri_off){
                                if($pri_off["PRICE_ID"]==2)
                                    $price_bas_off = $pri_off;
                                elseif($pri_off["PRICE_ID"]==3)   
                                    $price_disc_off = $pri_off;
                                elseif($pri_off["PRICE_ID"] == 1)
                                {
                                	$price_id_1 = $pri_off;
                                }
                            }

                            if (!$price_bas_off && $price_id_1)
                            {
                            	$price_bas_off = $price_id_1;
                            }
                            /*if ($price_disc_off["VALUE"] == $price_bas_off["VALUE"])
                            {
                            	unset ($price_disc_off);
                            }*/
                            ?>
							<?//foreach($off["PRICES"] as $code=>$arPrice):?>
								<span>
									<?if($price_bas_off["CAN_ACCESS"]):?>
										<?if($arParams["PRICE_VAT_SHOW_VALUE"] && ($price_bas_off["VATRATE_VALUE"] > 0)):?>
											<?if($arParams["PRICE_VAT_INCLUDE"]):?>
												(<?echo GetMessage("CATALOG_PRICE_VAT")?>)
											<?else:?>
												(<?echo GetMessage("CATALOG_PRICE_NOVAT")?>)
											<?endif?>
										<?endif;?>
										<?if($price_bas_off["DISCOUNT_VALUE"] < $price_bas_off["VALUE"]):?>
											<span class="cost" style="text-decoration: line-through;"><?=str_replace("р.","руб.", $price_bas_off["PRINT_VALUE"])?></span></br>
                                            </span>
                                            <div class="red_prise"><?=str_replace("р.","руб.", $price_bas_off["PRINT_DISCOUNT_VALUE"])?> -</div><p>цена<br />со скидкой</p>
										<?/*	<span class="cost"><?=$arPrice["PRINT_DISCOUNT_VALUE"]?></span>*/?>
										<?else:?>
											<?if($price_disc_off["VALUE"] > 0){?> 
                                                <?=str_replace("р.","руб.", $price_bas_off["PRINT_VALUE"])?>
                                                </span>
                                                <div class="red_prise">
                                                    <?echo(str_replace("р.","руб.", $price_disc_off["PRINT_VALUE"]));?> -
                                                </div>
                                                <p>цена при покупке за наличный расчет</p>
                                            <?}else{?>
                                            		<?echo(str_replace("р.","руб.",$price_bas_off["PRINT_VALUE"]));?>
                                                </span>
                                                <div class="red_prise">
                                                    <?echo str_replace("р.","руб.", FormatCurrency($price_bas_off["VALUE"] * $arResult["basket_discount"], $price_bas_off["CURRENCY"]));?>
                                                </div>
                                            <?}?>
										<?endif?>
									<?endif;?>
							<?//endforeach;?>
							
						</div>
                        <?if (intval($arResult["PROPERTIES"]["MULTI"]["VALUE"])>1){?>
                            <p>Кратность товара в заказе <?=$arResult["PROPERTIES"]["MULTI"]["VALUE"]?> шт.</p>
                        <?}?>
						<div id="tabs_<?=$off["ID"]?>" class="tocart hide tabs_<?=$off["ID"]?>">
							<?if($off["CAN_BUY"]):?>
								<?if($arParams["USE_PRODUCT_QUANTITY"] || count($arResult["PRODUCT_PROPERTIES"])):?>
									<form id="<?=$off["ID"]?>" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
									<input type="hidden" name="quantity" value="<?=$multi?>" class="quantity">
                                    <input type="hidden" name="<? echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
									<input type="hidden" name="<? echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?=$off["ID"]?>">
									<!--input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."BUY"?>" value="<?echo GetMessage("CATALOG_BUY")?>"-->
									<input type="hidden" name="<? echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?=$off["ID"]?>">
									</form>
								<?else:?>
									<noindex>
									<a href="<?echo $arResult["BUY_URL"]?>" rel="nofollow"><?echo GetMessage("CATALOG_BUY")?></a>
									 <a href="<?echo $arResult["ADD_URL"]?>" rel="nofollow"><?echo GetMessage("CATALOG_ADD_TO_BASKET")?></a>
									</noindex>
								<?endif;?>
							<?elseif((count($arResult["PRICES"]) > 0) || is_array($arResult["PRICE_MATRIX"])):?>
								<?=GetMessage("CATALOG_NOT_AVAILABLE")?>
								<?$APPLICATION->IncludeComponent("bitrix:sale.notice.product", ".default", array(
									"NOTIFY_ID" => $arResult['ID'],
									"NOTIFY_URL" => htmlspecialcharsback($arResult["SUBSCRIBE_URL"]),
									"NOTIFY_USE_CAPTHA" => "N"
									),
									$component
								);?>
							<?endif?>
							<a class="addtocart" href="#" onclick="yaCounter28820320.reachGoal('VK_KART');" id="<?=$off["ID"]?>">Купить</a>
						</div>
						<a href="#" data-remodal-target="modal-oneclick-<?=$off["ID"]?>" class="buy1" onclick="yaCounter28820320.reachGoal('START_BUY_ONE_CLICK'); return true;">купить в 1 клик</a>
						<!--<img src="<?=SITE_TEMPLATE_PATH?>/images/maincontent/grafic.png" alt="">-->
						<div class="remodal" data-remodal-id="modal-oneclick-<?=$off["ID"]?>">
							<?$APPLICATION->IncludeComponent(
								"h2o:buyoneclick",
								"main",
								Array(
									"ASD_ID" => $off["ID"],
									"ASD_TITLE" => $_REQUEST["title"],
									"ASD_URL" => $_REQUEST["url"],
									"ASD_PICTURE" => $_REQUEST["picture"],
									"ASD_TEXT" => $_REQUEST["text"],
									"ASD_LINK_TITLE" => "Расшарить в #SERVICE#",
									"ASD_SITE_NAME" => "",
									"ASD_INCLUDE_SCRIPTS" => array()
								)
							);?>
						</div>
						<p>Адрес сервисного центра BOSCH:</p>
						<?if($arResult['SECTION']["PATH"][0]['ID'] == 1797):?>
							<p>г. Новосибирск, ул. Сибревкома, д. 2, оф. 718</p>
						<?else:?>
							<p>г. Новосибирск, ул. Красноярская, д. 36, цокольный этаж</p>
						<?endif;?>
						<hr class="underline">
						<div class="status_block">
							<?
							$arDelivery = AGGetDeliveryTime($off["ID"]);
							if ($_SESSION["CURRENT_CITY"]["NAME"] == "Новосибирск")
							{?>
							<div class="status">
								<span class="status_left"><?=$off["PROPERTIES"]["NSK"]["NAME"]?></span>
								<span class="status_right" style="color: #6a983c"><b><?=$arDelivery["NSK"];?></b></span>
							</div>
							<?}?>
							<div class="status">
								<span class="status_left"><?=$off["PROPERTIES"]["OMSK"]["NAME"]?></span>
								<span class="status_right" style="color: #6a983c"><b><?=$arDelivery["OMSK"];?></b></span>
							</div>
						</div>
					</div>
				<?endforeach;?>
			<?}else{?>
				<?if(!empty($price_bas)){?>
					<div class="prise">
						<?//foreach($arResult["PRICES"] as $code=>$arPrice):?>
							<span style="max-width: 110px;">
								<?if($price_bas["CAN_ACCESS"]):?>
									<?if($arParams["PRICE_VAT_SHOW_VALUE"] && ($price_bas["VATRATE_VALUE"] > 0)):?>
										<?if($arParams["PRICE_VAT_INCLUDE"]):?>
											(<?echo str_replace("р.","руб.",GetMessage("CATALOG_PRICE_VAT"))?>)
										<?else:?>
											(<?echo str_replace("р.","руб.",GetMessage("CATALOG_PRICE_NOVAT"))?>)
										<?endif?>
									<?endif;?>
									<?if($price_bas["DISCOUNT_VALUE"] < $price_bas["VALUE"]):?>
										<span class="cost" style="text-decoration: line-through;"><?=str_replace("р.", "руб.", $price_bas["PRINT_VALUE"])?></span></br>
                                        </span>
                                        <div class="red_prise"><?=str_replace("р.", "руб.", $price_bas["PRINT_DISCOUNT_VALUE"])?> -</div><p>цена<br />со скидкой</p>
									<?else:?>
										<?if($price_disc["VALUE"] > 0){?> 
                                            <?=str_replace("р.", "руб.",$price_bas["PRINT_VALUE"])?>
                                            </span>
                                            <div class="red_prise">
                                                <?echo(str_replace("р.","руб.",$price_disc["PRINT_VALUE"]));?> -
                                            </div>
                                            <p>цена при покупке за наличный расчет</p>
                                        <?}else{?>
                                            <?echo(str_replace("р.","руб.",$price_bas["PRINT_VALUE"]));?>
                                            </span>
                                            <?$cur = $price_bas["VALUE"]*$arResult["basket_discount"];?>
                                            <div class="red_prise">
                                                <?=str_replace("р.","руб.",FormatCurrency($cur, $price_bas['CURRENCY']))?>
                                            </div>
                                            <p>цена при покупке за наличный расчет</p>
                                        <?}?>
									<?endif?>
								<?endif;?>
						<?//endforeach;?>
					</div>
                    <?if (intval($arResult["PROPERTIES"]["MULTI"]["VALUE"])>1){?>
                        <p>Кратность товара в заказе <?=$arResult["PROPERTIES"]["MULTI"]["VALUE"]?> шт.</p>
                    <?}?>
					<div class="tocart">
						<?if(($arResult["CAN_BUY"])!=""):?>
							<?//if($arParams["USE_PRODUCT_QUANTITY"] || count($arResult["PRODUCT_PROPERTIES"])):?>
								<form id="<? if(empty($arResult["OFFERS"])){echo($arResult["ID"]);}else{echo($arResult["OFFERS"][0]["ID"]);}?>" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
								<input type="hidden" name="quantity" value="<?=$multi?>" class="quantity">
                                <input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
								<input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<? if(empty($arResult["OFFERS"])){echo($arResult["ID"]);}else{echo($arResult["OFFERS"][0]["ID"]);}?>">
								<!--input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."BUY"?>" value="<?echo GetMessage("CATALOG_BUY")?>"-->
								<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("CATALOG_ADD_TO_BASKET")?>">
								</form>
							<?/*else:?>
								<noindex>
								<a href="<?echo $arResult["BUY_URL"]?>" rel="nofollow"><?echo GetMessage("CATALOG_BUY")?></a>
								 <a href="<?echo $arResult["ADD_URL"]?>" rel="nofollow"><?echo GetMessage("CATALOG_ADD_TO_BASKET")?></a>
								</noindex>
							<?endif;*/?>
						<?elseif((count($arResult["PRICES"]) > 0) || is_array($arResult["PRICE_MATRIX"])):?>
							<?=GetMessage("CATALOG_NOT_AVAILABLE")?>
							<?$APPLICATION->IncludeComponent("bitrix:sale.notice.product", ".default", array(
								"NOTIFY_ID" => $arResult['ID'],
								"NOTIFY_URL" => htmlspecialcharsback($arResult["SUBSCRIBE_URL"]),
								"NOTIFY_USE_CAPTHA" => "N"
								),
								$component
							);?>
						<?endif?>
						<a class="addtocart" href="#" onclick="yaCounter28820320.reachGoal('VK_KART');" id="<? if(empty($arResult["OFFERS"])){echo($arResult["ID"]);}else{echo($arResult["OFFERS"][0]["ID"]);}?>">Купить</a>
					</div>
					<a href="#" data-remodal-target="modal-oneclick-<?=$arResult["ID"]?>" class="buy1">купить в 1 клик</a>
					<!--<img src="<?=SITE_TEMPLATE_PATH?>/images/maincontent/grafic.png" alt="">-->
					<div class="remodal" data-remodal-id="modal-oneclick-<?=$arResult["ID"]?>">
						<?$APPLICATION->IncludeComponent(
							"h2o:buyoneclick",
							"main",
							Array(
								"ASD_ID" => $arResult["ID"],
								"ASD_TITLE" => $_REQUEST["title"],
								"ASD_URL" => $_REQUEST["url"],
								"ASD_PICTURE" => $_REQUEST["picture"],
								"ASD_TEXT" => $_REQUEST["text"],
								"ASD_LINK_TITLE" => "Расшарить в #SERVICE#",
								"ASD_SITE_NAME" => "",
								"ASD_INCLUDE_SCRIPTS" => array()
							)
						);?>
					</div>
				<?}?>
				<p>Адрес сервисного центра BOSCH:</p>
				<?if($arResult['SECTION']["PATH"][0]['ID'] == 1797):?>
					<p>г. Новосибирск, ул. Сибревкома, д. 2, оф. 718</p>
				<?else:?>
					<p>г. Новосибирск, ул. Красноярская, д. 36, цокольный этаж</p>
				<?endif;?>
				<hr class="underline">
				<div class="status_block">
					<?
					$arDelivery = AGGetDeliveryTime ($arResult["ID"]);
					if ($_SESSION["CURRENT_CITY"]["NAME"] == "Новосибирск")
					{?>
					<div class="status">
						<span class="status_left"><?=$arResult["PROPERTIES"]["NSK"]["NAME"]?></span>
						<span class="status_right" style="color: #6a983c"><b><?=$arDelivery["NSK"];?></b></span>
					</div>
					<?}?>
					<div class="status">
						<span class="status_left"><?=$arResult["PROPERTIES"]["OMSK"]["NAME"]?></span>
						<span class="status_right" style="color: #6a983c"><b><?=$arDelivery["OMSK"];?></b></span>
					</div>
				</div>
			<?}?>
			
		</div>
			<div class="tabs">
				<div class="menu_product">
					<ul>
						<?if ($arResult["PROPERTIES"]["ADVANTAGES"]["VALUE"]["TEXT"] || $arResult["PROPERTIES"]["DOP_PREIM"]["VALUE"]["TEXT"])
						{?>
							<li><a href="#fragment-1">Преимущества</a></li>
						<?}?>
						<li><a href="#fragment-2">Характеристики</a></li>
						<?if (!empty($arResult["PROPERTIES"]["VIDEO"]["~VALUE"]))
						{?>
							<li><a href="#fragment-3">Видео</a></li>
						<?}
						if ($arResult["PROPERTIES"]["LEADERSHIP"]["VALUE"][0] > 0)
						{?>
							<li><a href="#fragment-4">Руководства</a></li>
						<?}?>
						<li><a href="#fragment-5">Гарантии</a></li>
						<?if(!empty($arResult["PROPERTIES"]["KOMPL_POST"]["VALUE"]) || !empty($arResult["OFFERS"])){?>
							<li ><a href="#fragment-6" >Комплект поставки</a></li>
						<?}?>
					</ul>
				</div>
				<?if ($arResult["PROPERTIES"]["ADVANTAGES"]["VALUE"]["TEXT"] || $arResult["PROPERTIES"]["DOP_PREIM"]["VALUE"]["TEXT"])
				{?>
				<div class="content_menu" id="fragment-1">
					<?if($arResult["PROPERTIES"]["ADVANTAGES"]["VALUE"]["TEXT"]):?>
						<?$onlyconsonants = str_replace("&amp;amp;amp;amp;", "&", $arResult["PROPERTIES"]["ADVANTAGES"]["VALUE"]["TEXT"]);
						$onlyconsonants = str_replace("&amp;amp;amp;", "&", $onlyconsonants);
						$onlyconsonants = str_replace("&amp;amp;", "&", $onlyconsonants);
						$onlyconsonants = str_replace("&amp;", "&", $onlyconsonants);
						$onlyconsonants=(htmlspecialchars_decode($onlyconsonants));?>
						<?=htmlspecialchars_decode($onlyconsonants);?>
					<?endif;?>
					<?if($arResult["PROPERTIES"]["DOP_PREIM"]["VALUE"]["TEXT"]):?>
						<?$onlyconsonants = str_replace("&amp;amp;amp;amp;", "&", $arResult["PROPERTIES"]["DOP_PREIM"]["VALUE"]["TEXT"]);
						$onlyconsonants = str_replace("&amp;amp;amp;", "&", $onlyconsonants);
						$onlyconsonants = str_replace("&amp;amp;", "&", $onlyconsonants);
						$onlyconsonants = str_replace("&amp;", "&", $onlyconsonants);
						$onlyconsonants=(htmlspecialchars_decode($onlyconsonants));?>
						<?=htmlspecialchars_decode($onlyconsonants);?>
					<?endif;?>
				</div>
				<?}?>
				<div class="content_menu" id="fragment-2">
					<table class="tab_table">
						<?
						$bComplectShow = true;
						foreach($arResult["PROP_CHAR"] as $propChar):
							$bComplectShow = false;
							?>
							<tr>
								<td><?=$arResult["PROPERTIES"][$propChar]["NAME"]?></td>
								<td><?=$arResult["PROPERTIES"][$propChar]["VALUE"]?></td>
							</tr>
						<?endforeach;?>
					</table>
				</div>
				<?if (!empty($arResult["PROPERTIES"]["VIDEO"]["~VALUE"]))
				{?>
					<div class="content_menu" id="fragment-3">
						<?foreach($arResult["PROPERTIES"]["VIDEO"]["~VALUE"] as $video):?>
								<div style="margin: 15px;"><?=htmlspecialchars_decode($video["TEXT"]);?></div>
							<?endforeach;?>
					</div>
				<?}
				if ($arResult["PROPERTIES"]["LEADERSHIP"]["VALUE"][0] > 0)
				{
				?>
				<div class="content_menu" id="fragment-4">
					<?$rsFile = CFile::GetByID($arResult["PROPERTIES"]["LEADERSHIP"]["VALUE"][0]);
					$arFile = $rsFile->Fetch();
					$arFile["PATH"] = CFile::GetPath($arFile["ID"]);?>
					<ul class="adv">
						<li>
							<a href="<?=$arFile["PATH"]?>" target="_blank"><?if ($arFile["DESCRIPTION"] != "") echo $arFile["DESCRIPTION"]; else echo $arFile['ORIGINAL_NAME']?> (<?=round($arFile['FILE_SIZE']/1000000, 1)?> MB)</a>
						</li>
					</ul>
				</div>
				<?}?>
				<div class="content_menu" id="fragment-5">
				<? $APPLICATION->IncludeFile( '/include/garantii.php', array(),
            array()
        ); ?>
				</div>
				<?if(!empty($arResult["PROPERTIES"]["KOMPL_POST"]["VALUE"]) || !empty($arResult["OFFERS"])){?>
					<div class="content_menu" id="fragment-6">
						<?if(!empty($arResult["OFFERS"])){
							foreach($arResult["OFFERS"] as $keyoff=>$off):?>
								<div class="hide tabs_<?=$off["ID"]?>">
									<?$onlyconsonants = str_replace("&amp;amp;amp;amp;", "&", $off["PROPERTIES"]["KOMPLEKT"]["VALUE"]["TEXT"]);
									$onlyconsonants = str_replace("&amp;amp;amp;", "&", $onlyconsonants);
									$onlyconsonants = str_replace("&amp;amp;", "&", $onlyconsonants);
									$onlyconsonants = str_replace("&amp;", "&", $onlyconsonants);
									$onlyconsonants=(htmlspecialchars_decode($onlyconsonants));?>
									<?=htmlspecialchars_decode($onlyconsonants);?>
								</div>
							<?endforeach;
						}else{?>
							<?if($arResult["PROPERTIES"]["KOMPL_POST"]["VALUE"]["TEXT"] !=""):?>
								<?$onlyconsonants = str_replace("&amp;amp;amp;amp;", "&", $arResult["PROPERTIES"]["KOMPL_POST"]["VALUE"]["TEXT"]);
								$onlyconsonants = str_replace("&amp;amp;amp;", "&", $onlyconsonants);
								$onlyconsonants = str_replace("&amp;amp;", "&", $onlyconsonants);
								$onlyconsonants = str_replace("&amp;", "&", $onlyconsonants);
								$onlyconsonants=(htmlspecialchars_decode($onlyconsonants));?>
								<?=htmlspecialchars_decode($onlyconsonants);?>
							<?endif;?>
						<?}?>
					</div>
				<?
				if ($bComplectShow)
				{
					?><script>
						 document.getElementById("ui-id-7").onclick();
					</script><?
				}
			}?>
			</div>
		<?if(!empty($arResult["PROPERTIES"]["NEEDED"]["VALUE"])){?>
			<div class="necessary">
				<p><?=$arResult["PROPERTIES"]["NEEDED"]["NAME"]?>:</p>

				<?foreach ($arResult["PROPERTIES"]["NEEDED"]["VALUE"] as $key=>$needed){?>
						<?
						if($needed["NAME"] == "Аккумуляторная дрель-шуруповёрт GSR 14,4 V-EC FC2") 
						{
							$needed["PRICE"]["PRICE"] = "28500.00";	
						}
						if($needed["NAME"] == "Аккумуляторная дрель-шуруповёрт GSR 18 V-EC FC2") 
						{
							$needed["PRICE"]["PRICE"] = "31300.00";
						}
						$needImg = CFile::ResizeImageGet($needed["IMG_URL"], array('width'=>500), BX_RESIZE_IMAGE_PROPORTIONAL, true);
					?>

						<div class="product_necess">
							<a class="link" href="<?=$needed["DETAIL_PAGE_URL"]?>">
							<div style="display: inline-block; width: 110px; height: 75px; text-align: center; float: left;">
								<img src="<?=$needImg["src"];?>" alt="">
							</div>
							<div class="teod"><?=$needed["NAME"]?></div>
							<p>Артикул: <?=$needed["XML_ID"]?></p>
							<span class="grey_pr"><?=FormatCurrency($needed["PRICE"]['PRICE'], $needed["PRICE"]['CURRENCY'])?></span>
							<?$cur = $needed["PRICE"]['PRICE']*$arResult["basket_discount"];?>
							<div class="red_small_pr"><?=FormatCurrency($cur, $needed["PRICE"]['CURRENCY'])?></div>
							<?if($needed["PRICE"]['PRICE']>0):?>
								<form id="<?=($needed["ID"]);?>" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
								<input type="hidden" name="<?=$arParams["ACTION_VARIABLE"]?>" value="BUY">
								<input type="hidden" name="<?=$arParams["PRODUCT_ID_VARIABLE"]?>" value="<?=($needed["ID"]);?>">
								<!--input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."BUY"?>" value="<?echo GetMessage("CATALOG_BUY")?>"-->
								<input type="hidden" name="<?=$arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?=GetMessage("CATALOG_ADD_TO_BASKET")?>">
								</form>
								
							<?endif;?>
							</a>
							<?if($needed["PRICE"]['PRICE']>0):?>
								<a class="addtocart needed_buy" onclick="yaCounter28820320.reachGoal('VK_KART');" href="#" ids="need" id="<?=($needed["ID"])?>" onclick="yaCounter28820320.reachGoal('VK_DOP_KART');">Купить</a>
							<?endif;?>
						</div>
					
				<?}?>
			</div>
		<?}?>

<?
$arSelectRec = Array("ID");
$arFilterRec = Array("IBLOCK_ID"=>IntVal(5), "ACTIVE"=>"Y", "PROPERTY_RECOMMENDED_ALL_VALUE" => "Да");
$resRec = CIBlockElement::GetList(Array(), $arFilterRec, false, Array("nPageSize"=>5), $arSelectRec);
$REC_ID = array();
while($obRec = $resRec->GetNext())
{
 $REC_ID[] = $obRec['ID'];
}
?>
		<?//if(!empty($arResult["PROPERTIES"]["RECOMMEND"]["VALUE"])){?>
		<?if(count($REC_ID) > 0){?>
		<div class="head"> 
			<p style="color:#333; font-weight: bold; font-size: 18px; letter-spacing: 2px;padding-bottom: 12px;margin-right: 19px;">Рекомендуем приобрести</p>
		</div>
		<div class=" content" style="margin-left: 0px;padding-right: 0px;width: auto;">
<pre><?// print_r($arResult["PROPERTIES"]["RECOMMEND"]["VALUE"])?></pre>
			<?$GLOBALS['arrFilter'] = array("ID" => $REC_ID/*$arResult["PROPERTIES"]["RECOMMEND"]["VALUE"]*/)?>
			<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"h2o_salelider", 
	array(
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "5",
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"ELEMENT_SORT_FIELD" => "RAND",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER2" => "desc",
		"FILTER_NAME" => "arrFilter",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "Y",
		"HIDE_NOT_AVAILABLE" => "N",
		"PAGE_ELEMENT_COUNT" => "4",
		"LINE_ELEMENT_COUNT" => "3",
		"PROPERTY_CODE" => array(
			0 => "ARTNUMBER",
			1 => "SPECIALOFFER",
			2 => "",
		),
		"OFFERS_LIMIT" => "4",
		"SECTION_URL" => "",
		"DETAIL_URL" => "",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"META_KEYWORDS" => "-",
		"SET_META_DESCRIPTION" => "N",
		"META_DESCRIPTION" => "-",
		"BROWSER_TITLE" => "-",
		"ADD_SECTIONS_CHAIN" => "N",
		"DISPLAY_COMPARE" => "N",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"CACHE_FILTER" => "N",
		"PRICE_CODE" => array(
			0 => "базовая",
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "0",
		"PRICE_VAT_INCLUDE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"BASKET_URL" => "/personal/cart/",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"USE_PRODUCT_QUANTITY" => "Y",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"SET_BROWSER_TITLE" => "Y",
		"COMPARE_PATH" => "",
		"OFFERS_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"OFFERS_PROPERTY_CODE" => array(
			0 => "SPECIALOFFER",
			1 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER2" => "desc",
		"PLACE" => "DETAIL",
		"OFFERS_CART_PROPERTIES" => array(
		)
	),
	false
);?>
		</div>

		<?}?>
	</div>

			
				<?/*foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
					<b><?=$arProperty["NAME"]?>:</b> <?
					if(is_array($arProperty["DISPLAY_VALUE"])):
						echo implode(" / ", $arProperty["DISPLAY_VALUE"]);
					elseif($pid=="MANUAL"):
						?><a href="<?=$arProperty["VALUE"]?>"><?=GetMessage("CATALOG_DOWNLOAD")?></a><?
					else:
						echo $arProperty["DISPLAY_VALUE"];?>
					<?endif?><br />
				<?endforeach*/?>
				
	<?/*if($arResult["PREVIEW_TEXT"]):?>
		<br /><?=$arResult["PREVIEW_TEXT"]?><br />
	<?elseif($arResult["DETAIL_TEXT"]):?>
		<br /><?=$arResult["DETAIL_TEXT"]?><br />
	<?endif;*/?>
	<?if(count($arResult["LINKED_ELEMENTS"])>0):?>
		<br /><b><?=$arResult["LINKED_ELEMENTS"][0]["IBLOCK_NAME"]?>:</b>
		<ul>
	<?foreach($arResult["LINKED_ELEMENTS"] as $arElement):?>
		<li><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></li>
	<?endforeach;?>
		</ul>
	<?endif?>
	<?
	if(count($arResult["MORE_PHOTO"])>0):?>
		<a name="more_photo"></a>
		<?foreach($arResult["MORE_PHOTO"] as $PHOTO):?>

			<? $MORE_PHOTO = CFile::ResizeImageGet($needed["IMG_URL"], array('width'=>78,'height'=>78), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>

			<img border="0" src="<?=$MORE_PHOTO["src"]?>" width="<?//=$PHOTO["WIDTH"]?>" height="<?//=$PHOTO["HEIGHT"]?>" alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>" /><br />
		<?endforeach?>
	<?endif?>
	<?if(is_array($arResult["SECTION"])):?>
		<br /><a href="<?=$arResult["SECTION"]["SECTION_PAGE_URL"]?>"><?=GetMessage("CATALOG_BACK")?></a>
	<?endif?>
</div>
<script type="text/javascript">
		$(document).on('click', 'a.addtocart', function (e) {
			e.preventDefault();
			var id = $(this).attr('id');
			console.log(id);
			$('div.tocart').find('a[id="'+id+'"]').addClass('already_in_cart').html('Уже в корзине');
			$('a.needed_buy[id="'+id+'"]').addClass('already_in_cart').html('Уже в корзине');
			$('form#'+id).bitrixAjax(
				"<?=$arParams['AJAX_ID']?>",
				function (data) {
					update_basket_line();
					//$('div.left_side').html(data.find('div.left_side').html());  
				},
				{
					post: {
						submit: 'Y',
					}
				}
			);
		});
</script>
<script>
	$(document).ready(function() {
		var val = $(".menu_product ul li").last();
		var res = val.find("a");
		var txt = res.text();
		if(txt == "Комплект поставки") 
		{
			res.trigger('click');	
		}
		 
		var select = $("select.offers_hide").val();
		console.log(select);
		$(".hide").hide();
		$(".tabs_"+select).show();
		$('select.offers_hide').change(function(e){
			e.preventDefault();
			var select = $(this).val();
			console.log(select);
			$(".hide").hide();
			$(".tabs_"+select).show();
		});
	});
</script>
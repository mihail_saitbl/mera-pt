<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $templateData */
/** @var @global CMain $APPLICATION */
global $APPLICATION;
if (isset($templateData['TEMPLATE_THEME']))
{
	$APPLICATION->SetAdditionalCSS($templateData['TEMPLATE_THEME']);
}
CJSCore::Init(array("popup"));
?>
<script type="text/javascript">
<?
	CModule::IncludeModule('sale');
	$list = array();
	$dbBasketItems = CSaleBasket::GetList(
		array(
			"NAME" => "ASC",
			"ID" => "ASC"
		),
		array(
			"FUSER_ID" => CSaleBasket::GetBasketUserID(),
			"LID" => SITE_ID,
			"ORDER_ID" => "NULL"
		),
		false,
		false,
		array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "PRODUCT_PROVIDER_CLASS")
	);
	while ($arItems = $dbBasketItems->Fetch()) {
		$list[] = $arItems['PRODUCT_ID'];
	}
	//print_r($arResult['ITEMS_ID']);
	foreach ($arResult['ITEMS_ID'] as $id) {
		if (in_array($id, $list)):?>
			$('div.tocart').find('a[id="<?=$id?>"]').addClass('already_in_cart').html('Купить еще');
		<?endif;
	}
?>
</script>
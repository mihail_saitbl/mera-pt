<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

if (method_exists($this, 'setFrameMode')) {
	$this->setFrameMode(true);
}
global $USER;
?>
<div class="buy_one_click_ajaxwrap_<?=$arParams["ASD_ID"]?>">
	<pre><?//print_r($arResult)?></pre>
	<div class="change_pass_forms">
		<?if($arResult['SUCCESS']):?>
			<div class="new remod_h1">
				<h2>КУПИТЬ В 1 КЛИК</h2>
				<div class="clear"></div>
				<hr class="under_menu hr_margin">
				<p>Поздравляем!</p>
				<p>Вы успешно оформили заказ №<?=$arResult['SUCCESS']?>!</p>
			</div>
			<!--<script type="text/javascript">
				window.location.hash = '#?';
				location.reload(true);
			</script>-->
		<?else:
			if(!empty($arResult["ERROR_STRING"])) {
				foreach($arResult["ERROR_STRING"] as $v)
					ShowError($v);
				}?>
			<div class="new remod_h1">
				<h2>КУПИТЬ В 1 КЛИК</h2>
			</div>
			<div class="clear"></div>
			<hr class="under_menu hr_margin">
			<form action="<?=POST_FORM_ACTION_URI?>" class="buy_one_click_form_<?=$arParams["ASD_ID"]?>" method="post" enctype="multipart/form-data">
				<input type="hidden" name="buy_one_click" value="Y" />
			        <div class="change_line">
						<div class="before_input" >ФИО*:</div>
					<input type="text" class="input_line" name="ONECLICK[NAME]" value="<?=$USER->GetFullName()?>" id="individual-name"/>
				</div>
				<div class="change_line">
					<div class="before_input" >Email*:</div>
					<input class="input_line" type="email" name="ONECLICK[EMAIL]" value="<?=$USER->GetEmail()?>" id="individual-email"/>
				</div>
				<div class="change_line">
					<div class="before_input" >Контактный телефон*:</div>
					<input class="input_line" type="phone" name="ONECLICK[PROP][PHONE]" id="individual-phone"/>
				</div>
				<div class="change_line">
					<div class="before_input" style=" height: 100%;">Комментарий к заказу:</div>
					<textarea class="input_line" name="ONECLICK[COMMENT]" id="individual-phone-comm"></textarea>
				</div>
				<span class="required_forms">*Обязательные поля для заполнения</span>
				<div class="clear"></div>
				<hr class="under_catalog">
				<div class="move_button">
					<input class="button_enter" type="submit" name="submit" value="ОФОРМИТЬ ЗАКАЗ"/>
				</div>
	                  <!--button>ОФОРМИТЬ ЗАКАЗ <i class="arrow-white"></i></button-->
			</form>
		<?endif;?>
			
        </div>
</div>
<script>
$(document).on('submit','.buy_one_click_form_<?=$arParams["ASD_ID"]?>',function(e){
	e.preventDefault();
	var $this = $(this);
	$(this).bitrixAjax(
		"<?=$arResult['AJAX_ID']?>",
		function (data) { 
			console.log(data);
			$('.buy_one_click_ajaxwrap_<?=$arParams["ASD_ID"]?>').html(data.find(".buy_one_click_ajaxwrap_<?=$arParams["ASD_ID"]?>").html());
		},{
				post:{
					submit: 'Y',
				}
			}
	);
});
</script>
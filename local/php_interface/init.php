<?



include(__DIR__.'/include/Mobile_Detect.php');
$detect = new Mobile_Detect;
$GLOBALS['isMobile'] = $detect->isMobile();

if ($_SERVER["REQUEST_URI"] == "/robots.txt")
{
    $arDomain = explode(".", $_SERVER["SERVER_NAME"]);
    if (count ($arDomain) == 2 || $arDomain[0] == "www")
    {
        $cur_domain = "";
        $content = file_get_contents($_SERVER["DOCUMENT_ROOT"]."/_robots.txt");
    }
    else
    {
        $cur_domain = $arDomain[0];
        if (is_file($_SERVER["DOCUMENT_ROOT"]."/".$cur_domain."_robots.txt"))
        {
            $content = file_get_contents($_SERVER["DOCUMENT_ROOT"]."/".$cur_domain."_robots.txt");
        }
        else
        {
            $content = file_get_contents($_SERVER["DOCUMENT_ROOT"]."/_robots.txt");
            $content = str_replace('mera-pt.ru', $cur_domain.'.mera-pt.ru', $content);
        }
    }
    header('Content-Type: text/plain');
    header('HTTP/1.1 200 OK');
    echo $content;
    die;
}

if ($_SERVER["REQUEST_URI"] == "/sitemap.xml")
{
    $arDomain = explode(".", $_SERVER["SERVER_NAME"]);
    if (count ($arDomain) == 2 || $arDomain[0] == "www")
    {
        $cur_domain = "";
        $content = file_get_contents($_SERVER["DOCUMENT_ROOT"]."/sitemap_000.xml");
    }
    else
    {
        $cur_domain = $arDomain[0];
        if (is_file($_SERVER["DOCUMENT_ROOT"]."/".$cur_domain."sitemap_000.xml"))
        {
            $content = file_get_contents($_SERVER["DOCUMENT_ROOT"]."/".$cur_domain."sitemap_000.xml");
        }
        else
        {
            $content = file_get_contents($_SERVER["DOCUMENT_ROOT"]."/sitemap_000.xml");
            $content = str_replace('mera-pt.ru', $cur_domain.'.mera-pt.ru', $content);
        }
    }
    header('Content-Type: text/xml');
    header('HTTP/1.1 200 OK');
    echo $content;
    die;
}

include("include/ajax.php");
include("include/buy_one_click.php");
include("include/ipgeobase.php");
/*29.08.2016*/
include("include/logger.php");
//include("include/PHPExcel.php");
//include("include/PHPExcel/IOFactory.php");

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/ecommerce.php");

AddEventHandler("main", "OnBeforeProlog", "AddCouponToSession");
function AddCouponToSession ()
{
    if (isset($_POST["coupon"]) && strlen($_POST["coupon"]) > 0)
    {
        $_SESSION["BW_COUPON_ENTERED"] = $_POST["coupon"];
    }
}

AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "DoIBlockAfterSave");
AddEventHandler("iblock", "OnAfterIBlockElementAdd", "DoIBlockAfterSave");
AddEventHandler("catalog", "OnPriceAdd", "DoIBlockAfterSave");
AddEventHandler("catalog", "OnPriceUpdate", "DoIBlockAfterSave");
AddEventHandler("main", "OnBeforeUserRegister", "OnBeforeUserUpdateHandler");
AddEventHandler("main", "OnBeforeUserUpdate", "OnBeforeUserUpdateHandler");
AddEventHandler("sale", "OnOrderNewSendEmail", 'bxModifySaleMails');
AddEventHandler("sale", "OnOrderAdd", 'ChangeBasketPrices');
AddEventHandler("sale", "OnOrderUpdate", 'ChangeBasketPrices');

function ChangeBasketPrices ($ID, $arFields)
{
    /*$log = '';
    $log .= $ID;
    $log .= print_r($arFields, 1);*/

    $ar = CSaleDiscount::GetByID(1);
    $arar = unserialize($ar['ACTIONS']);
    $basket_discount = $arar['CHILDREN'][0]['DATA']['Value']/100;

    $dbBasketItems = CSaleBasket::GetList(
            array("ID" => "ASC"),
            array("ORDER_ID" => $ID),
            false,
            false,
            array("ID", "PRODUCT_ID", "NAME", "QUANTITY", "PRICE", "CURRENCY", "TYPE", "SET_PARENT_ID")
        );
    while ($arItem = $dbBasketItems->Fetch())
    {
        $arPrices = AGFindTwoPrices($arItem["PRODUCT_ID"], $arItem["QUANTITY"]);

        //$log .= print_r ($arPrices, 1);

        if ($arFields["PAY_SYSTEM_ID"] == 8) // если оплата безнал, то скидка не применяется
        {
            if ($arPrices["DISC_TYPE"] != "always") // если тип скидки - для безнала
            {
                $arPrices["PRICE_DISC"] = $arPrices["PRICE"]; // убираем скидку
            }
        }
        else // если оплата налом, а цены одинаковые, то надо применить скидку
        {
            /*if ($arPrices["PRICE"] == $arPrices["PRICE_DISC"])
            {
                $arPrices["PRICE_DISC"] = ($arPrices["PRICE"] - $arPrices["PRICE"]*$basket_discount);
            }*/
        }

        $arItem["PRICE"] = $arPrices["PRICE_DISC"]/$arItem["QUANTITY"];
        $arItem["DISCOUNT_PRICE"] = $arPrices["PRICE"]/$arItem["QUANTITY"];

        //file_put_contents($_SERVER["DOCUMENT_ROOT"]."/basket.log", "Set price ".$arItem["PRICE"]." for ".$arItem["NAME"]."\n", FILE_APPEND);
        //echo "Set price ".$arItem["PRICE"]." for ".$arItem["NAME"]."\n";

        $arAr = Array ("PRICE" => $arItem["PRICE"], "NAME" => $arItem["NAME"]/*, "DISCOUNT_PRICE" => $arItem["DISCOUNT_PRICE"]*/);

        //$log .= print_r($arAr, 1);

        $res = CSaleBasket::Update ($arItem["ID"], $arAr);
        /*if ($res)
            $log .= "ok\n";
        else
            $log .= "no\n";*/


    }
    //file_put_contents($_SERVER["DOCUMENT_ROOT"]."/basket.log", $log);
    //die;
}

//Автоматическое проставление Ставка НДС = 18% и НДС включен в цену = ДА
AddEventHandler("catalog", "OnProductAdd", "OnProductAdd");
function OnProductAdd($ID,$Fields)
{
$res=Array("VAT_INCLUDED"=>'Y', "VAT_ID"=>'1');
CCatalogProduct::Update($ID,$res);
}
//конец

function multiple($prop, $quant){
    $res = $quant % $prop;
    while($res>0){
        $quant++;
        $res = (($quant % $prop));
    }
    return $quant;
}

function AGFindTwoPrices ($item_id, $quantity)
{
    global $USER;
    $ar = CSaleDiscount::GetByID(1);
    $arar = unserialize($ar['ACTIONS']);
    $basket_discount = $arar['CHILDREN'][0]['DATA']['Value']/100;
    $res = CIBlockElement::GetByID($item_id);
    if($ar_res = $res->GetNext())
        $IBlock_id=$ar_res['IBLOCK_ID'];
    $res = CIBlockElement::GetProperty($IBlock_id, $item_id, "sort", "asc", array("CODE" => "MULTI"));
    while ($ob = $res->GetNext())
    {
        $multi = $ob["VALUE"];
    }

    $Cashless = 0;
    $Cashless_disc = 0;
    $arPrices = Array();
    $db_price = CPrice::GetList(array(),array("PRODUCT_ID" => $item_id,));
    while ($arPrice = $db_price->Fetch()){
        $arPrices[$arPrice["CATALOG_GROUP_ID"]] = $arPrice;
    }
    $price_bas_id = 2;
    if (!($arPrices[2]['PRICE'] > 0) && $arPrices[1]['PRICE'] > 0)
    {
        $price_bas_id = 1;
    }
    $arPriceR = CCatalogProduct::GetOptimalPrice($item_id, $price_bas_id, $USER->GetUserGroupArray(), "N");
    $Cashless = $arPrices[$price_bas_id]['PRICE'];



    if($arPriceR["RESULT_PRICE"]["BASE_PRICE"] > $arPriceR["RESULT_PRICE"]["DISCOUNT_PRICE"]){
       $Cashless_disc = $arPriceR["RESULT_PRICE"]["DISCOUNT_PRICE"];
       $disc_type = 'always';
    }
    elseif ($arPrices[3]['PRICE'] > 0 && $arPrices[3]['PRICE'] <= $arPrices[$price_bas_id]['PRICE'])
    {
        $Cashless_disc = $arPrices[3]['PRICE'];
        $Cashless = $arPrices[$price_bas_id]['PRICE'];
        $disc_type = 'cashless';
    }
    else
    {
        //$Cashless_disc =  $arPriceR["RESULT_PRICE"]["BASE_PRICE"];
        //$Cashless_disc = $arPrices[$price_bas_id]['PRICE'];
        $Cashless_disc = ($Cashless - $Cashless*$basket_discount);
    }
    //$Cashless_disc = $Cashless;
    if ($Cashless/$Cashless_disc > 2 && $multi > 0)
    {
        //$Cashless_disc *= $arItem['CATALOG']['PROPERTIES']['MULTI']['VALUE'];
        $Cashless_disc *= $quantity;
        if ($Cashless_disc > $Cashless)
        {
            $Cashless *= $quantity/$multi;
        }
    }
    else
    {
        $Cashless = $Cashless*$quantity;
        $Cashless_disc = $Cashless_disc*$quantity;
    }

    if (((1 - $Cashless_disc/$Cashless) > $basket_discount+0.01 || $disc_type == 'always') && ($disc_type != 'cashless'))
    {
        $disc_type = 'always';
    }
    else
    {
        $disc_type = 'cashless';
    }

    $arPrices = Array ("PRICE" => $Cashless, "PRICE_DISC" => $Cashless_disc, "DISC_TYPE" => $disc_type);
    return $arPrices;
}

function AGGetDeliveryTime ($item_id)
{
    $arDelivery = Array ("NSK" => 'Спецзаказ', "OMSK" => 'Спецзаказ', "NSK_N" => 30, "OMSK_N" => 30);
    $item_id = IntVal ($item_id);
    if ($item_id <= 0) { return $arDelivery; }
    $arStores = Array();
    $rs = CCatalogStoreProduct::GetList(array(), array('PRODUCT_ID' => $item_id), false, false, array());
    while ($arStore = $rs->Fetch())
    {
        $arStores[$arStore['STORE_ID']] = $arStore;
    }
    if ($arStores[7]["AMOUNT"] > 0) // чехов
    {
        $arDelivery["NSK"] = "Через 2 недели";
        $arDelivery["OMSK"] = "Через 2 недели";
        $arDelivery["NSK_N"] = 14;
        $arDelivery["OMSK_N"] = 14;
    }
    if ($arStores[2]["AMOUNT"] > 0) // бош ново
    {
        $arDelivery["NSK"] = "Через 2 дня";
        $arDelivery["OMSK"] = "Через 3 дня";
        $arDelivery["NSK_N"] = 3;
        $arDelivery["OMSK_N"] = 4;
    }
    if ($arStores[3]["AMOUNT"] > 0 || $arStores[6]["AMOUNT"] > 0 || $arStores[1]["AMOUNT"] > 0 || $arStores[4]["AMOUNT"] > 0) // все новосибирские склады
    {
        $arDelivery["NSK"] = "На следующий день";
        $arDelivery["OMSK"] = "Через день";
        $arDelivery["NSK_N"] = 1;
        $arDelivery["OMSK_N"] = 2;
    }
    if ($arStores[5]["AMOUNT"] > 0) // склад омск
    {
        $arDelivery["OMSK"] = "На следующий день";
        $arDelivery["OMSK_N"] = 1;
    }
    return $arDelivery;
}

function bxModifySaleMails($orderID, &$eventName, &$arFields){
    //file_put_contents($_SERVER["DOCUMENT_ROOT"]."/mail.log", print_r ($arFields, 1));
    ob_start();
	global $APPLICATION;
	$APPLICATION->IncludeComponent("bitrix:sale.personal.order.detail", "mail_items", Array(
		"ID" => $orderID,
		),
		false
	);
	$out = ob_get_contents();
	ob_end_clean();
	$arFields['ORDER_LIST_CUSTOM'] = $out;
    /*$dbBasketItems = CSaleBasket::GetList(
        array(),
        array(
                "ORDER_ID" => $orderID,
                "LID" => SITE_ID,
            ),
        false,
        false,
        array("ID", "CALLBACK_FUNC", "MODULE",
              "PRODUCT_ID", "QUANTITY", "DELAY",
              "CAN_BUY", "PRICE", "WEIGHT")
        );
    $amount = 0;
    while ($arItems = $dbBasketItems->Fetch())
    {
        $ID = $arItems['ID'];
        $ar_prod = CCatalogProduct::GetByID($ID);
        $ar_price = CPrice::GetBasePrice($ID);
        $db_props = CIBlockElement::GetProperty(5, $ID, array("sort" => "asc"), Array("CODE"=>"STATUS"));
        if($ar_props = $db_props->Fetch())
            $status = IntVal($ar_props["VALUE"]);
        if($status>0)
            $amount = 2;
        if($ar_price["PRICE"]>0 && $amount==0){
            $amount = 2;
        }
        $rsStoreNsk = CCatalogStoreProduct::GetList(array(), array('PRODUCT_ID' =>$ID, 'STORE_ID' => 1), false, false, array());
        $rsStoreBsh = CCatalogStoreProduct::GetList(array(), array('PRODUCT_ID' =>$ID, 'STORE_ID' => 2), false, false, array());
        if ($arStoreNsk = $rsStoreNsk->Fetch())
            if(($arStoreNsk['AMOUNT']>$arItems['QUANTITY']) || $ar_prod['QUANTITY'] > 0)
                $amount = 1;
        if ($arStoreBsh = $rsStoreBsh->Fetch())
            if(($arStoreBsh['AMOUNT']>$arItems['QUANTITY'] && $amount == 1))
                $amount = 2;
    }
    if($_SESSION['CURRENT_CITY']['ID']==338){
        if($amount==1){
            $del = "Доставка на следующий день";
        }elseif($amount==2){
            $del = "Доставка через 2 дня";
        }else{
            $del = "Спецзаказ";
        }
    }else{
        if($amount==1){
            $del = "Доставка через один  день";
        }elseif($amount==2){
            $del = "Доставка через 3 дня";
        }else{
            $del = "Спецзаказ";
        }
    }
    $arFields['DEL']=$del;*/
    $dbOrder = CSaleOrderPropsValue::GetOrderProps($arFields['ORDER_ID']);
    while ($arOrder = $dbOrder -> Fetch()){
        $props[$arOrder["CODE"]]=$arOrder;
    }
    $arFields["PHONE"] = $props["PHONE"]['VALUE'];
    $arFields["ADDRESS"] = $props["ADDRESS"]['VALUE'];
    $arFields["CITY"] = $props["CITY"]['VALUE'];
//    file_put_contents($_SERVER["DOCUMENT_ROOT"]."/mail.log", print_r ($arFields, 1), FILE_APPEND);
}

function OnBeforeUserUpdateHandler(&$arFields)
{
        $arFields["LOGIN"] = $arFields["EMAIL"];
        return $arFields;
}
function DoIBlockAfterSave($arg1, $arg2 = false){
    CModule::IncludeModule("sale");
    $ar = CSaleDiscount::GetByID(1);
    $arar = unserialize($ar['ACTIONS']);
    $ct = 1 - $arar['CHILDREN'][0]['DATA']['Value']/100;
    $ELEMENT_ID = false;
    $IBLOCK_ID = false;
    $OFFERS_IBLOCK_ID = false;
    $OFFERS_PROPERTY_ID = false;
    if (CModule::IncludeModule('currency'))
        $strDefaultCurrency = CCurrency::GetBaseCurrency();

    //Check for catalog event
    if(is_array($arg2) && $arg2["PRODUCT_ID"] > 0)
    {
        //Get iblock element
        $rsPriceElement = CIBlockElement::GetList(
            array(),
            array(
                "ID" => $arg2["PRODUCT_ID"],
            ),
            false,
            false,
            array("ID", "IBLOCK_ID")
        );
        if($arPriceElement = $rsPriceElement->Fetch())
        {
            $arCatalog = CCatalog::GetByID($arPriceElement["IBLOCK_ID"]);
            if(is_array($arCatalog))
            {
                //Check if it is offers iblock
                if($arCatalog["OFFERS"] == "Y")
                {
                    //Find product element
                    $rsElement = CIBlockElement::GetProperty(
                        $arPriceElement["IBLOCK_ID"],
                        $arPriceElement["ID"],
                        "sort",
                        "asc",
                        array("ID" => $arCatalog["SKU_PROPERTY_ID"])
                    );
                    $arElement = $rsElement->Fetch();
                    if($arElement && $arElement["VALUE"] > 0)
                    {
                        $ELEMENT_ID = $arElement["VALUE"];
                        $IBLOCK_ID = $arCatalog["PRODUCT_IBLOCK_ID"];
                        $OFFERS_IBLOCK_ID = $arCatalog["IBLOCK_ID"];
                        $OFFERS_PROPERTY_ID = $arCatalog["SKU_PROPERTY_ID"];
                    }
                }
                //or iblock which has offers
                elseif($arCatalog["OFFERS_IBLOCK_ID"] > 0)
                {
                    $ELEMENT_ID = $arPriceElement["ID"];
                    $IBLOCK_ID = $arPriceElement["IBLOCK_ID"];
                    $OFFERS_IBLOCK_ID = $arCatalog["OFFERS_IBLOCK_ID"];
                    $OFFERS_PROPERTY_ID = $arCatalog["OFFERS_PROPERTY_ID"];
                }
                //or it's regular catalog
                else
                {
                    $ELEMENT_ID = $arPriceElement["ID"];
                    $IBLOCK_ID = $arPriceElement["IBLOCK_ID"];
                    $OFFERS_IBLOCK_ID = false;
                    $OFFERS_PROPERTY_ID = false;
                }
            }
        }
    }
    //Check for iblock event
    elseif(is_array($arg1) && $arg1["ID"] > 0 && $arg1["IBLOCK_ID"] > 0)
    {
        //Check if iblock has offers
        $arOffers = CIBlockPriceTools::GetOffersIBlock($arg1["IBLOCK_ID"]);
        if(is_array($arOffers))
        {
            $ELEMENT_ID = $arg1["ID"];
            $IBLOCK_ID = $arg1["IBLOCK_ID"];
            $OFFERS_IBLOCK_ID = $arOffers["OFFERS_IBLOCK_ID"];
            $OFFERS_PROPERTY_ID = $arOffers["OFFERS_PROPERTY_ID"];
        }
    }

    if($ELEMENT_ID)
    {
        static $arPropCache = array();
        if(!array_key_exists($IBLOCK_ID, $arPropCache))
        {
            //Check for MINIMAL_PRICE property
            $rsProperty = CIBlockProperty::GetByID("MINIMUM_PRICE", $IBLOCK_ID);
            $arProperty = $rsProperty->Fetch();
            if($arProperty)
                $arPropCache[$IBLOCK_ID] = $arProperty["ID"];
            else
                $arPropCache[$IBLOCK_ID] = false;
        }

        if($arPropCache[$IBLOCK_ID])
        {
            //Compose elements filter
            if($OFFERS_IBLOCK_ID)
            {
                $rsOffers = CIBlockElement::GetList(
                    array(),
                    array(
                    "IBLOCK_ID" => $OFFERS_IBLOCK_ID,
                    "PROPERTY_".$OFFERS_PROPERTY_ID => $ELEMENT_ID,
                    ),
                    false,
                    false,
                    array("ID")
                );
                while($arOffer = $rsOffers->Fetch())
                    $arProductID[] = $arOffer["ID"];

                if (!is_array($arProductID))
                    $arProductID = array($ELEMENT_ID);
            }
            else
                $arProductID = array($ELEMENT_ID);

            $minPrice = false;
            $maxPrice = false;
    	    foreach ($arProductID as $ELEM_ID):
    		$arPrice = CCatalogProduct::GetOptimalPrice($ELEM_ID, 1, array(1,2,3,4,5,6,7), "N", array(), 's1');
    		if (CModule::IncludeModule('currency') && $strDefaultCurrency != $arPrice['CURRENCY'])
    		    $arPrice["PRICE"] = CCurrencyRates::ConvertCurrency($arPrice["PRICE"], $arPrice["CURRENCY"], $strDefaultCurrency);

    		if ($arPrice["DISCOUNT_PRICE"]>0){
    		    $PRICE = $arPrice["DISCOUNT_PRICE"];
    		}

    		if($minPrice === false || $minPrice > $PRICE)
    		    $minPrice = ceil($PRICE*$ct);

    		if($maxPrice === false || $maxPrice < $PRICE)
    		    $maxPrice = $PRICE;
    	    endforeach;

            if ($minPrice == 0)
            {
                $minPrice = NULL;
            }

    	    //Save found minimal price into property
    	    if($minPrice !== false)
    	    {
                if ($_SESSION["1C_EXCHANGE_GOING"] == "YES")
                {
                    $PRICE_1C = "да";
                }
                else
                {
                    $PRICE_1C = "нет";
                }
        		CIBlockElement::SetPropertyValuesEx(
        		    $ELEMENT_ID,
        		    $IBLOCK_ID,
        		    array(
                        "MINIMUM_PRICE" => $minPrice,
                        "MAXIMUM_PRICE" => $maxPrice,
                        "PRICE_1C" => $PRICE_1C,
                    )
        		);
    	    }
        }
    }
}

?>
<?
function discount12($lastId = 19963){
    $res = CIBlockElement::GetList(Array('ID' => 'ASC'), Array("IBLOCK_ID"=>array(5), "ID"=>$lastId, "ACTIVE" => "Y"), false, false, Array("ID", "IBLOCK_ID", "PROPERTY_SPECIALOFFER"));
    while($ob = $res->GetNext()){
        $productID=$ob["ID"];
        $IBLOCK_ID=$ob["IBLOCK_ID"];
        $quantity=1;
        $renewal="N";
        $arPrice = CCatalogProduct::GetOptimalPrice($productID, $quantity, array(1,2,3,4,5,6,7), $renewal, array(), 's1');
        $obee=$ob;
    return $ob;
    }
}?><?
function addAgentForActions(){
    /*$res = CAgent::GetList(Array("ID" => "DESC"), array("NAME" => "discount%"));
    if($ob = $res->GetNext()){
	return "addAgentForActions();";
    }else{
	$ID_AGENT = CAgent::AddAgent(
	    "discount();",
	    "",
	    "N",
	    40,
	    "",
	    "Y",
	    date("d.m.Y H:i:s",time()-3595)
	);
	return "addAgentForActions();";
    }*/
}?><?
function discount($lastId = 0){
    /*$path_to_log = $_SERVER["DOCUMENT_ROOT"].'/mylog.log';
    $log = new Logger($path_to_log);
    //$log->log("start discount(".$lastId.")");
    $res = CIBlockElement::GetList(Array('ID' => 'ASC'), Array("IBLOCK_ID"=>array(5), ">ID"=>$lastId, "ACTIVE" => "Y"), false, false, Array("ID", "IBLOCK_ID", "PROPERTY_SPECIALOFFER"));
    $endtime = time() + 2;
    while($ob = $res->GetNext()){
        $productID=$ob["ID"];
	$IBLOCK_ID=$ob["IBLOCK_ID"];
	//DoIBlockAfterSave($ob);
        $quantity=1;
        $renewal="N";
        $arPrice = CCatalogProduct::GetOptimalPrice($productID, $quantity, array(1,2,3,4,5,6,7), $renewal, array(), 's1');
        $PROPERTY_CODE = "SPECIALOFFER";
        if (($arPrice['RESULT_PRICE']['DISCOUNT']>0) && ($ob["PROPERTY_SPECIALOFFER_VALUE"]!="Y")){
            $PROPERTY_VALUE = 18;
            CIBlockElement::SetPropertyValuesEx($productID, false, array($PROPERTY_CODE => $PROPERTY_VALUE));
	    DoIBlockAfterSave($ob);
	    $log->log("add action $productID");
	    $log->__destruct();
        }elseif(($arPrice['RESULT_PRICE']['DISCOUNT']==0) && ($ob["PROPERTY_SPECIALOFFER_VALUE"]=="Y")){
            $PROPERTY_VALUE = "";
            CIBlockElement::SetPropertyValuesEx($productID, false, array($PROPERTY_CODE => $PROPERTY_VALUE));
	    DoIBlockAfterSave($ob);
	    $log->log("lost action $productID");
	    $log->__destruct();
        }
        if(time()>$endtime)
          break;
    }
    if ($ob != false) {
        $log->__destruct();
        return "discount(".$ob['ID'].");";
    }
    $log->log("end agent");
    $log->__destruct();
    return "";*/
}?><?
function getRealIpAddr()
{
	if (!empty($_SERVER['HTTP_CLIENT_IP']))
	{
	$ip=$_SERVER['HTTP_CLIENT_IP'];
	}
	elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
	{
	$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
	}
	else
	{
	$ip=$_SERVER['REMOTE_ADDR'];
	}
	return $ip;
}

session_start();

global $defaultCity;
$defaultCity = 338;

if(empty($_SESSION['ALL_CITY'])){
    if(CModule::IncludeModule("iblock")){
        $rows2 = array();

        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_*");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
        $arFilter = Array("IBLOCK_ID"=>'7', "ACTIVE"=>"Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

        while($ob = $res->GetNextElement()){
            $arFields = $ob->GetFields();
            $arProps = $ob->GetProperties();
            $row2 = array(
                    'ID' => $arFields['ID'],
                    'NAME' => $arFields['NAME'],
                    'CITY_PHONE' => $arProps['CITY_PHONE']['VALUE'],
                    'CITY_PHONE_CODE' => $arProps['CITY_PHONE_CODE']['VALUE'],
                    'CITY_ADDRESS' => $arProps['CITY_ADDRESS']['VALUE'],
                    'CITY_MAP' => $arProps['MAP']['VALUE'],
                    'DELIVERY' => $arProps['DELIVERY']['VALUE'],
                    'COEFFICIENT' => $arProps['COEFFICIENT']['VALUE'],
                    'PRICE_CODE'=> $arProps['CODE_PRICE']['VALUE'],
                    'NAME_PP' => $arProps['NAME_PP']["VALUE"],
                    'DOMAIN' => $arProps["DOMAIN"]["VALUE"],
            );

            $rows2[$row2["ID"]] = $row2;
        }


        $_SESSION['ALL_CITY'] = $rows2;
    }
}

if($_REQUEST['SET_CITY'] == 'Y'){
    if(CModule::IncludeModule("iblock")){
        $rows2 = array();
        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_*");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
        $arFilter = Array("IBLOCK_ID"=>'7', "ACTIVE"=>"Y", 'ID' => $_REQUEST['CURRENT_CITY']);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

        /*
        // DEBUG: 2014.10.17
        while($ob = $res->GetNextElement()){
            $arFields = $ob->GetFields();
            $arProps = $ob->GetProperties();
            $row2 = array(
                    'ID' => $arFields['ID'],
                    'NAME' => $arFields['NAME'],
                    'CITY_PHONE' => $arProps['CITY_PHONE']['VALUE'],
                    'CITY_ADDRESS' => $arProps['CITY_ADDRESS']['VALUE'],
                    'DELIVERY' => $arProps['DELIVERY']['VALUE'],
            );

            $rows2[] = $row2;
        }
        $_SESSION['CURRENT_CITY'] = $rows2;
        */
		if($ob = $res->GetNextElement()){
			$arFields = $ob->GetFields();
			$arProps = $ob->GetProperties();
			$row2 = array(
				'ID' => $arFields['ID'],
				'NAME' => $arFields['NAME'],
				'CITY_PHONE' => $arProps['CITY_PHONE']['VALUE'],
                                'CITY_PHONE_CODE' => $arProps['CITY_PHONE_CODE']['VALUE'],
				'CITY_ADDRESS' => $arProps['CITY_ADDRESS']['VALUE'],
                                'CITY_MAP' => $arProps['MAP']['VALUE'],
				'DELIVERY' => $arProps['DELIVERY']['VALUE'],
				'COEFFICIENT' => $arProps['COEFFICIENT']['VALUE'],
				'PRICE_CODE'=> $arProps['CODE_PRICE']['VALUE'],
                'NAME_PP' => $arProps['NAME_PP']["VALUE"],
                'DOMAIN' => $arProps["DOMAIN"]["VALUE"],
			);
			// $rows2[] = $row2;
			// $_SESSION['CURRENT_CITY'] = $rows2;
			$_SESSION['CURRENT_CITY'] = $row2;
            //$_COOKIE["BW_CITY"] = $_SESSION['CURRENT_CITY']["ID"];
            setcookie("BW_CITY", $_SESSION['CURRENT_CITY']["ID"], time() + 3600*24*30, "/", ".mera-pt.ru");
		}
	}
}
else
{
    $bSearch = true;
    if ($_COOKIE["BW_CITY"] > 0)
    {
        $data['city'] = $_SESSION["ALL_CITY"][$_COOKIE["BW_CITY"]]["NAME"];
    }
    else
    {
        $_SESSION["SHOW_CITY_POPUP"] = "Y";
        if (!empty($_SERVER["SERVER_NAME"]) && $_SERVER["SERVER_NAME"] != "accept.mera-pt.ru" && $_SERVER["SERVER_NAME"] != "mera-pt.ru" && $_SERVER["SERVER_NAME"] != "www.mera-pt.ru")
        {
            foreach ($_SESSION["ALL_CITY"] as $id => $arCity)
            {
                if ($arCity["DOMAIN"] == $_SERVER["SERVER_NAME"])
                {
                    $_SESSION["CURRENT_CITY"] = $arCity;
                    $bSearch = false;
                    break;
                }
            }
        }
        else
        {
            $ip = getRealIpAddr();
            $gb = new IPGeoBase();
            $data = $gb->getRecord($ip);
        }
    }
    if ($bSearch)
    {
        if(CModule::IncludeModule("iblock")){
            $rows2 = array();
            $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_*");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
            $arFilter = Array("IBLOCK_ID"=>'7', "ACTIVE"=>"Y", 'NAME' => $data['city']);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

            if($ob = $res->GetNextElement()){
                $arFields = $ob->GetFields();
                $arProps = $ob->GetProperties();
                $row2 = array(
                    'ID' => $arFields['ID'],
                    'NAME' => $arFields['NAME'],
                    'CITY_PHONE' => $arProps['CITY_PHONE']['VALUE'],
                        'CITY_PHONE_CODE' => $arProps['CITY_PHONE_CODE']['VALUE'],
                    'CITY_ADDRESS' => $arProps['CITY_ADDRESS']['VALUE'],
                        'CITY_MAP' => $arProps['MAP']['VALUE'],
                    'DELIVERY' => $arProps['DELIVERY']['VALUE'],
                    'COEFFICIENT' => $arProps['COEFFICIENT']['VALUE'],
                    'PRICE_CODE'=> $arProps['CODE_PRICE']['VALUE'],
                    'NAME_PP' => $arProps['NAME_PP']["VALUE"],
                    'DOMAIN' => $arProps["DOMAIN"]["VALUE"],
                );

                // $rows2[] = $row2;
                $_SESSION['CURRENT_CITY'] = $row2;
            }
            else
            {
                $_SESSION["CURRENT_CITY"] = $_SESSION["ALL_CITY"][$defaultCity];
            }
        }
    }
    if ($_SESSION["SHOW_CITY_POPUP"] == "Y")
    {
        setcookie("BW_CITY", $_SESSION['CURRENT_CITY']["ID"], time() + 3600*24*30, "/", ".mera-pt.ru");
    }
}
$arCity = array();
if(CModule::IncludeModule("iblock")){
	$arFilter = Array("IBLOCK_ID"=>'7', "ACTIVE"=>"Y", 'ID' => $_SESSION['CURRENT_CITY']['ID']);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array());
	if($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();
		$arFields['PROPERTIES'] = $ob->GetProperties();
		$arCity = $arFields;
	}
}

$_GLOBALS['arCityGlobal'] = $arCity;
/*
if(empty($_SESSION['CURRENT_CITY'])){

    if ($_COOKIE["BW_CITY"] > 0)
    {
        $data['city'] = $_SESSION["ALL_CITY"][$_COOKIE["BW_CITY"]]["NAME"];
    }
    else
    {
        $_SESSION["SHOW_CITY_POPUP"] = "Y";
    	$ip = getRealIpAddr();
    	$gb = new IPGeoBase();
    	$data = $gb->getRecord($ip);
    }

	if(CModule::IncludeModule("iblock")){
        $rows2 = array();
        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_*");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
        $arFilter = Array("IBLOCK_ID"=>'7', "ACTIVE"=>"Y", 'NAME' => $data['city']);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

        if($ob = $res->GetNextElement()){
            $arFields = $ob->GetFields();
            $arProps = $ob->GetProperties();
            $row2 = array(
	            'ID' => $arFields['ID'],
	            'NAME' => $arFields['NAME'],
	            'CITY_PHONE' => $arProps['CITY_PHONE']['VALUE'],
                    'CITY_PHONE_CODE' => $arProps['CITY_PHONE_CODE']['VALUE'],
	            'CITY_ADDRESS' => $arProps['CITY_ADDRESS']['VALUE'],
                    'CITY_MAP' => $arProps['MAP']['VALUE'],
	            'DELIVERY' => $arProps['DELIVERY']['VALUE'],
	            'COEFFICIENT' => $arProps['COEFFICIENT']['VALUE'],
	            'PRICE_CODE'=> $arProps['CODE_PRICE']['VALUE'],
                'NAME_PP' => $arProps['NAME_PP']["VALUE"],
                'DOMAIN' => $arProps["DOMAIN"]["VALUE"],
            );

            // $rows2[] = $row2;
            $_SESSION['CURRENT_CITY'] = $row2;
        }
        else
        {
            $_SESSION["CURRENT_CITY"] = $_SESSION["ALL_CITY"][$defaultCity];
        }
        //setcookie("BW_CITY", $_SESSION['CURRENT_CITY']["ID"], time() + 3600*24*30, "/", ".mera-pt.ru");
    }
    //$_SESSION['data_ip'] = $data;
}*/

if ($_SESSION["CURRENT_CITY"]["DOMAIN"] != $_SERVER["SERVER_NAME"] && !empty($_SERVER["SERVER_NAME"]) && $_SERVER["SERVER_NAME"] != "accept.mera-pt.ru" && $_SERVER["SERVER_NAME"] != "dev.mera-pt.ru" && $_SESSION["SHOW_CITY_POPUP"] != "Y")
{
    echo '
    <html>
        <head>
        </head>
        <body>
            <script>location.host = "'.$_SESSION["CURRENT_CITY"]["DOMAIN"].'";</script>
        </body>
    </html>
    ';
    die;
}

unset($_REQUEST['SET_CITY']);

AddEventHandler("catalog", "OnSuccessCatalogImport1C", "after1CExchange");
function after1CExchange ($arParams, $arFields)
{
    unset ($_SESSION["1C_EXCHANGE_GOING"]);
    global $DB;
    CModule::IncludeModule ("iblock");
    $arItems = Array();
    $arDelete = Array();
    $items = CIBlockElement::GetList (Array("timestamp_x" => "DESC"), Array ("IBLOCK_ID" => 5, "SECTION_ID" => 0, "ACTIVE" => "N"), false, false, Array ("IBLOCK_ID", "ID", "NAME", "XML_ID"));
    while ($arItem = $items->Fetch())
    {
        if (isset($arItems[$arItem["XML_ID"]]))
        {
            $arDelete[] = $arItem["ID"];
        }
        else
        {
            $arItems[$arItem["XML_ID"]] = $arItem["ID"];
        }
    }
    foreach ($arDelete as $id)
    {
        $DB->StartTransaction();
        if(!CIBlockElement::Delete($id))
        {
            //$strWarning .= 'Error!';
            $DB->Rollback();
        }
        else
            $DB->Commit();
    }
}

AddEventHandler("catalog", "OnBeforeCatalogImport1C", "before1CExchange");
function before1CExchange ($arParams, $arFields){
    $_SESSION["1C_EXCHANGE_GOING"] = "YES";
}


/*
For component informunity:feedback
*/
include_once('/home/mera-pt/mera-pt.ru/docs/bitrix/components/informunity/feedback/attach/add_event_handler.php');

AddEventHandler('main', 'OnProlog', 'SetSeoInfo');
function SetSeoInfo ()
{
    global $APPLICATION;
    global $arSeoInfo;
    global $_SESSION;
    $cacheTime = 3600*7;
    $obCache = new CPHPCache();
    $cache_id = $arSeoInfo["OLD_URL"].$_SESSION["CURRENT_CITY"]["NAME"];

    if ($obCache->InitCache($cacheTime, $cache_id, $cache_path))
    {
        $vars = $obCache->GetVars();
        $arSeoInfo = $vars["arSeoInfo"];
    }
    elseif( $obCache->StartDataCache()  )
    {
        CModule::IncludeModule ("iblock");
        $items = CIBlockElement::GetList (Array(), Array ("IBLOCK_ID" => 28, "ACTIVE" => "Y", "NAME" => $arSeoInfo["OLD_URL"]), false, false, Array ("IBLOCK_ID", "ID", "NAME", "PROPERTY_H1", "PROPERTY_TITLE", "PROPERTY_TEXT", "PROPERTY_DESCRIPTION", "PREVIEW_TEXT"));
        if ($arItem = $items->Fetch())
        {
            $arSeoInfo["H1"] = $arItem["PROPERTY_H1_VALUE"];
            $arSeoInfo["TITLE"] = $arItem["PROPERTY_TITLE_VALUE"];
            $arSeoInfo["DESCRIPTION"] = $arItem["PROPERTY_DESCRIPTION_VALUE"];
            $arSeoInfo["TEXT"] = $arItem["PREVIEW_TEXT"];
            if (!empty($arItem["PROPERTY_TEXT_VALUE"]["TEXT"]) && $_SESSION["CURRENT_CITY"]["DOMAIN"] == 'omsk.mera-pt.ru')
            {
                $arSeoInfo["TEXT"] = $arItem["PROPERTY_TEXT_VALUE"]["TEXT"];
                /*foreach ($arItem["PROPERTY_TEXT_DESCRIPTION"] as $key => $code)
                {
                    if (($code == "" && $_SESSION["CURRENT_CITY"]["DOMAIN"] == 'mera-pt.ru') || ($_SESSION["CURRENT_CITY"]["DOMAIN"] == $_SERVER["SERVER_NAME"]))
                    {
                        $arSeoInfo["TEXT"] = $arItem["PROPERTY_TEXT_VALUE"][$key]["TEXT"];
                    }
                }*/
            }
        }
        $obCache->EndDataCache(Array("arSeoInfo" => $arSeoInfo));
    }
    /*global $USER;
    if ($USER->IsAdmin())
    {
        echo '<pre>'.print_r($arSeoInfo, 1).'</pre>'; echo __FILE__.'@'.__LINE__;
    }*/
}

AddEventHandler ('main', 'OnEpilog', 'SetMetaTags');
function SetMetaTags ($content)
{
    global $arSeoInfo;
    global $APPLICATION;
    $page_number = 0;
    if (strpos($_SERVER["REQUEST_URI"], '/catalog') !== false )
    {
        if (isset($GLOBALS["CATALOG_PAGE_NAV_NUMBER"]))
        {
            $page_number = $GLOBALS["CATALOG_PAGE_NAV_NUMBER"];
            if(!empty($arSeoInfo["TITLE"])){
                $arSeoInfo["TITLE"] = $arSeoInfo["TITLE"]." - страница ".$page_number;
            }else{
                $newTitle = $APPLICATION->GetTitle()." - страница ".$page_number;
                $APPLICATION->SetTitle($newTitle);
                $APPLICATION->SetPageProperty ("TITLE", $newTitle);
                $APPLICATION->SetPageProperty ("title", $newTitle);
            }
        }
    }
    if (!empty($arSeoInfo["TITLE"]))
    {
        $APPLICATION->SetTitle ($arSeoInfo["TITLE"]);
        $APPLICATION->SetPageProperty ("TITLE", $arSeoInfo["TITLE"]);
        $APPLICATION->SetPageProperty ("title", $arSeoInfo["TITLE"]);
    }
    if (!empty($arSeoInfo["H1"]))
    {
        $h1 = $arSeoInfo["H1"];
    }
    elseif (!empty($arSeoInfo["H1_BW"]))
    {
        $h1 = $arSeoInfo["H1_BW"];
    }
    else
    {
        $h1 = $APPLICATION->GetProperty("H1");
    }
    if ($page_number > 0)
    {
        $h1 .= $arSeoInfo["H1"];
    }
    $APPLICATION->SetPageProperty ("H1", $h1);
    if (!empty($arSeoInfo["DESCRIPTION"]))
    {
        $APPLICATION->SetPageProperty ("description", $arSeoInfo["DESCRIPTION"]);
    }
    elseif (isset($arSeoInfo["URL"]))
    {
        $APPLICATION->SetPageProperty ("description", "");
    }
    if ($page_number > 0)
    {
        $APPLICATION->SetPageProperty ("description", "");
    }
    $APPLICATION->SetPageProperty ("keywords", "");

}

AddEventHandler('main', 'OnEndBufferContent', 'AddCityTitles', 100);
function AddCityTitles(&$content)
{
    global $arSubDomainInfo;
    if (!defined("ADMIN_SECTION") || !ADMIN_SECTION)
    {
        if (!empty($_SESSION["CURRENT_CITY"]["NAME_PP"]))
        {
            $content = str_replace('#CITY#', $_SESSION["CURRENT_CITY"]["NAME_PP"], $content);
        }
    }
}

AddEventHandler('main', 'OnEpilog', '_Check404Error', 1);
function _Check404Error()
{
    if (defined('ERROR_404') && ERROR_404 == 'Y') {
        global $APPLICATION;
        $APPLICATION->RestartBuffer();
        include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/header.php';
        include $_SERVER['DOCUMENT_ROOT'] . '/404.php';
        include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/footer.php';
    }
}



function clean_expire_cache($path = "")
{
   if (!class_exists("CFileCacheCleaner"))
   {
      require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/cache_files_cleaner.php");
   }

   $curentTime = mktime();

   if (defined("BX_CRONTAB") && BX_CRONTAB === true)
      $endTime = time()+50; //Если на кроне, то работаем 5 секунд
   else
      $endTime = time()+10; //Если на хитах, то не более секунды

   //Работаем со всем кешем
   $obCacheCleaner = new CFileCacheCleaner("all");

   if(!$obCacheCleaner->InitPath($path))
   {
      //Произошла ошибка
      return "clean_expire_cache();";
   }

   $obCacheCleaner->Start();

   while($file = $obCacheCleaner->GetNextFile())
   {
      if (is_string($file))
      {
         $date_expire = $obCacheCleaner->GetFileExpiration($file);
         if($date_expire)
         {
            if($date_expire < $curentTime)
            {
               unlink($file);
            }
         }
         if(time() >= $endTime)
            break;
      }
   }

   if (is_string($file))
   {
      return "clean_expire_cache(\"".$file."\");";
   }
   else
   {
      return "clean_expire_cache();";
   }
}



/*
 * Вывод переменных подробно для админа + backtrace
 * @param mixed[] $mVar переменная для вывода
 * @param int $iToFile сохранить в файл. 1 - сохранить переписать, 2 - сохранить дописать
 * @param int $iMore вывести backtrace
 * @param string путь к файлу относительно корня прим. /mylog.txt, по умолчанию создаст файл printrlog.txt в корне DOCUMENT_ROOT
 */
function printr($mVar, $iToFile = 0, $iMore = 0, $sFileName = ''){
	global $USER;
    if($USER->IsAdmin()) {

        if($iToFile > 0){

            if(strlen($sFileName) <= 0){
                $sFileName = '/printrlog.txt';
            }
            $sFilePath = $_SERVER['DOCUMENT_ROOT'].$sFileName;
            if(!file_exists( $sFilePath )){
                fclose(fopen($sFilePath, 'w+'));
            }

            ob_start();
                if($iMore == 1){
                    debug_print_backtrace();
                }
            is_array($mVar) ? print_r($mVar) : var_dump($mVar);
            $sVarResult = ob_get_clean();


            if($iToFile == 2){
                file_put_contents($sFilePath, $sVarResult, FILE_APPEND);
            } else {
                file_put_contents($sFilePath, $sVarResult);
            }

        } else {
            echo '<pre>';
            if($iMore == 1){
                echo "<br>########################################################<br>";
                debug_print_backtrace();
                echo "########################################################<br>";
            }
            is_array($mVar) ? print_r($mVar) : var_dump($mVar);
            echo "-------<br><br>";
            echo '</pre>';
        }
	}
}
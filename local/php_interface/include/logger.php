<?
class Logger 
{
    protected $file_path;
    
    protected $fp;
 
    public function __construct($file_path){
        $this->file_path=$file_path;
        
 
        $this->open();
    }
 
    public function open(){
        $this->fp = fopen($this->file_path, 'a+');
    }
 
    public static function getLogger($name='root',$file=null){
        if(!isset(self::$loggers[$name])){
            self::$loggers[$name]=new Logger($name, $file);
        }
 
        return self::$loggers[$name];
    }
 
    public function log($message){
        if(!is_string($message)){
            $this->logPrint($message);
 
            return ;
        }
 
        $log='';
 
        $log.='['.date('Y-m-d H:i:s',time()).'] ';
        if(func_num_args()>1){
            $params=func_get_args();
 
            $message=call_user_func_array('sprintf',$params);
        }
 
        $log.=$message;
        $log.="\n";
 
        $this->_write($log);
    }
 
    public function logPrint($obj){
        ob_start();
 
        print_r($obj);
 
        $ob=ob_get_clean();
        $this->log($ob);
    }
 
    protected function _write($string){
        fwrite($this->fp, $string);
 
        //echo $string;
    }
 
    public function __destruct(){
        fclose($this->fp);
    }
}
?>
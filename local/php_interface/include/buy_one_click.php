<?
class BuyOneClick{
	private $name;
	private $email;
	private $phone;
	private $element_id;
	private $newUserGroup = array(5);
	private $PERSON_TYPE_ID = 1;
	private $PAY_SYSTEM_ID = 1;
	private $PRICE_DELIVERY = 0;
	private $DELIVERY_ID = 1;
	private $DISCOUNT_VALUE = 0;
	private $USER_DESCRIPTION = "";
	private $addProperties = array();
	function __construct($element_id, $name,$email,$phone){
		$this->element_id = $element_id;
		$this->name = $name;
		$this->email = $email;
		$this->phone = $phone;
	}
	function SetNewUserGroupId($arGroup){
		$this->newUserGroup = $arGroup;
	}
	function SetPERSON_TYPE_ID($PERSON_TYPE_ID){
		$this->PERSON_TYPE_ID = $PERSON_TYPE_ID;
	}
	function SetPAY_SYSTEM_ID($PAY_SYSTEM_ID){
		$this->PAY_SYSTEM_ID = $PAY_SYSTEM_ID;
	}
	function SetPRICE_DELIVERY($PRICE_DELIVERY){
		$this->PRICE_DELIVERY = $PRICE_DELIVERY;
	}
	function SetDELIVERY_ID($DELIVERY_ID){
		$this->DELIVERY_ID = $DELIVERY_ID;
	}
	function SetDISCOUNT_VALUE($DISCOUNT_VALUE){
		$this->DISCOUNT_VALUE = $DISCOUNT_VALUE;
	}
	function SetPropertiesOrder($addProperties){
		$this->addProperties = $addProperties;
	}
	function SetUserDescription($USER_DESCRIPTION){
		$this->USER_DESCRIPTION = $USER_DESCRIPTION;
	}
	private function AddOrderProperty($code, $value, $order) {
	  if (!strlen($code)) {
	     return false;
	  }
	  if (CModule::IncludeModule('sale')) {
	     if ($arProp = CSaleOrderProps::GetList(array(), array('CODE' => $code))->Fetch()) {
	        return CSaleOrderPropsValue::Add(array(
	           'NAME' => $arProp['NAME'],
	           'CODE' => $arProp['CODE'],
	           'ORDER_PROPS_ID' => $arProp['ID'],
	           'ORDER_ID' => $order,
	           'VALUE' => $value,
	        ));
	     }
	  }
	}
	private function addOrder($PRODUCT_ID, $USER_ID, $user_name = '', $user_email = '', $addProperties = array(), $PERSON_TYPE_ID = 1, $PAY_SYSTEM_ID = 1, $PRICE_DELIVERY = 0, $DELIVERY_ID = 1, $DISCOUNT_VALUE = 0,$USER_DESCRIPTION = ""){
		
		
		
		CModule::IncludeModule('iblock');
		CModule::IncludeModule('sale');
		CModule::IncludeModule('catalog');
		$res = CIBlockElement::GetByID($PRODUCT_ID);
		if($arProduct = $res->GetNext()){
			
			$ar_price = GetCatalogProductPriceList(
			 $PRODUCT_ID,
			 'ID',
			 'ASC'
			);  
			$arProduct['PRICE'] = $ar_price; 
			 	
		}
		$arFields = array(
		   "LID" => SITE_ID,
		   "PERSON_TYPE_ID" => $PERSON_TYPE_ID,
		   "PAYED" => "N",
		   "CANCELED" => "N",
		   "STATUS_ID" => "N",
		   "PRICE" => $arProduct['PRICE'][0]['PRICE'],
		   "CURRENCY" => $arProduct['PRICE'][0]['CURRENCY'],
		   "USER_ID" => $USER_ID,
		   "PAY_SYSTEM_ID" => $PAY_SYSTEM_ID,
		   "PRICE_DELIVERY" => $PRICE_DELIVERY,
		   "DELIVERY_ID" => $DELIVERY_ID,
		   "DISCOUNT_VALUE" => $DISCOUNT_VALUE,
		   //"TAX_VALUE" => 0.0,
		   "USER_DESCRIPTION" => $USER_DESCRIPTION,
		   "ADDITIONAL_INFO" => "Buy one click", 
		);
		
		// add Guest ID
		if (CModule::IncludeModule("statistic"))
		   $arFields["STAT_GID"] = CStatistic::GetEventParam();
		$csaleorder = new CSaleOrder;
		if(!$ORDER_ID = $csaleorder->Add($arFields)){
			return false;
		}
		$ORDER_ID = IntVal($ORDER_ID);
		
		if(!empty($addProperties)){
			foreach($addProperties as $code => $value){
				$this->AddOrderProperty($code,$value,$ORDER_ID);
			}
		}
	
		$arFields = array(
		"PRODUCT_ID" => $PRODUCT_ID,
		"PRODUCT_PRICE_ID" => 0,
		"PRICE" => $arProduct['PRICE'][0]['PRICE'],
		"CURRENCY" => $arProduct['PRICE'][0]['CURRENCY'],
		"QUANTITY" => 1,
		"LID" => SITE_ID,
		"PHONE"=> $this->phone,
		"DELAY" => "N",
		"CAN_BUY" => "Y",
		"NAME" => $arProduct['NAME'],
		"DETAIL_PAGE_URL" => $arProduct['DETAIL_PAGE_URL'],
		"ORDER_ID" => $ORDER_ID
		);
		
		CSaleBasket::Add($arFields);
		
		$arFields = Array(
			"ORDER_ID" => $ORDER_ID,
			"ORDER_DATE" => date("d.m.y"),
			"ORDER_USER" => $user_name,
			"PRICE" => $arProduct['PRICE'][0]['PRICE'],
			//"BCC" => COption::GetOptionString("sale", "order_email", "order@".$SERVER_NAME),
			"EMAIL" => $user_email,
			"PHONE"=> $this->phone,
			"ORDER_LIST" => $arProduct['NAME'],
			//"SALE_EMAIL" => COption::GetOptionString("sale", "order_email", "order@".$SERVER_NAME),
			"DELIVERY_PRICE" => $PRICE_DELIVERY,
		);
		
		$eventName = "SALE_NEW_ORDER";
		
		
		$event = new CEvent;
		$event->Send($eventName, SITE_ID, $arFields, "N");
		return $ORDER_ID;
		
		//CSaleMobileOrderPush::send("ORDER_CREATED", array("ORDER_ID" => $arFields["ORDER_ID"]));
		
	} 
	public function Buy() {
		global $USER;
		/*if($this->name == ''){
			$err_name = true;
		}
		if($this->email == ''){
			$err_email = true;
		}*/
		if($this->phone == '') {
			$err_phone = true;	
		}else {
			$err_phone = false;
		}
		if(!$err_phone && !$err_email){
			if ($USER->IsAuthorized()){
				$return = $this->addOrder($this->element_id,$USER->GetID(),$this->name,$this->email, $this->addProperties,$this->PERSON_TYPE_ID,$this->PAY_SYSTEM_ID,$this->PRICE_DELIVERY,$this->DELIVERY_ID,$this->DISCOUNT_VALUE,$this->USER_DESCRIPTION);
			}else{
		
				$cUser = new CUser; 
				$sort_by = "EMAIL";
				$sort_ord = "ASC";
				$arFilter = array(
				   "email" => $this->email,
				);
				$dbUsers = $cUser->GetList($sort_by, $sort_ord, $arFilter);
				if ($arUser = $dbUsers->Fetch())
				{
					$ID = $arUser["ID"];
				}
				else
				{
					$user = new CUser;
					$arName = explode(" ",$this->name);
					$arFields = Array(
					  "NAME"              => $arName[0],
					  "LAST_NAME"         => $arName[1],
					  "EMAIL"             => $this->email,
					  "LOGIN"             => $this->email,
					  "LID"               => SITE_ID,
					  "ACTIVE"            => "Y",
					  "GROUP_ID"          => $this->newUserGroup,
					  "PASSWORD"          => substr(md5($this->email),0,8),
					  "CONFIRM_PASSWORD"  => substr(md5($this->email),0,8),
					);
					$ID = $user->Add($arFields);
				}
				//echo $ID = $user->Add($arFields);
				if (intval($ID) > 0){
					$return = $this->addOrder($this->element_id,$ID,$this->name,$this->email, $this->addProperties,$this->PERSON_TYPE_ID,$this->PAY_SYSTEM_ID,$this->PRICE_DELIVERY,$this->DELIVERY_ID,$this->DISCOUNT_VALUE,$this->USER_DESCRIPTION);
					$USER->Authorize($ID);
				}else{
					$return = array("ERROR" => $user->LAST_ERROR);
					print_r($return);
				}
			}
		}
		return $return;
	}
	
}
?>

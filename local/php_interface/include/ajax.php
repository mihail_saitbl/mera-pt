<?
class CH2oAjax
{
	function getID($name, $template, $params, $add = array()) {
		global $DB;
		$params = array_merge($params, $add);
		$ajax_call_id = md5($name."|".$template."|".serialize($params)."|".COption::GetOptionString("main", "server_uniq_id", $_SERVER['SERVER_NAME'])); 
		
		// ���������� ajax id � ��� ���������� � ����
		$DB->Query('CREATE TABLE IF NOT EXISTS `c_ajax_id` (`id` varchar(32) NOT NULL, `name` varchar(64) NOT NULL, `template` varchar(64), `params` text, PRIMARY KEY (`id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8;');
		$s_params = serialize($params);
		$DB->Query("REPLACE INTO `c_ajax_id` (`id`, `name`, `template`, `params`) VALUES ('$ajax_call_id', '$name', '$template', '$s_params');");
		
		return $ajax_call_id;
	}
	
	function getComponentID($component, $add = array()) {
		if (is_object($component)) {
			$component_name = $component->GetName();
			$template = $component->GetTemplateName();
			$params = $component->arParams;
			foreach ($params as $key => $value) {
				if ($key[0] == "~") {
					$x = $value;
					$new_key = substr($key, 1);
					$params[$new_key] = $x;
				} else {
					unset($params[$key]);
				}
			}
		} elseif (strlen($component)) {
			$component_name = $component;
		} else {
			return false;
		}
		
		if (is_array($params)) {
			foreach ($params as $p=>$v) if (substr($p, 0, 1)=="~") unset($params[$p]);
		} else {
			$params = array();
		}
		$ajax_call_id = CH2oAjax::getID($component_name, $template, $params, $add);
		
		// $component->arParams["AJAX_ID"] = $ajax_call_id; // ����������� � ������
		
		return $ajax_call_id;
	}
	function prepareComponentAjaxId($name, $template, $params) {
		return CH2oAjax::getID($name, $template, $params);
	}
}

AddEventHandler("main", "OnBeforeProlog", "AjaxHandler", 1);
function AjaxHandler()
{
	session_start();
	global $USER, $DB;
	
	$add_params = $_REQUEST['ajax_component_params'];
	if ((!empty($_SESSION["LAST_AJAX_ID_REDIRECT"]))&&(empty($_REQUEST['ajax_call']))) {
		$ajax_call_id = $_SESSION["LAST_AJAX_ID_REDIRECT"];
		$add_params = $_SESSION["LAST_AJAX_ID_REDIRECT_COMPONENT"];
	} else {
		$ajax_call_id = $_REQUEST['ajax_call'];
		$add_params = $_REQUEST['ajax_component_params'];
	}
	
	if (!empty($ajax_call_id)) {
		$res = $DB->Query("SELECT * FROM `c_ajax_id` WHERE `id`='$ajax_call_id'");
		if ($component_arr = $res->GetNext()) {
			$component['name'] = $component_arr['~name'];
			$component['template'] = $component_arr['~template'];
			$component['params'] = unserialize($component_arr['~params']);
		} else {
			return;
		}
		
		require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
		if (SITE_CHARSET == "windows-1251") {
			function convert_get_data(&$value, $key) { $value = iconv('UTF-8', 'CP1251', $value); }
			array_walk_recursive($_REQUEST, "convert_get_data");
			array_walk_recursive($_POST, "convert_get_data");
			array_walk_recursive($_GET, "convert_get_data");
		}
		$_SESSION["LAST_AJAX_ID_REDIRECT"] = $_REQUEST['ajax_call'];
		$_SESSION["LAST_AJAX_ID_REDIRECT_COMPONENT"] = $_REQUEST['ajax_component_params'];
		
		unset($_REQUEST['ajax_call']);
		unset($_POST['ajax_call']);
		unset($_GET['ajax_call']);
		foreach ($add_params as $key => $value) {
			$component["params"][$key] = $value;
		}
		unset($_REQUEST['ajax_component_params']);
		//$component['params']['AJAX_CALL_ID'] = $ajax_call_id;
		$APPLICATION->IncludeComponent($component['name'], $component['template'], $component["params"], false);
		unset($_SESSION["LAST_AJAX_ID_REDIRECT"]);
		unset($_SESSION["LAST_AJAX_ID_REDIRECT_COMPONENT"]);
		require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
		die();
	}
}
?>
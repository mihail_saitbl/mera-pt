<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$isAjax = ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST["ajax_action"]) && $_POST["ajax_action"] == "Y");

$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
	'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME']
);

?> <div class="bx_compare <? echo $templateData['TEMPLATE_CLASS']; ?>" id="bx_catalog_compare_block"><?

$i = count($arResult["ITEMS"]);
$i--;
if($i>3)$i=3;
$j = 0;
$s = count($arResult["SHOW_PROPERTIES"]);
?>

<?
if (!empty($arResult["SHOW_FIELDS"]))
{
	foreach ($arResult["SHOW_FIELDS"] as $code => $arProp)
	{
		$showRow = true;
		if (!isset($arResult['FIELDS_REQUIRED'][$code]) || $arResult['DIFFERENT'])
		{
			$arCompare = array();
			foreach($arResult["ITEMS"] as &$arElement)
			{
				$arPropertyValue = $arElement["FIELDS"][$code];
				if (is_array($arPropertyValue))
				{
					sort($arPropertyValue);
					$arPropertyValue = implode(" / ", $arPropertyValue);
				}
				$arCompare[] = $arPropertyValue;
			}
			unset($arElement);
			$showRow = (count(array_unique($arCompare)) > 0);
		}


		if ($showRow)
		{
			?> <?
			foreach($arResult["ITEMS"] as &$arElement)
			{
		?>
				<td valign="top">
		<?
				switch($code)
				{
					case "NAME":
 $i = $i + 2;
 $name[$i] = "<a href='".$arElement["DETAIL_PAGE_URL"]."'>".$arElement[$code]."</a>";
						if($arElement["CAN_BUY"]): 
 $btn[$i] = "<noindex><br /><a class='bx_bt_button bx_small' href='".$arElement["BUY_URL"]."' rel='nofollow'>Купить</a></noindex>";
						 elseif(!empty($arResult["PRICES"]) || is_array($arElement["PRICE_MATRIX"])):?>
						<br /><?=GetMessage("CATALOG_NOT_AVAILABLE")?>
						<?endif;
						break;
					case "PREVIEW_PICTURE":
					case "DETAIL_PICTURE":
						if(is_array($arElement["FIELDS"][$code])):?>
<? $img[$i] = "<a href=".$arElement['DETAIL_PAGE_URL']."><img border='0' src=".$arElement['FIELDS'][$code]['SRC']." width='auto' height='150' alt=".$arElement['FIELDS'][$code]['ALT']." title=".$arElement['FIELDS'][$code]['TITLE']." /></a>"; ?>
						<?endif;
						break;
					default:
						echo $arElement["FIELDS"][$code];
						break;
				}
			?>

			<?
$i--;
			}
			unset($arElement);
		}
	?>

	<?
	}
}

$i = count($arResult["ITEMS"]);
$i--;

?>  



<?
$i = 0;
$i = count($arResult["ITEMS"]);
$i--; ?>

<div class="table_compare"> 
<div>
	<p style="height:200px;"></p>
<? foreach ($arResult["SHOW_PROPERTIES"] as $code => $arProperty)
{ ?> <p><?=$arProperty["NAME"]?></p> <? } ?>
</div>
	<div> 
				<? 
    while($i>=0) {
	foreach ($arResult["SHOW_PROPERTIES"] as $code => $arProperty)
	{   
		if($i > 2) { if($j==0)echo'<div class="compare_item" style="display:none;">'; }else{
			         if($j==0)echo'<div class="compare_item">'; }


foreach($arResult["ITEMS"] as $key=>$arElement)
{ 
    if($key==$i) {
		if($j==0){ !$arElement["FIELDS"]["DETAIL_PICTURE"]["SRC"]?$isrc=$arElement["FIELDS"]["PREVIEW_PICTURE"]["SRC"]:$isrc=$arElement["FIELDS"]["DETAIL_PICTURE"]["SRC"];
?>
<a class="close" onclick="CatalogCompareObj.MakeAjaxAction('<?=CUtil::JSEscape($arElement['~DELETE_URL'])?>');" href="javascript:void(0)"><?=GetMessage("CATALOG_REMOVE_PRODUCT")?></a>
		<? echo '<a style="display:block;width:100%;height:120px;" href="'.$arElement["DETAIL_PAGE_URL"].'"><img style="max-height:120px;max-width:120px;margin:0 auto;" src="'.$isrc.'" /></a>'; 
		   echo '<h5><a href="'.$arElement["DETAIL_PAGE_URL"].'">'.$arElement["NAME"].'</a></h5>'; 
           echo '<noindex><br /><a class="bx_bt_button bx_small" href="'.$arElement["BUY_URL"].'" rel="nofollow">Купить</a></noindex>'; }?>

<p><?if($arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]):?>
	<strong><?=(is_array($arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"])? implode("/ ", $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]): $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"])?></strong>
<?else:?>-<?endif?></p>
<?}

}  unset($arElement);
                $j++;
		if($j==$s)echo'</div>';
	}$i--; $j=0;}  ?> 


<? $h = count($arResult["SHOW_PROPERTIES"])*34; ?>
<? $c = count($arResult["ITEMS"]); ?>

<? if($c == 1)echo '<div class="compare_item" style="min-height:'.$h.'px"><p>Подберите дополнительный инструмент для сравнения</p></div><div class="compare_item" style="min-height:'.$h.'px"><p>Подберите дополнительный инструмент для сравнения</p></div>'; ?>
<? if($c == 2)echo '<div class="compare_item" style="min-height:'.$h.'px"><p>Подберите дополнительный инструмент для сравнения</p></div>'; ?>
</div>  
</div> 


<div>
<?
if ($isAjax)
{
	die();
}
?>
</div>
<script type="text/javascript">
	var CatalogCompareObj = new BX.Iblock.Catalog.CompareClass("bx_catalog_compare_block");
</script>
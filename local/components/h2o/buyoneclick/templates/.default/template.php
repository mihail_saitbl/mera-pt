<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

if (method_exists($this, 'setFrameMode')) {
	$this->setFrameMode(true);
}
global $USER;
?>
<h2>Покупка в один клик</h2>

<form action="<?=POST_FORM_ACTION_URI?>" class="buy_one_click_form" method="post" enctype="multipart/form-data">
	<?if($arResult['SUCCESS']):?>
		<h3>Поздравляем!</h3>
		<p>Вы успешно оформили заказ!</p>
	<?else:?>
	<?if(isset($arResult['ERROR_STRING'])):?>
		<?=$arResult['ERROR_STRING']?>
	<?endif;?>
	<input type="hidden" name="buy_one_click" value="Y" />
	<div>
		<span>*ФИО:</span>
		<input type="text" name="ONECLICK[NAME]" value="<?=$USER->GetFullName()?>" class="<?if($arResult['ERR_NAME']):?>error_class<?endif;?>" />
	</div>
	<div>
		<span>*Телефон:</span>
		<input type="text" name="ONECLICK[PHONE]" value="<?=$arUser['PERSONAL_PHONE']?>" class="<?if($arResult['ERR_PHONE']):?>error_class<?endif;?>" />
	</div>
	<div>
		<span>*EMAIL:</span>
		<input type="text" name="ONECLICK[EMAIL]" value="<?=$USER->GetEmail()?>" class="<?if($arResult['ERR_EMAIL']):?>error_class<?endif;?>" />
	</div>
	<button type="submit" class="btn-arr" style="margin-left: 78px;">Купить</button>
	<?endif;?>
</form>
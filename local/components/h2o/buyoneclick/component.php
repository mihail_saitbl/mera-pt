<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
CModule::IncludeModule('iblock');
CModule::IncludeModule('sale');
CModule::IncludeModule('catalog');
$arResult = array();
$db_ptype = CSalePaySystem::GetList($arOrder = Array("SORT"=>"ASC", "PSA_NAME"=>"ASC"), Array("LID"=>SITE_ID, "ACTIVE"=>"Y", "PERSON_TYPE_ID"=>$arParams['PERSON_TYPE_ID']));
$arPaySystem = array();
while ($ptype = $db_ptype->Fetch())
{
   $arPaySystem[] = $ptype;
}
$arResult['PAY_SYSTEM'] = $arPaySystem;
if($_REQUEST['buy_one_click'] == 'Y'){
	$err = false;
	/*if($_REQUEST['ONECLICK']['NAME'] == ''){
		$arResult['ERR_NAME'] = true;
		$err = true;
	}else{*/
		$arResult['ERR_NAME'] = false;
		$name = $_REQUEST['ONECLICK']['NAME'];
	/*}*/
	if($_REQUEST['ONECLICK']['PROP']['PHONE'] == '') 
	{
		$arResult['ERR_PHONE'] = true;
		$err = true;
	}else {
		$arResult['ERR_PHONE'] = false;
		$phone = $_REQUEST['ONECLICK']['PROP']['PHONE'];
	}
	/*if($_REQUEST['ONECLICK']['EMAIL'] == ''){
		$arResult['ERR_EMAIL'] = true;
		$err = true;
	}else{*/
		$arResult['ERR_EMAIL'] = false;
		$email = $_REQUEST['ONECLICK']['EMAIL'];
	/*}*/
	if(!$err){
		$b1c = new BuyOneClick($arParams['ASD_ID'],$name,$email,$phone);
		if(is_array($arParams['NEW_USER_GROUP_ID']) && !empty($arParams['NEW_USER_GROUP_ID'])){
			$b1c->SetNewUserGroupId($arParams['NEW_USER_GROUP_ID']);
		}
		if(is_array($_REQUEST['ONECLICK']['PROP']) && !empty($_REQUEST['ONECLICK']['PROP'])){
			$b1c->SetPropertiesOrder($_REQUEST['ONECLICK']['PROP']);
		}
		if($_REQUEST['ONECLICK']['PAYSYSTEM'] != ''){
			$b1c->SetPAY_SYSTEM_ID($_REQUEST['ONECLICK']['PAYSYSTEM']);
		}
		if($_REQUEST['ONECLICK']['COMMENT'] != ''){
			$b1c->SetUserDescription($_REQUEST['ONECLICK']['COMMENT']);
		}
		if($arParams['PERSON_TYPE_ID'] != ''){
			$b1c->SetPERSON_TYPE_ID($arParams['PERSON_TYPE_ID']);
		}
		$return = $b1c->Buy();
		$arResult['TEST'] = $return;
		if($return['ERROR'] != ""){
			$arResult['ERROR_STRING'] = $return;
		}elseif(intval($return) > 0){
			$arResult['SUCCESS'] = $return;
		}
	}
}
$this->IncludeComponentTemplate();


?>
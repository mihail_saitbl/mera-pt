<? include("include/ajax.php"); ?>
<? include("include/buy_one_click.php"); ?>
<? include("include/ipgeobase.php"); ?>
<? include("include/logger.php"); ?>
<?
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "DoIBlockAfterSave");
AddEventHandler("iblock", "OnAfterIBlockElementAdd", "DoIBlockAfterSave");
AddEventHandler("catalog", "OnPriceAdd", "DoIBlockAfterSave");
AddEventHandler("catalog", "OnPriceUpdate", "DoIBlockAfterSave");
AddEventHandler("main", "OnBeforeUserRegister", "OnBeforeUserUpdateHandler");
AddEventHandler("main", "OnBeforeUserUpdate", "OnBeforeUserUpdateHandler");
function OnBeforeUserUpdateHandler(&$arFields)
{
        $arFields["LOGIN"] = $arFields["EMAIL"];
        $arFields["NEW_LOGIN"] = $arFields["NEW_EMAIL"];
        return $arFields;
}
function DoIBlockAfterSave($arg1, $arg2 = false){
    $ELEMENT_ID = false;
    $IBLOCK_ID = false;
    $OFFERS_IBLOCK_ID = false;
    $OFFERS_PROPERTY_ID = false;
    if (CModule::IncludeModule('currency'))
        $strDefaultCurrency = CCurrency::GetBaseCurrency();
    
    //Check for catalog event
    if(is_array($arg2) && $arg2["PRODUCT_ID"] > 0)
    {
        //Get iblock element
        $rsPriceElement = CIBlockElement::GetList(
            array(),
            array(
                "ID" => $arg2["PRODUCT_ID"],
            ),
            false,
            false,
            array("ID", "IBLOCK_ID")
        );
        if($arPriceElement = $rsPriceElement->Fetch())
        {
            $arCatalog = CCatalog::GetByID($arPriceElement["IBLOCK_ID"]);
            if(is_array($arCatalog))
            {
                //Check if it is offers iblock
                if($arCatalog["OFFERS"] == "Y")
                {
                    //Find product element
                    $rsElement = CIBlockElement::GetProperty(
                        $arPriceElement["IBLOCK_ID"],
                        $arPriceElement["ID"],
                        "sort",
                        "asc",
                        array("ID" => $arCatalog["SKU_PROPERTY_ID"])
                    );
                    $arElement = $rsElement->Fetch();
                    if($arElement && $arElement["VALUE"] > 0)
                    {
                        $ELEMENT_ID = $arElement["VALUE"];
                        $IBLOCK_ID = $arCatalog["PRODUCT_IBLOCK_ID"];
                        $OFFERS_IBLOCK_ID = $arCatalog["IBLOCK_ID"];
                        $OFFERS_PROPERTY_ID = $arCatalog["SKU_PROPERTY_ID"];
                    }
                }
                //or iblock which has offers
                elseif($arCatalog["OFFERS_IBLOCK_ID"] > 0)
                {
                    $ELEMENT_ID = $arPriceElement["ID"];
                    $IBLOCK_ID = $arPriceElement["IBLOCK_ID"];
                    $OFFERS_IBLOCK_ID = $arCatalog["OFFERS_IBLOCK_ID"];
                    $OFFERS_PROPERTY_ID = $arCatalog["OFFERS_PROPERTY_ID"];
                }
                //or it's regular catalog
                else
                {
                    $ELEMENT_ID = $arPriceElement["ID"];
                    $IBLOCK_ID = $arPriceElement["IBLOCK_ID"];
                    $OFFERS_IBLOCK_ID = false;
                    $OFFERS_PROPERTY_ID = false;
                }
            }
        }
    }
    //Check for iblock event
    elseif(is_array($arg1) && $arg1["ID"] > 0 && $arg1["IBLOCK_ID"] > 0)
    {
        //Check if iblock has offers
        $arOffers = CIBlockPriceTools::GetOffersIBlock($arg1["IBLOCK_ID"]);
        if(is_array($arOffers))
        {
            $ELEMENT_ID = $arg1["ID"];
            $IBLOCK_ID = $arg1["IBLOCK_ID"];
            $OFFERS_IBLOCK_ID = $arOffers["OFFERS_IBLOCK_ID"];
            $OFFERS_PROPERTY_ID = $arOffers["OFFERS_PROPERTY_ID"];
        }
    }
    
    if($ELEMENT_ID)
    {
        static $arPropCache = array();
        if(!array_key_exists($IBLOCK_ID, $arPropCache))
        {
            //Check for MINIMAL_PRICE property
            $rsProperty = CIBlockProperty::GetByID("MINIMUM_PRICE", $IBLOCK_ID);
            $arProperty = $rsProperty->Fetch();
            if($arProperty)
                $arPropCache[$IBLOCK_ID] = $arProperty["ID"];
            else
                $arPropCache[$IBLOCK_ID] = false;
        }
        
        if($arPropCache[$IBLOCK_ID])
        {
            //Compose elements filter
            if($OFFERS_IBLOCK_ID)
            {
                $rsOffers = CIBlockElement::GetList(
                    array(),
                    array(
                    "IBLOCK_ID" => $OFFERS_IBLOCK_ID,
                    "PROPERTY_".$OFFERS_PROPERTY_ID => $ELEMENT_ID,
                    ),
                    false,
                    false,
                    array("ID")
                );
                while($arOffer = $rsOffers->Fetch())
                    $arProductID[] = $arOffer["ID"];
                
                if (!is_array($arProductID))
                    $arProductID = array($ELEMENT_ID);
            }
            else
                $arProductID = array($ELEMENT_ID);
            
            $minPrice = false;
            $maxPrice = false;
	    foreach ($arProductID as $ELEM_ID):
		$arPrice = CCatalogProduct::GetOptimalPrice($ELEM_ID, 1, array(1,2,3,4,5,6,7), "N", array(), 's1');
		if (CModule::IncludeModule('currency') && $strDefaultCurrency != $arPrice['CURRENCY'])
		    $arPrice["PRICE"] = CCurrencyRates::ConvertCurrency($arPrice["PRICE"], $arPrice["CURRENCY"], $strDefaultCurrency);
		
		if ($arPrice["DISCOUNT_PRICE"]>0){
		    $PRICE = $arPrice["DISCOUNT_PRICE"];
		}
		
		if($minPrice === false || $minPrice > $PRICE)
		    $minPrice = ceil($PRICE*0.9);
		
		if($maxPrice === false || $maxPrice < $PRICE)
		    $maxPrice = $PRICE;
	    endforeach;
		
	    //Save found minimal price into property
	    if($minPrice !== false)
	    {
		CIBlockElement::SetPropertyValuesEx(
		    $ELEMENT_ID,
		    $IBLOCK_ID,
		    array(
			"MINIMUM_PRICE" => $minPrice,
			"MAXIMUM_PRICE" => $maxPrice,
		    )
		);
	    }
        }
    }
}

?>
<?
function discount12($lastId = 19963){
    $res = CIBlockElement::GetList(Array('ID' => 'ASC'), Array("IBLOCK_ID"=>array(5), "ID"=>$lastId, "ACTIVE" => "Y"), false, false, Array("ID", "IBLOCK_ID", "PROPERTY_SPECIALOFFER"));
    while($ob = $res->GetNext()){
        $productID=$ob["ID"];
        $IBLOCK_ID=$ob["IBLOCK_ID"];
        $quantity=1;
        $renewal="N";
        $arPrice = CCatalogProduct::GetOptimalPrice($productID, $quantity, array(1,2,3,4,5,6,7), $renewal, array(), 's1');
        $obee=$ob;
    return $ob;
    }
}?><?
function addAgentForActions(){
    $res = CAgent::GetList(Array("ID" => "DESC"), array("NAME" => "discount%"));
    if($ob = $res->GetNext()){
	return "addAgentForActions();";
    }else{
	$ID_AGENT = CAgent::AddAgent(
	    "discount();", 
	    "", 
	    "N", 
	    40,
	    "",
	    "Y",
	    date("d.m.Y H:i:s",time()-3595)
	);
	return "addAgentForActions();";
    }
}?><?
function discount($lastId = 0){
    $path_to_log = $_SERVER["DOCUMENT_ROOT"].'/mylog.log';
    $log = new Logger($path_to_log);
    $log->log("start discount(".$lastId.")");
    $res = CIBlockElement::GetList(Array('ID' => 'ASC'), Array("IBLOCK_ID"=>array(5), ">ID"=>$lastId, "ACTIVE" => "Y"), false, false, Array("ID", "IBLOCK_ID", "PROPERTY_SPECIALOFFER"));
    $endtime = time() + 2;
    while($ob = $res->GetNext()){
        $productID=$ob["ID"];
	$IBLOCK_ID=$ob["IBLOCK_ID"];
	//DoIBlockAfterSave($ob);
        $quantity=1;
        $renewal="N";
        $arPrice = CCatalogProduct::GetOptimalPrice($productID, $quantity, array(1,2,3,4,5,6,7), $renewal, array(), 's1');
        $PROPERTY_CODE = "SPECIALOFFER";
        if (($arPrice['RESULT_PRICE']['DISCOUNT']>0) && ($ob["PROPERTY_SPECIALOFFER_VALUE"]!="Y")){
            $PROPERTY_VALUE = 18;
            CIBlockElement::SetPropertyValuesEx($productID, false, array($PROPERTY_CODE => $PROPERTY_VALUE));
	    DoIBlockAfterSave($ob);
	    $log->log("add action $productID");
	    $log->__destruct();
        }elseif(($arPrice['RESULT_PRICE']['DISCOUNT']==0) && ($ob["PROPERTY_SPECIALOFFER_VALUE"]=="Y")){
            $PROPERTY_VALUE = "";
            CIBlockElement::SetPropertyValuesEx($productID, false, array($PROPERTY_CODE => $PROPERTY_VALUE));
	    DoIBlockAfterSave($ob);
	    $log->log("lost action $productID");
	    $log->__destruct();
        }
        if(time()>$endtime)
          break;
    }
    if ($ob != false) {
        $log->__destruct();
        return "discount(".$ob['ID'].");";
    }
    $log->log("end agent");
    $log->__destruct();
    return "";
}?><?
function getRealIpAddr()
{
	if (!empty($_SERVER['HTTP_CLIENT_IP']))
	{
	$ip=$_SERVER['HTTP_CLIENT_IP'];
	}
	elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
	{
	$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
	}
	else
	{
	$ip=$_SERVER['REMOTE_ADDR'];
	}
	return $ip;
}

session_start();

if($_REQUEST['SET_CITY'] == 'Y'){
    if(CModule::IncludeModule("iblock")){
        $rows2 = array();
        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_*");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
        $arFilter = Array("IBLOCK_ID"=>'7', "ACTIVE"=>"Y", 'ID' => $_REQUEST['CURRENT_CITY']);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        
        /*
        // DEBUG: 2014.10.17
        while($ob = $res->GetNextElement()){ 
            $arFields = $ob->GetFields();         
            $arProps = $ob->GetProperties();
            $row2 = array(
                    'ID' => $arFields['ID'],
                    'NAME' => $arFields['NAME'],
                    'CITY_PHONE' => $arProps['CITY_PHONE']['VALUE'],
                    'CITY_ADDRESS' => $arProps['CITY_ADDRESS']['VALUE'],
                    'DELIVERY' => $arProps['DELIVERY']['VALUE'],
            );
            
            $rows2[] = $row2;
        }
        $_SESSION['CURRENT_CITY'] = $rows2;
        */
		if($ob = $res->GetNextElement()){
			$arFields = $ob->GetFields();         
			$arProps = $ob->GetProperties();
			$row2 = array(
				'ID' => $arFields['ID'],
				'NAME' => $arFields['NAME'],
				'CITY_PHONE' => $arProps['CITY_PHONE']['VALUE'],
                                'CITY_PHONE_CODE' => $arProps['CITY_PHONE_CODE']['VALUE'],
				'CITY_ADDRESS' => $arProps['CITY_ADDRESS']['VALUE'],
                                'CITY_MAP' => $arProps['MAP']['VALUE'],
				'DELIVERY' => $arProps['DELIVERY']['VALUE'],
				'COEFFICIENT' => $arProps['COEFFICIENT']['VALUE'],
				'PRICE_CODE'=> $arProps['CODE_PRICE']['VALUE'],
			);
			// $rows2[] = $row2;
			// $_SESSION['CURRENT_CITY'] = $rows2;
			$_SESSION['CURRENT_CITY'] = $row2; 
		}
	}
        unset($_REQUEST['SET_CITY']);
}
$arCity = array();
if(CModule::IncludeModule("iblock")){
	$arFilter = Array("IBLOCK_ID"=>'7', "ACTIVE"=>"Y", 'ID' => $_SESSION['CURRENT_CITY']['ID']);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array());
	if($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();         
		$arFields['PROPERTIES'] = $ob->GetProperties();
		$arCity = $arFields;
	}
}

$_GLOBALS['arCityGlobal'] = $arCity;
if(empty($_SESSION['CURRENT_CITY'])){
	$ip = getRealIpAddr();	
	$gb = new IPGeoBase();
	$data = $gb->getRecord($ip);
	
	if(CModule::IncludeModule("iblock")){
        $rows2 = array();
        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_*");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
        $arFilter = Array("IBLOCK_ID"=>'7', "ACTIVE"=>"Y", 'NAME' => $data['city']);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        
        if($ob = $res->GetNextElement()){ 
            $arFields = $ob->GetFields();         
            $arProps = $ob->GetProperties();
            $row2 = array(
	            'ID' => $arFields['ID'],
	            'NAME' => $arFields['NAME'],
	            'CITY_PHONE' => $arProps['CITY_PHONE']['VALUE'],
                    'CITY_PHONE_CODE' => $arProps['CITY_PHONE_CODE']['VALUE'],
	            'CITY_ADDRESS' => $arProps['CITY_ADDRESS']['VALUE'],
                    'CITY_MAP' => $arProps['MAP']['VALUE'],
	            'DELIVERY' => $arProps['DELIVERY']['VALUE'],
	            'COEFFICIENT' => $arProps['COEFFICIENT']['VALUE'],
	            'PRICE_CODE'=> $arProps['CODE_PRICE']['VALUE'],
            );
            
            // $rows2[] = $row2;
            $_SESSION['CURRENT_CITY'] = $row2;
        }
    } 	
    //$_SESSION['data_ip'] = $data;
}
	
if(empty($_SESSION['ALL_CITY'])){
    $rows2 = array();
    
    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_*");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
    $arFilter = Array("IBLOCK_ID"=>'7', "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    
    while($ob = $res->GetNextElement()){ 
        $arFields = $ob->GetFields();         
        $arProps = $ob->GetProperties();
        $row2 = array(
                'ID' => $arFields['ID'],
                'NAME' => $arFields['NAME'],
                'CITY_PHONE' => $arProps['CITY_PHONE']['VALUE'],
                'CITY_PHONE_CODE' => $arProps['CITY_PHONE_CODE']['VALUE'],
                'CITY_ADDRESS' => $arProps['CITY_ADDRESS']['VALUE'],
                'CITY_MAP' => $arProps['MAP']['VALUE'],
                'DELIVERY' => $arProps['DELIVERY']['VALUE'],
                'COEFFICIENT' => $arProps['COEFFICIENT']['VALUE'],
                'PRICE_CODE'=> $arProps['CODE_PRICE']['VALUE'],
        );
        
        $rows2[] = $row2;
    }
	
		
	$_SESSION['ALL_CITY'] = $rows2;
}

?><?
/*
For component informunity:feedback
*/
include_once('/home/mera-pt/mera-pt.ru/docs/bitrix/components/informunity/feedback/attach/add_event_handler.php');
?>
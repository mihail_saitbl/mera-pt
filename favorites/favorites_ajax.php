<?
require_once ('../bitrix/modules/main/include.php');
 

if ($USER->isAuthorized() and isset($_POST['id']))
{ 
	$id = $_POST['id']; 
	$rsUser = CUser::GetByID($USER->GetID());
	$arUser = $rsUser->Fetch(); 
	$favorites = $arUser['UF_FAVORITES'];

	if (!in_array($id, $favorites)) 
	{
		$favorites[] = $id;
		$response = 'added';
	}
	else
	{
		$favorites = array_diff($favorites,array($id));
		$response = 'removed';
	}

	global $USER_FIELD_MANAGER;
	$res = $USER_FIELD_MANAGER->Update('USER', $USER->GetID(), array(
		'UF_FAVORITES' => $favorites
	));

	if (defined('BX_COMP_MANAGED_CACHE') and is_object($GLOBALS['CACHE_MANAGER']))
		$GLOBALS['CACHE_MANAGER']->ClearByTag('sess_'.$_SESSION['fixed_session_id'].'_item_'.$id);

	if ($res)
	{  
		header('Content-Type: application/json');
		echo json_encode(array('status'=>$response, 'count'=>count($favorites)));
	}
}else{

		header('Content-Type: application/json');
		echo json_encode(array('status'=>'no'));

}

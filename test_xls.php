<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

include($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/include/PHPExcel.php");
include($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/include/PHPExcel/IOFactory.php");

global $USER;
if (!$USER->IsAdmin()) {
    die ('Только администраторам');
}

if (!function_exists('p')) {
  function p($obj) {
    echo "***<pre>";
    print_r($obj);
    echo "</pre>";
  }
}

if (!function_exists('pe')) {
  function pe($obj) {
    echo "***<pre>";
    print_r($obj);
    echo "</pre>";
    die();
  }
}

if (!function_exists('pa')) {
  function pa($obj) {
    global $USER;
    if ($USER->IsAdmin()) {
      echo "***<pre>";
      print_r($obj);
      echo "</pre>";
    }
  }
}

CModule::IncludeModule("sale");
$ar = CSaleDiscount::GetByID(1);
$arar = unserialize($ar['ACTIONS']);
$ct = 1 - $arar['CHILDREN'][0]['DATA']['Value']/100;
$file = $_REQUEST['file'];
$pr = $_REQUEST['pr'];
$f_24=fopen($_SERVER["DOCUMENT_ROOT"].'/df3.log','a');if($f_24){fwrite($f_24, $_SERVER['REMOTE_ADDR'].':'.$_SERVER['REQUEST_URI']."\n");fclose($f_24);}

//var_dump($pr);exit;
$art = isset($_SESSION['art']) ? $_SESSION['art'] : 2;
if (!empty($_REQUEST["art"]))
{
    $art = $_REQUEST["art"];
    $_SESSION['art'] = $art;
}
set_time_limit(1800);

class chunkReadFilter implements PHPExcel_Reader_IReadFilter{
    private $_startRow = 0;
    private $_endRow = 0;

    public function setRows($startRow, $chunkSize) {
        $this->_startRow    = $startRow;
        $this->_endRow      = $startRow + $chunkSize;
    }

    public function readCell($column, $row, $worksheetName = '') {
        if (($row == 1) || ($row >= $this->_startRow && $row < $this->_endRow)) {
            return true;
        }
        return false;
    }
}

?>
<h1>Импорт прайс-листа</h1>
Подождите завершения импорта, не закрывайте данную страницу!
<div id="progress-bar">
</div>
<div id="content">
</div>
<div id="array">
    <?
    if ($_REQUEST['startRow']>0)
    {
        $startRow = $_REQUEST['startRow'];
    }
    else
    {
        $startRow = 1;   	//начинаем читать со строки 2, в PHPExcel первая строка имеет индекс 1, и как правило это строка заголовков
    }
    $chunkSize = 50;		//размер считываемых строк за раз
    $exit = false;			//флаг выхода
    $empty_value = 0;		//счетчик пустых знаений
    if($_REQUEST['active_sheet'] > 0)
        $active_sheet = $_REQUEST['active_sheet'];
    else
        $active_sheet = 0;

    if (!file_exists($file)) {
        die("Укажите файл");
    }

    $objReader = PHPExcel_IOFactory::createReaderForFile($file);
    $objReader->setReadDataOnly(true);
    if(!$_SESSION['hr']){
        $objRow = $objReader->load($file);
        $objRow->setActiveSheetIndex($active_sheet);
        $objWorksheetRow = $objRow->getActiveSheet();
        $_SESSION['hr']  = $objWorksheetRow->getHighestRow();
        if($_SESSION['hr']<50){
            $_SESSION['hr']=1000;
        }
        $_SESSION['sheet']  = $objRow->getSheetCount();
        $objRow->disconnectWorksheets(); 				//чистим
        unset($objRow);
    }
    $sheet = $_SESSION['sheet'];
    $chunkFilter = new chunkReadFilter();
    $objReader->setReadFilter($chunkFilter);
    $chunkFilter->setRows($startRow,$chunkSize); 	//устанавливаем знаечние фильтра
    $objPHPExcel = $objReader->load($file);		//открываем файл
    $objPHPExcel->setActiveSheetIndex($active_sheet);		//устанавливаем индекс активной страницы
    $objWorksheet = $objPHPExcel->getActiveSheet();	//делаем активной нужную страницу
    $highestColumn      = $objWorksheet->getHighestColumn();
    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
    //внешний цикл, пока файл не кончится
    if($startRow < $_SESSION['hr']){
        if($_SESSION['hr']<= ($startRow + $chunkSize)){
            $row_c = $_SESSION['hr'];
        }else{
            $row_c = $startRow + $chunkSize;
        }
        $array = array();
        for ($i = $startRow; $i < $row_c; $i++) 	//внутренний цикл по строкам
        {
            $item = array();
            for ($col = 0; $col < $highestColumnIndex; ++ $col){
                $value = ($objWorksheet->getCellByColumnAndRow($col, $i));

                $val   = $value->getOldCalculatedValue();
                if($_REQUEST['file'] == 'Pricelist_Blue.xlsx'){

                    if($val == 0 || $val == ''){
                        $val   = $value->getValue();
                    }
					
                }else{
                    if($val == 0 || $val == ''){
                        $val   = $value->getCalculatedValue();
                    }	//	получаем первое знаение в строке
                    if($val == 0 || $val == ''){
                        $val   = $value->getValue();
                    }
                }
                array_push($item, $val);
            }
            array_push($array, $item);
        }			
        $startRow = $row_c;
    }else{
        if ($active_sheet < ($sheet - 1))
        {
            $startRow = 0;
            unset($_REQUEST['startRow']);
            $active_sheet += 1;
            $objRow = $objReader->load($file);
            $objRow->setActiveSheetIndex($active_sheet);
            $objWorksheetRow = $objRow->getActiveSheet();
            $_SESSION['hr']  = $objWorksheetRow->getHighestRow();
            if($_SESSION['hr']<50){
                $_SESSION['hr']=1000;
            }
            $_SESSION['sheet']  = $objRow->getSheetCount();
            $objRow->disconnectWorksheets(); 				//чистим
            unset($objRow);
        } else {
            unset($_SESSION['hr']);
            unset($_REQUEST['active_sheet']);
            echo "<div id='end'>The End</div>";
        }
    }
    $objPHPExcel->disconnectWorksheets(); 				//чистим
    unset($objPHPExcel);
    $log = new Logger("cron.log");
	$log->log("Ожидания обработать: ".count($array).", pr=$pr, art=$art");
	$log_cnt1=$log_cnt2=0;
	
    foreach($array as $element){
        if(($element[$art]) != ""){
			
			
			
            $elem_xml = (str_replace(".", "", $element[$art]));
            $elem_xml2 = (str_replace(".", "", $element[$art + 1]));
            $arSelect = Array("ID", "IBLOCK_ID", "NAME", "XML_ID", 'CATALOG_GROUP_1');
            $arFilter = Array(
                "!PROPERTY_PRICE_1C" => "да",
                Array (
                    "LOGIC" => "OR",
                    array("EXTERNAL_ID"=>($elem_xml)),
                    array("EXTERNAL_ID"=>($elem_xml2)),
                ),
            );
            if(intval ($element[$pr]) > 0){
                $val_pr = $element[$pr];
            }else{
                if(intval ($element[($pr - 1)]) > 0){
                    $val_pr = $element[($pr - 1)];
                }else{
                    $val_pr = $element[($pr - 2)];
                }
            }


            if ($val_pr == 0) { continue; }
            $val_pr_nal = $val_pr * $ct;
            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
//	$log->log("Ищем артикул ".$elem_xml . ' цена:'.$val_pr);

            if($ob = $res->GetNext()){
//	$log->log("		найдено");		

                $PRODUCT_ID = $ob['ID'];
                $PRICE_TYPE_ID = 2;
				$f_24=fopen($_SERVER["DOCUMENT_ROOT"].'/df3.log','a');if($f_24){fwrite($f_24, $element[1].':'.$val_pr."\n");fclose($f_24);}
                $arFields = Array(
                    "PRODUCT_ID" => $PRODUCT_ID,
                    "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
                    "PRICE" => $val_pr,
                    "CURRENCY" => "RUB",
                );
                $res = CPrice::GetList (array(),array("PRODUCT_ID" => $PRODUCT_ID,"CATALOG_GROUP_ID" => $PRICE_TYPE_ID));
                if ($arr = $res->Fetch()){
                    CPrice::Update($arr["ID"], $arFields);
                    $log->log("update ".$ob["NAME"]." ".$val_pr);
					$log_cnt1++;
                }else{
                    CPrice::Add($arFields);
                    $log->log("add ".$ob["NAME"]);
					$log_cnt1++;
                }

                $PRICE_TYPE_ID_NAL = 3;
                $arFieldsNal = Array(
                    "PRODUCT_ID" => $PRODUCT_ID,
                    "CATALOG_GROUP_ID" => $PRICE_TYPE_ID_NAL,
                    "PRICE" => $val_pr_nal,
                    "CURRENCY" => "RUB",
                );
                $res_nal = CPrice::GetList(array(),array("PRODUCT_ID" => $PRODUCT_ID,"CATALOG_GROUP_ID" => $PRICE_TYPE_ID_NAL));
				$f_24=fopen($_SERVER["DOCUMENT_ROOT"].'/df3.log','a');if($f_24){fwrite($f_24, $element[1].':'.$val_pr_nal."\n");fclose($f_24);}
                if ($arr_nal = $res_nal->Fetch()){
                    if (CPrice::Update($arr_nal["ID"], $arFieldsNal))
                    $log->log("update ".$ob["NAME"]." ".$val_pr_nal);
					$log_cnt2++;
                }else{
                    CPrice::Add($arFieldsNal);
                    $log->log("add ".$ob["NAME"]);
					$log_cnt2++;
                }
                $arCompl[] = $ob;
            }
        }else {
			$log->log('ERROR Не сработал поиск по БД:'.$elem_xml2);
		}
    }
			
	$log->log("Обработано  log_cnt1=".$log_cnt1 . ' log_cnt2='.$log_cnt2);
    ?>
    <div id="actwsht" style="display: none;"><?print_r($active_sheet)?></div>
    обработанно
    <div id="strtrw"><?print_r($startRow)?></div> строк из
    <pre><?print_r($_SESSION['hr'])?></pre>
</div>
<script>
    function repeat_import(sR, aS) {
        var strtrw;
        var actwsht;
        var file_a = "<?=$file?>";
        var pr = <?=$pr?>;
        $.ajax({
            url: "test_xls.php",
            timeout: 50000,
            data: ({startRow : sR, active_sheet : aS, file : file_a, pr : pr}),
            success: function(data, textStatus){
                console.log (data);
                var obj = $("<div />").html(data);
                $("#progress-bar").append("I");
                if (obj.find('#end').html() == "The End") {
                    $("#content").html("<h2>Импорт завершен!</h2>");
                }
                else {
                    $("#content").html("<p>" + obj.find('#content').html() + "</p>");
                    $("#array").html("<p>" + obj.find('#array').html() + "</p>");
                    strtrw = obj.find('#strtrw').html();
                    actwsht= obj.find('#actwsht').html();
                    repeat_import(strtrw, actwsht);
                }
            },
            complete: function(xhr, textStatus){
                if (textStatus != "success") {
                    $("#progress-bar").append("I");
                    //repeat_import();
                }
            }
        });
    };
    $( document ).ready(function() {
        repeat_import(<?=$startRow?>, <?=$active_sheet?>);
    });
</script>
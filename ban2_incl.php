<!-- <a href="https://webapp3.bosch.de/warranty/locale.do" style="text-decoration: none;" target="_blank">
<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:xlink="http://www.w3.org/1999/xlink"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   width="265"
   height="126"
  <title
     id="title3002">Our service promise for professional power tools</title>
  <defs
     id="defs4">
    <linearGradient
       id="linearGradient7866">
      <stop
         style="stop-color:#224764;stop-opacity:1;"
         offset="0"
         id="stop7868" />
      <stop
         style="stop-color:#0e72a3;stop-opacity:1;"
         offset="1"
         id="stop7870" />
    </linearGradient>
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient7866"
       id="linearGradient7872"
       x1="132.5"
       y1="926.36218"
       x2="132.5"
       y2="1013.3622"
       gradientUnits="userSpaceOnUse"
       gradientTransform="translate(0,-452.36218)" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient7866"
       id="linearGradient7896"
       gradientUnits="userSpaceOnUse"
       gradientTransform="translate(0,-452.36218)"
       x1="132.5"
       y1="926.36218"
       x2="132.5"
       y2="1013.3622" />
  </defs>
   <g
     inkscape:label="Layer 1"
     inkscape:groupmode="layer"
     id="layer1"
     transform="translate(0,-926.36218)">
    <rect
       style="opacity:1;fill:#e8e8e8;fill-opacity:1;stroke:none"
       id="rect3033"
       width="265"
       height="125.5"
       x="0"
       y="926.36218"
       rx="14"
		ry="14" />
    <path
       style="fill:url(#linearGradient7896);fill-opacity:1;stroke:none"
       d="m 14,474 c -7.756,0 -14,6.244 -14,14 l 0,73 265,0 0,-73 c 0,-7.756 -6.244,-14 -14,-14 z"
       id="rect7346"
       inkscape:connector-curvature="0"
       sodipodi:nodetypes="sccccss"
       transform="translate(0,452.36218)" />
    <image
       y="1013.8622"
       x="52.5"
       id="image7341"
       xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUAAAABMCAYAAAAcJW5lAAAABHNCSVQICAgIfAhkiAAAIABJREFU eJztnXd8HNXV93/nzmzRqveykq3mKndhcAVTTEswLSaFkARCIMCTSgipb3jIk0ZCEtJJQhqQgqnB gKk27gbbuMm2bPWyaltUts/MPe8fsoRlS/KuvHJjv5+P/tk5c++5q9kzt5wCxIkTJ06cOHHixPlg Qaexb3H++efPsynKgszc3EV5+fmViqKmCrCVhEhgZsHMMAypMRiBQMDp83oPBXt73/AEg0+uX7++ +TTqHidOnHOAU2oA582bNys/L+/ukuLia00mU25paSlNmjKFS0tLKSsrC2azGUIIKIoKBkPTNIRC YWi6jp6eHrQ7HNzY2IiD+6rI3e0xQv5Ai6u7++8vvvTi9wHop3IsceLEOfsZdwNYUVGRMSGv4FtT KqZ9PjMzM/GKK6/kyVOmUFJSEsKajlAohJCmQdN0MDOklBBESEiwgpnBLMEMGJJhSAld06BpYYQ1 HW6XC3t2vYeqPXvY7/c1d3Y4v7bm9TWrxntMceLEOTcYNwO4aNq0iSWzZj9eNHHC0iuuuornzz+f zBYzvF4f/MEgwmFtzG1LZmi6Di0cRjAUhq5pcLtd2L55Mxrq6wJtnR3fffXVVx+O4XDixIlzDhJz A1iSU5K7ePmiF4sKC+d/4pOf5GnTppE/EESv1zvE6DEzXE4nmlpa0NjQhObWFg74A1JRFO7qcoqe 3l6RkpoikxOTkJmZSROK7JSXn4+83Bzk5eVBVdXBtvp8PvR6+xDwB6GFQ9iyYQOa6mqCnQ0NN6/Z tOnZWI8xTpw45waxNIDiiosv/lHF7Nn33XzLLZg9ezZ5fX709PXBMCQAIBwOY9euPdhbVcWHDx0y 3G43BUNhIYQgSKlLQq+ADBCZExVVJCuqQopQBJFgVSFNVU3SbDGLpKQkJSsrR0ydOonmzp6FlJQU AEAwFEJHZyd6vX4YWhhvrXmFHa0tVXUN9Ut37drVHcOxxokT5xwgJgZw5syZpUsWLdo+f/78tJtv uYXCmgaXpwdS9hu+7Tt2YtPGTWhsbDR6enuNYCCkK6qiADALoTiI5F6wOCxVNDNzu1lV0yRTgYnV /YpJySOFclTVXKoqVCxUUxpDmknKFJPJaktMTBDp2Rlicmk5ll24BKqqIhAMobGpGV6fDx5nJ29d t9aor2v4+LrNG56OxXjjxIlzbnDSBnDJwoWfWLBw4RN33nUXTSwuRpfTjVA4DACora3Hk088ydU1 h4mloYMVt6JCGiw2mwU/C6lsXr/+tfpo+lu58o5UTgguVcl6paKKSoLMJUUtMptManJSMi5avBgz ZkyDoihwe3pQU18PqWvY+NbrXHfo0D/XvPnmJ092zHHixDk3OCkDeN2HVjy2cMnC277wpS9DNwy4 PN1gZvh8fjz2l79iz+7dhtfrM4hUnUgaTPQ1aP5/b9u2rTdW+n/6jruXq4r6U0UVxYZmWMwWizkr O4uWX3IJCu35kFKi+lANOtxOtNTV8bubN1W/+NLqCgAyRjrEiRPnLGWsBpBWfmTlq5decvFlt99x J7k8Hvj8AQDA2nXr8PSqZ6XL5SYGMxHtVEzKD7e8/dZzsVP7eG6+/QuFKUm2H6hCrADJlMTERJSW loqLLlwCk6KguaUVNfX16HW75bo1L3W88PLLJQBC46lTnDhxzmzGYgDFLR//+PbLrrxyzs0330yd TjeCoRA0TcOjv/8jNm7eAgmEhOBaQPzinU3r/hxzrUfh3m98o9KA+jNFsVykqqCcrGxcdOEiZKSl o6enF1u374AeDvIbq//b/cLq1XYAgVOpX5w4cc4cojaAH1u58s0PffjDF3/85pupvdOJsKahu6cb //u978u29nYJZjeD1knNfMeOHW/0jIfSkXD/d/73LlKV7wpFybOYLbR4wXyUFhejz+fDtne3I+Dz yddf/G/L6ldeKgbAp0vPOENxFBTYrCLhImZxHoNnsuQSKEc9pwaYBNUTaC+R3B6UgbcLHA7/6dC1 ubBwkoUtixTwQgaywUhhQekAQBIhgP0AWgzifSqrewyHeUsOqrynQ9eelIqMQEJoCRFPURUuY4gM sOzXlZQwg32GlM1CoWoZ1vfldDZuI8A4HbqeSqIygNd/+Jq/XH71VZ/53B13UFunE5qmoampCb/8 5a+NluZmIkWE2cC3tr+z4RdH33fjjTf+PCcl/fO94UD9k08+WRHLAdx66623C8YfDSlZD/i/9MSq Vb8ZuPbtb//cbko0npPSqExOSqaKimk0dfIkeL1erN2wGSF/n3zjlZe2v/b66xfEUqfxosNevlBh +WDMGybRIyV7SGG3ZKVe02iLvfPQvlP1A+DKSlNnh+calZXPSshLCGSN+F5wUIDf1CU9ltNW9CJh 3biGRLYl5WWbU5M/z+A7AdijvD3M4E1k8NOyPeEf420Me+32TI1sn2TmT4C5EkRKxDczXEz8ssJ4 NMNRu2ks/TvzJv2KBZdEKh8Mic8XuQ61RttPV0HZCwCJiPtx1NxUdGTlF7EBXLJo0a3XXHPNX+69 7z50OF0IhcKoPlCN//e9BwFig0H7yRBffffddW8M3HPllVdOKZ44ceMXv3JvZmnxRHr4kUfw7fu/ HlPn689/7g5efvlyzKysxKp/PYWd725d98zzz188cP3HP/5xqqGY/ywU041ms4rpU6fSlEnl6Ovr w/ot29DZ1mqsfXn1bzdu3fqlWOo1HrTnlq9QVbxwKvpiRh+Bng1r9Fh+16GNNA6zZAaULnvZJwXj f0E08eQb5EZDyv+X017/BMX4kKu+uDgtURf/R0yfjcZAjwz3SIP+4LPqPy5paIipj2pHYk6ukpz6 bVb4DgIsJ9seA28LXf9GZkfD1mju67RP2i3AsyKVD4WMqXZnfXW0+jkLyiSIIrYr1OpOzYS7FwAi sppzps4pXrBgwWNf/PKX4fJ0IxQKw+Px4Ne/+Z0EscEMJsP4+NHGb8WKFd+smDbtwL1f+1rWhKJC qq5twLq31sX8RyRUBSuuuw7tHV2YVDETV19347LrV6zoGhjbN77xjR7N1/tRoSiPBQJBo7qmFk0t LUhOTsacmTNgtSUr02bOubuysvKsmAWeKoiQDOJPm81yfVfBpPWd2YXlsWy/y1462WUv3yRAf4uJ 8QMAoomKovzdZS/f1FxYOCkmbQJwFhRfl6SpVYLFPbExfgBAqULB/clhtaYjv+xOjoFLGgPUYZ/0 eZGaUgOFvxAL4wcABFzEirLRVVjy0Nri4hiN/8wgEgOoLFy2YOc9X/gCaboBnz+AUCiEH/zgJ9Lp cgkQGqVQrnn33U1VAzfcdP31f5pfWfnDH/zoR5Sbl4+Nm7fiK1+9Fx6nM+YGUFFVFkKgp7cPmq6D LFYsvnR51nUrrvXiyAPwwAMPyK9/8a47EpOTntFCQeNgdQ1cbjfs+XmYOmUSyitmqPYC++sA1NF7 +2AiiJeQybqrK7fsY7For9NefhNY7AQwXi+dBVZpfc9lL195sg0580u+D1KfI6AgFoodByFTEfQH t33S6l67PXOszXSiIslZUP6CAv49EZJiqSIAgEhhVu6bqalr25LysmPe/mnihAbwqiuuenjphRem FdjtcHn6Z+o///kjcLQ6mIm7FFY/sXPLujUD8h9bufKV+QsW3H7/t76F3j4fnG7PYERIXr495jn8 VCFARLj6sothNinQwxqstmQsvOhS64qrP+TBESNIRGwjeYs1wfKqZIma2nqENQ2zZ82ELcGGC5Ys S1x0wQVPxFq/cwUiJJKCf3YVlN9+Mu04CyZ9XTD/mwiJsdJtOIiQyMz/6cgr+dpY2+goLH8AQvlO DNUaEQZfHeKEjW15U4ujvbc9qSyHCoPriHDNOKh2LAvU1MTN3alTIt7bO5MZ1QDOmDGjaPr0qV/8 yEc+Qp1ON5gZL7+8BlVVByQJDqgkHty6de22AfmVN9zw78VLllzxlXvvhcvdDa//1B3OCSGwbPEi ZKSlQtM1KGYzLVh2ifXKyy+vGZC58847Nd3nvdkw5KG+Pi8OHDoMYsaypYsBRYgpFbNuWDRtWmyW Y+ciRESE37lzS2eO5XZX4aQvgfgn0ezXnBREpCjKTzsLy74Y7a3tueUrFMb3xkOtkSDCVJPQ1ndM mFwa6T09KRUZajLeIqbK8dTtaAhUricZ61szCopOVZ/jxagGsHLuvFduve02BI74+bk9Hqx6+lmW UvcxKS9s2bRu8MT1w8uv+PrMWbM++vm776YulweBYBAABmd/p4p5c2ahqMAOIQiqJYGmVMyyX3TR RYMZYb7yla90q6TeqZhNstvtQXtXJxITbZg2ZTKKyyebbPn5L59Shc8+TLpCv492z6otb+LFLPln 46XUaAhJD3flFV8UqfzBrKxkRfCj46nTiBAVKRrfFokoA4qWFPgPBMXUsyJCCs22hOfakTuuM/nx ZkQDOHfu3NkTJhRVTJk2jTzd/e58jz32N9Y0jUiIw6T7Pzcgu2zZsmUVc2b9+L7770d3b9+g8du5 Yyd8p3AWCAB7qvZj3pyZWDD/PBiGjryiiVRcWnbtBXMvGHyo7vrcLevMivkPQlW5s9MJXdcxa0YF TGYTZsyZVz5vyryIT64+iAiixe15E5dFKt9ZXJynkvpP0GnaYyWoUMS/OhJzciMRz7CkfokE5Y23 WsNBkI9mth3+biSyrvySB0DisvHWaSSIqVK1J/3udPUfC0Y2gDNnPvGpT38GPb19kJJRtX8/qqur JcABMsSDW7ZsGYigEDOnTfvvF7/8ZdJ0A31eHwBg27Z3sWfPPqSnpZ6KcQxyuPowNm3dhrzcHJw/ vxLMjPJpM0R2fubvcNR4LUL7lirUbm9vHxxtHRBCYOaMCpgTU8zJ2ba/nFKlz0LMivLRSGWFpv7m dBmUAQgin1KTHzmRHAOKAN15KnQ6rm8p/5DRWndXJC5HbXmTzweJb50KvUaHPnWy+8Knk2EN4Kwp s0ryCgpmFJeWDBq0p556hsOhEBOJjZs3vzHoi3bT9df/4/xFi5Kyc3IGD0mcXU488fiTuOXmm2Do p9aZPBgMoK+vT+7aU4WSCRNQXlYCvy+AuRcsNi2+4ILVg3rfdFNPotXyHZPZZPT5vdA0DdOnToFZ VTG5YmZFZWVl/ilVPNYwG0y8Y9Q/oBY8tloqDPHhSJbBR5aeN46lj1gjgJvcBWWLR5PptJefD6Aw qoYZOgNPgIxbdNIXGIooY8gpYP16EP8GDNcJm2D8Pqut7u5I/S3Nqv4zUOTOv+MFM7zCOMX7XDFk 2CVJ+dSSH93wkY9wn9dHzIwDB6vR5nAYIMFQ6EcDcpctuax0YmnpJz760Y+R0+UZrOnxy0d+hZzs NKOwsFBp63SeutEASE9Pw8svvCA/dP2NosvlRuXcOWhsakJfn1dMLCtfLj0e+5ZD/d7m4WDfn022 xO8G/P48p6cb+TnZmD17Ft7VdItiGD8G8OlTqnwsIfRlt9SedyKxTlQkUaH/48TiIYDSoujB3me3 Z6C1dfQft6I8EEWb78MswXjdAJ4zKWgyDHQqCnI0AxMUohsBvjRqA0BEkuUDAJaPKCKxLDLv2AE1 uZMU/nB2c927w1w+BOB5rqi4190dulaCf0ig4/wpJeN32Y6a/4nU+HUUlV/BEksj13KIwgYT1gvJ LxqQBwhKl1QpC9KYQVJcJogui3SrgsHrTD71trSemqhS2p1JDPuvnlhcfENFxQzq8/ZH6rz11loO hUIhKPT2pjdfXTsgl1+c9/eVH/0owpqGYKg/scpzzz0Pj9sVTEtOUnDMYV96dua4b5iazWboWtCz Z/tWebimFgTg4guXwjAkJpRNU5CW+c8B2ZtuuilsNSU+pKomGQoEYEiJWRXTEAgEqKi07Mrx1vVM IAdV3uyW+j/pZFwJ5qim6z4lYdRlrSu3eAGBlkWrEwNvSy00Jaut9srcttpHM1pqX8luq92R0VL7 Sm5b7aNZjprLAyI0lSTWR9s2SFzWlV824okpCZ4WVXuSb8sa3vi932ZVVTiztWbVPpMxkwz5vaNn 3ZLkb6MxfgCgsvx2VDoegcFvBURoWnZr7SWZbXW/yGlrWJPdVrsjt7nm1dzW+odz2mqvCojgdLB8 Y9R2GD7JxheyWmsvSeupPmuNHzDMDPC8885bPrG4WCVBMAwJn8+Huto6g4RgQxq/HJC79NJLc3Nz chbPnTeP2ju6AADd3d3YsH4jN9fWbZhaXrJ8wNthz94qJCalYGJxceZnPvUpabPZ4PP59mrhcN/J DoCZB60sg2E2m2ExW4LNtQ0Heuf0VNQ1NKK0eCImFBZi7/4qKiopWbDlnS0KjsS5ptiUv/nC1h+G QkFrMBhAoi0RxSUTAKmnVk6dOnPHwYN7T1bHs4G8loZtTnv5swAidh42hTgXQNVI11lR74hWD2J+ ONNRe/+J4pCLWloOM3BJZ37pQ4oQX42mDwHcCWB43VgUgCKzRSy5Pau9LmKvgYsbGoIAHmzPLXlP UcS/SeAv2S11X4zG+DnyJ05jFlHP/gzCgzkttQ+cqK8j3+sVrvySX0Aox7kPEckNfk27dUJnc220 OpyJHGcAJ5WW3nv1hz5EvX39s7/tO7ajp9tjsIBn85Ilr2Jt/8shKy3tdyuuvRaapkHT+19oD//8 EXg87nC7s/1GAL0DlsnlcSErJxMlJaX0lS/+DywmEzo62mfFwhnMYrEOupXpugGTyQRFNXNTc9PF m9e93pGelUnFE4pw/vxK7N67DxNKy9V5M+b8aue+XfcAwNKlSz1vbtr6nB4KfDwUCiPRlohZ06ej vr7erKvq1wHcEgM1zw4MuQ6KiNgAGqwHR7rWjtxEBlZG8z8mpn9nOGrui9QgEGBwW93X3IWl+czi 45H2wwp9tBn4UtEwqdAIMiFSDx8ScI4lRjqvo/7FppyiWUWdzXXR3q/C9Klo+5MwvpPbUv+DSOUJ kNxW/2WXvTwfR16IDA4Iyd/KaKv7VeY5lEz4OAOYk5u7tLCwEJ0uDwBg9569UpesQOAZPPDA4NQ9 326/qvK8+dTd2z+JC4XC8Pb2coej402n03lkZtf/IF234ho89fRzyMrKRkpSIkCE0tKIfT0joqe3 D8FQ6MiqW/B7773XVZRn36UFAnNc3T2UnZGO/NxctHd1iOy83JuwD/cM3JucYH7ML+VKaRiqZEZJ STE0zSC7fcLi3fv2xVTPMxmCEuAofo8mVWkZ8WKu7ZJoQrIYsk06Ej4XrUEggB0yeLuZbBci0uws jBRLfvFFaGtYc+wlIvZwhAaQGaXu0tLUjLq6qNO+jXUGRYTropFnxos5jsiN32A/ALst8nMySBcw xGGVjc+mt9U3RtvOmc4QA2i32zOtVqtNVVVIKaFpYVQfPAwSQiWmwan+smXLFqSnpydYLBYEnf17 4Js2boIWDIST2y3HHRzk5+Vh6dLFIAUwDAnd0NHqjmUCDIaUx/9uWjvbbt/89ls7JpQUA0jHvDkz 8eLLrbAXFycXFBTYHEfyyDXU1KwrKinz64aRwlJCURQU2gsQ7OtOj6GSZz6KcUGE+TEAQm966wQH UDfsZZXU5VHZMuLvjTU9VIHD4Xfllv4/VsVjkd6jkHIZgOMMIJPoiFRtIrIZAfotA58+FanDWjMK iogwNYpbwuGg/54Tiw1PRl1dT8eEyRfnNB1qPFdzAw552ksmTPjQ7NmzORjqL2rU0NCIQCAoiNmV k5H49oBcVmbmfVdedRX7A++vIHbt3o2O1qbWHX07hj32nTd7FmZMnQpHRyc6nW5IKWP4N/wTu2PH jp3BgN8vdR26oWPKlMlQVBW25FRTTkra1wfkbrrpJsNkMW9VhIAQ/W9/e0E+UtIybNOmTZs39q/3 7KG5sHCSZIp8uc/8/Gi590hwFPtU7MlKtf09cvnjyShMf1wyInY5YKIlw31OumyKpl8SuNlVULah K6/0QwxEnm9vDJit1mF1HhGW/7S7HScVf5/bdKjuXDV+wDEzwJSkpEXTpk8fPNGtrj6M/roeYuuq VavCA3LZWVkLJ02eTAMGMKxp8Hjcstvn/fdIHfn8fvz6N7+Tuq5zLPb+gP49v6KiIqVgQiGuvPSS YWVcru71He2OK3Jyc8hmtSI3OxuNTU3CbEtcCeCBAbnEpMTXAn3e5Tiybi8qKsS2re8oUsrLAOyM kcpnJM6i0vkwaBWIbJHew4YxorP42uJiK4cxI9K2SOJpqqoKn1hylDZ27NCchWX/BtP/RCLPwByu rDTRjh3a0Z9rqlyjsnggus5pISm02mUvb+kk+QJYrIdJX5/T0NAeVTsn0pnFrGiiqA0Sf4xl/+ON 2Sz+67SXjaFOz9hjy4cYwPwC+wW5eXkU1vqficamZjBzWBDtP1ouwWbLTkpORlt7JwCgquoAQsFQ 2DCMX4/U0Z49e9He3NhevWvn/WNV9liKp069O8WqLrzvq18cdMI+lt7e3r9U7626Yu7c/olcQV4e auvqkJ6TnXG0nNVk2s7WBBaCiBmwFxSgz+dXrCbTKQsyjy1kc9knjRrPymyYGTQLBuZGk6BAMj+V 097w9kjXp+v6ZJAaedgby9cilh2tGZ3XkBKZASTA4mpzlQE4ePTnuS0N7zjt5Y4xpr8qFCzuAXAP NBVdBeUHmXmjQnIrKWJbenNt1ckkliVCFDG/3JPTWvvOWPs6HRDR5FPd55CHNDk5qSQlJRVOd/8B SCDglzCkh01icKNn6dKl2WazWRVCYMABvKmxEb0eV1/DKG88wzAgJWnVTU0xSzlV3dT0xJTJk1ko I+9beQPe513OTihHZOyFBdB1Hamp6clHy3Xr+u5kq0UKEoIBWC0WSEMiKTXtdASaxwIzg0d3QyER dRZOZj5k9PpGNTKsmSZGE/WrBMw7olRj+H6tcgdpkXsxE2gCjjGABLCT5K/A4scnqw8RphLRVIZy O0vAaS9tc5J4RdeM53M7Jr4Sbfp+QZgQqfVkpvXn8tI1Vgx5WhSTKcFkUiG5/2v2+7wGK8LPxIN5 +hVFuaCosJB17f3/XUdnJ3r7ek5LASRFCDaPMtnYsWOHJpmDqqqCANjtBQgHQzBZLGYcNf65JSXd VkWEVEVAqP1bOcnJNqQkppjHewxnCxK0R5emK/K97V2jyZEQEc+emOFL7aluOGnlAOQ0NLRHEnY2 iFSHPTXeq8pHwBzVXmAkEEQ+GLepqvJfZ35zc2de+Y8dBQVZkd4vOYpZqZAHxqTkB4whBlAyD0Zv SCnh9wdJCDIhxIObyxaLJT81NRWGfP/l4na60NfdPeqPYtyIYOVmGEaAuF82KTERkiV0gykzM3NI vK9qMmsJCTYoot8Ams0WkCriBpDZkMAvQ62HF+S3H2w4sTxSIm2awK5Y1hthIPLnUB1ez4sbGoIE +bmxxklHAgnKEwruN1PC4S57+VcZy048ZyaO2K2IpBL5i+ADzBADyMxiYCtI0zR0e7oVhSnDsKqD Oa0sijIhNS0NzDx4Ytrn93E4bLhPpeJRQfAJAShq/3AZgJQGZSWlTTlaTAjyJSbaYDb1P4tWswVE pnE92TsbkIy/kmp6aDjH4eFQwQkRN07oHbNiwzVHiHglYhg84qFPpqP+NRbG3bHRajQojYCHnfbm 1+qLi0eMxWaAiBFNtbwz9/d4BjHUABqSFCGgqgoCoSB0lkSKSEwWNJhHLS09fWFJaSkl2mzITO// f7ndHiLIaALpTynMbJhUFWbFBAAQRDAMHYpJHbL8SLBaFbPJBLOpXw4EqOr4ujacDQhBt5OuNXTm l/2pe8KEE/pG6qqI+DtjJu3EUpFDJCM/TRY8qp7ZLfV/gsSnmOE7acVOAIEuTtbUNc3AsC+PI7Pk yCMwBJ/2TDFnA0OXwGAOaxqICAkWKxRFgVAUaNAHlxWKEKwbEpphgI5soWdlZkAoIqLZwemADRkC AN3oX9EYug5FEegN+hqOlmtqaqoCgGCw/yTeMAwYYTmudWbPIsxC0O26Yd7jyi1eMJqgAhn5s0CR L5cjwWAl4gSUHIGeWW01j4tg+HwAUZWEHCMX2OylvxjpIoMi/l7ZoDEXWPogMXQGKBlaWIMggslk gtVqYUUIQxg0eGIaNowWj8uJoD+AsNb/srUlJsFqsYxP1awToCjKCfePVFVNNJstMI44TAcCAQBC WiyWhqPldE0zA4Bu9O9vBoNBaDIcP0kbSqFU1Dc6isqvGEmAoERuAKPYL4wEwdGsRNSIZnaZ7qb9 ma01i5jlJxhcc+I7xg4z3THiC4Y54kgZRaGME0vFGWIAiYjDug5wf5GhlORkJqHoijl5sPgJAXVd LjcC4RACof7VS0pyElLSs87YL9xsMWckJFhhGAaCwSAgBFiyrK2tHbJhTkKkA+/XMQmFw9A0YwyO mec2REgUBj/bZS8d1m/LYG6Loq3sSJbVkeAoKLAh0nhgAAprETsqE8DZjrp/ZbXWTmZDXwaWf2OO 4sAl4o6IpEkZyVd25NjrY2Dmc6Jq23gz9OTJYD0QCpoHlrYZGRno7umFakLZgIjX73983+5dD16w YOGRk2BCQUE+UlJTR32Tu9xulE2dmofVo0lFz4n8d5dhmSoUJUFR+rd7mpqaYFJVaCF/AMecPqal phYCGPRv9Pl8CHp7xhSfegYQJtDfRhMwWKYqJJIly0oiiqhexgBEZCPiPzOwjI7Zm2IgqvArLSxm ARjRsTpShDDPBFMU+49K1GFiBDD6ncDfZkC05RfNM5P1EpBxAUtaFIvU/yRxTU9KRUZqb9WQgwwS 3ATQCZPcAgAzL+V+H4mY1+I+lxhiAIPhUI+3z5dts/Xvw+bl54uGxkYQqYMB2M8//3xDbnYOB0IB Ave7ihQXlyA1I8syYcKE6U1NTfsxDNt37kZFxXTLnZ/9LJutVqiqqseiOqJhGKNu9vLC0DV5+fk0 YNRqa+qhCAG3s9tzrGxmZmYK0D8DDIXDCAYC8HR7hh3PmQ/7M1trIqpbZA4uAAAXb0lEQVRtwQC1 502eryrGT6JJYMoslroKilfA0fD8kM97uBapkf9vSZguQAwMoAmmBRFns2Fmr8U4qZx2BEi0NW8H sH3gs9askilmq7gIBl1Egq8CKPrZLZGip4QvRi+eGfK55P0QuCGyJijHnTFhGtzD/x7PRATzEt0c /f9EhBXHWEutDjGAIX+wrafbk222mEEg5Bfkw6yqxEIZUgeWWQb8voBNUVUAAtOnT8Vrr6eIRKv1 ewCGLZaz4pqroWk6vnzX5yCEAMWoQhgRobdv5K2cgokT71q6dCn8/v7UdYdqa0BEcHa0NwwdE2cD EPqR3IYtzc1ISUoywoaxKRZ6nsn0z2oOvcOVlZc727vXEGj4wOphYCi3AhhiAPO8tZ3O1PIWRFhb g8AfB/BQVEoPpwvzJyKVJaL6koaGWKYkAgDYnfXVAKoB/JErKsxdTt/1pKgPRJnFBTp4FnCMASRl +/DSI2A13QZgzIXhTzWBsHTaHdHHTzsLyk4sNAJDjFBYD77X1uqYlZGZBVVVUTpxIkwWmzDApStX rsxYtWqVGwD6vL79bW1tlTm5eWQYBtJSU5GWnkrZOfYPHzh0aNiO5s+bCwBo7zq1NULycnKWTq+o QHefD7qmoebwYdgSEgwt5HvqaLnq6uobp0yZglC4f1+zvr4Rikq6qqoxiVM9G6AdO7SugvL7QIgm NO0yBpRjw66YeScRRWQAmTDHlTFheuZJzFZas0qmgHl+pHHxjKjGOCaOJHj4j6Og4EUz2Z4D4/JI 7xWM4xJmSrO+TYQVjnS2w4TPHszK+t+pg/k54xzLkOWjzxn62sb16xEIhcBA/95eciIJCClsGdcO yPV5+27fsn4DhbUw/IH+mdW82XMwffZM85TyKaNmFPb5/TH9G5ixDcely5Z9MjMry2qx9h+AbHvn XfgDIQT8Xv/hhoYhiQKCgcBn+sfWP5usr29Ab09Pb3X12V3zIFqyHTU7AY54ZkRENkdWyXGFfoj4 uFx7oyFtph9GI38sFgv9MJplELHxaiRyDIiOnImLxq5Zf75CGQpGl5ePcdxpdk5DQzuDdkXeCKVl qGljqh8ygDOv9O5Oe/kvRvJPPNsZMgN8cd2Lzs9PulMPhzU1oARhs1qRnZcner21ZpNJuRbAXwFg 9erVuwtycvVQKKwyGF6fD3PmzMb+6oNqdlb6z6trsOrYjjZs3ISnn3kOPl9sfUrLJk/GlLJS3HD9 tcddmzx16k9u+MhHMJDef9269QAz2pqb63HM5nBhUdFcAAgdSQXW0tqKDkdrKz6AMJMWzY6KsCr5 6F/2DWIo6quKEbnfLkmsaCssX5bfUrMu8p77cRSVXAiDro84swMzE+j1E4oBitNe9lcF9LHO/OIV OcNkkI6UnK6WGqe9rAegiPwUidk03OcSxmoFytxI+xWC72vLm/hqfnvj2hNLD6U1q2QKC/qpAGzW grKrO4g+k9tasyXads5kjjtACPi8Td3dLvj9/a5clXNmkcmsCoWUS+64447B2Nm+vr4NjtYWDms6 2ru6YDKpKC8rQ8WsefmzZs0aUii5q6sL/3j8SaSlJodyM1I9ORkpMflLspoDMyumY868uWgaYqsM LFq06MK01NSCsrIyeH1+NDc1oaGxESlJSbrb1fXTo/WrP3x4WWZmpjmsaZDMaGxuhqEbssvR9lJs v+4zn/akshwCRxygDwCWYULfcpsO1UnmyPdPicgEvNBpL54TTd+d9uI5ZlZejGb2x0TrMxx1oyY7 YEBx5pf/g0C3ADCRUJ5rzy1fEY1ux7QnGBRxKBsEDRsiSJr2DzBHfrJLJBRhesqRXxTR6fEA3RMm pFssYhUdyRFJRJMV5g1dhaU/WVtcHPk4znCOO4jocLu+/+6mzX9dcslygBlTJpfDYrUoLJGsS8tP AHwKAPasqroyKTk5uOKmTyDEEpqm4by5c9HY1KK0tjQ8AiIMnMi1tLaBmeHpcrWvXv1ccSwHUJiX w3ff/hl4fX64u3sQDmvQwhrNmTX72c/cdtvgAcl/X3wZkiU87g5n/TEpuZzd3f9XjP66IgCweeMW 6OFAwOxx/TyWup4NUBrdg0iLYgwgh4/nFcRPAjRqIfIhMFKIlVc7ciffnNtxaNTSjADgKii5XLLy BBClMzVj1JRsjGWqu6D1cSL+2MBnBLKqCp7pspffn91aE/Vz4Sksu4IYlkjlJTDs6iOnq6Wmy172 NgHLIm1LELLMwvKmy15+e2ZrzXGrs2NxZdgLDcO8GsCQw08QKcT09Zm6+LAjv+jTBf0n4Gc1x80A 16xZ87f6+jqp6Ro6nC4kJNiQmZ5F0jAM1WIafDtXoSrc7Xat7+pqZ13TUV1TB4vVgunTpmLu/EVW AIMvKh7HIlJCVRAMhTHg5xcOh6Hp4dTy8rLM4uJi9Pl82Ld3H97buQsJFqt01DUNmf0xc0pFRcUi APD5A2BmVO3fD0djU1NVb+8HJqCcAdFZUPIFRXLUe0ZaT9+w0RGyNeHxqNJTod99Q1GMVzsLy//s LihbzMeUaGOA3AWlS5wF5X9hiFeIkB1N+8zcqcH/zxGvV1aaXPaWf/JRxu995aAS8LCzoPT1lpzJ syPtszWjoEhK/l00eiq6HLEcqxQUfa5CRgoDT3XZy9Z25JddP9yenrugdEJHYfkD0pqwn4GRx8eY bibLFmf+pP/jioqzOlvSsK4ooUCgzePstHNm/7N13Yqr8Z9Vz5BhyGlfuPe+j//64Z/+CwBaOjqu 2rz2Te+lH76WQqEgXJ5uzJo5A03NLUI3QgyOWfb7ETGpKtsSrO+n5w+HUTyxOOmue+5Bl8sDaRj4 x+NPQjWr8HpcnTXNdUPe3u++++6j8+fPJ6/PD2bG3r17Yeia7mh3/Gm8dR9PmGHuKiwbNSGqZCFA nKdIKu4SfKFgRB89wLIh19fZMdylHFR5XTTpEQY/GFWbREIwPiuJPuuyl3k6IZoFG52SlBwXZNH7 vnXRP17M9MuCNod/2GuVlSZXe8+/ANw4un7iMqtqvOeyT3pFJ+Mxakl4bbiCTo6CApsJtk+A+fug 6Bykw2R+a6Rruc01rzoLyjaDKOrDGQItUwSWWQvKfU7iembRQZAJIJooAbvCiOxrJagg/nZXd/ia Tnvxp3NaG6I4nDlzGNYANjc4rlj72qv7rrp+JXr6epGbk4P8gnzR3NwKk8X2ZYD/DRBv2bIlkGKz fbNm/4EfF5WV074DB7F0wfm4eNmFePX1N6ivzzuGR3Ss9Pfk8XTja1+/T/H5AwhrGp544l9wtDmQ kZYZrG+qH1JTlZltXq/3JgCDKfVffuVVeHs83XUNdY+cMtXHASKygTFqSnwF3H8URAwxxngBg/Hs aNfdrYcfSiso++TY051TugCngwQEIv11Dg8zDmZnWB7GMIF6XFFhdnZ0/4dAkZWdJCIGX62wuBr2 kOaUZYdYwWE6kt2GgRwwzyeCLdqSFSx5+4nyLjLoC8T8DijyyJeh6iMRoBlEPONkvlMBnsWsrHEU FJQWOIZ/sZzJDBtFsXbT2qoej6e1r7cbrW39L/cLFy2CLcFGgsT8+7/z4OAhx6tvvvnQrm3vOMLh IGthDZvfeQfJiTZcuGgh1m99F0oUpSFiQXlpCbKzs9Hr9WHj5k3YsGkjTCYTNx86sOFwU9OQk793 tm17NikpSfj8AUgpcaj6MDraO2RD9cFVOIeKP48jGunh348mMAkIQfJXo9q4Hw+YWSr48kjFl5zd wT8TR2j8jscEQRVH7l+J/oLwF1EURaaOhhR5wpdvtqNmJwG/HEv7sUYy7jobjR8wShHYjvb26za+ 9QZCgSCcbjdyc3NQVlpCggiK2XzfN7/5zcF0O6697vK1r6zWpaGjr8+LTe+8i5ycbORkZ/YnHziF zJhRAZenG81Nzfj7X/8Bi9mMkNfbsfvgvg8fLbd7+/Z5leeddwUzo8vVv9X3wuqXwHrIY61J+dIp VfosxSD8MKer5YTZUbLb614iIR4+FTqNBAn5s9zmmhF9/0jwr5lx+uO+mbdkttSPuEd5NBnp1m8B OL2Fj4h/k9tW+9xp1eEkGNEAvr5u3Xan09ng8bi5vrEZUkosXrwAmRnpIEUpt6RlD+YtW9ewLtjT 0jtjy7o3DcNgOLtcWLdhIwoLCqCqKnAKX/6JSUlYv2EDvve9B+Ht80EL+j1NzfXnAxh88zOzkpia tlZRFLg83WBm7N61G63NzfJQ9YEnd2BHTJN0noswy+pwS81PIpV3txz+DpHcMJ46jQSR3OBuqf/u aDJZzXXvGgZuZuC0Zf9hcFCScfexySVGgqqqwoLlSjCfVO3fsSLBr2S2FH3ldPQdK0ZNJFCzsXb+ +tfXIBQOYk/VAdisVsydO4ssqkIw9Ot+8LPf3jsg+/LGlw+1HG68bt972w1DGuh0evD62nXjPoAB BnYxXnhhNf7w+z/BF/Aj0ZYYaDy4/1qHY2hx6M2bN79UVl6WEg6H0ef1QdcNPP3M8+hxOdsOVlfH Z38nQBLqw8Hg8khT5AP9S2Ey4xpmvDeeuh0LM+8kM66ZFIFhy+uo+a9guuZUZIA+DmZm4K5oDxMy HHVNobBczpJjWoM4At5Bq/WmaCvbnWmMagB3OHY43d3dLzTW1sHT0wu3x4NJpaUoLy2B2WpNVkz8 3V/89g8fGZB/c8u61XU1h67ct32bwVJHZ6cTz7/40uD2T0+vJ6bJL4cMRBF46KGf4z//eQqSJZIS Evx1+9+74VBz85BZxxtvvPHFhQsXXiGlhKOjP53bs888C7/PG2praPjMeOl3zsDcRCyvtLsdUc86 MurqegxH31ICvTweqh0LgV42HN4LM+rqIq4Tkuk4/DqTvoSAuhNLxwiGTgbfntNa+7ex3G531lcH tHDlqXq5SMlrwuy/eLiT77ONE9YNWL169fV7tm3zhENB3ru/GrpuYH7lPKSnpwG6TGUof/vFY3/9 zID8G2+88cbevXtKt2/YEJZSwtPdix2796KoaCL0sB7zuiGGppOUEl6vH7t274IiFFgtpt6G+kPz GtrahoQuvf3W2zcsXbr0ESJCW6cTzIxdu/Zgy9Ztsvbg/tcPNdWd0Pn2AwuzlAZ+EnDUTs1urRs+ 40UE5KHD5249fAOk8atxOxhhZkjjV+7WwzfkoSPq2VxOa8MuRQmfRyT/NR7qHQ1LbgeFr8zsqPvL ybQzwdnsMKnhSyXzf2Kl23Aw8ESVxbj+bD30OJaICqd0NTdMfnvNy6yFQ3hnxw4oioJlixchLSMN JrOaqDAe+d3f/z5rQH7Hjh1Nzzy3yvrac8/Va4YOw2AkJNmQnBlVhFVESGZs3bkbb27cClURbAR8 Bz21h4rr6+uHxKa++eabl8+/4LynzWYzOl0uhMNhuNxu/PGPf4Kztbl1T9WeMYc5nfOwfEMTdGlO e803oln2jsQkIJTVVv8lkLYc4H2xUPF9eB9IW57VVv+lSJa9I5HW1OTJbKn7hGZolxCwO5YaHkEj yEfNPmtFVmvTm7FoMK2pyZPjqP0YGcZKBk4q1+GxSIZTAh/Nbq255eKGhlN7sjmORGQA1+3Y4ex0 tHzsnU0buLevD+9s3wmLxYL5c+fBpKowCTVFavKVR//2rw8ddRtv2rax9KX//Kuup6cPAa8frvbY 5xbwuF384our8cyqVezsaH9p6/Yt0/Y2NQ1Jdrphw4ZbFy9a9GqCzUae7h74fAGEw2H89re/hzRC vfWHDy5GPHPu0WiSaaNBeJAZlVmOuuVjSVJwIrJam97MbK2dDTJuAbDtJJvbBjJuyWytnR0rgwIA +e2NazNaa+YS0+UEevlkawUzo4tJPmQoYmpma93nj836HAsy2+ufzspLncZk3MHMO0+mLYZsMwzj PjgsJTmtNU+d+I6zi6g8IC9Ztuz7k6dVfHvK7DmUlZ6BxQvORyAYxL79B+ByuWG2WnXVpHz70zd9 5KdENMSgFBcXWxvG6c2RCyR2AEEck5MOALZs2fLb+fPn360oCpxuz5FDDx0/+9nP0dLU5Dvw3p4b 6h31Z0XOP0dBgU1Rk0465fpIWHqIjZ4uVybcMa3VGynuorIZhqTLwVhGhLkYPaFqCzPeA2GdIvi1 jObaGM8kh6d7woT0cEhcLhT1ChAqAJ4M0Cj1fDlIjPdA/A6RsiEj1fziSL6I40VLzuTZZkVeTYKX EzB3NH0BuAHUwKBtDO2ZrPaGjcfmeowUZ0HxVIYasS9kt6Omaiyz9tbsiXPNJlPEtizLUbNr4KQ9 ahfw5cuW/Xna3PNuKyoro8nlZZg5bRrCmoa9VQfgcbsBRdVtFtObHE669ZZbroy4OE6saW5uzujt 7d05ffr0icyMTqcL/kAQhmHgl7/8FRwtzYH3dr57T0NDw19Pl45xRqd7woR0TTeXQOrJks0mQWEN Qu0zqeH6tGNm+aeT9qSyHCWV0zSppSgyIU2BIQ1TyCmU5K6sLJuTdpxZblWe/JKJAejZikxIAwBJ 6FUVvdvcl+AcjxnpmcyYYmAuv/TSVyZVzLh80rSZIj8/F/PnzoGUEjX1DXC73fD2eaUlwdJpsyZ/ v2rXtj888MADpzSq4u233753xowZD2VkZAhd1+Ho6IJhGOjr7cVvfvsHdDha/Pt37P5uTWvdBy7b S5w4cd5nzEGAixYs+Oq06TMemjJ7rpKakoJlFy6BSVXh6e5BXWMjDE1HMOTXrNbEvSkptofe2Xjp qgceoHE1hFVVVUvMZvNz5eXlWQDg9fkHozw6u7rwy188Am9Pd2/Vnvc+W9/U9PR46hInTpwzn5PK VVBZWXldWWnpfy648BKTEIIWL7gAWVmZMKSEo60Dnp5uSF2HFgqFbYm2zgRr0tZwwLj76qsvjFk9 VWZW6uvrbwiHw7+aPHlyHhFB03V0dLmgaf0rjzfeeAuvvPwK93hcXYcPH/xQc/PZn8csTpw4J89J J2spKSnJnVRW/u78JRcWGCBlxvRpWHBeJYSiIBgMocvVv/emKARdNyAE9SVaLdtsaemvWaT+WkVF xZ5jD0xOBDObnE7nktbW1u+WlZVdmJSUpACArhtwe7rhO5Iaq72zA//46+NwOBx6W1Pj7tb2lgsd 54j/Upw4cU6emGWrWnjeeY+WTJ5669RZc1QiQQsuOA9lJf3p5ULhMDq7nGAAJkUBQ0JAsKKQpipK t1DVxoyM9N1pKSlNJpOpWwDSYrH0AVgDIMnj8eQ31jcuM9hYmJGeUVlYVJhrOurUJxQOw+3pQfBI PY9wOIzVL72MDes3Mutab+2BA49WHT5wf6zGGidOnHODmKbrKygoKJo0ceJrFectKE9MTVPTU9Nw 3rw5KC/vr/DHDIS1EAxDgpmhqgIsGQBDAFBVlU0mE8wmE7KyskgdIZUWMyMUCsPrD8Dn90HK/gmk 3+/HK6+8ivfe2wUtrIfqDx3Y6+hovcrhcJzaWpxx4sQ5KxiXfKVTp069Jisr69fTZ87OI9VqSU6y YdasGZg7ezYSExOPkuwvcWoymWAxmSAUBaoQUFQFVosFkhlS9hvLsKYhGAwhGArDMIa6Je3fvx8b Nm7GoepDEALBlvraOqfb9YXa2toRs+rGiRMnzrgmbJ48efLsnKysxyaUTppmTUxOCIbDNLm8HGWl JZg6ZRIyMzNP3MgwdHu6sXffXlTtP4ia2lqEAiG2qORtbWw60NvjuutATc1Jeb/HiRPng8Gpyliv Ti0v/0VOTu7KorJJSWkZmWbdkKpQBKWmpCIlNRmpqWlITLAiwWqFFtbR5+9DdmYWvH4//F4f3B43 HI52tLe3w+32IDsz3QBzqKOrw9nR0vR4SNP+b7wiTeLEiXNucupKdhzVZ3Fx8RUJ5oRb8wsLKtPS MhJtSUk2q81mYiZFIVB3b6/i9XopLS3TIJJsGDpIGmGv1+v19vX1dHa01wf8gb/XNdQ9hTGG6cSJ EyfO6TCAw1JRXJynCVGkG0YOMZcpREkkxGFdCIdhGK6GhobDiBu7OHHixIkTJ06cOHHixIkTJ06c OHGi5f8D+VwVKOOSGBMAAAAASUVORK5CYII= "
       height="38"
       width="160" />
    <text
       xml:space="preserve"
       style="font-size:78.0451355px;font-style:normal;font-variant:normal;font-weight:bold;font-stretch:normal;line-height:125%;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;font-family:Verdana;-inkscape-font-specification:Verdana Bold"
       x="11.095959"
       y="998.23358"
       id="text7880"
       sodipodi:linespacing="125%"><tspan
         sodipodi:role="line"
         id="tspan7882"
         x="11.095959"
         y="998.23358"
         style="font-style:normal;font-variant:normal;font-weight:bold;font-stretch:normal;fill:#ffffff;font-family:Verdana;-inkscape-font-specification:Verdana Bold">3</tspan></text>
    <text
       xml:space="preserve"
       style="font-size:67.61245728px;font-style:normal;font-weight:normal;line-height:125%;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;font-family:Sans"
       x="69.675781"
       y="965.5614"
       id="text7884"
       sodipodi:linespacing="125%"><tspan
         sodipodi:role="line"
         id="tspan7886"
         x="69.675781"
         y="965.5614"
         style="font-size:28px;font-style:normal;font-variant:normal;font-weight:bold;font-stretch:normal;fill:#ffffff;font-family:Verdana;-inkscape-font-specification:Verdana Bold">года</tspan></text>
    <text
       sodipodi:linespacing="125%"
       id="text7888"
       y="984.995"
       x="71.203125"
       style="font-size:67.61245728px;font-style:normal;font-weight:normal;line-height:125%;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;font-family:Sans"
       xml:space="preserve"><tspan
         style="font-size:12px;font-style:normal;font-variant:normal;font-weight:bold;font-stretch:normal;fill:#ffffff;font-family:Arial;-inkscape-font-specification:Arial Bold"
         y="984.995"
         x="71.203125"
         id="tspan7890"
         sodipodi:role="line">гарантия при регистрации</tspan><tspan
         style="font-size:12px;font-style:normal;font-variant:normal;font-weight:bold;font-stretch:normal;fill:#ffffff;font-family:Arial;-inkscape-font-specification:Arial Bold"
         y="999.995"
         x="71.203125"
         sodipodi:role="line"
         id="tspan7894">на сайте bosch-pt.com</tspan></text>
  </g>
</svg></a> -->
<a href="https://webapp3.bosch.de/warranty/locale.do">
	<span><span>3</span><span>года</span><span>гарантия при регистрации на сайте bosh-pt.com</span></span>
	<span class="sertif_logo"></span>
</a>
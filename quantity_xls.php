<?
if($_SERVER["DOCUMENT_ROOT"] == ""){
    $bRunFromConsole = true;
}
$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)) ;
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
set_time_limit(0);
//define("LANG", "ru");
$sMailMessage = '';
$sMailAddres = 'info@mera-pt.ru';


require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


global $USER;
if (!$USER->IsAdmin() && $bRunFromConsole !== true) {
    die ('Только администраторам');
}

mail($sMailAddres, 'Отчет о выполнении скрипта quantity_xls.php', "Запуск");

include($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/include/PHPExcel.php");
include($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/include/PHPExcel/IOFactory.php");

error_reporting(E_ERROR);
ini_set("display_errors", 1);

@set_time_limit(0);
@ignore_user_abort(true);

$arCnt = Array();

//$log = new Logger($_SERVER["DOCUMENT_ROOT"]."/cron_quant.log");
//$log->log('start script');

$xls = 'stocknovo.xlsx';
$ftp_server = "195.208.154.13";
$ftp_user_name = "mera-pt";
$ftp_user_pass = "555mera-pt";
if ($conn_id = ftp_connect($ftp_server))
{
    $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
    ftp_pasv($conn_id, true);
    //var_dump($login_result);
    ftp_get($conn_id, ($_SERVER["DOCUMENT_ROOT"]."/".$xls), $xls, FTP_BINARY);

    ftp_close($conn_id);

    $sMailMessage .= "Файл скачан успешно\n\r";

}
else
{
    var_dump($sMailMessage);
    mail($sMailAddres, 'Отчет о выполнении скрипта quantity_xls.php', "Ошибка соединения");
    die;
}

/*$curl = curl_init();
$dfile = fopen($xls, 'w');
curl_setopt($curl, CURLOPT_URL, "ftp://".$ftp_server."/".$xls); #input
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_FILE, $dfile); #output
curl_setopt($curl, CURLOPT_USERPWD, "$ftp_user_name:$ftp_user_pass");
if (!curl_exec($curl))
{
    echo 'Ошибка скачивания файла';
    echo $xls." ".curl_error($curl);
    die;
}
curl_close($curl);
fclose($dfile);*/

CModule::IncludeModule("iblock");
CModule::IncludeModule("catalog");
CModule::IncludeModule("sale");

//$log->log('start cron_quant.php');

//$log->log("start getAll");
$arSelectAll = Array("ID", "NAME", "EXTERNAL_ID");
$arFilterAll = Array("IBLOCK_ID"=>5, "ACTIVE"=>"Y");
$resAll = CIBlockElement::GetList(Array(), $arFilterAll, false, false, $arSelectAll);
$i=0;
while($obAll = $resAll->GetNextElement())
{
    $i++;
    if($i==1000){
        $s=$s+$i;
        $i=0;
        //$log->log($s);
    }
     $arFieldsAll = $obAll->GetFields();
     $arAll[$arFieldsAll['EXTERNAL_ID']] = $arFieldsAll;
}
//$log->log("finish getAll");
$sMailMessage .= "Получено ".count($arAll)." элементов для обработки из БД сайта \n\r";
//echo '<pre>'; print_r($arAll); echo "#".__LINE__."@".__FILE__; echo '</pre>';

//$log->log("start xlsToArray");
$objReader = PHPExcel_IOFactory::createReaderForFile($_SERVER["DOCUMENT_ROOT"]."/".$xls);
$objReader->setReadDataOnly(true);
$objPHPExcel = $objReader->load($_SERVER["DOCUMENT_ROOT"]."/".$xls);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//$log->log("start Chekhov");
// Склад Чехов
$objPHPExcel->setActiveSheetIndex(0);
$aSheet = $objPHPExcel->getActiveSheet();
//этот массив будет содержать массивы содержащие в себе значения ячеек каждой строки
$array = array();
//получим итератор строки и пройдемся по нему циклом
$i=0;
$highestRow         = $aSheet->getHighestRow(); // например, 10
$highestColumn      = $aSheet->getHighestColumn(); // например, 'F'
$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
$nrColumns = ord($highestColumn) - 64;
$iter++;
if($step>0 && $iter<$highestRow && $highestRow>$step)
    $highestRow = $iter + $step;
for ($row = $iter; $row <= $highestRow; ++ $row)
{
    $item = array();
    for ($col = 0; $col < $highestColumnIndex; ++ $col)
    {
        $cell = $aSheet->getCellByColumnAndRow($col, $row);
        $val = trim(htmlspecialchars($cell->getValue()));
        array_push($item, $val);
    }
    $keyArr = (str_replace(".", "", $item[0]));
    ////$log->log("update ".$item[1]." ".$keyArr);
    $array[$keyArr] = $item;
}

//echo '<pre>'; print_r($array); echo "#".__LINE__."@".__FILE__; echo '</pre>';
//die;

//$log->log($highestRow);
//$log->log("finish xlsToArray");

//$log->log("start getQuantXLS");
$show = false;
foreach($arAll as $keyAll => $elemAll)
{
    $amount = 0;
    if (array_key_exists($keyAll, $array)){
        $amount = 100;
        if (!$show)
        {
            ////$log->log("chekhov update ".$elemAll['ID']." ".$amount);
            $show = true;
        }
        $arCnt["chekhov"]++;
    }
    $arFields = Array(
        "PRODUCT_ID" => $elemAll['ID'],   //Ид товара
        "STORE_ID" => 7,      //Ид склада
        "AMOUNT" => $amount,    //Количество
    );
    $ID = CCatalogStoreProduct::UpdateFromForm($arFields);
    //CIBlockElement::SetPropertyValues($elemAll['ID'], 5, 2, "STATUS");
}

$sMailMessage .= "Обработано 'Склад Чехов'\n\r";
//$log->log("finish getQuantXLS");

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Склад Bosch NOVO
//$log->log("start Bosch NOVO");
$objPHPExcel->setActiveSheetIndex(2);
$aSheet = $objPHPExcel->getActiveSheet();
//этот массив будет содержать массивы содержащие в себе значения ячеек каждой строки
$array = array();
//получим итератор строки и пройдемся по нему циклом
$i=0;
$highestRow         = $aSheet->getHighestRow(); // например, 10
$highestColumn      = $aSheet->getHighestColumn(); // например, 'F'
$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
$nrColumns = ord($highestColumn) - 64;
$iter++;
if($step>0 && $iter<$highestRow && $highestRow>$step)
    $highestRow = $iter + $step;
for ($row = $iter; $row <= $highestRow; ++ $row)
{
    $item = array();
    for ($col = 0; $col < $highestColumnIndex; ++ $col)
    {
        $cell = $aSheet->getCellByColumnAndRow($col, $row);
        $val = trim(htmlspecialchars($cell->getValue()));
        array_push($item, $val);
    }
    $keyArr = (str_replace(".", "", $item[0]));
    ////$log->log("update ".$item[1]." ".$keyArr);
    $array[$keyArr] = $item;
}

//$log->log($highestRow);
//$log->log("finish xlsToArray");

//$log->log("start getQuantXLS");
$show = false;
foreach($arAll as $keyAll => $elemAll)
{
    $amount = 0;
    if (array_key_exists($keyAll, $array)){
        $amount = 100;
        if (!$show)
        {
            //$log->log("nsk update ".$elemAll['ID']." ".$amount);
            $show = true;
        }
        $arCnt["nsk"]++;
    }
    $arFields = Array(
        "PRODUCT_ID" => $elemAll['ID'],   //Ид товара
        "STORE_ID" => 2,      //Ид склада
        "AMOUNT" => $amount,    //Количество
    );
    $ID = CCatalogStoreProduct::UpdateFromForm($arFields);
    //CIBlockElement::SetPropertyValues($elemAll['ID'], 5, 2, "STATUS");
}

$sMailMessage .= "Обработано 'Склад Bosch NOVO'\n\r";
mail($sMailAddres, 'Отчет о выполнении скрипта quantity_xls', $sMailMessage);

<?php ini_set('display_errors', 1); ?><?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Широкий выбор инструмента Bosch по выгодной цене от официального дилера Мера");
$APPLICATION->SetTitle("Интернет-магазин инструмента Bosch");
#$USER->Authorize(1);
?>

<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.comments",
	"",
	array(
		"ELEMENT_ID" => 121462,
		"ELEMENT_CODE" => "",
		"IBLOCK_ID" => 5,
		"SHOW_DEACTIVATED" => $arParams['SHOW_DEACTIVATED'],
		"URL_TO_COMMENT" => "",
		"WIDTH" => "",
		"COMMENTS_COUNT" => "5",
		"BLOG_USE" => "Y",
		"FB_USE" => "N",
		"FB_APP_ID" => $arParams['FB_APP_ID'],
		"VK_USE" => "N",
		"VK_API_ID" => $arParams['VK_API_ID'],
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "0",
		'CACHE_GROUPS' => "N",
		"BLOG_TITLE" => "",
		"BLOG_URL" => $arParams['BLOG_URL'],
		"PATH_TO_SMILE" => "",
		"EMAIL_NOTIFY" => $arParams['BLOG_EMAIL_NOTIFY'],
		"AJAX_POST" => "N",
		"SHOW_SPAM" => "N",
		"SHOW_RATING" => "N",
		"FB_TITLE" => "",
		"FB_USER_ADMIN_ID" => "",
		"FB_COLORSCHEME" => "light",
		"FB_ORDER_BY" => "reverse_time",
		"VK_TITLE" => "",
		"TEMPLATE_THEME" => $arParams['~TEMPLATE_THEME']
	),
	$component,
	array("HIDE_ICONS" => "Y")
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

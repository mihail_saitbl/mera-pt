<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Услуги компании Мера - ремонт. ООО Мера - Оригинальные профессиональные геодезические приборы и измерительный инструмент для стройки от Bosch.");
$APPLICATION->SetTitle("Ремонт и сервисное обслуживание");
?>
<div class="container">
<p style="color: #464646; background-color: #ffffff;">
</p>
<p>
 <span style="color: #000000;">Сервисный центр компании «Мера» специализируется на ремонте геодезического оборудования и&nbsp;оснащён всем необходимым оборудованием для проведения ремонта лазерных дальномеров, лазерных нивелиров, оптических нивелиров, детекторов, электронных и оптических теодолитов.</span><br>
 <span style="color: #000000;">
	В марте 2011 года наш сервисный центр прошёл процесс аккредитации в компании Robert Bosch. Теперь мы обеспечиваем все гарантийные обязательства Bosch по измерительной технике и&nbsp;производим бесплатное сервисное обслуживание измерительных приборов под брендами: Bosch Professional, Bosch DIY, CST/Berger и&nbsp;Skil. Кроме этого, сервисный центр компании «Мера» готов выполнить коммерческий ремонт оптических нивелиров и теодолитов сторонних производителей, таких как: УОМЗ, Setl, Vega, Sokkia, Topcon, Trimble, Nikon, Leica и Geobox. </span><br>
</p>
<p>
	<span style="color: #000000;"><br>
	</span>
</p>
<p style="color: #464646; background-color: #ffffff;">
 <span style="color: #000000;"><a href="http://www.powertools-aftersalesservice.com/public/boschprof/service?lgRedirect=true&country=RU&lg=ru">Подбор&nbsp;запчастей для ремонта инструментов Bosch.</a> Полезный сервис для подбора запасных и комплектующих, у&nbsp;нас их можно заказать с доставкой.</span>
</p>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Услуги компании Мера - аренда. ООО Мера - Оригинальные профессиональные геодезические приборы и измерительный инструмент для стройки от Bosch.");
$APPLICATION->SetTitle("Аренда оборудования");
?><p>
	 Если Вам нужен измерительный инструмент на короткий срок для разовых работ, то вы можете взять его в Аренду в нашей компании. <br>
 <br>
	 За небольшую стоимость Вы получаете профессиональное оборудование от надежного поставщика.<br>
 <br>
	 Данная услуга доступна как юридическим, так и физическим лицам. Оборудование предоставляется под залог денежных средств. Стоимость аренды оборудования&nbsp;составляет от 500 рублей в сутки. <br>
 <br>
	 Перечень оборудования&nbsp;для&nbsp;аренды:<br>
 <br>
</p>
<p style="text-align: center;">
</p>
<table style="font-family : calibri" class="">
<tbody>
<tr>
	<th>
		 &nbsp; &nbsp; &nbsp;<br>
		 Фотография<br>
 <br>
	</th>
	<th>
		 Наименование оборудования
	</th>
	<th>
		 Цена сутки
	</th>
	<th>
		 Размер залога
	</th>
	<th>
		 &nbsp;
	</th>
</tr>
<tr>
	<td>
		 &nbsp;<img src="http://www.mera.pro/images/upload/img41778168.jpg"><br>
	</td>
	<td class="product">
		 &nbsp;&nbsp;Нивелир оптический <br>
		 &nbsp;&nbsp;(в комплекте штатив и рейка)
	</td>
	<td class="price">
		 &nbsp; &nbsp;600 руб.
	</td>
	<td>
		 &nbsp; &nbsp;12 000 руб.&nbsp;
	</td>
	<td>
		 &nbsp; &nbsp;<a href="#popupform" class="popupbutton" style="text-decoration: none;" product="Нивелир оптический (в комплекте штатив и рейка)" id="popupbutton">Заказать</a>
	</td>
</tr>
<tr>
	<td style="background-color: #EBEBEB;">
		 &nbsp;<img src="http://www.mera.pro/images/upload/img90407600.jpg"><br>
	</td>
	<td style="background-color: #EBEBEB;" class="product">
		 &nbsp; Теодолит CST/Berger DGT 10 (5")<br style="color: #464646; background-color: #ffffff;">
		 &nbsp;&nbsp;(в комплекте штатив и рейка)&nbsp; &nbsp; &nbsp;&nbsp;
	</td>
	<td style="background-color: #EBEBEB;" class="price">
		 &nbsp; &nbsp;800 руб. &nbsp; &nbsp;
	</td>
	<td style="background-color: #EBEBEB;">
		 &nbsp;&nbsp;25 000 руб. &nbsp; &nbsp;&nbsp;
	</td>
	<td style="background-color: #EBEBEB;">
		 &nbsp; &nbsp;<a href="#popupform" class="popupbutton" style="text-decoration: none;" product="Теодолит CST/Berger DGT 10 (5 (в комплекте штатив и рейка)" id="popupbutton">Заказать</a>
	</td>
</tr>
<tr>
	<td>
		 &nbsp;<img src="http://www.mera.pro/images/upload/img94909399.jpg">
	</td>
	<td class="product">
		 Тахеометр Nikon DTM-352W&nbsp;<br>
		 (на отражатель, -30°С)<br>
 <br>
 <span>Тахеометр Nikon NPL-332&nbsp;<br>
		 (безотражательный, -20°С)</span>
	</td>
	<td class="price">
		 &nbsp;&nbsp;1д.=2000 р.<br>
		 &nbsp;7д.=8000 р.<br>
		 &nbsp;14д.=12000 р.<br>
		 &nbsp;30д.=20000 р.
	</td>
	<td>
		 &nbsp;&nbsp;35 000 руб./<br>
		 &nbsp; договор
	</td>
	<td>
		 &nbsp; &nbsp;<a href="#popupform" class="popupbutton" style="text-decoration: none;" product="Тахеометр Nikon NPL-332&nbsp;или Тахеометр Nikon DTM-352W" id="popupbutton">Заказать</a>
	</td>
</tr>
<tr>
	<td style="background-color: #ebebeb;">
 <img src="http://www.mera.pro/images/upload/img34138705.jpg">
	</td>
	<td rowspan="1" style="background-color: #ebebeb;" class="product">
		 &nbsp;&nbsp;Нивелир лазерный&nbsp;<br style="color: #464646; background-color: #f4f2f3;">
		 &nbsp;&nbsp;Bosch GLL 3-80 Professional&nbsp; &nbsp; &nbsp;&nbsp;
	</td>
	<td rowspan="1" style="background-color: #ebebeb;" class="price">
		 &nbsp;&nbsp;700 руб.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
	</td>
	<td rowspan="1" style="background-color: #ebebeb;">
		 &nbsp;&nbsp;18 000 руб.
	</td>
	<td style="background-color: #ebebeb;">
		 &nbsp; &nbsp;<a href="#popupform" class="popupbutton" style="text-decoration: none;" product="&nbsp;Нивелир лазерный&nbsp;Bosch GLL 3-80 Professional&nbsp;" id="popupbutton">Заказать</a>
	</td>
</tr>
<tr>
	<td>
 <img src="http://www.mera.pro/images/upload/img57692447.jpg">
	</td>
	<td rowspan="1">
 <span style="color: #464646; background-color: #f4f2f3;">&nbsp;</span>&nbsp;Детектор скрытой&nbsp;<br style="color: #464646; background-color: #f4f2f3;">
		 &nbsp;&nbsp;проводки и арматуры<br style="color: #464646; background-color: #f4f2f3;">
		 &nbsp;&nbsp;Bosch GMS 120&nbsp;
	</td>
	<td rowspan="1" class="price">
		 &nbsp; &nbsp;500 руб.
	</td>
	<td rowspan="1">
		 &nbsp; 4 000&nbsp;руб. &nbsp;
	</td>
	<td>
		 &nbsp; &nbsp;<a href="#popupform" class="popupbutton" style="text-decoration: none;" product="&nbsp;Детектор скрытой&nbsp;проводки и арматуры Bosch GMS 120&nbsp;" id="popupbutton">Заказать</a>
	</td>
</tr>
<tr>
	<td style="background-color: #ebebeb;">
 <img src="http://www.mera.pro/images/upload/img97955775.jpg">
	</td>
	<td style="background-color: #ebebeb;" class="product">
		 &nbsp;Нивелир лазерный ротационный&nbsp;<br style="color: #464646; background-color: #ffffff;">
		 &nbsp;Bosch GRL 300 Professional&nbsp;<br style="color: #464646; background-color: #ffffff;">
		 &nbsp;(штатив в комплекте) <br>
		 &nbsp;предназначен для наружных <br>
		 &nbsp;работ&nbsp;
	</td>
	<td style="background-color: #ebebeb;" class="price">
		 &nbsp; &nbsp;800 руб.
	</td>
	<td style="background-color: #ebebeb;">
		 &nbsp;&nbsp;25 000 руб./<br>
		 &nbsp;договор.<br>
	</td>
	<td style="background-color: #ebebeb;">
		 &nbsp; &nbsp;<a href="#popupform" class="popupbutton" style="text-decoration: none;" product="Нивелир лазерный ротационный&nbsp;Bosch GRL 300 Professional" id="popupbutton">Заказать</a>
	</td>
</tr>
<tr>
	<td>
 <img src="http://www.mera.pro/images/upload/img29841524.jpg">
	</td>
	<td class="product">
		 &nbsp;&nbsp;Дальномер&nbsp;лазерный<br>
		 &nbsp; Bosch&nbsp;GLM&nbsp;80&nbsp;Professional
	</td>
	<td class="price">
		 &nbsp; &nbsp; 500 руб. &nbsp;
	</td>
	<td>
		 &nbsp;&nbsp;8&nbsp;500&nbsp;руб.
	</td>
	<td>
		 &nbsp; &nbsp;<a href="#popupform" class="popupbutton" style="text-decoration: none;" product="Дальномер&nbsp;лазерный Bosch&nbsp;GLM&nbsp;80&nbsp;Professional" id="popupbutton">Заказать</a>
	</td>
</tr>
</tbody>
</table>
<p>
</p>
<p>
	 Более подробную информацию Вы можете получить у менеджеров компании. <br>
	 Обращайтесь по телефонам: <br>
 <br>
	 Новосибирск: &nbsp;+7 (383) 319‒55‒58<br>
	 Омск: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;+7 (3812) 910‒522
</p>
<p>
	 Екатеринбург: +7 (343) 287-41-96
</p>
 <script type="text/javascript">
$(document).ready(function() {
    /*  $("#telephone").mask(" 8 (999) 999-99-99");     */
    $("a.popupbutton").fancybox();

    $(".popupbutton").click(function(){
    	var product = $(this).attr("product");
    	$(".input-product").attr("value",product);
    })


});
</script>
<div id="popupform" style="display:none;">
	<div class="change_pass_forms">
		<div style="margin: auto; padding: 0; height: auto; width: 20%;" class="new remod_h1">
			<h2>Купить</h2>
		</div>
		<div class="clear">
		</div>
		<hr class="under_menu hr_margin">
		<form method="POST" id="order_form_zakaz_zwonka2" class="feedback" enctype="multipart/form-data">
 <input style="display:none;" name="CAPCHA"> <input style="display:none;" class="classOk" name="IBLOCK_CODE" value="BW_ORDER"> <input style="display:none;" class="classOk input-product" name="product" value="">
			<div class="change_line">
				<div class="before_input">
					 Имя*:
				</div>
 <input class="classOk requied input_lin" id="name" name="name" type="text"><br>
			</div>
			<div class="change_line">
				<div class="before_input">
					 Номер телефона*:
				</div>
 <input class="classOk input_line requied" id="telephone" name="telephone" placeholder=" 8 (999) 999-99-99" type="text">
			</div>
 <span class="required_forms">*Обязательные поля для заполнения</span>
			<div class="clear">
			</div>
			<hr class="under_catalog">
			<div class="move_button">
 <input id="buttonSubmit" class="button_enter" type="submit" name="submit" value="ОТПРАВИТЬ">
			</div>
		</form>
	</div>
</div>
<p>
</p>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
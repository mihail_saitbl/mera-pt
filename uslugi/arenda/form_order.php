<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

	foreach ($_POST as $key => $value) {
		if (is_array($value))
		{
			foreach ($value as $k => $val) {
				$_POST[$key][$k] = htmlspecialchars($val);
			}
		}
		else
		{
			$_POST[$key] = htmlspecialchars($value);
		}
	}
$cap = $_POST["CAPCHA"];
if(empty($cap)) 
{

	global $USER;
	$arResult = Array();
	// определим инфоблок
	if ($_POST["IBLOCK_CODE"])
	{
		CModule::IncludeModule ("iblock");
		$iblock = CIBlock::GetList (Array(), Array ("CODE" => $_POST["IBLOCK_CODE"]));
		if ($arBlock = $iblock->Fetch())
		{
			// получим свойства
			$arProps = Array();
			$props = CIBlockProperty::GetList (Array(), Array ("IBLOCK_ID" => $arBlock["ID"]));
			while ($arProp = $props->Fetch())
			{
				$arProps[$arProp["CODE"]] = $arProp;
				if ($arProp["PROPERTY_TYPE"] == "L")
				{
					$props_enum = CIBlockPropertyEnum::GetList (Array (), Array ("IBLOCK_ID" => $arBlock["ID"], "CODE" => $arProp["CODE"]));
					while ($arProp = $props_enum->Fetch())
					{
						$arPropEnum[$arProp["ID"]] = $arProp["VALUE"];
					}
				}
				elseif ($arProp["PROPERTY_TYPE"] == "E")
				{
					$items = CIBlockElement::GetList (Array(), Array("IBLOCK_ID" => $arProp["LINK_IBLOCK_ID"]), false, false, Array ("NAME", "ID", "IBLOCK_ID", "PROPERTY_PRICE_A"));
					while ($arItem = $items->Fetch())
					{
						$arItems[$arItem["ID"]] = $arItem;
					}
				}
			}
			$arFields = Array(
				"IBLOCK_ID" => $arBlock["ID"],
				"ACTIVE" => "Y",
				"NAME" => ConvertTimeStamp(),
			);
			if($_POST["IBLOCK_CODE"] == "LF_REVIEWS") {
				$arFields["NAME"] = $_POST["NAME"];
				$arFields["ACTIVE"] = "N";
			}
			$arMailFields = Array(
				"SUBJECT" => "Новый элемент инфоблока ".$arBlock["NAME"],
			);
			foreach ($arProps as $code => $arProp)
			{
				if ($arProp["IS_REQUIRED"] == "Y" && !$_POST[$arProp["CODE"]])
				{
					$arResult["RESULT"] = "ERROR";
					$arResult["ERROR"] .= "Поле ".$arProp["NAME"]." не заполнено\n";
				}
				$arFields["PROPERTY_VALUES"][$arProp["CODE"]] = $_POST[$arProp["CODE"]];
				if (is_array($_POST[$arProp["CODE"]]))
				{
					foreach ($_POST[$arProp["CODE"]] as $id)
					{
						$arMailFields["FIELDS"] .= $arProp["NAME"].": ".$arPropEnum[$id]."\n";
					}
				}
				elseif ($arProp["PROPERTY_TYPE"] == "L")
				{
					$arMailFields["FIELDS"] .= $arProp["NAME"].": ".$arPropEnum[$_POST[$arProp["CODE"]]]."\n";
				}
				elseif ($arProp["USER_TYPE"] == "UserID")
				{
					$arMailFields["FIELDS"] .= $arProp["NAME"].": ".$USER->GetID()."\n";
					$arFields["PROPERTY_VALUES"][$arProp["CODE"]] = $USER->GetID();
				}
				elseif ($arProp["PROPERTY_TYPE"] == "E")
				{
					$arMailFields["FIELDS"] .= $arProp["NAME"].": ".$arItems[$_POST[$arProp["CODE"]]]["NAME"]."\n";
				}
				elseif ($arProp["CODE"] == "PRICE")
				{
					$arMailFields["FIELDS"] .= $arProp["NAME"].": ".$arItems[$_POST["SECTION"]]["PROPERTY_PRICE_A_VALUE"]."\n";
					$arFields["PROPERTY_VALUES"][$arProp["CODE"]] = $arItems[$_POST["SECTION"]]["PROPERTY_PRICE_A_VALUE"];
				}
				else
				{
					$arMailFields["FIELDS"] .= $arProp["NAME"].": ".$_POST[$arProp["CODE"]]."\n";
				}
			}
			if ($_POST["PREVIEW_TEXT"])
			{
				$arFields["PREVIEW_TEXT"] = $_POST["PREVIEW_TEXT"];
				$arMailFields["FIELDS"] .= "Комментарий пользователя: ".$_POST["PREVIEW_TEXT"]."\n";
			}
			if (is_array ($_FILES["FILE"]))
			{
				$arFields["PROPERTY_VALUES"]["FILE"] = $_FILES["FILE"];
			}
			if (is_array($arProp["USER"]) && $USER->IsAuthorized())
			{
				$arFields["PROPERTY_VALUES"]["USER"] = $USER->GetID();
			}
			if ($arResult["RESULT"] != "ERROR")
			{
				$el = new CIBlockElement;
				$product_id = $el->Add ($arFields);
				if ($product_id > 0)
				{
					// отправляем письмо
					CEvent::Send ($arBlock["CODE"], "s1", $arMailFields);
					if ($arBlock["DESCRIPTION"])
					{
						$arResult["TEXT"] = $arBlock["DESCRIPTION"];
					}
					$arResult["RESULT"] = "OK";
				}
				else
				{
					$arResult["ERROR"] = $el->LAST_ERROR;
					$arResult["RESULT"] = "ERROR";
				}
			}
		}
		else
		{
			$arResult["RESULT"] = "ERROR";
			$arResult["ERROR"] = "Инфоблок не найден";
		}
	}
	else
	{
		$arResult["RESULT"] = "ERROR";
		$arResult["ERROR"] = "Нет имени инфоблока";
	}

	echo json_encode($arResult);

	/*
	if ($_POST["NAME"] && $_POST["PHONE"])
	{
		$arFields = Array (
			"IBLOCK_ID" => #IBLOCK_ID#,
			"ACTIVE" => "Y",
			"NAME" => "Заявка от ".$_POST["NAME"],
			"PROPERTY_VALUES" => #PROPERTY_VALUES#
		);
		$el = new CIBlockElement;
		$product_id = $el->Add ($arFields);
		LF ($product_id > 0)
		{
			// отправляем письмо
			$arFields = #MESSAGE_PROPS#
			CEvent::Send ("#MESSAGE_TYPE#", "s1", $arFields);
			echo "OK";
		}
		else
		{
			echo $el->LAST_ERROR;
		}
	}
	elseif (!$_POST["NAME"])
	{
		echo "Пожалуйста, заполните имя";
	}
	elseif (!$_POST["PHONE"])
	{
		echo "Пожалуйста, заполните телефон";
	}*/
}	
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

define("IBLOCK_CATALOG", 1);


CModule::IncludeModule ("iblock");
$arSections = Array();
$sections = CIBlockSection::GetList (Array ("LEFT_MARGIN" => "ASC", "SORT" => "ASC", "NAME" => "ASC"), Array ("IBLOCK_ID" => IBLOCK_CATALOG, "ACTIVE" => "Y", "GLOBAL_ACTIVE" => "Y"), false, Array ("IBLOCK_ID", "ID", "NAME", "IBLOCK_SECTION_ID", "SECTION_PAGE_URL", ));
while ($arSection = $sections->Fetch())
{
	$arSections[$arSection["ID"]] = $arSection;
			
}


$arItems = Array();
$items = CIBlockElement::GetList (Array ("SORT" => "ASC"), Array ("IBLOCK_ID" => IBLOCK_CATALOG, "ACTIVE" => "Y", "GLOBAL_ACTIVE" => "Y"), false, false, Array ("IBLOCK_ID", "ID", "NAME","SECTION_ID", "PREVIEW_TEXT", "DETAIL_PICTURE", "PROPERTY_PRICE", "DETAIL_PAGE_URL", "IBLOCK_SECTION_ID", "DETAIL_TEXT", "PROPERTY_PRICE_FROM"));
while ($arItem = $items->GetNext(true, false))
{
	/*$arItem["picture"] = CFile::GetPath ($arItem["DETAIL_PICTURE"]);
	//$arItem["text"] = htmlspecialchars($arItem["DETAIL_TEXT"]);
	$price = preg_replace('/[^0-9]/', '', $arItem["PROPERTY_MIN_PRICE_VALUE"]);
	if ($price > 0)
	{
		$arItem["price"] = $price;
	}*/
	$price = false;
	if (!empty($arItem["PROPERTY_PRICE_VALUE"]))
	{
		$price = $arItem["PROPERTY_PRICE_VALUE"];
	}
	else
	{
		if (!empty($arItem["PROPERTY_PRICE_FROM_VALUE"]))
		{
			$price = $arItem["PROPERTY_PRICE_FROM_VALUE"];
		}
		else
		{
			continue;
		}
	}
	$arItem["PRICE"] = $price;
	$arItems[$arItem["ID"]] = $arItem;
}

/*$items = CIBlockElement::GetList (Array(), Array ("IBLOCK_ID" => IBLOCK_OFFERS), false, false, Array ("IBLOCK_ID", "ID", "NAME", "PROPERTY_PRICE_1", "PROPERTY_SKU_LIST"));
while ($arItem = $items->Fetch())
{
	if (!isset($arItems[$arItem["PROPERTY_SKU_LIST_VALUE"][0]]["price"]) || $arItems[$arItem["PROPERTY_SKU_LIST_VALUE"][0]]["price"] > $arItem["PROPERTY_PRICE_1_VALUE"])
	{
		foreach ($arItem["PROPERTY_SKU_LIST_VALUE"] as $id)
		{
			//echo '<pre>'; print_r($arItems[$id]); echo "#".__LINE__."@".__FILE__; echo '</pre>';
			$arItems[$id]["price"] = $arItem["PROPERTY_PRICE_1_VALUE"];
			//$arItems[$id]["text"] = $arItem["NAME"];
		}
		//$arItems[$arItem["PROPERTY_SKU_LIST_VALUE"][0]]["price"] = $arItem["PROPERTY_PRICE_1_VALUE"];
	}
}*/
header("Content-Type: text/xml; charset=utf-8");
echo '<?xml version="1.0" encoding="utf-8"?>';
?><!DOCTYPE yml_catalog SYSTEM "shops.dtd">
<yml_catalog date="<?=date('Y-m-d H:i');?>">
	<shop>
		<name>Виола</name>
		<company>Цветочный салон Виола</company>
		<url>http://viola-sib.ru</url>
		<platform>1C-Bitrix</platform>
		<?/*<delivery-options> <option cost="300" days="3-6"/> </delivery-options>*/?>
		<currencies>
			<currency id="RUB" rate="1"/>
		</currencies>
		<categories>
			<?foreach ($arSections as $id => $arSection)
			{
				if ($arSection["IBLOCK_SECTION_ID"] > 0)
				{
					?><category id="<?=$id;?>" parentId="<?=$arSection["IBLOCK_SECTION_ID"];?>"><?=$arSection["NAME"];?></category>
					<?
				}
				else
				{
					?><category id="<?=$id;?>"><?=$arSection["NAME"];?></category>
					<?
				}
			}?>
		</categories>
		<offers>
			<?foreach ($arItems as $id => $arItem)
			{
				/*if (!($arItem["price"] > 0)) { continue; }*/
				?>
				<offer id="<?=$id;?>" available="true">
					<url>
					http://viola-sib.ru<?=$arItem["DETAIL_PAGE_URL"];?>
					</url>
					<price><?=$arItem["PRICE"];?></price>
					<currencyId>RUB</currencyId>
					<categoryId><?=$arItem["IBLOCK_SECTION_ID"];?></categoryId>
					<name>
					<?=$arItem["NAME"];?>
					</name><?if (!empty($arItem["text"])) {?>
					<description>
					<?=$arItem["text"];?>
					</description><?}?>
					<?/*<sales_notes></sales_notes>
					<picture>
					<?=$arItem["picture"];?>
					</picture>*/?>
					<store>true</store>
					<pickup>true</pickup>
					<delivery>true</delivery>
				</offer>
			<?}?>
		</offers>
	</shop>
</yml_catalog>
<?
 require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>
